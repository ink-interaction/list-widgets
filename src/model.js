import Recognizer from './pdollar'
import {TAGS} from './Predefined_Tags'
import {EMAILS} from './EMAILS'
import {v1 as uuidv1} from 'uuid'
import {map} from './utils'

console.log('TAGS', TAGS)
let canvasTwo // Two.js instance of CANVAS
let twoInvokedMenu

export let originEmails = {}
Object.entries(EMAILS).map(([folder, items]) => {
  originEmails[folder] = items.map((item) => {
    item.wordCount = item.content.split(/\s/).length
    item.id = uuidv1()
    item.groupIDs = new Set() // groups the email belongs to
    item.tagIDs = new Set() // tags attached to the email
    return item
  })
})
export let emails = {...originEmails}

let recognizer = new Recognizer()
function Point(x, y, id) {
  // constructor
  this.X = x
  this.Y = y
  this.ID = id // stroke ID to which this point belongs (1,2,3,etc.)
}

export let tags = {...TAGS} // tag-uuid -> tag
export let tagsByInkID = {} // inkID->tags
export let tagSearchRslt
export let groups = {} // id->group:{id:string,memberIDs:emailIDs,color:string,subGroups:[[a,b,c],[d,e]]}
// subGroups are used to generate fragmented group lines, by default, it includes all members

export let isDrawing = false
export let touchedElms = new Set() // all UI elements that was underneath the ink while drawing
export let currentOnInkCmd
export let currentGroupID
let currentMail = emails.inbox[0]
let viewingSubElm = undefined

//// function ////////////
//////////// function ////
export function setCurrentGroupID(id) {
  currentGroupID = id
}
export function setEmails(m) {
  emails = {...m}
  console.log('setEmails')
}
export function getEmail(id) {
  for (const l of Object.values(emails)) {
    console.log(l)
    for (const v of l) {
      if (v.id === id) return v
    }
  }
  return null //not found
}
export function getEmails(ids) {
  return Object.values(emails).reduce(
    (acc, cur) => acc.concat(cur.filter((email) => ids.includes(email.id))),

    [],
  )
}
export function addGroup(group) {
  //TODO  check if a group with same members already exists
  const groupID = `group-${group.type}:` + uuidv1()
  group.path._renderer.elem.setAttribute('data-id', groupID)
  Object.values(emails).forEach((list) => {
    list
      .filter((mail) => group.memberIDs.includes(mail.id))
      .forEach((mail) => mail.groupIDs.add(groupID))
  })
  groups[groupID] = {...group, id: groupID}
}

export let trigger = false
export function setTrigger(val) {
  trigger = val
}
export function bringTogether(groupID) {
  // console.log('bring together')
  let tmpEmails = {}
  Object.keys(emails).forEach((folder) => {
    console.log(
      'before bringTogether',
      emails[folder].map((e) => e.title),
    )
    let firstEmailID = false
    let gotFirst = false
    // console.log('folder', folder, 'emails', emails[folder])
    let emailsInGroup = emails[folder].filter((email) => {
      const found = groups[groupID].memberIDs.includes(email.id)
      if (found && !gotFirst) {
        console.log('GOT FIRST')
        gotFirst = true
        firstEmailID = email.id
      }
      return found
    })
    let emailsNotInGroup = emails[folder].filter((email) => {
      const found = !groups[groupID].memberIDs.includes(email.id)
      return found
    })

    const indexOfFirstEmailInGroup = emails[folder].indexOf(
      emails[folder].filter((e) => e.id == firstEmailID)[0],
    )
    console.log(
      'memberids',
      groups[groupID].memberIDs,
      'in',
      emailsInGroup.map((e) => e.id),
      'notin',
      emailsNotInGroup,
      'firstIndex',
      indexOfFirstEmailInGroup,
    )
    emailsNotInGroup.splice(indexOfFirstEmailInGroup, 0, ...emailsInGroup)
    console.log(
      'after bringTogether',
      emailsNotInGroup.map((e) => e.title),
    )
    tmpEmails[folder] = [...emailsNotInGroup]
  })
  setEmails({...tmpEmails})
  setTimeout(() => {
    trigger = true
  }, 100)
}
export function removeGroup(id) {
  //remove SVG
  groups[id].g.remove()
  // remove groupID in all member emails
  Object.values(emails).forEach((list) => {
    list
      .filter((mail) => groups[id].memberIDs.includes(mail.id))
      .forEach((mail) => mail.groupIDs.delete(id))
  })
  // delete group obj
  delete groups[id]
  twoInvokedMenu.update()
}
export function redrawAllGroups() {
  Object.keys(groups).map((id) => redrawGroup(id))
}
export function redrawGroup(id) {
  let processedCount = 0
  let isInSubGroup = false
  let subMemberIDs = []
  const group = groups[id]
  console.log('redraw group', group)
  group.allPath.forEach((p) => group.g.remove(p))
  group.allPath.length = 0
  // traverse all emails
  const linewidth = 2
  // const color =
  //   '#' + Math.round((0.2 + Math.random()) * 0x777777).toString(16) + '88'
  let path
  let x,
    y,
    h = 0,
    w
  group.subGroupCount = 0
  Object.values(emails).forEach((folder) =>
    folder.forEach((mail) => {
      if (group.memberIDs.includes(mail.id)) {
        // 进入一个sub group
        if (!isInSubGroup) {
          group.subGroupCount++
          path = createPath(group.color, linewidth, false)
          const firstElm = group.memberDoms[group.memberIDs.indexOf(mail.id)]
          x = firstElm.offsetLeft + linewidth
          y = firstElm.offsetTop + linewidth
          h = 0
        }
        //在sub group 中
        else {
        }
        const elm = group.memberDoms[group.memberIDs.indexOf(mail.id)]
        console.log('redraw elm', elm)
        h += elm.clientHeight
        w = elm.clientWidth
        subMemberIDs.push(mail.id)
        isInSubGroup = true
        processedCount++
      } else {
        //刚结束一个 sub group
        if (isInSubGroup) {
          console.log('end subgroup', subMemberIDs)
          group.allPath.push(path)
          group.subGroupDims.push({x: x, y: y, h: h})
          const occupied = getEmails(subMemberIDs).reduce((acc, cur) => {
            const tmp = Array.from(cur.groupIDs.values())
              .filter((id) => {
                return groups[id].type === group.type
              })
              .map((id) => groups[id].lineLevel)
            acc = [...acc, ...tmp]
            return acc
          }, [])
          const lineLevel = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].filter(
            (l) => !occupied.includes(l),
          )[0]
          makeGroupLine(
            group.type === 'r' ? x : w,
            y,
            4,
            h,
            group.type,
            path,
            lineLevel,
          )
          group.g.add(path)
        }
        subMemberIDs.length = 0
        isInSubGroup = false
      }
    }),
  )

  group.two.update()
  // must call after update, because DOM with be re-generated by TWO.js
  const c = group.g._renderer.elem.children
  for (let index = 0; index < c.length; index++) {
    const element = c[index]
    element.style.stroke = group.color
  }
  group.g._renderer.elem.onpointerenter = (e) => {
    for (let index = 0; index < c.length; index++) {
      const element = c[index]
      element.style.stroke = '#333'
      element.style.strokeWidth = '3px'
    }
  }
  group.g._renderer.elem.onpointerout = (e) => {
    for (let index = 0; index < c.length; index++) {
      const element = c[index]
      element.style.stroke = group.color
      element.style.zIndex = 10 - group.lineLevel
      element.style.strokeWidth = '2px'
    }
  }
  group.g._renderer.elem.style.pointerEvents = 'visible'
  group.g._renderer.elem.style.zIndex = 10 - group.lineLevel
  // leader-line related
  if (group.subGroupCount >= 2) {
    group.allPath.forEach((p) => {
      let leaderLines = []
      const leaderLineG = new Two.Group()
      p._renderer.elem.onpointerenter = (e) => {
        viewingSubElm = p._renderer.elem
        //iterate over all line-grouping widgets
        for (let i = 0; i < group.allPath.length; i++) {
          const pp = group.allPath[i]._renderer.elem
          if (viewingSubElm != pp) {
            const selfBoundingRect = viewingSubElm.getBoundingClientRect()
            const otherBoundingRect = pp.getBoundingClientRect()

            /**
             * LeaderLine V1
             *
             */
            // const line = new LeaderLine(viewingSubElm, pp, {
            //   path: 'arc',
            //   size: 3,
            //   // startSocket: 'right', //group.type === 'l' ? 'right' : 'left',
            //   // endSocket: 'right', //group.type === 'l' ? 'right' : 'left',
            //   startPlug: 'disc',
            //   endPlug: 'disc',
            // })
            // line.setOptions({
            //   startSocketGravity:
            //     selfBoundingRect.y < otherBoundingRect.y ? [30, 20] : [30, -20],
            //   endSocketGravity:
            //     selfBoundingRect.y < otherBoundingRect.y
            //       ? [-30, -20]
            //       : [30, 20],
            //   endSocket:
            //     selfBoundingRect.y < otherBoundingRect.y ? 'left' : 'right',
            // })
            // leaderLines.push(line)

            /**
             * LeaderLine V2
             *
             */
            const startX = selfBoundingRect.right
            const startY = selfBoundingRect.top + selfBoundingRect.height / 2
            const endX = otherBoundingRect.right
            const endY = otherBoundingRect.top + otherBoundingRect.height / 2
            const lineLen = Math.abs(endY - startY)
            const midX = (startX + endX) / 2 + map(lineLen, 0, 3000, 0, 500)
            const midY = (startY + endY) / 2
            console.log(
              'start',
              selfBoundingRect,
              'e',
              otherBoundingRect,
              'mid',
              midX,
              midY,
            )
            let lLine = new Two.Path(
              [
                new Two.Vector(startX, startY),
                // new Two.Vector((startX + midX) / 2, (startY + midY) / 2),
                new Two.Vector(midX, midY),
                // new Two.Vector((midX + endX) / 2, (midY + endY) / 2),
                new Two.Vector(endX, endY),
              ],
              false,
              true,
            )
            lLine.cap = lLine.join = 'round'
            lLine.curved = true
            lLine.noFill().stroke = '#fa5'
            lLine.linewidth = 3
            // group.g.add(lLine)
            canvasTwo.add(lLine)
            // }
            // else{

            // }
            leaderLines.push(lLine)
          }
          canvasTwo.update()
          // group.two.update()
        }
      }
      p._renderer.elem.onpointerout = (e) => {
        viewingSubElm = undefined
        leaderLines.forEach((l) => l.remove())
        leaderLines.length = 0
        // group.two.update()
        canvasTwo.update()
      }
    })
  }
}

function createPath(color, lineWidth, curved) {
  let path = new Two.Path([], false, true)
  path.curved = curved
  path.cap = path.join = 'round'
  path.noFill().stroke = color
  path.linewidth = lineWidth
  return path
}
export function setTwoInvokedMenu(two) {
  twoInvokedMenu = two
}
export function setCanvasTwo(two) {
  canvasTwo = two
}
export function setCurrentOnInkCmd(callback) {
  currentOnInkCmd = callback
  console.log('set', currentOnInkCmd)
}
export function setDrawing(flag) {
  isDrawing = flag
}
export function addFolder(name) {
  emails[name] = []
}
export function addMail(folder, mail) {
  const m = {...mail, id: emails[folder].length}
  emails[folder].push(m)
}
export function setCurrentMail(mail) {
  currentMail = mail
}
export function getTag(uuid) {
  return tags[uuid]
}
export function recognize(tag, amount = 1) {
  let points = []
  for (const [i, path] of tag.pathList.entries()) {
    points = [...points, ...path.map((p) => new Point(p.x, p.y, i + 1))]
  }

  tagSearchRslt = recognizer.Recognize(points, amount)
  return tagSearchRslt
}

export function addTag(tag) {
  let points = []
  for (const [i, path] of tag.pathList.entries()) {
    points = [...points, ...path.map((p) => new Point(p.x, p.y, i + 1))]
    console.log(points, 'sdssdgsakldgaj;sdklj')
  }
  console.log('before adding', points)
  recognizer.AddGesture(tag.uuid, points)

  if (tags.hasOwnProperty(tag.uuid)) {
    for (let id of tag.targetIDs) {
      tags[tag.uuid].targetIDs.add(id)
    }
    tag = tags[tag.uuid]
  } else {
    tags[tag.uuid] = tag
  }
  console.log('Recognizer: Added gesture', tag.uuid, points)
  tag.targetIDs.forEach((id) => {
    if (tagsByInkID[id] == undefined) {
      tagsByInkID[id] = {}
    }
    tagsByInkID[id][tag.uuid] = tag
  })
  console.log('TAGS By ID:', tagsByInkID)
}
export function findByTagUUID(uuid) {
  let tmp = {}
  console.log('search email with tag', uuid)
  for (const folder in emails) {
    if (emails.hasOwnProperty(folder)) {
      tmp[folder] = emails[folder].filter(
        (mail) =>
          tags[uuid] &&
          tags[uuid].targetIDs.has(folder + 'mail-item' + mail.id),
      )
      console.log('foler', folder, tags[uuid])
    }
  }
  // emailResults = tmp
  console.log('tag search result emails:', tmp)
  return tmp
}
export function clearSearch() {
  emailResults = emails
}
export function search(query, cap = false) {
  query = cap ? query : query.toLowerCase()
  let result = {}
  for (const key in emails) {
    if (!emails.hasOwnProperty(key)) continue
    const folder = [...emails[key]]
    result[key] = []
    for (const mail of folder) {
      if (cap) {
        mail.title = mail.title.toLowerCase()
        mail.content = mail.content.toLowerCase()
      }
      if (mail.title.includes(query) || mail.content.includes(query)) {
        result[key].push(mail)
      }
    }
  }
  return result
}

TAGS.forEach((tag) => addTag(tag))
console.log(TAGS, 'ADDED predefined tags', tags)

//---------
function makeGroupLine(x, y, w, h, type, path, level = 1) {
  const xOffset = type === 'r' ? 0 : 5
  const levelOffset = 7 * level
  // w = levelOffset;
  x += xOffset
  path.vertices.push(
    new Two.Vector(type === 'r' ? x - levelOffset + w : x + levelOffset - w, y),
  )
  path.vertices.push(
    new Two.Vector(type === 'r' ? x - levelOffset : x + levelOffset, y),
  )
  path.vertices.push(
    new Two.Vector(type === 'r' ? x - levelOffset : x + levelOffset, y + h),
  )
  path.vertices.push(
    new Two.Vector(
      type === 'r' ? x - levelOffset + w : x + levelOffset - w,
      y + h,
    ),
  )
}
