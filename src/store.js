import {writable} from 'svelte/store'
import {emails} from './model'
// export const itemStates = writable({})
// export const generalStates = writable({isShooting: true})
// export const itemIds = writable([])
// export const imgCount = writable(0)
// export const sharedStates = writable({})
export const inkTarget = writable('inboxmail-item0')
export const currentMailID = writable(emails['inbox'][0].id)
export const pathTable = writable({}) //save stroke belonging to elements
export const allInks = writable([]) // {id: uuid, path:[....],targetID: targetid}
export const isPenActive = writable(false)
export const isInking = writable(false)
export const currentTool = writable('')
export const currentFolder = writable('inbox')

/** for context menu */
export const menuPos = writable({x: 0, y: 0})
export const showMenu = writable(false)
export const menuTarget = writable('') // could be emailID or groupID

export const DEV_SETTINGS = writable({
  postDrawDelay: 600, // ms
})
