import {v1 as uuidv1} from 'uuid'
import {* as CONSTS} from './CONSTS'

export class Group {
  constructor(x, y, memberIDs,memberDoms,color,type) {
this.x=x
this.y=y
this.memberIDs=memberIDs
this.memberDoms=memberDoms
this.color=color
this.type=type
// auto created
this.id=uuidv1()
this.tagIDs=new Set()
this.twoGroup=new Two.Group()
this.subGroups=[new SubGroup(this,memberIDs,memberDoms)]// 最初只有一个subgroup which包含所有memberIDs 

  }
}

// subgroup is a group of positionaly adjacent members in a same group
export class SubGroup{
  constructor(parentGroup,memberIDs,memberDoms){
    this.parentGroup=parentGroup
    this.memberIDs=memberIDs
    this.memberDoms=memberDoms
    // auto inited
    this.lineLevel=1
    this.twoPath=createPath(parentGroup.color,CONSTS.GROUP_LINE_W,false)
    this.dim={}// x y h
  }

  function clearSVG(){
    
  }

}

function createPath(color, lineWidth, curved) {
  let path = new Two.Path([], false, true)
  path.curved = curved
  path.cap = path.join = 'round'
  path.noFill().stroke = color
  path.linewidth = lineWidth
  return path
}
