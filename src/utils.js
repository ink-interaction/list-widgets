export function map(v, inStart, inEnd, outStart, outEnd) {
  const inSpan = inEnd - inStart
  let percent = (v - inStart) / inSpan
  const outSpan = outEnd - outStart
  if (v > inEnd) percent = 1
  return percent * outSpan + outStart
}
