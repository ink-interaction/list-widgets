import {v1 as uuidv1} from 'uuid'

const commandPath = {
  'group-left': [
    [
      {X: 0, Y: 0, ID: 1},
      {X: 12, Y: 40, ID: 1},
      {X: 0, Y: 80, ID: 1},
    ],
    [
      {X: 0, Y: 0, ID: 1},
      {X: 22, Y: 40, ID: 1},
      {X: 0, Y: 80, ID: 1},
    ],
    [
      {X: 0, Y: 0, ID: 1},
      {X: 12, Y: 20, ID: 1},
      {X: 0, Y: 80, ID: 1},
    ],
    [
      {X: 0, Y: 0, ID: 1},
      {X: 16, Y: 60, ID: 1},
      {X: 0, Y: 80, ID: 1},
    ],
  ],
  'group-right': [
    [
      {X: 12, Y: 0, ID: 1},
      {X: 0, Y: 40, ID: 1},
      {X: 12, Y: 80, ID: 1},
    ],
    [
      {X: 22, Y: 0, ID: 1},
      {X: 0, Y: 40, ID: 1},
      {X: 22, Y: 80, ID: 1},
    ],
    [
      {X: 12, Y: 0, ID: 1},
      {X: 0, Y: 20, ID: 1},
      {X: 12, Y: 80, ID: 1},
    ],
    [
      {X: 16, Y: 0, ID: 1},
      {X: 0, Y: 60, ID: 1},
      {X: 16, Y: 80, ID: 1},
    ],
  ],
  crossing: [
    // -
    [
      {X: 0, Y: 0, ID: 1},
      {X: 10, Y: 0, ID: 1},
    ],
    //    /
    [
      {X: 0, Y: 0, ID: 1},
      {X: -10, Y: -10, ID: 1},
    ],
    //  \
    [
      {X: 0, Y: 0, ID: 1},
      {X: 10, Y: 10, ID: 1},
    ],
  ],
}
const tagPath = {
  // A: [
  //   [
  //     {X: 0, Y: 0, ID: 1},
  //     {X: -5, Y: 10, ID: 1},
  //     {X: 0, Y: 0, ID: 2},
  //     {X: 5, Y: 10, ID: 2},
  //     {X: -3, Y: 5, ID: 3},
  //     {X: 3, Y: 5, ID: 3},
  //   ],
  // ],
  B: [
    [
      {X: 0, Y: 0, ID: 1},
      {X: 0, Y: 40, ID: 1},
      {X: 0, Y: 0, ID: 2},
      {X: 11, Y: 8, ID: 2},
      {X: 20, Y: 10, ID: 2},
      {X: 11, Y: 12, ID: 2},
      {X: 0, Y: 20, ID: 2},
      {X: 11, Y: 28, ID: 2},
      {X: 30, Y: 10, ID: 2},
      {X: 11, Y: 32, ID: 2},
      {X: 0, Y: 40, ID: 2},
    ],
  ],
  // 1: [
  //   [
  //     {X: 12, Y: 5, ID: 1},
  //     {X: 12, Y: 40, ID: 1},
  //     {X: 5, Y: 102, ID: 1},
  //   ],
  //   [
  //     {X: 17, Y: 5, ID: 1},
  //     {X: 15, Y: 47, ID: 1},
  //     {X: 5, Y: 85, ID: 1},
  //   ],
  //   [
  //     {X: 8, Y: 5, ID: 1},
  //     {X: 5, Y: 92, ID: 1},
  //   ],
  // ],
  // 2: [
  //   [
  //     {X: 16, Y: 21, ID: 1},
  //     {X: 30, Y: 5, ID: 1},
  //     {X: 87, Y: 7, ID: 1},
  //     {X: 86, Y: 29, ID: 1},
  //     {X: 62, Y: 59, ID: 1},
  //     {X: 52, Y: 62, ID: 1},
  //     {X: 48, Y: 67, ID: 1},
  //     {X: 16, Y: 76, ID: 1},
  //     {X: 5, Y: 83, ID: 1},
  //     {X: 5, Y: 86, ID: 1},
  //     {X: 28, Y: 81, ID: 1},
  //     {X: 89, Y: 79, ID: 1},
  //   ],
  //   [
  //     {X: 18, Y: 40, ID: 1},
  //     {X: 19, Y: 28, ID: 1},
  //     {X: 31, Y: 9, ID: 1},
  //     {X: 45, Y: 5, ID: 1},
  //     {X: 66, Y: 5, ID: 1},
  //     {X: 72, Y: 8, ID: 1},
  //     {X: 81, Y: 21, ID: 1},
  //     {X: 81, Y: 49, ID: 1},
  //     {X: 77, Y: 59, ID: 1},
  //     {X: 69, Y: 65, ID: 1},
  //     {X: 60, Y: 85, ID: 1},
  //     {X: 46, Y: 97, ID: 1},
  //     {X: 34, Y: 101, ID: 1},
  //     {X: 34, Y: 104, ID: 1},
  //     {X: 22, Y: 106, ID: 1},
  //     {X: 18, Y: 111, ID: 1},
  //     {X: 10, Y: 113, ID: 1},
  //     {X: 5, Y: 120, ID: 1},
  //     {X: 45, Y: 102, ID: 1},
  //     {X: 102, Y: 97, ID: 1},
  //   ],
  //   [
  //     {X: 5, Y: 37, ID: 1},
  //     {X: 8, Y: 18, ID: 1},
  //     {X: 18, Y: 5, ID: 1},
  //     {X: 58, Y: 5, ID: 1},
  //     {X: 72, Y: 15, ID: 1},
  //     {X: 79, Y: 32, ID: 1},
  //     {X: 82, Y: 42, ID: 1},
  //     {X: 82, Y: 70, ID: 1},
  //     {X: 68, Y: 95, ID: 1},
  //     {X: 44, Y: 106, ID: 1},
  //     {X: 27, Y: 106, ID: 1},
  //     {X: 27, Y: 100, ID: 1},
  //     {X: 37, Y: 92, ID: 1},
  //     {X: 75, Y: 88, ID: 1},
  //     {X: 114, Y: 89, ID: 1},
  //   ],
  // ],
  // 3: [
  //   [
  //     {X: 5, Y: 21, ID: 1},
  //     {X: 11, Y: 19, ID: 1},
  //     {X: 18, Y: 9, ID: 1},
  //     {X: 26, Y: 7, ID: 1},
  //     {X: 52, Y: 5, ID: 1},
  //     {X: 69, Y: 13, ID: 1},
  //     {X: 73, Y: 18, ID: 1},
  //     {X: 72, Y: 35, ID: 1},
  //     {X: 53, Y: 54, ID: 1},
  //     {X: 43, Y: 61, ID: 1},
  //     {X: 36, Y: 61, ID: 1},
  //     {X: 27, Y: 70, ID: 1},
  //     {X: 37, Y: 64, ID: 1},
  //     {X: 64, Y: 64, ID: 1},
  //     {X: 85, Y: 85, ID: 1},
  //     {X: 84, Y: 104, ID: 1},
  //     {X: 65, Y: 120, ID: 1},
  //     {X: 36, Y: 127, ID: 1},
  //     {X: 5, Y: 126, ID: 1},
  //   ],
  //   [
  //     {X: 24, Y: 43, ID: 1},
  //     {X: 43, Y: 18, ID: 1},
  //     {X: 59, Y: 5, ID: 1},
  //     {X: 82, Y: 5, ID: 1},
  //     {X: 101, Y: 29, ID: 1},
  //     {X: 101, Y: 46, ID: 1},
  //     {X: 96, Y: 55, ID: 1},
  //     {X: 32, Y: 84, ID: 1},
  //     {X: 34, Y: 77, ID: 1},
  //     {X: 46, Y: 72, ID: 1},
  //     {X: 67, Y: 72, ID: 1},
  //     {X: 84, Y: 79, ID: 1},
  //     {X: 101, Y: 93, ID: 1},
  //     {X: 107, Y: 105, ID: 1},
  //     {X: 110, Y: 124, ID: 1},
  //     {X: 94, Y: 138, ID: 1},
  //     {X: 82, Y: 143, ID: 1},
  //     {X: 20, Y: 143, ID: 1},
  //     {X: 5, Y: 134, ID: 1},
  //   ],
  // ],
  // 4: [
  //   [
  //     {X: 53, Y: 20, ID: 1},
  //     {X: 12, Y: 95, ID: 1},
  //     {X: 8, Y: 112, ID: 1},
  //     {X: 5, Y: 112, ID: 1},
  //     {X: 35, Y: 100, ID: 1},
  //     {X: 133, Y: 84, ID: 1},
  //     {X: 73, Y: 5, ID: 2},
  //     {X: 73, Y: 138, ID: 2},
  //   ],
  //   [
  //     {X: 60, Y: 23, ID: 1},
  //     {X: 52, Y: 30, ID: 1},
  //     {X: 33, Y: 61, ID: 1},
  //     {X: 10, Y: 116, ID: 1},
  //     {X: 5, Y: 138, ID: 1},
  //     {X: 26, Y: 138, ID: 1},
  //     {X: 88, Y: 126, ID: 1},
  //     {X: 140, Y: 122, ID: 1},
  //     {X: 139, Y: 119, ID: 1},
  //     {X: 103, Y: 5, ID: 2},
  //     {X: 100, Y: 5, ID: 2},
  //     {X: 96, Y: 64, ID: 2},
  //     {X: 88, Y: 106, ID: 2},
  //     {X: 88, Y: 168, ID: 2},
  //   ],
  //   [
  //     {X: 45, Y: 17, ID: 1},
  //     {X: 24, Y: 78, ID: 1},
  //     {X: 5, Y: 116, ID: 1},
  //     {X: 66, Y: 112, ID: 1},
  //     {X: 112, Y: 98, ID: 1},
  //     {X: 125, Y: 98, ID: 1},
  //     {X: 98, Y: 5, ID: 2},
  //     {X: 96, Y: 59, ID: 2},
  //     {X: 88, Y: 99, ID: 2},
  //     {X: 90, Y: 162, ID: 2},
  //   ],
  // ],
  circle: [
    [
      {X: 10, Y: 0, ID: 1},
      {X: 8.7, Y: 5, ID: 1},
      {X: 5, Y: 8.7, ID: 1},
      {X: 0, Y: 10, ID: 1},
      {X: -5, Y: 8.7, ID: 1},
      {X: -8.7, Y: 5, ID: 1},
      {X: -10, Y: 0, ID: 1},
      {X: -8.7, Y: -5, ID: 1},
      {X: -5, Y: -8.7, ID: 1},
      {X: 0, Y: -10, ID: 1},
      {X: 5, Y: -8.7, ID: 1},
      {X: 8.7, Y: -5, ID: 1},
      {X: 10, Y: 0, ID: 1},
    ],
    [
      {X: 46, Y: 14, ID: 1},
      {X: 30, Y: 18, ID: 1},
      {X: 11, Y: 37, ID: 1},
      {X: 5, Y: 59, ID: 1},
      {X: 6, Y: 76, ID: 1},
      {X: 27, Y: 95, ID: 1},
      {X: 44, Y: 99, ID: 1},
      {X: 66, Y: 98, ID: 1},
      {X: 106, Y: 79, ID: 1},
      {X: 125, Y: 52, ID: 1},
      {X: 130, Y: 38, ID: 1},
      {X: 126, Y: 24, ID: 1},
      {X: 100, Y: 9, ID: 1},
      {X: 85, Y: 5, ID: 1},
      {X: 52, Y: 5, ID: 1},
      {X: 45, Y: 9, ID: 1},
    ],
  ],
  tirangleUp: [
    [
      {X: 10, Y: 0, ID: 1},
      {X: 0, Y: -17.3, ID: 1},
      {X: -10, Y: 0, ID: 1},
      {X: 10, Y: 0, ID: 1},
    ],
  ],
  tirangleDown: [
    [
      {X: 10, Y: 0, ID: 1},
      {X: 0, Y: 17.3, ID: 1},
      {X: -10, Y: 0, ID: 1},
      {X: 10, Y: 0, ID: 1},
    ],
    [
      {X: 62, Y: 5, ID: 1},
      {X: 55, Y: 12, ID: 1},
      {X: 36, Y: 48, ID: 1},
      {X: 5, Y: 119, ID: 1},
      {X: 50, Y: 118, ID: 1},
      {X: 69, Y: 113, ID: 1},
      {X: 88, Y: 113, ID: 1},
      {X: 88, Y: 110, ID: 1},
      {X: 116, Y: 110, ID: 1},
      {X: 60, Y: 6, ID: 2},
      {X: 73, Y: 43, ID: 2},
      {X: 76, Y: 43, ID: 2},
      {X: 81, Y: 52, ID: 2},
      {X: 85, Y: 67, ID: 2},
      {X: 88, Y: 67, ID: 2},
      {X: 92, Y: 93, ID: 2},
      {X: 102, Y: 115, ID: 2},
    ],
  ],
  square: [
    [
      {X: 10, Y: 10, ID: 1},
      {X: -10, Y: 10, ID: 1},
      {X: -10, Y: -10, ID: 1},
      {X: 10, Y: -10, ID: 1},
      {X: 10, Y: 10, ID: 1},
    ],
    [
      {X: 9, Y: 15, ID: 1},
      {X: 11, Y: 102, ID: 1},
      {X: 9, Y: 9, ID: 2},
      {X: 59, Y: 8, ID: 2},
      {X: 64, Y: 5, ID: 2},
      {X: 76, Y: 7, ID: 2},
      {X: 78, Y: 98, ID: 2},
      {X: 5, Y: 88, ID: 3},
      {X: 81, Y: 90, ID: 3},
    ],
  ],
}

export let tag = {
  uuid: uuidv1(),
  pathList: [],
  targetIDs: new Set(),
  type: '', // command|tag
  info: {}, // for command only, info differs according to command type
  position: {x: 0, y: 0},
  valid: true,
}
function normalizedPathList(pathList) {
  let top = 10000,
    left = 10000,
    right = 0,
    bottom = 0
  // get bounding box
  for (const path of pathList) {
    for (const pos of path) {
      top = top > pos.y ? pos.y : top
      bottom = bottom < pos.y ? pos.y : bottom
      left = left > pos.x ? pos.x : left
      right = right < pos.x ? pos.x : right
    }
  }
  // normalize coords
  for (const path of pathList) {
    for (const pos of path) {
      pos.x -= left - 5 // add some padding
      pos.y -= top - 5
    }
  }
  tag.position.x = left
  tag.position.y = top
  return [{top: top - 10, bottom, left: left - 10, right}, pathList]
}

function buildTag(type, pathData) {
  Object.entries(pathData).forEach(([name, sampleList]) => {
    sampleList.forEach((l) => {
      let lastID = 1
      let newL = [[]]
      for (let p of l) {
        if (p.ID > lastID) {
          newL[lastID] = []
          lastID = p.ID
        }
        newL[lastID - 1].push({x: p.X, y: p.Y})
      }
      const [dims, xxx] = normalizedPathList(newL)

      _tags.push({
        ...tag,
        uuid: 'PREDEFINED: ' + name,
        pathList: newL,
        dims: dims,
        type: type,
        name: name, // for command tag only
      })
    })
  })
}
console.log('+++++++', Object.entries(tagPath))
let _tags = []
buildTag('tag', tagPath)
buildTag('command', commandPath)
console.log('_tags ', _tags)
export let TAGS = _tags

// Object.entries(tagPath).map(([name, l]) => {
//   return {
//     uuid: uuidv1(),
//     pathList: l,
//     targetIDs: new Set(),
//     type: '', // command|tag
//     info: {}, // for command only, info differs according to command type
//     position: {x: 0, y: 0},
//     valid: true,
//   }
// })
console.log('GENERATED Predefined TGS:', TAGS)
