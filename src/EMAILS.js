//TODO move all email items into a single list and use a logical folder tree to "Contain" them.
export let EMAILS = {
  inbox: [
    {
      title: 'Billing for March 2020',
      content: `Bonjour,
      
      Here is your billing info for the month of March:
      
      amount: 5000      1888
      date:2019-09-09   2020-04-05
      account: 1242424     1111

      Regards,
      XXXX
      `,
      time: new Date('2020-05-03'),
      from: 'sales@car.com',
      to: 'ming@me.com',
      transacs: [
        {account: '1242424', type: 'other', amount: 5000},
        {account: '1111', type: 'other', amount: 1888},
      ],
    },
    {
      title: 'New payment info',
      content: `amount: 5000      1888
      date:2019-09-09   2020-04-05
      account: 1242424     1111
      `,
      time: new Date('2020-05-02'),
      from: 'sales@car.com',
      to: 'ming@me.com',
      transacs: [
        {account: '1242424', type: 'other', amount: 5000},
        {account: '1111', type: 'other', amount: 1888},
      ],
    },
    {
      title: 'Job Application from Juliet',
      content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque nemo earum, accusamus cupiditate nisi eum quis similique reiciendis ipsa vitae autem quasi necessitatibus ratione mollitia dicta. Totam quam perspiciatis culpa.
      `,
      time: new Date('2020-06-22'),
      from: 'juliet@gmail.com',
      to: 'hr@company.com',
    },
    {
      title: 'Job Application from Brian',
      content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque nemo earum, accusamus cupiditate nisi eum quis similique reiciendis ipsa vitae autem quasi necessitatibus ratione mollitia dicta. Totam quam perspiciatis culpa.
      `,
      time: new Date('2020-06-07'),
      from: 'biran@yahoo.com',
      to: 'hr@company.com',
    },
    {
      title: 'Round 2: Job Application from Brian',
      content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque nemo earum, accusamus cupiditate nisi eum quis similique reiciendis ipsa vitae autem quasi necessitatibus ratione mollitia dicta. Totam quam perspiciatis culpa.
      `,
      time: new Date('2020-07-08'),
      from: 'hr2@company.com',
      to: 'hr@company.com',
    },
    {
      title: 'FW: Job Application from Juliet',
      content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque nemo earum, accusamus cupiditate nisi eum quis similique reiciendis ipsa vitae autem quasi necessitatibus ratione mollitia dicta. Totam quam perspiciatis culpa.
      `,
      time: new Date('2020-06-27'),
      from: 'brian@yahoo.com',
      to: 'hr@company.com',
    },
    {
      title: 'Follow Up: Job Application from Juliet',
      content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque nemo earum, accusamus cupiditate nisi eum quis similique reiciendis ipsa vitae autem quasi necessitatibus ratione mollitia dicta. Totam quam perspiciatis culpa.
      `,
      time: new Date('2020-07-07'),
      from: 'juliet@gmail.com',
      to: 'hr@company.com',
    },
    {
      title: 'Just a ramdom email',
      content: `Hello,

      blabalbla blabla bla bla

      bye
      `,
      time: new Date('2020-07-03'),
      from: 'sneakyguy@outlook.com',
      to: 'me@me.com',
    },
    {
      title: 'Invoice #June,2020',
      content: '',
      time: new Date('2020-05-01'),
      from: 'sales@car.com',
      to: 'ming@me.com',
      transacs: [
        {account: '2222', type: 'food', amount: 200},
        {account: '1111', type: 'transport', amount: 18},
      ],
    },
    {
      title: 'Welcome to the "All-exsitu" mailing list',
      content: `Welcome to the All-exsitu@lri.fr mailing list!

      To post to this list, send your message to:
      
        all-exsitu@lri.fr
      
      General information about the mailing list is at:
      
        http://serveur-listes.lri.fr/cgi-bin/mailman/listinfo/all-exsitu
      
      If you ever want to unsubscribe or change your options (eg, switch to
      or from digest mode, change your password, etc.), visit your
      subscription page at:
      
        http://serveur-listes.lri.fr/cgi-bin/mailman/options/all-exsitu/yyaomingm%40outlook.com
      
      
      You can also make such adjustments via email by sending a message to:
      
        All-exsitu-request@lri.fr
      
      with the word help
      
    `,
      time: new Date('2020-06-19'),
      from: 'exsitu@test.com',
      to: 'ming@me.com',
    },
    {
      title: 'To the Graduating Class of 2020',
      content: `Dear Ming,
 
      On this 10th anniversary of EIT Digital, we would like to congratulate you as you are about to step into the vibrant and international community of Alumni of the EIT Digital Master School! We are very proud of those of you who have already completed their studies as well as salute the students on the finishing line towards achieving their academic goals in these exceptional circumstances!
       
      Graduation is a moment to celebrate and honour the time, energy and effort you have committed to earn your degree. Unfortunately, in light of the significant uncertainty surrounding the current pandemic, EIT Digital Master School had to make a difficult decision to postpone the graduation ceremony which was scheduled to take place in Madrid, Spain, in November 2020.
       
      We understand this is disappointing news for you and we truly regret the inconvenience. We believe this is the best course of action to support the safety and wellbeing of you and your guests, as well as our staff and the wider community. As you know, our student community, along with friends and family, come from all over the world and this large event poses a potential negative health impact on those attending. Further, it is difficult to predict how the situation unfolds later this year and whether different governments will have travel restrictions, as well as restrictions on large gatherings.
       
      While we will not be able to welcome you in Madrid later this year, we have the intention to welcome you in the same location at the beginning of next year, 2021. Details of these arrangements will be communicated to you as soon as possible, depending on how the COVID-19 situation develops.
       
      Please contact your exit university for questions about the local graduation ceremony.
       
      We appreciate your patience, as we navigate all the changes together with you during these unprecedented times. If you have any questions, please reach out to us at masterschool@eitdigital.eu.
       
      Stay safe!
      
    `,
      time: new Date('2020-06-18'),
      from: 'eit@test.com',
      to: 'ming@me.com',
    },
    {
      title: '[All-exsitu] [Seminar] Kate and Zack',
      content: `Dear all,

    Tomorrow, Kate and Zack will both present their internship work:
    - Kate will present her research on creative writing and how technology can support the process. The presentation will summarize the literature and thematic analysis findings. She plans to show a video prototype and would be interested in hearing any feedback, that you can send her through this link. 
    - Zack will present his research on bimanual tools for creativity – from stories of the creative process to several technology probes he designed. 
    
    
    The seminar will be at 10h on this link: https://meet.jit.si/ExSitu
    
    
    
    Have a nice day,
    
    
    
    Téo
    
    `,
      time: new Date('2020-06-08'),
      from: 'user1@test.com',
      to: 'me@me.com',
    },
    {
      title: "Let's get you started with Listen Notes",
      content: `
      This is Wenbin, the founder of Listen Notes. I'm so excited to welcome you to the Listen Notes community (User #93645)! You may want to edit your profile page (please log in first) for a better presence in the podcast universe :) 
      You can search ALL podcast episodes by people or topics via listennotes.com or Chrome Extension, e.g., Coronavirus. 
      
      Tired of listening to a few top-chart podcasts from big publishers? Discover interesting niche podcasts in Listen Community! 
      

      We also provide some powerful tools to help you discover podcasts and explore podcast meta data, which can be found at the top right "..." menu: 
       

      If you have questions about using Listen Notes, you can visit listennotes.com/help or just reply this email - I reply every email myself. 
      
      PS: If you own a podcast, you can claim and manage it on Listen Notes or post classified ads to find guests, sponsors, co-hosts, cross-promotion... 
      
      PS: Check out our short-form audio project: inca.fm - it's like micro-podcasting :) 
      
      PS: You can embed a podcast player or a playlist to your own website, blog, or online courses. 
      
      - Wenbin 
      Founder and CEO of Listen Notes, Inc. 
      
        `,
      time: new Date('2020-04-08'),
      from: 'test@test.com',
      to: 'me@me.com',
    },
    {
      title: 'CodePen Verification',
      content: `Thanks for signing up for CodePen! 

      We're happy you're here. Let's get your email address verified: 
      Click to Verify Email 
      Verifying your email address enables these features:
      •	Full Page View
      •	Collections
      •	Commenting on Pens, Projects, and Posts
      •	Your Pens are searchable on CodePen
      Getting Started on CodePen 
      Ready to get coding? Here are a few links to help you! 
      •	Quick overview of what you can do with CodePen
      Take a guided tour through the Pen editor
      
        `,
      time: new Date('2019-05-05'),
      from: 'no-reply@codepen.com',
      to: 'me@me.com',
    },
    {
      title: 'UX in MR - UXinMR / #17',
      content: `Hi guys!
 
        In this issue, you won't read about Travis Scott event in Fortnite or Magic Leap, but you will find interaction demos, MR design guidelines, tutorials, projects and more.
        
        Vova Kurbatov
        `,
      time: new Date('2020-03-05'),
      from: 'test@test.com',
      to: 'me@me.com',
    },
    {
      title: 'Lime est de retour !',
      content: `Alors que les villes commencent à assouplir leurs restrictions de circulation, Lime est ravie de rétablir son service. Nous sommes de retour pour que vous puissiez utiliser Lime quand vous le souhaitez.
      Comme nos trottinettes sont prévues pour des balades en plein air, c'est l'un des moyens les plus sûrs de se déplacer. Pour des recommandations en matière de sécurité, consultez notre guide. 
      
        `,
      time: new Date('2020-05-15'),
      from: 'test@test.com',
      to: 'me@me.com',
    },
    {
      title: 'AD Microsoft 365 ',
      content: `
 
      2020年初的新冠病毒黑天鹅事件，让各大企业开启了远程办公模式，线上协同办公成为一段时间内很多人最主要的工作和协作方式。尽管随着新冠病毒疫情情况陆续好转，各地逐渐有序复产复工，线上协同办公依然以高效、便捷等优势成为很多办公室中职场人士的“利器”。然而如何在协同办公过程中充分提升企业的生产力和效率，通过更紧密的协作消除传统工作方式中存在的诸多局限，仍是很多企业面临的挑战！ 
      很多企业依然在使用老版本 Office 2010。Office 2010 在促进协作方面的功能在10年间虽有完善但滞后于新时代的企业需求，对于企业提供协作效率不利。再加上这个版本即将在2020年10月13日停止技术支持，微软建议客户尽快升级到 Microsoft 365（原名 Office 365）。 


      Microsoft 365（原名 Office 365）基于云的生产力平台，充分融合了云平台在集成方面的优势，提供了行业领先的生产力应用（Word、Excel、PowerPoint 等）、智能云服务以及一流的安全性。Microsoft 365 不仅提供了始终维持最新版本的软件，同时还具备丰富的联机协作办公功能，可以帮助企业用户用更简单、快捷的操作体验现代化协同办公的强大之处。 
      
        `,
      time: new Date('2020-04-28'),
      from: 'microsoft@test.com',
      to: 'me@me.com',
    },
    {
      title: "We're updating our Terms",
      content: `Hi, 

      We’re making some small changes to our Terms of Service, Payments Terms of Service, and Privacy Policy (collectively, “Terms”). You can review the new Terms by clicking here. We’ve also put up information to explain these changes in more detail on our Terms of Service Update Page. Both the old and new versions of the Terms can be found at the Terms of Service, Payments Terms of Service, and Privacy Policy pages through January 24, 2020. 
      Thank you for being a member of our global community. 
      Thanks, 
      The Airbnb Team 
      
        `,
      time: new Date('2020-03-05'),
      from: 'test@test.com',
      to: 'me@me.com',
    },
    {
      title: 'Welcome to Roam',
      content: `Welcome to Roam!

      Thanks for signing up for early access.
      
      If you haven't already, create your account here.
      
      Your account will start with just a blank page in your daily notes, if you want some inspiration for how to organize your Roam, check out the help database
      
      And if you need more help figuring out how to use Roam to organize your thoughts, you can book a call with our CEO and he can help you move notes over into Roam and figure out what workflows will be most helpful.  You can book a time on his calendar here
      
      Feel free to respond to this email with any questions
      
      Sincerely,
      
      The Roam Team
      
        `,
      time: new Date('2020-04-18'),
      from: 'roam@test.com',
      to: 'me@me.com',
    },
    {
      title: 'Welcome',
      content: `Thank you for creating an account with Bitwarden. You may now log in with your new account. 
      Did you know that Bitwarden is free to sync with all of your devices? Download Bitwarden today on: 
      Desktop 
      Access Bitwarden on Windows, macOS, and Linux desktops with our native desktop application. 
       
      Web Browser 
      Integrate Bitwarden directly into your favorite browser. Use our browser extensions for a seamless browsing experience. 
       
      Mobile 
      Take Bitwarden on the go with our mobile apps for your phone or tablet device. 
       
      Web 
      Stuck without any of your devices? Using a friend's computer? You can access your Bitwarden vault from any web enabled device by using the web vault. 
      vault.bitwarden.com 
      
      If you have any questions or problems you can email us from our website at https://bitwarden.com/contact. 
      
      Thank you!
      The Bitwarden Team 
       `,
      time: new Date('2020-04-08'),
      from: 'test@test.com',
      to: 'me@me.com',
    },
    {
      title: 'GraphQL live Docs | 50% OFF annual | Product Hunt',
      content: `Hello, 

      We have three major announcements for you.
      
      â€¢	50% OFF all annual plans coupon expires on 30.04. Use ILOV3GRAPHQL in checkout.
      â€¢	GraphQL Live Docs have been added to our app. Check below. It's only visible for paid users.
      We're live on Product Hunt with OS GraphQL Centaur CLI - This tool allows to bootstrap backend applications from GraphQL schema, you can auto generate basic resolvers like CRUD.
      
       `,
      time: new Date('2020-04-18'),
      from: 'test@test.com',
      to: 'me@me.com',
    },
    {
      title: 'New insurance factsheet',
      content: `Dear students,
 

      I hope all is well with you and your nearest and dearest!
      
      We have received an update from the insurance provider, Kammarkollegiet, that scanned insurance claims and receipts should be sent to the MSO by email even when we are back at the office (we are still working from home due to the pandemic). It means that you no longer need to send in claim forms and receipts in hard copy by regular mail to Stockholm.
      
      However, please save the original receipts and relevant documentation for at least 6 months after the date of the incident/visit to the doctor. Kammarkollegiet might ask you to send the originals by regular post as part of a random control check.
      
      We also created a file sharing space in the online portal (called Plaza) for you to be able to access commonly used EIT Digital documents at any time when needed. We have saved the updated factsheet here.
      
      Please let me know if you have any questions.
      
      
       
      
      Kind regards,
      
       `,
      time: new Date('2020-05-11'),
      from: 'test@test.com',
      to: 'me@me.com',
    },
  ].map((m, i) => {
    return {...m, id: i, read: false}
  }),
  flagged: new Array(7)
    .fill({
      title: 'mail title',
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed tempor est pellentesque vel. Mauris tempus convallis.',
      time: new Date(),
      from: 'test@test.com',
      to: 'me@me.com',
    })
    .map((m, i) => {
      return {...m, title: i + m.title, id: i, read: false}
    }),
  drafts: new Array(5)
    .fill({
      title: 'mail title',
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus convallis.',
      time: new Date(),
      from: 'test@test.com',
      to: 'me@me.com',
    })
    .map((m, i) => {
      return {...m, title: i + m.title, id: i, read: true}
    }),
  sent: new Array(3)
    .fill({
      title: 'mail title',
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felisipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felisipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felisipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felisipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felisipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felis, sed tempor est pellentesque vel. Mauris tempus convallis.',
      time: new Date(),
      from: 'test@test.com',
      to: 'me@me.com',
    })
    .map((m, i) => {
      return {...m, title: i + m.title, id: i, read: true}
    }),
  spam: new Array(2)
    .fill({
      title: 'mail title',
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felis, sed tempor est pellentesque vel. Mauris tempus convallis.',
      time: new Date(),
      from: 'test@test.com',
      to: 'me@me.com',
    })
    .map((m, i) => {
      return {...m, title: i + m.title, id: i, read: false}
    }),
  archive: [],
}
