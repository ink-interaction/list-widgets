
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(window.document);
var app = (function () {
    'use strict';

    function noop() { }
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function is_empty(obj) {
        return Object.keys(obj).length === 0;
    }
    function validate_store(store, name) {
        if (store != null && typeof store.subscribe !== 'function') {
            throw new Error(`'${name}' is not a store with a 'subscribe' method`);
        }
    }
    function subscribe(store, ...callbacks) {
        if (store == null) {
            return noop;
        }
        const unsub = store.subscribe(...callbacks);
        return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
    }
    function component_subscribe(component, store, callback) {
        component.$$.on_destroy.push(subscribe(store, callback));
    }
    function create_slot(definition, ctx, $$scope, fn) {
        if (definition) {
            const slot_ctx = get_slot_context(definition, ctx, $$scope, fn);
            return definition[0](slot_ctx);
        }
    }
    function get_slot_context(definition, ctx, $$scope, fn) {
        return definition[1] && fn
            ? assign($$scope.ctx.slice(), definition[1](fn(ctx)))
            : $$scope.ctx;
    }
    function get_slot_changes(definition, $$scope, dirty, fn) {
        if (definition[2] && fn) {
            const lets = definition[2](fn(dirty));
            if ($$scope.dirty === undefined) {
                return lets;
            }
            if (typeof lets === 'object') {
                const merged = [];
                const len = Math.max($$scope.dirty.length, lets.length);
                for (let i = 0; i < len; i += 1) {
                    merged[i] = $$scope.dirty[i] | lets[i];
                }
                return merged;
            }
            return $$scope.dirty | lets;
        }
        return $$scope.dirty;
    }
    function update_slot(slot, slot_definition, ctx, $$scope, dirty, get_slot_changes_fn, get_slot_context_fn) {
        const slot_changes = get_slot_changes(slot_definition, $$scope, dirty, get_slot_changes_fn);
        if (slot_changes) {
            const slot_context = get_slot_context(slot_definition, ctx, $$scope, get_slot_context_fn);
            slot.p(slot_context, slot_changes);
        }
    }
    function null_to_empty(value) {
        return value == null ? '' : value;
    }
    function set_store_value(store, ret, value = ret) {
        store.set(value);
        return ret;
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_input_value(input, value) {
        input.value = value == null ? '' : value;
    }
    function toggle_class(element, name, toggle) {
        element.classList[toggle ? 'add' : 'remove'](name);
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error(`Function called outside component initialization`);
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function createEventDispatcher() {
        const component = get_current_component();
        return (type, detail) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail);
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
            }
        };
    }
    // TODO figure out if we still want to support
    // shorthand events, or if we want to implement
    // a real bubbling mechanism
    function bubble(component, event) {
        const callbacks = component.$$.callbacks[event.type];
        if (callbacks) {
            callbacks.slice().forEach(fn => fn(event));
        }
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function tick() {
        schedule_update();
        return resolved_promise;
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    let flushing = false;
    const seen_callbacks = new Set();
    function flush() {
        if (flushing)
            return;
        flushing = true;
        do {
            // first, call beforeUpdate functions
            // and update components
            for (let i = 0; i < dirty_components.length; i += 1) {
                const component = dirty_components[i];
                set_current_component(component);
                update(component.$$);
            }
            dirty_components.length = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        flushing = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }

    const globals = (typeof window !== 'undefined'
        ? window
        : typeof globalThis !== 'undefined'
            ? globalThis
            : global);

    function destroy_block(block, lookup) {
        block.d(1);
        lookup.delete(block.key);
    }
    function outro_and_destroy_block(block, lookup) {
        transition_out(block, 1, 1, () => {
            lookup.delete(block.key);
        });
    }
    function update_keyed_each(old_blocks, dirty, get_key, dynamic, ctx, list, lookup, node, destroy, create_each_block, next, get_context) {
        let o = old_blocks.length;
        let n = list.length;
        let i = o;
        const old_indexes = {};
        while (i--)
            old_indexes[old_blocks[i].key] = i;
        const new_blocks = [];
        const new_lookup = new Map();
        const deltas = new Map();
        i = n;
        while (i--) {
            const child_ctx = get_context(ctx, list, i);
            const key = get_key(child_ctx);
            let block = lookup.get(key);
            if (!block) {
                block = create_each_block(key, child_ctx);
                block.c();
            }
            else if (dynamic) {
                block.p(child_ctx, dirty);
            }
            new_lookup.set(key, new_blocks[i] = block);
            if (key in old_indexes)
                deltas.set(key, Math.abs(i - old_indexes[key]));
        }
        const will_move = new Set();
        const did_move = new Set();
        function insert(block) {
            transition_in(block, 1);
            block.m(node, next);
            lookup.set(block.key, block);
            next = block.first;
            n--;
        }
        while (o && n) {
            const new_block = new_blocks[n - 1];
            const old_block = old_blocks[o - 1];
            const new_key = new_block.key;
            const old_key = old_block.key;
            if (new_block === old_block) {
                // do nothing
                next = new_block.first;
                o--;
                n--;
            }
            else if (!new_lookup.has(old_key)) {
                // remove old block
                destroy(old_block, lookup);
                o--;
            }
            else if (!lookup.has(new_key) || will_move.has(new_key)) {
                insert(new_block);
            }
            else if (did_move.has(old_key)) {
                o--;
            }
            else if (deltas.get(new_key) > deltas.get(old_key)) {
                did_move.add(new_key);
                insert(new_block);
            }
            else {
                will_move.add(old_key);
                o--;
            }
        }
        while (o--) {
            const old_block = old_blocks[o];
            if (!new_lookup.has(old_block.key))
                destroy(old_block, lookup);
        }
        while (n)
            insert(new_blocks[n - 1]);
        return new_blocks;
    }
    function validate_each_keys(ctx, list, get_context, get_key) {
        const keys = new Set();
        for (let i = 0; i < list.length; i++) {
            const key = get_key(get_context(ctx, list, i));
            if (keys.has(key)) {
                throw new Error(`Cannot have duplicate keys in a keyed each`);
            }
            keys.add(key);
        }
    }
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const prop_values = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty,
            skip_bound: false
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, prop_values, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if (!$$.skip_bound && $$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set($$props) {
            if (this.$$set && !is_empty($$props)) {
                this.$$.skip_bound = true;
                this.$$set($$props);
                this.$$.skip_bound = false;
            }
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.24.1' }, detail)));
    }
    function append_dev(target, node) {
        dispatch_dev("SvelteDOMInsert", { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev("SvelteDOMInsert", { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev("SvelteDOMRemove", { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ["capture"] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev("SvelteDOMAddEventListener", { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev("SvelteDOMRemoveEventListener", { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev("SvelteDOMRemoveAttribute", { node, attribute });
        else
            dispatch_dev("SvelteDOMSetAttribute", { node, attribute, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.wholeText === data)
            return;
        dispatch_dev("SvelteDOMSetData", { node: text, data });
        text.data = data;
    }
    function validate_each_argument(arg) {
        if (typeof arg !== 'string' && !(arg && typeof arg === 'object' && 'length' in arg)) {
            let msg = '{#each} only iterates over array-like objects.';
            if (typeof Symbol === 'function' && arg && Symbol.iterator in arg) {
                msg += ' You can use a spread to convert this iterable into an array.';
            }
            throw new Error(msg);
        }
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error(`'target' is a required option`);
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn(`Component was already destroyed`); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    const subscriber_queue = [];
    /**
     * Creates a `Readable` store that allows reading by subscription.
     * @param value initial value
     * @param {StartStopNotifier}start start and stop notifications for subscriptions
     */
    function readable(value, start) {
        return {
            subscribe: writable(value, start).subscribe,
        };
    }
    /**
     * Create a `Writable` store that allows both updating and reading by subscription.
     * @param {*=}value initial value
     * @param {StartStopNotifier=}start start and stop notifications for subscriptions
     */
    function writable(value, start = noop) {
        let stop;
        const subscribers = [];
        function set(new_value) {
            if (safe_not_equal(value, new_value)) {
                value = new_value;
                if (stop) { // store is ready
                    const run_queue = !subscriber_queue.length;
                    for (let i = 0; i < subscribers.length; i += 1) {
                        const s = subscribers[i];
                        s[1]();
                        subscriber_queue.push(s, value);
                    }
                    if (run_queue) {
                        for (let i = 0; i < subscriber_queue.length; i += 2) {
                            subscriber_queue[i][0](subscriber_queue[i + 1]);
                        }
                        subscriber_queue.length = 0;
                    }
                }
            }
        }
        function update(fn) {
            set(fn(value));
        }
        function subscribe(run, invalidate = noop) {
            const subscriber = [run, invalidate];
            subscribers.push(subscriber);
            if (subscribers.length === 1) {
                stop = start(set) || noop;
            }
            run(value);
            return () => {
                const index = subscribers.indexOf(subscriber);
                if (index !== -1) {
                    subscribers.splice(index, 1);
                }
                if (subscribers.length === 0) {
                    stop();
                    stop = null;
                }
            };
        }
        return { set, update, subscribe };
    }
    function derived(stores, fn, initial_value) {
        const single = !Array.isArray(stores);
        const stores_array = single
            ? [stores]
            : stores;
        const auto = fn.length < 2;
        return readable(initial_value, (set) => {
            let inited = false;
            const values = [];
            let pending = 0;
            let cleanup = noop;
            const sync = () => {
                if (pending) {
                    return;
                }
                cleanup();
                const result = fn(single ? values[0] : values, set);
                if (auto) {
                    set(result);
                }
                else {
                    cleanup = is_function(result) ? result : noop;
                }
            };
            const unsubscribers = stores_array.map((store, i) => subscribe(store, (value) => {
                values[i] = value;
                pending &= ~(1 << i);
                if (inited) {
                    sync();
                }
            }, () => {
                pending |= (1 << i);
            }));
            inited = true;
            sync();
            return function stop() {
                run_all(unsubscribers);
                cleanup();
            };
        });
    }

    function regexparam (str, loose) {
    	if (str instanceof RegExp) return { keys:false, pattern:str };
    	var c, o, tmp, ext, keys=[], pattern='', arr = str.split('/');
    	arr[0] || arr.shift();

    	while (tmp = arr.shift()) {
    		c = tmp[0];
    		if (c === '*') {
    			keys.push('wild');
    			pattern += '/(.*)';
    		} else if (c === ':') {
    			o = tmp.indexOf('?', 1);
    			ext = tmp.indexOf('.', 1);
    			keys.push( tmp.substring(1, !!~o ? o : !!~ext ? ext : tmp.length) );
    			pattern += !!~o && !~ext ? '(?:/([^/]+?))?' : '/([^/]+?)';
    			if (!!~ext) pattern += (!!~o ? '?' : '') + '\\' + tmp.substring(ext);
    		} else {
    			pattern += '/' + tmp;
    		}
    	}

    	return {
    		keys: keys,
    		pattern: new RegExp('^' + pattern + (loose ? '(?=$|\/)' : '\/?$'), 'i')
    	};
    }

    /* node_modules/svelte-spa-router/Router.svelte generated by Svelte v3.24.1 */

    const { Error: Error_1, Object: Object_1, console: console_1 } = globals;

    // (219:0) {:else}
    function create_else_block(ctx) {
    	let switch_instance;
    	let switch_instance_anchor;
    	let current;
    	var switch_value = /*component*/ ctx[0];

    	function switch_props(ctx) {
    		return { $$inline: true };
    	}

    	if (switch_value) {
    		switch_instance = new switch_value(switch_props());
    		switch_instance.$on("routeEvent", /*routeEvent_handler_1*/ ctx[5]);
    	}

    	const block = {
    		c: function create() {
    			if (switch_instance) create_component(switch_instance.$$.fragment);
    			switch_instance_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			if (switch_instance) {
    				mount_component(switch_instance, target, anchor);
    			}

    			insert_dev(target, switch_instance_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (switch_value !== (switch_value = /*component*/ ctx[0])) {
    				if (switch_instance) {
    					group_outros();
    					const old_component = switch_instance;

    					transition_out(old_component.$$.fragment, 1, 0, () => {
    						destroy_component(old_component, 1);
    					});

    					check_outros();
    				}

    				if (switch_value) {
    					switch_instance = new switch_value(switch_props());
    					switch_instance.$on("routeEvent", /*routeEvent_handler_1*/ ctx[5]);
    					create_component(switch_instance.$$.fragment);
    					transition_in(switch_instance.$$.fragment, 1);
    					mount_component(switch_instance, switch_instance_anchor.parentNode, switch_instance_anchor);
    				} else {
    					switch_instance = null;
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			if (switch_instance) transition_in(switch_instance.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			if (switch_instance) transition_out(switch_instance.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(switch_instance_anchor);
    			if (switch_instance) destroy_component(switch_instance, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(219:0) {:else}",
    		ctx
    	});

    	return block;
    }

    // (217:0) {#if componentParams}
    function create_if_block(ctx) {
    	let switch_instance;
    	let switch_instance_anchor;
    	let current;
    	var switch_value = /*component*/ ctx[0];

    	function switch_props(ctx) {
    		return {
    			props: { params: /*componentParams*/ ctx[1] },
    			$$inline: true
    		};
    	}

    	if (switch_value) {
    		switch_instance = new switch_value(switch_props(ctx));
    		switch_instance.$on("routeEvent", /*routeEvent_handler*/ ctx[4]);
    	}

    	const block = {
    		c: function create() {
    			if (switch_instance) create_component(switch_instance.$$.fragment);
    			switch_instance_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			if (switch_instance) {
    				mount_component(switch_instance, target, anchor);
    			}

    			insert_dev(target, switch_instance_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const switch_instance_changes = {};
    			if (dirty & /*componentParams*/ 2) switch_instance_changes.params = /*componentParams*/ ctx[1];

    			if (switch_value !== (switch_value = /*component*/ ctx[0])) {
    				if (switch_instance) {
    					group_outros();
    					const old_component = switch_instance;

    					transition_out(old_component.$$.fragment, 1, 0, () => {
    						destroy_component(old_component, 1);
    					});

    					check_outros();
    				}

    				if (switch_value) {
    					switch_instance = new switch_value(switch_props(ctx));
    					switch_instance.$on("routeEvent", /*routeEvent_handler*/ ctx[4]);
    					create_component(switch_instance.$$.fragment);
    					transition_in(switch_instance.$$.fragment, 1);
    					mount_component(switch_instance, switch_instance_anchor.parentNode, switch_instance_anchor);
    				} else {
    					switch_instance = null;
    				}
    			} else if (switch_value) {
    				switch_instance.$set(switch_instance_changes);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			if (switch_instance) transition_in(switch_instance.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			if (switch_instance) transition_out(switch_instance.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(switch_instance_anchor);
    			if (switch_instance) destroy_component(switch_instance, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(217:0) {#if componentParams}",
    		ctx
    	});

    	return block;
    }

    function create_fragment(ctx) {
    	let current_block_type_index;
    	let if_block;
    	let if_block_anchor;
    	let current;
    	const if_block_creators = [create_if_block, create_else_block];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*componentParams*/ ctx[1]) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type(ctx);
    	if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	const block = {
    		c: function create() {
    			if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error_1("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if_blocks[current_block_type_index].m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if_blocks[current_block_type_index].p(ctx, dirty);
    			} else {
    				group_outros();

    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});

    				check_outros();
    				if_block = if_blocks[current_block_type_index];

    				if (!if_block) {
    					if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block.c();
    				}

    				transition_in(if_block, 1);
    				if_block.m(if_block_anchor.parentNode, if_block_anchor);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if_blocks[current_block_type_index].d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function wrap(route, userData, ...conditions) {
    	// Check if we don't have userData
    	if (userData && typeof userData == "function") {
    		conditions = conditions && conditions.length ? conditions : [];
    		conditions.unshift(userData);
    		userData = undefined;
    	}

    	// Parameter route and each item of conditions must be functions
    	if (!route || typeof route != "function") {
    		throw Error("Invalid parameter route");
    	}

    	if (conditions && conditions.length) {
    		for (let i = 0; i < conditions.length; i++) {
    			if (!conditions[i] || typeof conditions[i] != "function") {
    				throw Error("Invalid parameter conditions[" + i + "]");
    			}
    		}
    	}

    	// Returns an object that contains all the functions to execute too
    	const obj = { route, userData };

    	if (conditions && conditions.length) {
    		obj.conditions = conditions;
    	}

    	// The _sveltesparouter flag is to confirm the object was created by this router
    	Object.defineProperty(obj, "_sveltesparouter", { value: true });

    	return obj;
    }

    /**
     * @typedef {Object} Location
     * @property {string} location - Location (page/view), for example `/book`
     * @property {string} [querystring] - Querystring from the hash, as a string not parsed
     */
    /**
     * Returns the current location from the hash.
     *
     * @returns {Location} Location object
     * @private
     */
    function getLocation() {
    	const hashPosition = window.location.href.indexOf("#/");

    	let location = hashPosition > -1
    	? window.location.href.substr(hashPosition + 1)
    	: "/";

    	// Check if there's a querystring
    	const qsPosition = location.indexOf("?");

    	let querystring = "";

    	if (qsPosition > -1) {
    		querystring = location.substr(qsPosition + 1);
    		location = location.substr(0, qsPosition);
    	}

    	return { location, querystring };
    }

    const loc = readable(null, // eslint-disable-next-line prefer-arrow-callback
    function start(set) {
    	set(getLocation());

    	const update = () => {
    		set(getLocation());
    	};

    	window.addEventListener("hashchange", update, false);

    	return function stop() {
    		window.removeEventListener("hashchange", update, false);
    	};
    });

    const location = derived(loc, $loc => $loc.location);
    const querystring = derived(loc, $loc => $loc.querystring);

    function push(location) {
    	if (!location || location.length < 1 || location.charAt(0) != "/" && location.indexOf("#/") !== 0) {
    		throw Error("Invalid parameter location");
    	}

    	// Execute this code when the current call stack is complete
    	return tick().then(() => {
    		window.location.hash = (location.charAt(0) == "#" ? "" : "#") + location;
    	});
    }

    function pop() {
    	// Execute this code when the current call stack is complete
    	return tick().then(() => {
    		window.history.back();
    	});
    }

    function replace(location) {
    	if (!location || location.length < 1 || location.charAt(0) != "/" && location.indexOf("#/") !== 0) {
    		throw Error("Invalid parameter location");
    	}

    	// Execute this code when the current call stack is complete
    	return tick().then(() => {
    		const dest = (location.charAt(0) == "#" ? "" : "#") + location;

    		try {
    			window.history.replaceState(undefined, undefined, dest);
    		} catch(e) {
    			// eslint-disable-next-line no-console
    			console.warn("Caught exception while replacing the current page. If you're running this in the Svelte REPL, please note that the `replace` method might not work in this environment.");
    		}

    		// The method above doesn't trigger the hashchange event, so let's do that manually
    		window.dispatchEvent(new Event("hashchange"));
    	});
    }

    function link(node, hrefVar) {
    	// Only apply to <a> tags
    	if (!node || !node.tagName || node.tagName.toLowerCase() != "a") {
    		throw Error("Action \"link\" can only be used with <a> tags");
    	}

    	updateLink(node, hrefVar || node.getAttribute("href"));

    	return {
    		update(updated) {
    			updateLink(node, updated);
    		}
    	};
    }

    // Internal function used by the link function
    function updateLink(node, href) {
    	// Destination must start with '/'
    	if (!href || href.length < 1 || href.charAt(0) != "/") {
    		throw Error("Invalid value for \"href\" attribute");
    	}

    	// Add # to the href attribute
    	node.setAttribute("href", "#" + href);
    }

    function nextTickPromise(cb) {
    	// eslint-disable-next-line no-console
    	console.warn("nextTickPromise from 'svelte-spa-router' is deprecated and will be removed in version 3; use the 'tick' method from the Svelte runtime instead");

    	return tick().then(cb);
    }

    function instance($$self, $$props, $$invalidate) {
    	let $loc,
    		$$unsubscribe_loc = noop;

    	validate_store(loc, "loc");
    	component_subscribe($$self, loc, $$value => $$invalidate(6, $loc = $$value));
    	$$self.$$.on_destroy.push(() => $$unsubscribe_loc());
    	let { routes = {} } = $$props;
    	let { prefix = "" } = $$props;

    	/**
     * Container for a route: path, component
     */
    	class RouteItem {
    		/**
     * Initializes the object and creates a regular expression from the path, using regexparam.
     *
     * @param {string} path - Path to the route (must start with '/' or '*')
     * @param {SvelteComponent} component - Svelte component for the route
     */
    		constructor(path, component) {
    			if (!component || typeof component != "function" && (typeof component != "object" || component._sveltesparouter !== true)) {
    				throw Error("Invalid component object");
    			}

    			// Path must be a regular or expression, or a string starting with '/' or '*'
    			if (!path || typeof path == "string" && (path.length < 1 || path.charAt(0) != "/" && path.charAt(0) != "*") || typeof path == "object" && !(path instanceof RegExp)) {
    				throw Error("Invalid value for \"path\" argument");
    			}

    			const { pattern, keys } = regexparam(path);
    			this.path = path;

    			// Check if the component is wrapped and we have conditions
    			if (typeof component == "object" && component._sveltesparouter === true) {
    				this.component = component.route;
    				this.conditions = component.conditions || [];
    				this.userData = component.userData;
    			} else {
    				this.component = component;
    				this.conditions = [];
    				this.userData = undefined;
    			}

    			this._pattern = pattern;
    			this._keys = keys;
    		}

    		/**
     * Checks if `path` matches the current route.
     * If there's a match, will return the list of parameters from the URL (if any).
     * In case of no match, the method will return `null`.
     *
     * @param {string} path - Path to test
     * @returns {null|Object.<string, string>} List of paramters from the URL if there's a match, or `null` otherwise.
     */
    		match(path) {
    			// If there's a prefix, remove it before we run the matching
    			if (prefix && path.startsWith(prefix)) {
    				path = path.substr(prefix.length) || "/";
    			}

    			// Check if the pattern matches
    			const matches = this._pattern.exec(path);

    			if (matches === null) {
    				return null;
    			}

    			// If the input was a regular expression, this._keys would be false, so return matches as is
    			if (this._keys === false) {
    				return matches;
    			}

    			const out = {};
    			let i = 0;

    			while (i < this._keys.length) {
    				out[this._keys[i]] = matches[++i] || null;
    			}

    			return out;
    		}

    		/**
     * Dictionary with route details passed to the pre-conditions functions, as well as the `routeLoaded` and `conditionsFailed` events
     * @typedef {Object} RouteDetail
     * @property {SvelteComponent} component - Svelte component
     * @property {string} name - Name of the Svelte component
     * @property {string} location - Location path
     * @property {string} querystring - Querystring from the hash
     * @property {Object} [userData] - Custom data passed by the user
     */
    		/**
     * Executes all conditions (if any) to control whether the route can be shown. Conditions are executed in the order they are defined, and if a condition fails, the following ones aren't executed.
     * 
     * @param {RouteDetail} detail - Route detail
     * @returns {bool} Returns true if all the conditions succeeded
     */
    		checkConditions(detail) {
    			for (let i = 0; i < this.conditions.length; i++) {
    				if (!this.conditions[i](detail)) {
    					return false;
    				}
    			}

    			return true;
    		}
    	}

    	// Set up all routes
    	const routesList = [];

    	if (routes instanceof Map) {
    		// If it's a map, iterate on it right away
    		routes.forEach((route, path) => {
    			routesList.push(new RouteItem(path, route));
    		});
    	} else {
    		// We have an object, so iterate on its own properties
    		Object.keys(routes).forEach(path => {
    			routesList.push(new RouteItem(path, routes[path]));
    		});
    	}

    	// Props for the component to render
    	let component = null;

    	let componentParams = null;

    	// Event dispatcher from Svelte
    	const dispatch = createEventDispatcher();

    	// Just like dispatch, but executes on the next iteration of the event loop
    	const dispatchNextTick = (name, detail) => {
    		// Execute this code when the current call stack is complete
    		tick().then(() => {
    			dispatch(name, detail);
    		});
    	};

    	const writable_props = ["routes", "prefix"];

    	Object_1.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1.warn(`<Router> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Router", $$slots, []);

    	function routeEvent_handler(event) {
    		bubble($$self, event);
    	}

    	function routeEvent_handler_1(event) {
    		bubble($$self, event);
    	}

    	$$self.$$set = $$props => {
    		if ("routes" in $$props) $$invalidate(2, routes = $$props.routes);
    		if ("prefix" in $$props) $$invalidate(3, prefix = $$props.prefix);
    	};

    	$$self.$capture_state = () => ({
    		readable,
    		derived,
    		tick,
    		wrap,
    		getLocation,
    		loc,
    		location,
    		querystring,
    		push,
    		pop,
    		replace,
    		link,
    		updateLink,
    		nextTickPromise,
    		createEventDispatcher,
    		regexparam,
    		routes,
    		prefix,
    		RouteItem,
    		routesList,
    		component,
    		componentParams,
    		dispatch,
    		dispatchNextTick,
    		$loc
    	});

    	$$self.$inject_state = $$props => {
    		if ("routes" in $$props) $$invalidate(2, routes = $$props.routes);
    		if ("prefix" in $$props) $$invalidate(3, prefix = $$props.prefix);
    		if ("component" in $$props) $$invalidate(0, component = $$props.component);
    		if ("componentParams" in $$props) $$invalidate(1, componentParams = $$props.componentParams);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*component, $loc*/ 65) {
    			// Handle hash change events
    			// Listen to changes in the $loc store and update the page
    			 {
    				// Find a route matching the location
    				$$invalidate(0, component = null);

    				let i = 0;

    				while (!component && i < routesList.length) {
    					const match = routesList[i].match($loc.location);

    					if (match) {
    						const detail = {
    							component: routesList[i].component,
    							name: routesList[i].component.name,
    							location: $loc.location,
    							querystring: $loc.querystring,
    							userData: routesList[i].userData
    						};

    						// Check if the route can be loaded - if all conditions succeed
    						if (!routesList[i].checkConditions(detail)) {
    							// Trigger an event to notify the user
    							dispatchNextTick("conditionsFailed", detail);

    							break;
    						}

    						$$invalidate(0, component = routesList[i].component);

    						// Set componentParams onloy if we have a match, to avoid a warning similar to `<Component> was created with unknown prop 'params'`
    						// Of course, this assumes that developers always add a "params" prop when they are expecting parameters
    						if (match && typeof match == "object" && Object.keys(match).length) {
    							$$invalidate(1, componentParams = match);
    						} else {
    							$$invalidate(1, componentParams = null);
    						}

    						dispatchNextTick("routeLoaded", detail);
    					}

    					i++;
    				}
    			}
    		}
    	};

    	return [
    		component,
    		componentParams,
    		routes,
    		prefix,
    		routeEvent_handler,
    		routeEvent_handler_1
    	];
    }

    class Router extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, { routes: 2, prefix: 3 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Router",
    			options,
    			id: create_fragment.name
    		});
    	}

    	get routes() {
    		throw new Error_1("<Router>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set routes(value) {
    		throw new Error_1("<Router>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get prefix() {
    		throw new Error_1("<Router>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set prefix(value) {
    		throw new Error_1("<Router>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    // Unique ID creation requires a high quality random # generator. In the browser we therefore
    // require the crypto API and do not support built-in fallback to lower quality random number
    // generators (like Math.random()).
    // getRandomValues needs to be invoked in a context where "this" is a Crypto implementation. Also,
    // find the complete implementation of crypto (msCrypto) on IE11.
    var getRandomValues = typeof crypto != 'undefined' && crypto.getRandomValues && crypto.getRandomValues.bind(crypto) || typeof msCrypto != 'undefined' && typeof msCrypto.getRandomValues == 'function' && msCrypto.getRandomValues.bind(msCrypto);
    var rnds8 = new Uint8Array(16); // eslint-disable-line no-undef

    function rng() {
      if (!getRandomValues) {
        throw new Error('crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported');
      }

      return getRandomValues(rnds8);
    }

    /**
     * Convert array of 16 byte values to UUID string format of the form:
     * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
     */
    var byteToHex = [];

    for (var i = 0; i < 256; ++i) {
      byteToHex[i] = (i + 0x100).toString(16).substr(1);
    }

    function bytesToUuid(buf, offset) {
      var i = offset || 0;
      var bth = byteToHex; // join used to fix memory issue caused by concatenation: https://bugs.chromium.org/p/v8/issues/detail?id=3175#c4

      return [bth[buf[i++]], bth[buf[i++]], bth[buf[i++]], bth[buf[i++]], '-', bth[buf[i++]], bth[buf[i++]], '-', bth[buf[i++]], bth[buf[i++]], '-', bth[buf[i++]], bth[buf[i++]], '-', bth[buf[i++]], bth[buf[i++]], bth[buf[i++]], bth[buf[i++]], bth[buf[i++]], bth[buf[i++]]].join('');
    }

    //
    // Inspired by https://github.com/LiosK/UUID.js
    // and http://docs.python.org/library/uuid.html

    var _nodeId;

    var _clockseq; // Previous uuid creation time


    var _lastMSecs = 0;
    var _lastNSecs = 0; // See https://github.com/uuidjs/uuid for API details

    function v1(options, buf, offset) {
      var i = buf && offset || 0;
      var b = buf || [];
      options = options || {};
      var node = options.node || _nodeId;
      var clockseq = options.clockseq !== undefined ? options.clockseq : _clockseq; // node and clockseq need to be initialized to random values if they're not
      // specified.  We do this lazily to minimize issues related to insufficient
      // system entropy.  See #189

      if (node == null || clockseq == null) {
        var seedBytes = options.random || (options.rng || rng)();

        if (node == null) {
          // Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)
          node = _nodeId = [seedBytes[0] | 0x01, seedBytes[1], seedBytes[2], seedBytes[3], seedBytes[4], seedBytes[5]];
        }

        if (clockseq == null) {
          // Per 4.2.2, randomize (14 bit) clockseq
          clockseq = _clockseq = (seedBytes[6] << 8 | seedBytes[7]) & 0x3fff;
        }
      } // UUID timestamps are 100 nano-second units since the Gregorian epoch,
      // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so
      // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'
      // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.


      var msecs = options.msecs !== undefined ? options.msecs : new Date().getTime(); // Per 4.2.1.2, use count of uuid's generated during the current clock
      // cycle to simulate higher resolution clock

      var nsecs = options.nsecs !== undefined ? options.nsecs : _lastNSecs + 1; // Time since last uuid creation (in msecs)

      var dt = msecs - _lastMSecs + (nsecs - _lastNSecs) / 10000; // Per 4.2.1.2, Bump clockseq on clock regression

      if (dt < 0 && options.clockseq === undefined) {
        clockseq = clockseq + 1 & 0x3fff;
      } // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new
      // time interval


      if ((dt < 0 || msecs > _lastMSecs) && options.nsecs === undefined) {
        nsecs = 0;
      } // Per 4.2.1.2 Throw error if too many uuids are requested


      if (nsecs >= 10000) {
        throw new Error("uuid.v1(): Can't create more than 10M uuids/sec");
      }

      _lastMSecs = msecs;
      _lastNSecs = nsecs;
      _clockseq = clockseq; // Per 4.1.4 - Convert from unix epoch to Gregorian epoch

      msecs += 12219292800000; // `time_low`

      var tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;
      b[i++] = tl >>> 24 & 0xff;
      b[i++] = tl >>> 16 & 0xff;
      b[i++] = tl >>> 8 & 0xff;
      b[i++] = tl & 0xff; // `time_mid`

      var tmh = msecs / 0x100000000 * 10000 & 0xfffffff;
      b[i++] = tmh >>> 8 & 0xff;
      b[i++] = tmh & 0xff; // `time_high_and_version`

      b[i++] = tmh >>> 24 & 0xf | 0x10; // include version

      b[i++] = tmh >>> 16 & 0xff; // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)

      b[i++] = clockseq >>> 8 | 0x80; // `clock_seq_low`

      b[i++] = clockseq & 0xff; // `node`

      for (var n = 0; n < 6; ++n) {
        b[i + n] = node[n];
      }

      return buf ? buf : bytesToUuid(b);
    }

    var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

    function createCommonjsModule(fn, module) {
    	return module = { exports: {} }, fn(module, module.exports), module.exports;
    }

    var simplify = createCommonjsModule(function (module) {
    /*
     (c) 2017, Vladimir Agafonkin
     Simplify.js, a high-performance JS polyline simplification library
     mourner.github.io/simplify-js
    */

    (function () {
    // to suit your point format, run search/replace for '.x' and '.y';
    // for 3D version, see 3d branch (configurability would draw significant performance overhead)

    // square distance between 2 points
    function getSqDist(p1, p2) {

        var dx = p1.x - p2.x,
            dy = p1.y - p2.y;

        return dx * dx + dy * dy;
    }

    // square distance from a point to a segment
    function getSqSegDist(p, p1, p2) {

        var x = p1.x,
            y = p1.y,
            dx = p2.x - x,
            dy = p2.y - y;

        if (dx !== 0 || dy !== 0) {

            var t = ((p.x - x) * dx + (p.y - y) * dy) / (dx * dx + dy * dy);

            if (t > 1) {
                x = p2.x;
                y = p2.y;

            } else if (t > 0) {
                x += dx * t;
                y += dy * t;
            }
        }

        dx = p.x - x;
        dy = p.y - y;

        return dx * dx + dy * dy;
    }
    // rest of the code doesn't care about point format

    // basic distance-based simplification
    function simplifyRadialDist(points, sqTolerance) {

        var prevPoint = points[0],
            newPoints = [prevPoint],
            point;

        for (var i = 1, len = points.length; i < len; i++) {
            point = points[i];

            if (getSqDist(point, prevPoint) > sqTolerance) {
                newPoints.push(point);
                prevPoint = point;
            }
        }

        if (prevPoint !== point) newPoints.push(point);

        return newPoints;
    }

    function simplifyDPStep(points, first, last, sqTolerance, simplified) {
        var maxSqDist = sqTolerance,
            index;

        for (var i = first + 1; i < last; i++) {
            var sqDist = getSqSegDist(points[i], points[first], points[last]);

            if (sqDist > maxSqDist) {
                index = i;
                maxSqDist = sqDist;
            }
        }

        if (maxSqDist > sqTolerance) {
            if (index - first > 1) simplifyDPStep(points, first, index, sqTolerance, simplified);
            simplified.push(points[index]);
            if (last - index > 1) simplifyDPStep(points, index, last, sqTolerance, simplified);
        }
    }

    // simplification using Ramer-Douglas-Peucker algorithm
    function simplifyDouglasPeucker(points, sqTolerance) {
        var last = points.length - 1;

        var simplified = [points[0]];
        simplifyDPStep(points, 0, last, sqTolerance, simplified);
        simplified.push(points[last]);

        return simplified;
    }

    // both algorithms combined for awesome performance
    function simplify(points, tolerance, highestQuality) {

        if (points.length <= 2) return points;

        var sqTolerance = tolerance !== undefined ? tolerance * tolerance : 1;

        points = highestQuality ? points : simplifyRadialDist(points, sqTolerance);
        points = simplifyDouglasPeucker(points, sqTolerance);

        return points;
    }

    // export as AMD module / Node module / browser or worker variable
    {
        module.exports = simplify;
        module.exports.default = simplify;
    }

    })();
    });

    var two_min = createCommonjsModule(function (module) {
    (commonjsGlobal||self||window).Two=function(t){var e;e="undefined"!=typeof window?window:"undefined"!=typeof commonjsGlobal?commonjsGlobal:"undefined"!=typeof self?self:this;var i=Object.prototype.toString,r={_indexAmount:0,natural:{slice:Array.prototype.slice,indexOf:Array.prototype.indexOf,keys:Object.keys,bind:Function.prototype.bind,create:Object.create},identity:function(t){return t},isArguments:function(t){return "[object Arguments]"===i.call(t)},isFunction:function(t){return "[object Function]"===i.call(t)},isString:function(t){return "[object String]"===i.call(t)},isNumber:function(t){return "[object Number]"===i.call(t)},isDate:function(t){return "[object Date]"===i.call(t)},isRegExp:function(t){return "[object RegExp]"===i.call(t)},isError:function(t){return "[object Error]"===i.call(t)},isFinite:function(t){return isFinite(t)&&!isNaN(parseFloat(t))},isNaN:function(t){return r.isNumber(t)&&t!==+t},isBoolean:function(t){return !0===t||!1===t||"[object Boolean]"===i.call(t)},isNull:function(t){return null===t},isUndefined:function(t){return void 0===t},isEmpty:function(t){return null==t||(m&&(r.isArray(t)||r.isString(t)||r.isArguments(t))?0===t.length:0===r.keys(t).length)},isElement:function(t){return !(!t||1!==t.nodeType)},isArray:Array.isArray||function(t){return "[object Array]"===i.call(t)},isObject:function(t){var e=typeof t;return "function"===e||"object"===e&&!!t},toArray:function(t){return t?r.isArray(t)?_.call(t):m(t)?r.map(t,r.identity):r.values(t):[]},range:function(t,e,i){null==e&&(e=t||0,t=0),i=i||1;for(var r=Math.max(Math.ceil((e-t)/i),0),s=Array(r),n=0;n<r;n++,t+=i)s[n]=t;return s},indexOf:function(t,e){if(r.natural.indexOf)return r.natural.indexOf.call(t,e);for(var i=0;i<t.length;i++)if(t[i]===e)return i;return -1},has:function(t,e){return null!=t&&hasOwnProperty.call(t,e)},bind:function(t,e){var i=r.natural.bind;if(i&&t.bind===i)return i.apply(t,_.call(arguments,1));var s=_.call(arguments,2);return function(){t.apply(e,s);}},extend:function(t){for(var e=_.call(arguments,1),i=0;i<e.length;i++){var r=e[i];for(var s in r)t[s]=r[s];}return t},defaults:function(t){for(var e=_.call(arguments,1),i=0;i<e.length;i++){var r=e[i];for(var s in r)void 0===t[s]&&(t[s]=r[s]);}return t},keys:function(t){if(!r.isObject(t))return [];if(r.natural.keys)return r.natural.keys(t);var e=[];for(var i in t)r.has(t,i)&&e.push(i);return e},values:function(t){for(var e=r.keys(t),i=[],s=0;s<e.length;s++){var n=e[s];i.push(t[n]);}return i},each:function(t,e,i){for(var s=i||this,n=!m(t)&&r.keys(t),a=(n||t).length,o=0;o<a;o++){var l=n?n[o]:o;e.call(s,t[l],l,t);}return t},map:function(t,e,i){for(var s=i||this,n=!m(t)&&r.keys(t),a=(n||t).length,o=[],l=0;l<a;l++){var h=n?n[l]:l;o[l]=e.call(s,t[h],h,t);}return o},once:function(t){var e=!1;return function(){return e?t:(e=!0,t.apply(this,arguments))}},after:function(t,e){return function(){for(;--t<1;)return e.apply(this,arguments)}},uniqueId:function(t){var e=++r._indexAmount+"";return t?t+e:e}},s=Math.sin,n=Math.cos,a=(Math.sqrt),o=(Math.abs),l=Math.PI,h=l/2,c=Math.pow,d=Math.min,f=Math.max,u=0,_=r.natural.slice,g=e.performance&&e.performance.now?e.performance:Date,p=Math.pow(2,53)-1,m=function(t){var e,i=null==(e=t)?void 0:e.length;return "number"==typeof i&&i>=0&&i<=p},v={temp:e.document?e.document.createElement("div"):{},hasEventListeners:r.isFunction(e.addEventListener),bind:function(t,e,i,r){return this.hasEventListeners?t.addEventListener(e,i,!!r):t.attachEvent("on"+e,i),v},unbind:function(t,e,i,r){return v.hasEventListeners?t.removeEventListeners(e,i,!!r):t.detachEvent("on"+e,i),v},getRequestAnimationFrame:function(){var t,i=0,s=["ms","moz","webkit","o"],n=e.requestAnimationFrame;if(!n){for(var a=0;a<s.length;a++)n=e[s[a]+"RequestAnimationFrame"]||n,t=e[s[a]+"CancelAnimationFrame"]||e[s[a]+"CancelRequestAnimationFrame"]||t;n=n||function(t,r){var s=(new Date).getTime(),n=Math.max(0,16-(s-i)),a=e.setTimeout((function(){t(s+n);}),n);return i=s+n,a};}return n.init=r.once(M),n}},y=e.Two=function(t){var i=r.defaults(t||{},{fullscreen:!1,width:640,height:480,type:y.Types.svg,autostart:!1});if(r.each(i,(function(t,e){/fullscreen/i.test(e)||/autostart/i.test(e)||(this[e]=t);}),this),r.isElement(i.domElement)){var s=i.domElement.tagName.toLowerCase();/^(CanvasRenderer-canvas|WebGLRenderer-canvas|SVGRenderer-svg)$/.test(this.type+"-"+s)||(this.type=y.Types[s]);}if(this.renderer=new y[this.type](this),y.Utils.setPlaying.call(this,i.autostart),this.frameCount=0,i.fullscreen){var n=r.bind(C,this);r.extend(document.body.style,{overflow:"hidden",margin:0,padding:0,top:0,left:0,right:0,bottom:0,position:"fixed"}),r.extend(this.renderer.domElement.style,{display:"block",top:0,left:0,right:0,bottom:0,position:"fixed"}),v.bind(e,"resize",n),n();}else r.isElement(i.domElement)||(this.renderer.setSize(i.width,i.height,this.ratio),this.width=i.width,this.height=i.height);this.renderer.bind(y.Events.resize,r.bind(R,this)),this.scene=this.renderer.scene,y.Instances.push(this),i.autostart&&F.init();};r.extend(y,{root:e,nextFrameID:null,Array:e.Float32Array||Array,Types:{webgl:"WebGLRenderer",svg:"SVGRenderer",canvas:"CanvasRenderer"},Version:"v0.7.0",PublishDate:"2020-01-22T21:17:28.421Z",Identifier:"two-",Events:{play:"play",pause:"pause",update:"update",render:"render",resize:"resize",change:"change",remove:"remove",insert:"insert",order:"order",load:"load"},Commands:{move:"M",line:"L",curve:"C",arc:"A",close:"Z"},Resolution:12,Instances:[],noConflict:function(){return e.Two=t,y},uniqueId:function(){var t=u;return u++,t},Utils:r.extend(r,{performance:g,defineProperty:function(t){var e="_"+t,i="_flag"+t.charAt(0).toUpperCase()+t.slice(1);Object.defineProperty(this,t,{enumerable:!0,get:function(){return this[e]},set:function(t){this[e]=t,this[i]=!0;}});},Image:null,isHeadless:!1,shim:function(t,e){return y.CanvasRenderer.Utils.shim(t),r.isUndefined(e)||(y.Utils.Image=e),y.Utils.isHeadless=!0,t},release:function(t){if(r.isObject(t))return r.isFunction(t.unbind)&&t.unbind(),t.vertices&&(r.isFunction(t.vertices.unbind)&&t.vertices.unbind(),r.each(t.vertices,(function(t){r.isFunction(t.unbind)&&t.unbind();}))),t.children&&r.each(t.children,(function(t){y.Utils.release(t);})),t},xhr:function(t,e){var i=new XMLHttpRequest;return i.open("GET",t),i.onreadystatechange=function(){4===i.readyState&&200===i.status&&e(i.responseText);},i.send(),i},Curve:{CollinearityEpsilon:c(10,-30),RecursionLimit:16,CuspLimit:0,Tolerance:{distance:.25,angle:0,epsilon:Number.EPSILON},abscissas:[[.5773502691896257],[0,.7745966692414834],[.33998104358485626,.8611363115940526],[0,.5384693101056831,.906179845938664],[.2386191860831969,.6612093864662645,.932469514203152],[0,.4058451513773972,.7415311855993945,.9491079123427585],[.1834346424956498,.525532409916329,.7966664774136267,.9602898564975363],[0,.3242534234038089,.6133714327005904,.8360311073266358,.9681602395076261],[.14887433898163122,.4333953941292472,.6794095682990244,.8650633666889845,.9739065285171717],[0,.26954315595234496,.5190961292068118,.7301520055740494,.8870625997680953,.978228658146057],[.1252334085114689,.3678314989981802,.5873179542866175,.7699026741943047,.9041172563704749,.9815606342467192],[0,.2304583159551348,.44849275103644687,.6423493394403402,.8015780907333099,.9175983992229779,.9841830547185881],[.10805494870734367,.31911236892788974,.5152486363581541,.6872929048116855,.827201315069765,.9284348836635735,.9862838086968123],[0,.20119409399743451,.3941513470775634,.5709721726085388,.7244177313601701,.8482065834104272,.937273392400706,.9879925180204854],[.09501250983763744,.2816035507792589,.45801677765722737,.6178762444026438,.755404408355003,.8656312023878318,.9445750230732326,.9894009349916499]],weights:[[1],[.8888888888888888,.5555555555555556],[.6521451548625461,.34785484513745385],[.5688888888888889,.47862867049936647,.23692688505618908],[.46791393457269104,.3607615730481386,.17132449237917036],[.4179591836734694,.3818300505051189,.27970539148927664,.1294849661688697],[.362683783378362,.31370664587788727,.22238103445337448,.10122853629037626],[.3302393550012598,.31234707704000286,.26061069640293544,.1806481606948574,.08127438836157441],[.29552422471475287,.26926671930999635,.21908636251598204,.1494513491505806,.06667134430868814],[.2729250867779006,.26280454451024665,.23319376459199048,.18629021092773426,.1255803694649046,.05566856711617366],[.24914704581340277,.2334925365383548,.20316742672306592,.16007832854334622,.10693932599531843,.04717533638651183],[.2325515532308739,.22628318026289723,.2078160475368885,.17814598076194574,.13887351021978725,.09212149983772845,.04048400476531588],[.2152638534631578,.2051984637212956,.18553839747793782,.15720316715819355,.12151857068790319,.08015808715976021,.03511946033175186],[.2025782419255613,.19843148532711158,.1861610000155622,.16626920581699392,.13957067792615432,.10715922046717194,.07036604748810812,.03075324199611727],[.1894506104550685,.18260341504492358,.16915651939500254,.14959598881657674,.12462897125553388,.09515851168249279,.062253523938647894,.027152459411754096]]},devicePixelRatio:e.devicePixelRatio||1,getBackingStoreRatio:function(t){return t.webkitBackingStorePixelRatio||t.mozBackingStorePixelRatio||t.msBackingStorePixelRatio||t.oBackingStorePixelRatio||t.backingStorePixelRatio||1},getRatio:function(t){return y.Utils.devicePixelRatio/k(t)},setPlaying:function(t){return this.playing=!!t,this},getComputedMatrix:function(t,e){e=e&&e.identity()||new y.Matrix;for(var i=t,r=[];i&&i._matrix;)r.push(i._matrix),i=i.parent;r.reverse();for(var s=0;s<r.length;s++){var n=r[s].elements;e.multiply(n[0],n[1],n[2],n[3],n[4],n[5],n[6],n[7],n[8],n[9]);}return e},decomposeMatrix:function(t){return {translateX:t.e,translateY:t.f,scaleX:t.a,scaleY:t.d,rotation:Math.asin(-t.b)}},extractCSSText:function(t,e){var i,s,n,a;e||(e={}),i=t.split(";");for(var o=0;o<i.length;o++)n=(s=i[o].split(":"))[0],a=s[1],r.isUndefined(n)||r.isUndefined(a)||(e[n]=a.replace(/\s/,""));return e},getSvgStyles:function(t){for(var e={},i=y.Utils.getSvgAttributes(t),r=Math.max(i.length,t.style.length),s=0;s<r;s++){var n=t.style[s],a=i[s];n&&(e[n]=t.style[n]),a&&(e[a]=t.getAttribute(a));}return e},getSvgAttributes:function(t){for(var e=t.getAttributeNames(),i=["id","class","transform","xmlns","viewBox"],s=0;s<i.length;s++){var n=i[s],a=r.indexOf(e,n);a>=0&&e.splice(a,1);}return e},applySvgViewBox:function(t,e){var i=e.split(/\s/),r=parseFloat(i[0]),s=parseFloat(i[1]),n=parseFloat(i[2]),a=parseFloat(i[3]),o=Math.min(this.width/n,this.height/a);return t.translation.x-=r*o,t.translation.y-=s*o,t.scale=o,t},applySvgAttributes:function(t,i,s){var n,a,o,l,h={},c={},d={};if(e.getComputedStyle){var f=e.getComputedStyle(t);for(n=f.length;n--;)o=f[a=f[n]],r.isUndefined(o)||(h[a]=o);}for(n=0;n<t.attributes.length;n++)l=t.attributes[n],/style/i.test(l.nodeName)?y.Utils.extractCSSText(l.value,d):c[l.nodeName]=l.value;for(a in r.isUndefined(h.opacity)||(h["stroke-opacity"]=h.opacity,h["fill-opacity"]=h.opacity,delete h.opacity),s&&r.defaults(h,s),r.extend(h,c,d),h.visible=!(r.isUndefined(h.display)&&/none/i.test(h.display))||r.isUndefined(h.visibility)&&/hidden/i.test(h.visibility),h)switch(o=h[a],a){case"transform":if(/none/i.test(o))break;var u=t.transform&&t.transform.baseVal&&t.transform.baseVal.length>0?t.transform.baseVal[0].matrix:t.getCTM?t.getCTM():null;if(r.isNull(u))break;var _=y.Utils.decomposeMatrix(u);i.translation.set(_.translateX,_.translateY),i.rotation=Math.PI*(_.rotation/180),i.scale=new y.Vector(_.scaleX,_.scaleY);var g=parseFloat((h.x+"").replace("px")),p=parseFloat((h.y+"").replace("px"));g&&(i.translation.x=g),p&&(i.translation.y=p);break;case"viewBox":y.Utils.applySvgViewBox.call(this,i,o);break;case"visible":if(i instanceof y.Group){i._visible=o;break}i.visible=o;break;case"stroke-linecap":if(i instanceof y.Group){i._cap=o;break}i.cap=o;break;case"stroke-linejoin":if(i instanceof y.Group){i._join=o;break}i.join=o;break;case"stroke-miterlimit":if(i instanceof y.Group){i._miter=o;break}i.miter=o;break;case"stroke-width":if(i instanceof y.Group){i._linewidth=parseFloat(o);break}i.linewidth=parseFloat(o);break;case"opacity":case"stroke-opacity":case"fill-opacity":if(i instanceof y.Group){i._opacity=parseFloat(o);break}i.opacity=parseFloat(o);break;case"fill":case"stroke":if(i instanceof y.Group&&(a="_"+a),/url\(#.*\)/i.test(o)){var m=y.Utils.getScene(this);i[a]=m.getById(o.replace(/url\(#(.*)\)/i,"$1"));}else i[a]=/none/i.test(o)?"transparent":o;break;case"id":i.id=o,t.id=o+"-"+y.Identifier+"applied";break;case"class":case"className":i.classList=o.split(" ");}return h},getScene:function(t){for(;t.parent;)t=t.parent;return t.scene},read:{svg:function(t){var e=y.Utils.read.g.call(this,t);t.getAttribute("viewBox");return e},defs:function(t){var e=new y.Utils.Error("interpret <defs /> not supported.");return console.warn(e.name,e.message),null},use:function(t){var e=new y.Utils.Error("interpret <use /> not supported.");return console.warn(e.name,e.message),null},g:function(t,e){var i,r=new y.Group;y.Utils.applySvgAttributes.call(this,t,r,e),this.add(r),i=y.Utils.getSvgStyles.call(this,t);for(var s=0,n=t.childNodes.length;s<n;s++){var a=t.childNodes[s],o=a.nodeName;if(!o)return;var l=o.replace(/svg:/gi,"").toLowerCase();if(l in y.Utils.read){var h=y.Utils.read[l].call(r,a,i);h&&!h.parent&&r.add(h);}}return r},polygon:function(t,e){var i=t.getAttribute("points"),r=[];i.replace(/(-?[\d.?]+)[,|\s](-?[\d.?]+)/g,(function(t,e,i){r.push(new y.Anchor(parseFloat(e),parseFloat(i)));}));var s=new y.Path(r,!0).noStroke();return s.fill="black",y.Utils.applySvgAttributes.call(this,t,s,e),s},polyline:function(t,e){var i=y.Utils.read.polygon.call(this,t,e);return i.closed=!1,i},path:function(t,e){var i=t.getAttribute("d"),s=[],n=!1,a=!1;if(i){var o,l,h=new y.Anchor,d=i.match(/[a-df-z][^a-df-z]*/gi),f=d.length-1;r.each(d.slice(0),(function(t,e){var i,r,s,n,a,o,l,h,c,f=t[0],u=f.toLowerCase(),_=t.slice(1).trim().split(/[\s,]+|(?=\s?[+-])/),g=[],p=!1;for(a=0;a<_.length;a++)if((i=_[a]).indexOf(".")!==i.lastIndexOf(".")){for(s=(r=i.split("."))[0]+"."+r[1],_.splice(a,1,s),n=2;n<r.length;n++)_.splice(a+n-1,0,"0."+r[n]);p=!0;}switch(p&&(t=f+_.join(",")),e<=0&&(d=[]),u){case"h":case"v":_.length>1&&(c=1);break;case"m":case"l":case"t":_.length>2&&(c=2);break;case"s":case"q":_.length>4&&(c=4);break;case"c":_.length>6&&(c=6);break;case"a":_.length>7&&(c=7);}if(c){for(a=0,l=_.length,h=0;a<l;a+=c){if(o=f,h>0)switch(f){case"m":o="l";break;case"M":o="L";}g.push(o+_.slice(a,a+c).join(" ")),h++;}d=Array.prototype.concat.apply(d,g);}else d.push(t);})),r.each(d,(function(t,e){var i,d,u,_,g,p,m,v,x,b,w,k,S=t[0],A=S.toLowerCase();switch(l=(l=(l=t.slice(1).trim()).replace(/(-?\d+(?:\.\d*)?)[eE]([+-]?\d+)/g,(function(t,e,i){return parseFloat(e)*c(10,i)}))).split(/[\s,]+|(?=\s?[+-])/),a=S===A,A){case"z":if(e>=f)n=!0;else {d=h.x,u=h.y,i=new y.Anchor(d,u,void 0,void 0,void 0,void 0,y.Commands.close);for(var C=s.length-1;C>=0;C--){var R=s[C];if(/m/i.test(R.command)){h=R;break}}}break;case"m":case"l":o=void 0,d=parseFloat(l[0]),u=parseFloat(l[1]),i=new y.Anchor(d,u,void 0,void 0,void 0,void 0,/m/i.test(A)?y.Commands.move:y.Commands.line),a&&i.addSelf(h),h=i;break;case"h":case"v":var F=/h/i.test(A)?"x":"y",M=/x/i.test(F)?"y":"x";(i=new y.Anchor(void 0,void 0,void 0,void 0,void 0,void 0,y.Commands.line))[F]=parseFloat(l[0]),i[M]=h[M],a&&(i[F]+=h[F]),h=i;break;case"c":case"s":_=h.x,g=h.y,o||(o=new y.Vector),/c/i.test(A)?(p=parseFloat(l[0]),m=parseFloat(l[1]),v=parseFloat(l[2]),x=parseFloat(l[3]),b=parseFloat(l[4]),w=parseFloat(l[5])):(p=(k=E(h,o,a)).x,m=k.y,v=parseFloat(l[0]),x=parseFloat(l[1]),b=parseFloat(l[2]),w=parseFloat(l[3])),a&&(p+=_,m+=g,v+=_,x+=g,b+=_,w+=g),r.isObject(h.controls)||y.Anchor.AppendCurveProperties(h),h.controls.right.set(p-h.x,m-h.y),i=new y.Anchor(b,w,v-b,x-w,void 0,void 0,y.Commands.curve),h=i,o=i.controls.left;break;case"t":case"q":_=h.x,g=h.y,o||(o=new y.Vector),/q/i.test(A)?(p=parseFloat(l[0]),m=parseFloat(l[1]),v=parseFloat(l[0]),x=parseFloat(l[1]),b=parseFloat(l[2]),w=parseFloat(l[3])):(p=(k=E(h,o,a)).x,m=k.y,v=k.x,x=k.y,b=parseFloat(l[0]),w=parseFloat(l[1])),a&&(p+=_,m+=g,v+=_,x+=g,b+=_,w+=g),r.isObject(h.controls)||y.Anchor.AppendCurveProperties(h),h.controls.right.set(.33*(p-h.x),.33*(m-h.y)),i=new y.Anchor(b,w,v-b,x-w,void 0,void 0,y.Commands.curve),h=i,o=i.controls.left;break;case"a":_=h.x,g=h.y;var O=parseFloat(l[0]),P=parseFloat(l[1]),U=parseFloat(l[2]),T=parseFloat(l[3]),N=parseFloat(l[4]);b=parseFloat(l[5]),w=parseFloat(l[6]),a&&(b+=_,w+=g);var L=new y.Anchor(b,w);L.command=y.Commands.arc,L.rx=O,L.ry=P,L.xAxisRotation=U,L.largeArcFlag=T,L.sweepFlag=N,i=L,h=L,o=void 0;}i&&(r.isArray(i)?s=s.concat(i):s.push(i));}));}(i=new y.Path(s,n,void 0,!0).noStroke()).fill="black";var u=i.getBoundingClientRect(!0);return u.centroid={x:u.left+u.width/2,y:u.top+u.height/2},r.each(i.vertices,(function(t){t.subSelf(u.centroid);})),y.Utils.applySvgAttributes.call(this,t,i,e),i.translation.addSelf(u.centroid),i},circle:function(t,e){var i=parseFloat(t.getAttribute("cx")),r=parseFloat(t.getAttribute("cy")),s=parseFloat(t.getAttribute("r")),n=new y.Circle(i,r,s).noStroke();return n.fill="black",y.Utils.applySvgAttributes.call(this,t,n,e),n},ellipse:function(t,e){var i=parseFloat(t.getAttribute("cx")),r=parseFloat(t.getAttribute("cy")),s=parseFloat(t.getAttribute("rx")),n=parseFloat(t.getAttribute("ry")),a=new y.Ellipse(i,r,s,n).noStroke();return a.fill="black",y.Utils.applySvgAttributes.call(this,t,a,e),a},rect:function(t,e){var i=parseFloat(t.getAttribute("rx")),s=parseFloat(t.getAttribute("ry"));if(!r.isNaN(i)||!r.isNaN(s))return y.Utils.read["rounded-rect"](t);var n=parseFloat(t.getAttribute("x"))||0,a=parseFloat(t.getAttribute("y"))||0,o=parseFloat(t.getAttribute("width")),l=parseFloat(t.getAttribute("height")),h=o/2,c=l/2,d=new y.Rectangle(n+h,a+c,o,l).noStroke();return d.fill="black",y.Utils.applySvgAttributes.call(this,t,d,e),d},"rounded-rect":function(t,e){var i=parseFloat(t.getAttribute("x"))||0,r=parseFloat(t.getAttribute("y"))||0,s=parseFloat(t.getAttribute("rx"))||0,n=parseFloat(t.getAttribute("ry"))||0,a=parseFloat(t.getAttribute("width")),o=parseFloat(t.getAttribute("height")),l=a/2,h=o/2,c=new y.Vector(s,n),d=new y.RoundedRectangle(i+l,r+h,a,o,c).noStroke();return d.fill="black",y.Utils.applySvgAttributes.call(this,t,d,e),d},line:function(t,e){var i=parseFloat(t.getAttribute("x1")),r=parseFloat(t.getAttribute("y1")),s=parseFloat(t.getAttribute("x2")),n=parseFloat(t.getAttribute("y2")),a=new y.Line(i,r,s,n).noFill();return y.Utils.applySvgAttributes.call(this,t,a,e),a},lineargradient:function(t,e){for(var i=parseFloat(t.getAttribute("x1")),s=parseFloat(t.getAttribute("y1")),n=parseFloat(t.getAttribute("x2")),a=parseFloat(t.getAttribute("y2")),o=(n+i)/2,l=(a+s)/2,h=[],c=0;c<t.children.length;c++){var d=t.children[c],f=d.getAttribute("offset");/%/gi.test(f)&&(f=parseFloat(f.replace(/%/gi,""))/100),f=parseFloat(f);var u,_=d.getAttribute("stop-color"),g=d.getAttribute("stop-opacity"),p=d.getAttribute("style");r.isNull(_)&&(_=(u=!!p&&p.match(/stop-color:\s?([#a-fA-F0-9]*)/))&&u.length>1?u[1]:void 0),g=r.isNull(g)?(u=!!p&&p.match(/stop-opacity:\s?([0-9.-]*)/))&&u.length>1?parseFloat(u[1]):1:parseFloat(g),h.push(new y.Gradient.Stop(f,_,g));}var m=new y.LinearGradient(i-o,s-l,n-o,a-l,h);return y.Utils.applySvgAttributes.call(this,t,m,e),m},radialgradient:function(t,e){var i=parseFloat(t.getAttribute("cx"))||0,s=parseFloat(t.getAttribute("cy"))||0,n=parseFloat(t.getAttribute("r")),a=parseFloat(t.getAttribute("fx")),l=parseFloat(t.getAttribute("fy"));r.isNaN(a)&&(a=i),r.isNaN(l)&&(l=s);for(var h=o(i+a)/2,c=o(s+l)/2,d=[],f=0;f<t.children.length;f++){var u=t.children[f],_=u.getAttribute("offset");/%/gi.test(_)&&(_=parseFloat(_.replace(/%/gi,""))/100),_=parseFloat(_);var g,p=u.getAttribute("stop-color"),m=u.getAttribute("stop-opacity"),v=u.getAttribute("style");r.isNull(p)&&(p=(g=!!v&&v.match(/stop-color:\s?([#a-fA-F0-9]*)/))&&g.length>1?g[1]:void 0),m=r.isNull(m)?(g=!!v&&v.match(/stop-opacity:\s?([0-9.-]*)/))&&g.length>1?parseFloat(g[1]):1:parseFloat(m),d.push(new y.Gradient.Stop(_,p,m));}var x=new y.RadialGradient(i-h,s-c,n,d,a-h,l-c);return y.Utils.applySvgAttributes.call(this,t,x,e),x}},subdivide:function(t,e,i,r,s,n,a,l,h){var c=(h=h||y.Utils.Curve.RecursionLimit)+1;if(o(t-a)<.001&&o(e-l)<.001)return [new y.Anchor(a,l)];for(var d=[],f=0;f<c;f++){var u=f/c,_=S(u,t,i,s,a),g=S(u,e,r,n,l);d.push(new y.Anchor(_,g));}return d},getComponentOnCubicBezier:function(t,e,i,r,s){var n=1-t;return n*n*n*e+3*n*n*t*i+3*n*t*t*r+t*t*t*s},getCurveLength:function(t,e,i,r,s,n,o,l,h){if(t===i&&e===r&&s===o&&n===l){var c=o-t,d=l-e;return a(c*c+d*d)}var f=9*(i-s)+3*(o-t),u=6*(t+s)-12*i,_=3*(i-t),g=9*(r-n)+3*(l-e),p=6*(e+n)-12*r,m=3*(r-e);return A((function(t){var e=(f*t+u)*t+_,i=(g*t+p)*t+m;return a(e*e+i*i)}),0,1,h||y.Utils.Curve.RecursionLimit)},getCurveBoundingBox:function(t,e,i,r,s,n,a,l){for(var h,c,d,f,u,_,g,p,m=[],v=[[],[]],y=0;y<2;++y)if(0==y?(c=6*t-12*i+6*s,h=-3*t+9*i-9*s+3*a,d=3*i-3*t):(c=6*e-12*r+6*n,h=-3*e+9*r-9*n+3*l,d=3*r-3*e),o(h)<1e-12){if(o(c)<1e-12)continue;0<(f=-d/c)&&f<1&&m.push(f);}else g=c*c-4*d*h,p=Math.sqrt(g),g<0||(0<(u=(-c+p)/(2*h))&&u<1&&m.push(u),0<(_=(-c-p)/(2*h))&&_<1&&m.push(_));for(var x,b=m.length,w=b;b--;)x=1-(f=m[b]),v[0][b]=x*x*x*t+3*x*x*f*i+3*x*f*f*s+f*f*f*a,v[1][b]=x*x*x*e+3*x*x*f*r+3*x*f*f*n+f*f*f*l;return v[0][w]=t,v[1][w]=e,v[0][w+1]=a,v[1][w+1]=l,v[0].length=v[1].length=w+2,{min:{x:Math.min.apply(0,v[0]),y:Math.min.apply(0,v[1])},max:{x:Math.max.apply(0,v[0]),y:Math.max.apply(0,v[1])}}},integrate:function(t,e,i,r){for(var s=y.Utils.Curve.abscissas[r-2],n=y.Utils.Curve.weights[r-2],a=.5*(i-e),o=a+e,l=0,h=r+1>>1,c=1&r?n[l++]*t(o):0;l<h;){var d=a*s[l];c+=n[l++]*(t(o+d)+t(o-d));}return a*c},getCurveFromPoints:function(t,e){for(var i=t.length,s=i-1,n=0;n<i;n++){var a=t[n];r.isObject(a.controls)||y.Anchor.AppendCurveProperties(a);var o=e?w(n-1,i):f(n-1,0),l=e?w(n+1,i):d(n+1,s),h=t[o],c=a,u=t[l];b(h,c,u),c.command=0===n?y.Commands.move:y.Commands.curve;}},getControlPoints:function(t,e,i){var a=y.Vector.angleBetween(t,e),o=y.Vector.angleBetween(i,e),c=y.Vector.distanceBetween(t,e),d=y.Vector.distanceBetween(i,e),f=(a+o)/2;return c<1e-4||d<1e-4?(r.isBoolean(e.relative)&&!e.relative&&(e.controls.left.copy(e),e.controls.right.copy(e)),e):(c*=.33,d*=.33,o<a?f+=h:f-=h,e.controls.left.x=n(f)*c,e.controls.left.y=s(f)*c,f-=l,e.controls.right.x=n(f)*d,e.controls.right.y=s(f)*d,r.isBoolean(e.relative)&&!e.relative&&(e.controls.left.x+=e.x,e.controls.left.y+=e.y,e.controls.right.x+=e.x,e.controls.right.y+=e.y),e)},getReflection:function(t,e,i){return new y.Vector(2*t.x-(e.x+t.x)-(i?t.x:0),2*t.y-(e.y+t.y)-(i?t.y:0))},getAnchorsFromArcData:function(t,e,i,s,n,a,o){var l=y.Resolution;return r.map(r.range(l),(function(t){var e=(t+1)/l;o&&(e=1-e);var r=e*a+n,h=i*Math.cos(r),c=s*Math.sin(r),d=new y.Anchor(h,c);return y.Anchor.AppendCurveProperties(d),d.command=y.Commands.line,d}))},lerp:function(t,e,i){return i*(e-t)+t},toFixed:function(t){return Math.floor(1e3*t)/1e3},mod:function(t,e){for(;t<0;)t+=e;return t%e},Collection:function(){Array.call(this),arguments.length>1?Array.prototype.push.apply(this,arguments):arguments[0]&&Array.isArray(arguments[0])&&Array.prototype.push.apply(this,arguments[0]);},Error:function(t){this.name="Two.js",this.message=t;},Events:{on:function(t,e){return this._events||(this._events={}),(this._events[t]||(this._events[t]=[])).push(e),this},off:function(t,e){if(!this._events)return this;if(!t&&!e)return this._events={},this;for(var i=t?[t]:r.keys(this._events),s=0,n=i.length;s<n;s++){t=i[s];var a=this._events[t];if(a){var o=[];if(e)for(var l=0,h=a.length;l<h;l++){var c=a[l];c=c.handler?c.handler:c,e&&e!==c&&o.push(c);}this._events[t]=o;}}return this},trigger:function(t){if(!this._events)return this;var e=_.call(arguments,1),i=this._events[t];return i&&x(this,i,e),this},listen:function(t,e,i){var r=this;if(t){var s=function(){i.apply(r,arguments);};s.obj=t,s.name=e,s.handler=i,t.on(e,s);}return this},ignore:function(t,e,i){return t.off(e,i),this}}})}),y.Utils.Events.bind=y.Utils.Events.on,y.Utils.Events.unbind=y.Utils.Events.off;var x=function(t,e,i){var r;switch(i.length){case 0:r=function(r){e[r].call(t,i[0]);};break;case 1:r=function(r){e[r].call(t,i[0],i[1]);};break;case 2:r=function(r){e[r].call(t,i[0],i[1],i[2]);};break;case 3:r=function(r){e[r].call(t,i[0],i[1],i[2],i[3]);};break;default:r=function(r){e[r].apply(t,i);};}for(var s=0;s<e.length;s++)r(s);};y.Utils.Error.prototype=new Error,y.Utils.Error.prototype.constructor=y.Utils.Error,y.Utils.Collection.prototype=new Array,y.Utils.Collection.prototype.constructor=y.Utils.Collection,r.extend(y.Utils.Collection.prototype,y.Utils.Events,{pop:function(){var t=Array.prototype.pop.apply(this,arguments);return this.trigger(y.Events.remove,[t]),t},shift:function(){var t=Array.prototype.shift.apply(this,arguments);return this.trigger(y.Events.remove,[t]),t},push:function(){var t=Array.prototype.push.apply(this,arguments);return this.trigger(y.Events.insert,arguments),t},unshift:function(){var t=Array.prototype.unshift.apply(this,arguments);return this.trigger(y.Events.insert,arguments),t},splice:function(){var t,e=Array.prototype.splice.apply(this,arguments);return this.trigger(y.Events.remove,e),arguments.length>2&&(t=this.slice(arguments[0],arguments[0]+arguments.length-2),this.trigger(y.Events.insert,t),this.trigger(y.Events.order)),e},sort:function(){return Array.prototype.sort.apply(this,arguments),this.trigger(y.Events.order),this},reverse:function(){return Array.prototype.reverse.apply(this,arguments),this.trigger(y.Events.order),this}});y.Utils.getAnchorsFromArcData;var b=y.Utils.getControlPoints,w=(y.Utils.getCurveFromPoints,y.Utils.solveSegmentIntersection,y.Utils.decoupleShapes,y.Utils.mod),k=y.Utils.getBackingStoreRatio,S=y.Utils.getComponentOnCubicBezier,A=(y.Utils.getCurveLength,y.Utils.integrate),E=y.Utils.getReflection;function C(){var t=document.body.getBoundingClientRect(),e=this.width=t.width,i=this.height=t.height;this.renderer.setSize(e,i,this.ratio);}function R(t,e){this.width=t,this.height=e,this.trigger(y.Events.resize,t,e);}r.extend(y.prototype,y.Utils.Events,{constructor:y,appendTo:function(t){return t.appendChild(this.renderer.domElement),this},play:function(){return y.Utils.setPlaying.call(this,!0),F.init(),this.trigger(y.Events.play)},pause:function(){return this.playing=!1,this.trigger(y.Events.pause)},update:function(){var t=!!this._lastFrame,e=g.now();t&&(this.timeDelta=parseFloat((e-this._lastFrame).toFixed(3))),this._lastFrame=e;var i=this.width,r=this.height,s=this.renderer;return i===s.width&&r===s.height||s.setSize(i,r,this.ratio),this.trigger(y.Events.update,this.frameCount,this.timeDelta),this.render()},render:function(){return this.renderer.render(),this.trigger(y.Events.render,this.frameCount++)},add:function(t){var e=t;return e instanceof Array||(e=r.toArray(arguments)),this.scene.add(e),this},remove:function(t){var e=t;return e instanceof Array||(e=r.toArray(arguments)),this.scene.remove(e),this},clear:function(){return this.scene.remove(this.scene.children),this},makeLine:function(t,e,i,r){var s=new y.Line(t,e,i,r);return this.scene.add(s),s},makeArrow:function(t,e,i,s,n){var a=r.isNumber(n)?n:10,o=Math.atan2(s-e,i-t),l=[new y.Anchor(t,e,void 0,void 0,void 0,void 0,y.Commands.move),new y.Anchor(i,s,void 0,void 0,void 0,void 0,y.Commands.line),new y.Anchor(i-a*Math.cos(o-Math.PI/4),s-a*Math.sin(o-Math.PI/4),void 0,void 0,void 0,void 0,y.Commands.line),new y.Anchor(i,s,void 0,void 0,void 0,void 0,y.Commands.move),new y.Anchor(i-a*Math.cos(o+Math.PI/4),s-a*Math.sin(o+Math.PI/4),void 0,void 0,void 0,void 0,y.Commands.line)],h=new y.Path(l,!1,!1,!0);return h.noFill(),h.cap="round",h.join="round",this.scene.add(h),h},makeRectangle:function(t,e,i,r){var s=new y.Rectangle(t,e,i,r);return this.scene.add(s),s},makeRoundedRectangle:function(t,e,i,r,s){var n=new y.RoundedRectangle(t,e,i,r,s);return this.scene.add(n),n},makeCircle:function(t,e,i,r){var s=new y.Circle(t,e,i,r);return this.scene.add(s),s},makeEllipse:function(t,e,i,r,s){var n=new y.Ellipse(t,e,i,r,s);return this.scene.add(n),n},makeStar:function(t,e,i,r,s){var n=new y.Star(t,e,i,r,s);return this.scene.add(n),n},makeCurve:function(t){var e=arguments.length,i=t;if(!r.isArray(t)){i=[];for(var s=0;s<e;s+=2){var n=arguments[s];if(!r.isNumber(n))break;var a=arguments[s+1];i.push(new y.Anchor(n,a));}}var o=arguments[e-1],l=new y.Path(i,!(r.isBoolean(o)?o:void 0),!0),h=l.getBoundingClientRect();return l.center().translation.set(h.left+h.width/2,h.top+h.height/2),this.scene.add(l),l},makePolygon:function(t,e,i,r){var s=new y.Polygon(t,e,i,r);return this.scene.add(s),s},makeArcSegment:function(t,e,i,r,s,n,a){var o=new y.ArcSegment(t,e,i,r,s,n,a);return this.scene.add(o),o},makePath:function(t){var e=arguments.length,i=t;if(!r.isArray(t)){i=[];for(var s=0;s<e;s+=2){var n=arguments[s];if(!r.isNumber(n))break;var a=arguments[s+1];i.push(new y.Anchor(n,a));}}var o=arguments[e-1],l=new y.Path(i,!(r.isBoolean(o)?o:void 0)),h=l.getBoundingClientRect();return r.isNumber(h.top)&&r.isNumber(h.left)&&r.isNumber(h.right)&&r.isNumber(h.bottom)&&l.center().translation.set(h.left+h.width/2,h.top+h.height/2),this.scene.add(l),l},makeText:function(t,e,i,r){var s=new y.Text(t,e,i,r);return this.add(s),s},makeLinearGradient:function(t,e,i,r){var s=_.call(arguments,4),n=new y.LinearGradient(t,e,i,r,s);return this.add(n),n},makeRadialGradient:function(t,e,i){var r=_.call(arguments,3),s=new y.RadialGradient(t,e,i,r);return this.add(s),s},makeSprite:function(t,e,i,r,s,n,a){var o=new y.Sprite(t,e,i,r,s,n);return a&&o.play(),this.add(o),o},makeImageSequence:function(t,e,i,r,s){var n=new y.ImageSequence(t,e,i,r);return s&&n.play(),this.add(n),n},makeTexture:function(t,e){return new y.Texture(t,e)},makeGroup:function(t){var e=t;e instanceof Array||(e=r.toArray(arguments));var i=new y.Group;return this.scene.add(i),i.add(e),i},interpret:function(t,e,i){var r=t.tagName.toLowerCase();if(i=void 0===i||i,!(r in y.Utils.read))return null;var s=y.Utils.read[r].call(this,t);return i&&this.add(e&&s instanceof y.Group?s.children:s),s},load:function(t,e){var i,s,n,a=new y.Group,o=r.bind((function(t){for(v.temp.innerHTML=t,s=0;s<v.temp.children.length;s++)if(i=v.temp.children[s],/svg/i.test(i.nodeName))for(n=0;n<i.children.length;n++)a.add(this.interpret(i.children[n]));else a.add(this.interpret(i));if(r.isFunction(e)){var o=v.temp.children.length<=1?v.temp.children[0]:v.temp.children;e(a,o);}}),this);return /.*\.svg$/gi.test(t)?(y.Utils.xhr(t,o),a):(o(t),a)}});var F=v.getRequestAnimationFrame();function M(){for(var t=0;t<y.Instances.length;t++){var e=y.Instances[t];e.playing&&e.update();}y.nextFrameID=F(M);}return module.exports?module.exports=y:"function"==typeof undefined,y}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Utils,i=t.Registry=function(){this.map={};};e.extend(i.prototype,{constructor:i,add:function(t,e){return this.map[t]=e,this},remove:function(t){return delete this.map[t],this},get:function(t){return this.map[t]},contains:function(t){return t in this.map}});}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Utils,i=t.Vector=function(t,e){this.x=t||0,this.y=e||0;};e.extend(i,{zero:new t.Vector,add:function(t,e){return new i(t.x+e.x,t.y+e.y)},sub:function(t,e){return new i(t.x-e.x,t.y-e.y)},subtract:function(t,e){return i.sub(t,e)},ratioBetween:function(t,e){return (t.x*e.x+t.y*e.y)/(t.length()*e.length())},angleBetween:function(t,e){var i,r;return arguments.length>=4?(i=arguments[0]-arguments[2],r=arguments[1]-arguments[3],Math.atan2(r,i)):(i=t.x-e.x,r=t.y-e.y,Math.atan2(r,i))},distanceBetween:function(t,e){return Math.sqrt(i.distanceBetweenSquared(t,e))},distanceBetweenSquared:function(t,e){var i=t.x-e.x,r=t.y-e.y;return i*i+r*r},MakeObservable:function(i){i.bind=i.on=function(){return this._bound||(this._x=this.x,this._y=this.y,Object.defineProperty(this,"x",s),Object.defineProperty(this,"y",n),e.extend(this,r),this._bound=!0),t.Utils.Events.bind.apply(this,arguments),this};}}),e.extend(i.prototype,t.Utils.Events,{constructor:i,set:function(t,e){return this.x=t,this.y=e,this},copy:function(t){return this.x=t.x,this.y=t.y,this},clear:function(){return this.x=0,this.y=0,this},clone:function(){return new i(this.x,this.y)},add:function(t,i){return arguments.length<=0?this:(arguments.length<=1?e.isNumber(t)?(this.x+=t,this.y+=t):t&&e.isNumber(t.x)&&e.isNumber(t.y)&&(this.x+=t.x,this.y+=t.y):(this.x+=t,this.y+=i),this)},addSelf:function(t){return this.add.apply(this,arguments)},sub:function(t,i){return arguments.length<=0?this:(arguments.length<=1?e.isNumber(t)?(this.x-=t,this.y-=t):t&&e.isNumber(t.x)&&e.isNumber(t.y)&&(this.x-=t.x,this.y-=t.y):(this.x-=t,this.y-=i),this)},subtract:function(){return this.sub.apply(this,arguments)},subSelf:function(t){return this.sub.apply(this,arguments)},subtractSelf:function(t){return this.sub.apply(this,arguments)},multiply:function(t,i){return arguments.length<=0?this:(arguments.length<=1?e.isNumber(t)?(this.x*=t,this.y*=t):t&&e.isNumber(t.x)&&e.isNumber(t.y)&&(this.x*=t.x,this.y*=t.y):(this.x*=t,this.y*=i),this)},multiplySelf:function(t){return this.multiply.apply(this,arguments)},multiplyScalar:function(t){return this.multiply(t)},divide:function(t,i){return arguments.length<=0?this:(arguments.length<=1?e.isNumber(t)?(this.x/=t,this.y/=t):t&&e.isNumber(t.x)&&e.isNumber(t.y)&&(this.x/=t.x,this.y/=t.y):(this.x/=t,this.y/=i),e.isNaN(this.x)&&(this.x=0),e.isNaN(this.y)&&(this.y=0),this)},divideSelf:function(t){return this.divide.apply(this,arguments)},divideScalar:function(t){return this.divide(t)},negate:function(){return this.multiply(-1)},dot:function(t){return this.x*t.x+this.y*t.y},length:function(){return Math.sqrt(this.lengthSquared())},lengthSquared:function(){return this.x*this.x+this.y*this.y},normalize:function(){return this.divideScalar(this.length())},distanceTo:function(t){return Math.sqrt(this.distanceToSquared(t))},distanceToSquared:function(t){var e=this.x-t.x,i=this.y-t.y;return e*e+i*i},setLength:function(t){return this.normalize().multiplyScalar(t)},equals:function(t,e){return e=void 0===e?1e-4:e,this.distanceTo(t)<e},lerp:function(t,e){var i=(t.x-this.x)*e+this.x,r=(t.y-this.y)*e+this.y;return this.set(i,r)},isZero:function(t){return t=void 0===t?1e-4:t,this.length()<t},toString:function(){return this.x+", "+this.y},toObject:function(){return {x:this.x,y:this.y}},rotate:function(t){var e=Math.cos(t),i=Math.sin(t);return this.x=this.x*e-this.y*i,this.y=this.x*i+this.y*e,this}});var r={constructor:i,set:function(e,i){return this._x=e,this._y=i,this.trigger(t.Events.change)},copy:function(e){return this._x=e.x,this._y=e.y,this.trigger(t.Events.change)},clear:function(){return this._x=0,this._y=0,this.trigger(t.Events.change)},clone:function(){return new i(this._x,this._y)},add:function(i,r){return arguments.length<=0?this:(arguments.length<=1?e.isNumber(i)?(this._x+=i,this._y+=i):i&&e.isNumber(i.x)&&e.isNumber(i.y)&&(this._x+=i.x,this._y+=i.y):(this._x+=i,this._y+=r),this.trigger(t.Events.change))},sub:function(i,r){return arguments.length<=0?this:(arguments.length<=1?e.isNumber(i)?(this._x-=i,this._y-=i):i&&e.isNumber(i.x)&&e.isNumber(i.y)&&(this._x-=i.x,this._y-=i.y):(this._x-=i,this._y-=r),this.trigger(t.Events.change))},multiply:function(i,r){return arguments.length<=0?this:(arguments.length<=1?e.isNumber(i)?(this._x*=i,this._y*=i):i&&e.isNumber(i.x)&&e.isNumber(i.y)&&(this._x*=i.x,this._y*=i.y):(this._x*=i,this._y*=r),this.trigger(t.Events.change))},divide:function(i,r){return arguments.length<=0?this:(arguments.length<=1?e.isNumber(i)?(this._x/=i,this._y/=i):i&&e.isNumber(i.x)&&e.isNumber(i.y)&&(this._x/=i.x,this._y/=i.y):(this._x/=i,this._y/=r),e.isNaN(this._x)&&(this._x=0),e.isNaN(this._y)&&(this._y=0),this.trigger(t.Events.change))},dot:function(t){return this._x*t.x+this._y*t.y},lengthSquared:function(){return this._x*this._x+this._y*this._y},distanceToSquared:function(t){var e=this._x-t.x,i=this._y-t.y;return e*e+i*i},lerp:function(t,e){var i=(t.x-this._x)*e+this._x,r=(t.y-this._y)*e+this._y;return this.set(i,r)},toString:function(){return this._x+", "+this._y},toObject:function(){return {x:this._x,y:this._y}},rotate:function(t){var e=Math.cos(t),i=Math.sin(t);return this._x=this._x*e-this._y*i,this._y=this._x*i+this._y*e,this}},s={enumerable:!0,get:function(){return this._x},set:function(e){this._x=e,this.trigger(t.Events.change,"x");}},n={enumerable:!0,get:function(){return this._y},set:function(e){this._y=e,this.trigger(t.Events.change,"y");}};i.MakeObservable(i.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Commands,i=t.Utils,r=t.Anchor=function(r,s,n,a,o,l,h){t.Vector.call(this,r,s),this._broadcast=i.bind((function(){this.trigger(t.Events.change);}),this),this._command=h||e.move,this._relative=!0;var c=i.isNumber(n),d=i.isNumber(a),f=i.isNumber(o),u=i.isNumber(l);(c||d||f||u)&&t.Anchor.AppendCurveProperties(this),c&&(this.controls.left.x=n),d&&(this.controls.left.y=a),f&&(this.controls.right.x=o),u&&(this.controls.right.y=l);};i.extend(t.Anchor,{AppendCurveProperties:function(e){e.relative=!0,e.controls={},e.controls.left=new t.Vector(0,0),e.controls.right=new t.Vector(0,0);},MakeObservable:function(n){Object.defineProperty(n,"command",{enumerable:!0,get:function(){return this._command},set:function(s){return this._command=s,this._command!==e.curve||i.isObject(this.controls)||r.AppendCurveProperties(this),this.trigger(t.Events.change)}}),Object.defineProperty(n,"relative",{enumerable:!0,get:function(){return this._relative},set:function(e){return this._relative==e?this:(this._relative=!!e,this.trigger(t.Events.change))}}),i.extend(n,t.Vector.prototype,s),n.bind=n.on=function(){var e=this._bound;t.Vector.prototype.bind.apply(this,arguments),e||i.extend(this,s);};}});var s={constructor:t.Anchor,listen:function(){return i.isObject(this.controls)||t.Anchor.AppendCurveProperties(this),this.controls.left.bind(t.Events.change,this._broadcast),this.controls.right.bind(t.Events.change,this._broadcast),this},ignore:function(){return this.controls.left.unbind(t.Events.change,this._broadcast),this.controls.right.unbind(t.Events.change,this._broadcast),this},copy:function(e){return this.x=e.x,this.y=e.y,i.isString(e.command)&&(this.command=e.command),i.isObject(e.controls)&&(i.isObject(this.controls)||t.Anchor.AppendCurveProperties(this),this.controls.left.copy(e.controls.left),this.controls.right.copy(e.controls.right)),i.isBoolean(e.relative)&&(this.relative=e.relative),this.command===t.Commands.arc&&(this.rx=e.rx,this.ry=e.ry,this.xAxisRotation=e.xAxisRotation,this.largeArcFlag=e.largeArcFlag,this.sweepFlag=e.sweepFlag),this},clone:function(){var e=this.controls,i=new t.Anchor(this.x,this.y,e&&e.left.x,e&&e.left.y,e&&e.right.x,e&&e.right.y,this.command);return i.relative=this._relative,i},toObject:function(){var t={x:this.x,y:this.y};return this._command&&(t.command=this._command),this._relative&&(t.relative=this._relative),this.controls&&(t.controls={left:this.controls.left.toObject(),right:this.controls.right.toObject()}),t},toString:function(){return this.controls?[this._x,this._y,this.controls.left.x,this.controls.left.y,this.controls.right.x,this.controls.right.y,this._command,this._relative?1:0].join(", "):[this._x,this._y].join(", ")}};t.Anchor.MakeObservable(t.Anchor.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=Math.cos,i=Math.sin,r=Math.tan,s=t.Utils,n=[],a=t.Matrix=function(e,i,r,n,a,o){this.elements=new t.Array(9);var l=e;s.isArray(l)||(l=s.toArray(arguments)),this.identity(),l.length>0&&this.set(l);};s.extend(a,{Identity:[1,0,0,0,1,0,0,0,1],Multiply:function(e,i,r){if(i.length<=3){var s=e,n=i[0]||0,a=i[1]||0,o=i[2]||0;return {x:s[0]*n+s[1]*a+s[2]*o,y:s[3]*n+s[4]*a+s[5]*o,z:s[6]*n+s[7]*a+s[8]*o}}var l=e[0],h=e[1],c=e[2],d=e[3],f=e[4],u=e[5],_=e[6],g=e[7],p=e[8],m=i[0],v=i[1],y=i[2],x=i[3],b=i[4],w=i[5],k=i[6],S=i[7],A=i[8];return (r=r||new t.Array(9))[0]=l*m+h*x+c*k,r[1]=l*v+h*b+c*S,r[2]=l*y+h*w+c*A,r[3]=d*m+f*x+u*k,r[4]=d*v+f*b+u*S,r[5]=d*y+f*w+u*A,r[6]=_*m+g*x+p*k,r[7]=_*v+g*b+p*S,r[8]=_*y+g*w+p*A,r}}),s.extend(a.prototype,t.Utils.Events,{constructor:a,manual:!1,set:function(e,i,r,n,a,o,l,h,c){var d;return s.isUndefined(i)&&(e=(d=e)[0],i=d[1],r=d[2],n=d[3],a=d[4],o=d[5],l=d[6],h=d[7],c=d[8]),this.elements[0]=e,this.elements[1]=i,this.elements[2]=r,this.elements[3]=n,this.elements[4]=a,this.elements[5]=o,this.elements[6]=l,this.elements[7]=h,this.elements[8]=c,this.trigger(t.Events.change)},copy:function(e){return this.elements[0]=e.elements[0],this.elements[1]=e.elements[1],this.elements[2]=e.elements[2],this.elements[3]=e.elements[3],this.elements[4]=e.elements[4],this.elements[5]=e.elements[5],this.elements[6]=e.elements[6],this.elements[7]=e.elements[7],this.elements[8]=e.elements[8],this.manual=e.manual,this.trigger(t.Events.change)},identity:function(){return this.elements[0]=a.Identity[0],this.elements[1]=a.Identity[1],this.elements[2]=a.Identity[2],this.elements[3]=a.Identity[3],this.elements[4]=a.Identity[4],this.elements[5]=a.Identity[5],this.elements[6]=a.Identity[6],this.elements[7]=a.Identity[7],this.elements[8]=a.Identity[8],this.trigger(t.Events.change)},multiply:function(e,i,r,n,a,o,l,h,c){if(s.isUndefined(i))return this.elements[0]*=e,this.elements[1]*=e,this.elements[2]*=e,this.elements[3]*=e,this.elements[4]*=e,this.elements[5]*=e,this.elements[6]*=e,this.elements[7]*=e,this.elements[8]*=e,this.trigger(t.Events.change);if(s.isUndefined(n))return e=e||0,i=i||0,r=r||0,{x:(a=this.elements)[0]*e+a[1]*i+a[2]*r,y:a[3]*e+a[4]*i+a[5]*r,z:a[6]*e+a[7]*i+a[8]*r};var d=this.elements,f=[e,i,r,n,a,o,l,h,c],u=d[0],_=d[1],g=d[2],p=d[3],m=d[4],v=d[5],y=d[6],x=d[7],b=d[8],w=f[0],k=f[1],S=f[2],A=f[3],E=f[4],C=f[5],R=f[6],F=f[7],M=f[8];return this.elements[0]=u*w+_*A+g*R,this.elements[1]=u*k+_*E+g*F,this.elements[2]=u*S+_*C+g*M,this.elements[3]=p*w+m*A+v*R,this.elements[4]=p*k+m*E+v*F,this.elements[5]=p*S+m*C+v*M,this.elements[6]=y*w+x*A+b*R,this.elements[7]=y*k+x*E+b*F,this.elements[8]=y*S+x*C+b*M,this.trigger(t.Events.change)},inverse:function(e){var i=this.elements;e=e||new t.Matrix;var r=i[0],s=i[1],n=i[2],a=i[3],o=i[4],l=i[5],h=i[6],c=i[7],d=i[8],f=d*o-l*c,u=-d*a+l*h,_=c*a-o*h,g=r*f+s*u+n*_;return g?(g=1/g,e.elements[0]=f*g,e.elements[1]=(-d*s+n*c)*g,e.elements[2]=(l*s-n*o)*g,e.elements[3]=u*g,e.elements[4]=(d*r-n*h)*g,e.elements[5]=(-l*r+n*a)*g,e.elements[6]=_*g,e.elements[7]=(-c*r+s*h)*g,e.elements[8]=(o*r-s*a)*g,e):null},scale:function(t,e){var i=arguments.length;return i<=1&&(e=t),this.multiply(t,0,0,0,e,0,0,0,1)},rotate:function(t){var r=e(t),s=i(t);return this.multiply(r,-s,0,s,r,0,0,0,1)},translate:function(t,e){return this.multiply(1,0,t,0,1,e,0,0,1)},skewX:function(t){var e=r(t);return this.multiply(1,e,0,0,1,0,0,0,1)},skewY:function(t){var e=r(t);return this.multiply(1,0,0,e,1,0,0,0,1)},toString:function(e){return n.length=0,this.toTransformArray(e,n),n.map(t.Utils.toFixed).join(" ")},toTransformArray:function(t,e){var i=this.elements,r=!!e,s=i[0],n=i[1],a=i[2],o=i[3],l=i[4],h=i[5];if(t){var c=i[6],d=i[7],f=i[8];return r?(e[0]=s,e[1]=o,e[2]=c,e[3]=n,e[4]=l,e[5]=d,e[6]=a,e[7]=h,void(e[8]=f)):[s,o,c,n,l,d,a,h,f]}return r?(e[0]=s,e[1]=o,e[2]=n,e[3]=l,e[4]=a,void(e[5]=h)):[s,o,n,l,a,h]},toArray:function(t,e){var i=this.elements,r=!!e,s=i[0],n=i[1],a=i[2],o=i[3],l=i[4],h=i[5];if(t){var c=i[6],d=i[7],f=i[8];return r?(e[0]=s,e[1]=n,e[2]=a,e[3]=o,e[4]=l,e[5]=h,e[6]=c,e[7]=d,void(e[8]=f)):[s,n,a,o,l,h,c,d,f]}return r?(e[0]=s,e[1]=n,e[2]=a,e[3]=o,e[4]=l,void(e[5]=h)):[s,n,a,o,l,h]},toObject:function(){return {elements:this.toArray(!0),manual:!!this.manual}},clone:function(){return (new t.Matrix).copy(this)}});}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Utils.mod,i=t.Utils.toFixed,r=t.Utils,s={version:1.1,ns:"http://www.w3.org/2000/svg",xlink:"http://www.w3.org/1999/xlink",alignments:{left:"start",center:"middle",right:"end"},createElement:function(t,e){var i=t,n=document.createElementNS(s.ns,i);return "svg"===i&&(e=r.defaults(e||{},{version:s.version})),r.isEmpty(e)||s.setAttributes(n,e),n},setAttributes:function(t,e){for(var i=Object.keys(e),r=0;r<i.length;r++)/href/.test(i[r])?t.setAttributeNS(s.xlink,i[r],e[i[r]]):t.setAttribute(i[r],e[i[r]]);return this},removeAttributes:function(t,e){for(var i in e)t.removeAttribute(i);return this},toString:function(r,s){for(var n,a=r.length,o=a-1,l="",h=0;h<a;h++){var c,d,f,u,_,g,p,m,v,y,x,b,w,k,S=r[h],A=s?e(h-1,a):Math.max(h-1,0),E=s?e(h+1,a):Math.min(h+1,o),C=r[A],R=r[E],F=i(S.x),M=i(S.y);switch(S.command){case t.Commands.close:c=t.Commands.close;break;case t.Commands.arc:y=S.rx,x=S.ry,b=S.xAxisRotation,w=S.largeArcFlag,k=S.sweepFlag,c=t.Commands.arc+" "+y+" "+x+" "+b+" "+w+" "+k+" "+F+" "+M;break;case t.Commands.curve:g=C.controls&&C.controls.right||t.Vector.zero,p=S.controls&&S.controls.left||t.Vector.zero,C.relative?(d=i(g.x+C.x),f=i(g.y+C.y)):(d=i(g.x),f=i(g.y)),S.relative?(u=i(p.x+S.x),_=i(p.y+S.y)):(u=i(p.x),_=i(p.y)),c=(0===h?t.Commands.move:t.Commands.curve)+" "+d+" "+f+" "+u+" "+_+" "+F+" "+M;break;case t.Commands.move:n=S,c=t.Commands.move+" "+F+" "+M;break;default:c=S.command+" "+F+" "+M;}h>=o&&s&&(S.command===t.Commands.curve&&(R=n,m=S.controls&&S.controls.right||S,v=R.controls&&R.controls.left||R,S.relative?(d=i(m.x+S.x),f=i(m.y+S.y)):(d=i(m.x),f=i(m.y)),R.relative?(u=i(v.x+R.x),_=i(v.y+R.y)):(u=i(v.x),_=i(v.y)),c+=" C "+d+" "+f+" "+u+" "+_+" "+(F=i(R.x))+" "+(M=i(R.y))),S.command!==t.Commands.close&&(c+=" Z")),l+=c+" ";}return l},getClip:function(t){var e=t._renderer.clip;if(!e){for(var i=t;i.parent;)i=i.parent;e=t._renderer.clip=s.createElement("clipPath"),i.defs.appendChild(e);}return e},group:{appendChild:function(t){var e=t._renderer.elem;if(e){var i=e.nodeName;!i||/(radial|linear)gradient/i.test(i)||t._clip||this.elem.appendChild(e);}},removeChild:function(t){var e=t._renderer.elem;e&&e.parentNode==this.elem&&(e.nodeName&&(t._clip||this.elem.removeChild(e)));},orderChild:function(t){this.elem.appendChild(t._renderer.elem);},renderChild:function(t){s[t._renderer.type].render.call(t,this);},render:function(t){if(this._update(),0===this._opacity&&!this._flagOpacity)return this;this._renderer.elem||(this._renderer.elem=s.createElement("g",{id:this.id}),t.appendChild(this._renderer.elem));var e=this._matrix.manual||this._flagMatrix,i={domElement:t,elem:this._renderer.elem};e&&this._renderer.elem.setAttribute("transform","matrix("+this._matrix.toString()+")");for(var r=0;r<this.children.length;r++){var n=this.children[r];s[n._renderer.type].render.call(n,t);}return this._flagOpacity&&this._renderer.elem.setAttribute("opacity",this._opacity),this._flagClassName&&this._renderer.elem.setAttribute("class",this.classList.join(" ")),this._flagAdditions&&this.additions.forEach(s.group.appendChild,i),this._flagSubtractions&&this.subtractions.forEach(s.group.removeChild,i),this._flagOrder&&this.children.forEach(s.group.orderChild,i),this._flagMask&&(this._mask?this._renderer.elem.setAttribute("clip-path","url(#"+this._mask.id+")"):this._renderer.elem.removeAttribute("clip-path")),this.flagReset()}},path:{render:function(t){if(this._update(),0===this._opacity&&!this._flagOpacity)return this;var e={};if((this._matrix.manual||this._flagMatrix)&&(e.transform="matrix("+this._matrix.toString()+")"),this._flagVertices){var i=s.toString(this._renderer.vertices,this._closed);e.d=i;}if(this._fill&&this._fill._renderer&&(this._fill._update(),s[this._fill._renderer.type].render.call(this._fill,t,!0)),this._flagFill&&(e.fill=this._fill&&this._fill.id?"url(#"+this._fill.id+")":this._fill),this._stroke&&this._stroke._renderer&&(this._stroke._update(),s[this._stroke._renderer.type].render.call(this._stroke,t,!0)),this._flagStroke&&(e.stroke=this._stroke&&this._stroke.id?"url(#"+this._stroke.id+")":this._stroke),this._flagLinewidth&&(e["stroke-width"]=this._linewidth),this._flagOpacity&&(e["stroke-opacity"]=this._opacity,e["fill-opacity"]=this._opacity),this._flagClassName&&(e.class=this.classList.join(" ")),this._flagVisible&&(e.visibility=this._visible?"visible":"hidden"),this._flagCap&&(e["stroke-linecap"]=this._cap),this._flagJoin&&(e["stroke-linejoin"]=this._join),this._flagMiter&&(e["stroke-miterlimit"]=this._miter),this.dashes&&this.dashes.length>0&&(e["stroke-dasharray"]=this.dashes.join(" "),e["stroke-dashoffset"]=this.dashes.offset||0),this._renderer.elem?s.setAttributes(this._renderer.elem,e):(e.id=this.id,this._renderer.elem=s.createElement("path",e),t.appendChild(this._renderer.elem)),this._flagClip){var r=s.getClip(this),n=this._renderer.elem;this._clip?(n.removeAttribute("id"),r.setAttribute("id",this.id),r.appendChild(n)):(r.removeAttribute("id"),n.setAttribute("id",this.id),this.parent._renderer.elem.appendChild(n));}return this.flagReset()}},text:{render:function(t){this._update();var e={};if((this._matrix.manual||this._flagMatrix)&&(e.transform="matrix("+this._matrix.toString()+")"),this._flagFamily&&(e["font-family"]=this._family),this._flagSize&&(e["font-size"]=this._size),this._flagLeading&&(e["line-height"]=this._leading),this._flagAlignment&&(e["text-anchor"]=s.alignments[this._alignment]||this._alignment),this._flagBaseline&&(e["alignment-baseline"]=e["dominant-baseline"]=this._baseline),this._flagStyle&&(e["font-style"]=this._style),this._flagWeight&&(e["font-weight"]=this._weight),this._flagDecoration&&(e["text-decoration"]=this._decoration),this._fill&&this._fill._renderer&&(this._fill._update(),s[this._fill._renderer.type].render.call(this._fill,t,!0)),this._flagFill&&(e.fill=this._fill&&this._fill.id?"url(#"+this._fill.id+")":this._fill),this._stroke&&this._stroke._renderer&&(this._stroke._update(),s[this._stroke._renderer.type].render.call(this._stroke,t,!0)),this._flagStroke&&(e.stroke=this._stroke&&this._stroke.id?"url(#"+this._stroke.id+")":this._stroke),this._flagLinewidth&&(e["stroke-width"]=this._linewidth),this._flagOpacity&&(e.opacity=this._opacity),this._flagClassName&&(e.class=this.classList.join(" ")),this._flagVisible&&(e.visibility=this._visible?"visible":"hidden"),this.dashes&&this.dashes.length>0&&(e["stroke-dasharray"]=this.dashes.join(" "),e["stroke-dashoffset"]=this.dashes.offset||0),this._renderer.elem?s.setAttributes(this._renderer.elem,e):(e.id=this.id,this._renderer.elem=s.createElement("text",e),t.defs.appendChild(this._renderer.elem)),this._flagClip){var i=s.getClip(this),r=this._renderer.elem;this._clip?(r.removeAttribute("id"),i.setAttribute("id",this.id),i.appendChild(r)):(i.removeAttribute("id"),r.setAttribute("id",this.id),this.parent._renderer.elem.appendChild(r));}return this._flagValue&&(this._renderer.elem.textContent=this._value),this.flagReset()}},"linear-gradient":{render:function(t,e){e||this._update();var i={};if(this._flagEndPoints&&(i.x1=this.left._x,i.y1=this.left._y,i.x2=this.right._x,i.y2=this.right._y),this._flagSpread&&(i.spreadMethod=this._spread),this._renderer.elem?s.setAttributes(this._renderer.elem,i):(i.id=this.id,i.gradientUnits="userSpaceOnUse",this._renderer.elem=s.createElement("linearGradient",i),t.defs.appendChild(this._renderer.elem)),this._flagStops){var r=this._renderer.elem.childNodes.length!==this.stops.length;if(r)for(;this._renderer.elem.lastChild;)this._renderer.elem.removeChild(this._renderer.elem.lastChild);for(var n=0;n<this.stops.length;n++){var a=this.stops[n],o={};a._flagOffset&&(o.offset=100*a._offset+"%"),a._flagColor&&(o["stop-color"]=a._color),a._flagOpacity&&(o["stop-opacity"]=a._opacity),a._renderer.elem?s.setAttributes(a._renderer.elem,o):a._renderer.elem=s.createElement("stop",o),r&&this._renderer.elem.appendChild(a._renderer.elem),a.flagReset();}}return this.flagReset()}},"radial-gradient":{render:function(t,e){e||this._update();var i={};if(this._flagCenter&&(i.cx=this.center._x,i.cy=this.center._y),this._flagFocal&&(i.fx=this.focal._x,i.fy=this.focal._y),this._flagRadius&&(i.r=this._radius),this._flagSpread&&(i.spreadMethod=this._spread),this._renderer.elem?s.setAttributes(this._renderer.elem,i):(i.id=this.id,i.gradientUnits="userSpaceOnUse",this._renderer.elem=s.createElement("radialGradient",i),t.defs.appendChild(this._renderer.elem)),this._flagStops){var r=this._renderer.elem.childNodes.length!==this.stops.length;if(r)for(;this._renderer.elem.lastChild;)this._renderer.elem.removeChild(this._renderer.elem.lastChild);for(var n=0;n<this.stops.length;n++){var a=this.stops[n],o={};a._flagOffset&&(o.offset=100*a._offset+"%"),a._flagColor&&(o["stop-color"]=a._color),a._flagOpacity&&(o["stop-opacity"]=a._opacity),a._renderer.elem?s.setAttributes(a._renderer.elem,o):a._renderer.elem=s.createElement("stop",o),r&&this._renderer.elem.appendChild(a._renderer.elem),a.flagReset();}}return this.flagReset()}},texture:{render:function(e,i){i||this._update();var n={},a={x:0,y:0},o=this.image;if(this._flagLoaded&&this.loaded)switch(o.nodeName.toLowerCase()){case"canvas":a.href=a["xlink:href"]=o.toDataURL("image/png");break;case"img":case"image":a.href=a["xlink:href"]=this.src;}if((this._flagOffset||this._flagLoaded||this._flagScale)&&(n.x=this._offset.x,n.y=this._offset.y,o&&(n.x-=o.width/2,n.y-=o.height/2,this._scale instanceof t.Vector?(n.x*=this._scale.x,n.y*=this._scale.y):(n.x*=this._scale,n.y*=this._scale)),n.x>0&&(n.x*=-1),n.y>0&&(n.y*=-1)),(this._flagScale||this._flagLoaded||this._flagRepeat)&&(n.width=0,n.height=0,o)){switch(a.width=n.width=o.width,a.height=n.height=o.height,this._repeat){case"no-repeat":n.width+=1,n.height+=1;}this._scale instanceof t.Vector?(n.width*=this._scale.x,n.height*=this._scale.y):(n.width*=this._scale,n.height*=this._scale);}return (this._flagScale||this._flagLoaded)&&(this._renderer.image?r.isEmpty(a)||s.setAttributes(this._renderer.image,a):this._renderer.image=s.createElement("image",a)),this._renderer.elem?r.isEmpty(n)||s.setAttributes(this._renderer.elem,n):(n.id=this.id,n.patternUnits="userSpaceOnUse",this._renderer.elem=s.createElement("pattern",n),e.defs.appendChild(this._renderer.elem)),this._renderer.elem&&this._renderer.image&&!this._renderer.appended&&(this._renderer.elem.appendChild(this._renderer.image),this._renderer.appended=!0),this.flagReset()}}},n=t[t.Types.svg]=function(e){this.domElement=e.domElement||s.createElement("svg"),this.scene=new t.Group,this.scene.parent=this,this.defs=s.createElement("defs"),this.domElement.appendChild(this.defs),this.domElement.defs=this.defs,this.domElement.style.overflow="hidden";};r.extend(n,{Utils:s}),r.extend(n.prototype,t.Utils.Events,{constructor:n,setSize:function(e,i){return this.width=e,this.height=i,s.setAttributes(this.domElement,{width:e,height:i}),this.trigger(t.Events.resize,e,i)},render:function(){return s.group.render.call(this.scene,this.domElement),this}});}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Utils.mod,i=t.Utils.getRatio,r=t.Utils,s=[],n=2*Math.PI,a=Math.max,o=Math.min,l=Math.abs,h=Math.sin,c=Math.cos,d=Math.acos,f=Math.sqrt,u=function(t){return 1==t[0]&&0==t[3]&&0==t[1]&&1==t[4]&&0==t[2]&&0==t[5]},_={isHidden:/(undefined|none|transparent)/i,alignments:{left:"start",middle:"center",right:"end"},shim:function(t,e){return t.tagName=t.nodeName=e||"canvas",t.nodeType=1,t.getAttribute=function(t){return this[t]},t.setAttribute=function(t,e){return this[t]=e,this},t},group:{renderChild:function(t){_[t._renderer.type].render.call(t,this.ctx,!0,this.clip);},render:function(t){this._update();var e=this._matrix.elements,i=this.parent;this._renderer.opacity=this._opacity*(i&&i._renderer?i._renderer.opacity:1);var r=this._mask,s=u(e),n=!s||!!r;if(this._renderer.context||(this._renderer.context={}),this._renderer.context.ctx=t,n&&(t.save(),s||t.transform(e[0],e[3],e[1],e[4],e[2],e[5])),r&&_[r._renderer.type].render.call(r,t,!0),this.opacity>0&&0!==this.scale)for(var a=0;a<this.children.length;a++){var o=this.children[a];_[o._renderer.type].render.call(o,t);}return n&&t.restore(),this.flagReset()}},path:{render:function(i,n,o){var l,h,c,d,f,g,p,m,v,y,x,b,w,k,S,A,E,C,R,F,M,O,P,U,T,N,L,j,V,I,G,B,z;if(this._update(),l=this._matrix.elements,h=this._stroke,c=this._linewidth,d=this._fill,f=this._opacity*this.parent._renderer.opacity,g=this._visible,p=this._cap,m=this._join,v=this._miter,y=this._closed,w=(b=(x=this._renderer.vertices).length)-1,G=u(l),z=this.dashes,I=this._clip,!n&&(!g||I))return this;G||(i.save(),i.transform(l[0],l[3],l[1],l[4],l[2],l[5])),d&&(r.isString(d)?i.fillStyle=d:(_[d._renderer.type].render.call(d,i),i.fillStyle=d._renderer.effect)),h&&(r.isString(h)?i.strokeStyle=h:(_[h._renderer.type].render.call(h,i),i.strokeStyle=h._renderer.effect),c&&(i.lineWidth=c),v&&(i.miterLimit=v),m&&(i.lineJoin=m),!y&&p&&(i.lineCap=p)),r.isNumber(f)&&(i.globalAlpha=f),z&&z.length>0&&(i.lineDashOffset=z.offset||0,i.setLineDash(z)),i.beginPath();for(var D=0;D<x.length;D++)switch(j=(E=x[D]).x,V=E.y,E.command){case t.Commands.close:i.closePath();break;case t.Commands.arc:var H=E.rx,q=E.ry,W=E.xAxisRotation,X=E.largeArcFlag,Y=E.sweepFlag,K=(A=x[S=y?e(D-1,b):a(D-1,0)]).x,J=A.y;_.renderSvgArcCommand(i,K,J,H,q,X,Y,W,j,V);break;case t.Commands.curve:S=y?e(D-1,b):Math.max(D-1,0),k=y?e(D+1,b):Math.min(D+1,w),A=x[S],C=x[k],U=A.controls&&A.controls.right||t.Vector.zero,T=E.controls&&E.controls.left||t.Vector.zero,A._relative?(O=U.x+A.x,P=U.y+A.y):(O=U.x,P=U.y),E._relative?(F=T.x+E.x,M=T.y+E.y):(F=T.x,M=T.y),i.bezierCurveTo(O,P,F,M,j,V),D>=w&&y&&(C=R,N=E.controls&&E.controls.right||t.Vector.zero,L=C.controls&&C.controls.left||t.Vector.zero,E._relative?(O=N.x+E.x,P=N.y+E.y):(O=N.x,P=N.y),C._relative?(F=L.x+C.x,M=L.y+C.y):(F=L.x,M=L.y),j=C.x,V=C.y,i.bezierCurveTo(O,P,F,M,j,V));break;case t.Commands.line:i.lineTo(j,V);break;case t.Commands.move:R=E,i.moveTo(j,V);}return y&&i.closePath(),I||o||(_.isHidden.test(d)||((B=d._renderer&&d._renderer.offset)&&(i.save(),i.translate(-d._renderer.offset.x,-d._renderer.offset.y),i.scale(d._renderer.scale.x,d._renderer.scale.y)),i.fill(),B&&i.restore()),_.isHidden.test(h)||((B=h._renderer&&h._renderer.offset)&&(i.save(),i.translate(-h._renderer.offset.x,-h._renderer.offset.y),i.scale(h._renderer.scale.x,h._renderer.scale.y),i.lineWidth=c/h._renderer.scale.x),i.stroke(),B&&i.restore())),G||i.restore(),I&&!o&&i.clip(),z&&z.length>0&&i.setLineDash(s),this.flagReset()}},text:{render:function(t,e,i){this._update();var n,a,o,l,h,c,d,f=this._matrix.elements,g=this._stroke,p=this._linewidth,m=this._fill,v=this._opacity*this.parent._renderer.opacity,y=this._visible,x=u(f),b=m._renderer&&m._renderer.offset&&g._renderer&&g._renderer.offset,w=this.dashes,k=this._clip;return e||y&&!k?(x||(t.save(),t.transform(f[0],f[3],f[1],f[4],f[2],f[5])),b||(t.font=[this._style,this._weight,this._size+"px/"+this._leading+"px",this._family].join(" ")),t.textAlign=_.alignments[this._alignment]||this._alignment,t.textBaseline=this._baseline,m&&(r.isString(m)?t.fillStyle=m:(_[m._renderer.type].render.call(m,t),t.fillStyle=m._renderer.effect)),g&&(r.isString(g)?t.strokeStyle=g:(_[g._renderer.type].render.call(g,t),t.strokeStyle=g._renderer.effect),p&&(t.lineWidth=p)),r.isNumber(v)&&(t.globalAlpha=v),w&&w.length>0&&(t.lineDashOffset=w.offset||0,t.setLineDash(w)),k||i||(_.isHidden.test(m)||(m._renderer&&m._renderer.offset?(c=m._renderer.scale.x,d=m._renderer.scale.y,t.save(),t.translate(-m._renderer.offset.x,-m._renderer.offset.y),t.scale(c,d),n=this._size/m._renderer.scale.y,a=this._leading/m._renderer.scale.y,t.font=[this._style,this._weight,n+"px/",a+"px",this._family].join(" "),o=m._renderer.offset.x/m._renderer.scale.x,l=m._renderer.offset.y/m._renderer.scale.y,t.fillText(this.value,o,l),t.restore()):t.fillText(this.value,0,0)),_.isHidden.test(g)||(g._renderer&&g._renderer.offset?(c=g._renderer.scale.x,d=g._renderer.scale.y,t.save(),t.translate(-g._renderer.offset.x,-g._renderer.offset.y),t.scale(c,d),n=this._size/g._renderer.scale.y,a=this._leading/g._renderer.scale.y,t.font=[this._style,this._weight,n+"px/",a+"px",this._family].join(" "),o=g._renderer.offset.x/g._renderer.scale.x,l=g._renderer.offset.y/g._renderer.scale.y,h=p/g._renderer.scale.x,t.lineWidth=h,t.strokeText(this.value,o,l),t.restore()):t.strokeText(this.value,0,0))),x||t.restore(),k&&!i&&t.clip(),w&&w.length>0&&t.setLineDash(s),this.flagReset()):this}},"linear-gradient":{render:function(t){if(this._update(),!this._renderer.effect||this._flagEndPoints||this._flagStops){this._renderer.effect=t.createLinearGradient(this.left._x,this.left._y,this.right._x,this.right._y);for(var e=0;e<this.stops.length;e++){var i=this.stops[e];this._renderer.effect.addColorStop(i._offset,i._color);}}return this.flagReset()}},"radial-gradient":{render:function(t){if(this._update(),!this._renderer.effect||this._flagCenter||this._flagFocal||this._flagRadius||this._flagStops){this._renderer.effect=t.createRadialGradient(this.center._x,this.center._y,0,this.focal._x,this.focal._y,this._radius);for(var e=0;e<this.stops.length;e++){var i=this.stops[e];this._renderer.effect.addColorStop(i._offset,i._color);}}return this.flagReset()}},texture:{render:function(e){this._update();var i=this.image;return (!this._renderer.effect||(this._flagLoaded||this._flagImage||this._flagVideo||this._flagRepeat)&&this.loaded)&&(this._renderer.effect=e.createPattern(this.image,this._repeat)),(this._flagOffset||this._flagLoaded||this._flagScale)&&(this._renderer.offset instanceof t.Vector||(this._renderer.offset=new t.Vector),this._renderer.offset.x=-this._offset.x,this._renderer.offset.y=-this._offset.y,i&&(this._renderer.offset.x+=i.width/2,this._renderer.offset.y+=i.height/2,this._scale instanceof t.Vector?(this._renderer.offset.x*=this._scale.x,this._renderer.offset.y*=this._scale.y):(this._renderer.offset.x*=this._scale,this._renderer.offset.y*=this._scale))),(this._flagScale||this._flagLoaded)&&(this._renderer.scale instanceof t.Vector||(this._renderer.scale=new t.Vector),this._scale instanceof t.Vector?this._renderer.scale.copy(this._scale):this._renderer.scale.set(this._scale,this._scale)),this.flagReset()}},renderSvgArcCommand:function(i,r,s,o,d,u,_,g,m,v){g=g*Math.PI/180,o=l(o),d=l(d);var y=(r-m)/2,x=(s-v)/2,b=c(g)*y+h(g)*x,w=-h(g)*y+c(g)*x,k=o*o,S=d*d,A=b*b,E=w*w,C=A/k+E/S;if(C>1){var R=f(C);k=(o*=R)*o,S=(d*=R)*d;}var F=k*E+S*A,M=f(a(0,(k*S-F)/F));u===_&&(M=-M);var O=M*o*w/d,P=-M*d*b/o,U=c(g)*O-h(g)*P+(r+m)/2,T=h(g)*O+c(g)*P+(s+v)/2,N=p(1,0,(b-O)/o,(w-P)/d);!function(i,r,s,a,o,l,h,c,d){var f=t.Utils.Curve.Tolerance.epsilon,u=h-l,_=Math.abs(u)<f;(u=e(u,n))<f&&(u=_?0:n);!0!==c||_||(u===n?u=-n:u-=n);for(var g=0;g<t.Resolution;g++){var p=g/(t.Resolution-1),m=l+p*u,v=r+a*Math.cos(m),y=s+o*Math.sin(m);if(0!==d){var x=Math.cos(d),b=Math.sin(d),w=v-r,k=y-s;v=w*x-k*b+r,y=w*b+k*x+s;}i.lineTo(v,y);}}(i,U,T,o,d,N,N+p((b-O)/o,(w-P)/d,(-b-O)/o,(-w-P)/d)%n,0===_,g);}},g=t[t.Types.canvas]=function(e){var i=!1!==e.smoothing;this.domElement=e.domElement||document.createElement("canvas"),this.ctx=this.domElement.getContext("2d"),this.overdraw=e.overdraw||!1,r.isUndefined(this.ctx.imageSmoothingEnabled)||(this.ctx.imageSmoothingEnabled=i),this.scene=new t.Group,this.scene.parent=this;};function p(t,e,i,r){var s=t*i+e*r,n=f(t*t+e*e)*f(i*i+r*r),l=d(a(-1,o(1,s/n)));return t*r-e*i<0&&(l=-l),l}r.extend(g,{Utils:_}),r.extend(g.prototype,t.Utils.Events,{constructor:g,setSize:function(e,s,n){return this.width=e,this.height=s,this.ratio=r.isUndefined(n)?i(this.ctx):n,this.domElement.width=e*this.ratio,this.domElement.height=s*this.ratio,this.domElement.style&&r.extend(this.domElement.style,{width:e+"px",height:s+"px"}),this.trigger(t.Events.resize,e,s,n)},render:function(){var t=1===this.ratio;return t||(this.ctx.save(),this.ctx.scale(this.ratio,this.ratio)),this.overdraw||this.ctx.clearRect(0,0,this.width,this.height),_.group.render.call(this.scene,this.ctx),t||this.ctx.restore(),this}});}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.root,i=t.Matrix.Multiply,r=t.Utils.mod,s=[1,0,0,0,1,0,0,0,1],n=new t.Array(9),a=t.Utils.getRatio,o=(t.Utils.getComputedMatrix,t[t.Types.canvas].Utils),l=t.Utils,h={isHidden:/(undefined|none|transparent)/i,canvas:e.document?e.document.createElement("canvas"):{getContext:l.identity},alignments:{left:"start",middle:"center",right:"end"},matrix:new t.Matrix,group:{removeChild:function(t,e){if(t.children)for(var i=0;i<t.children.length;i++)h.group.removeChild(t.children[i],e);else e.deleteTexture(t._renderer.texture),delete t._renderer.texture;},render:function(e,r){this._update();var s,a=this.parent,o=a._matrix&&a._matrix.manual||a._flagMatrix,l=this._matrix.manual||this._flagMatrix;if((o||l)&&(this._renderer.matrix||(this._renderer.matrix=new t.Array(9)),this._matrix.toTransformArray(!0,n),i(n,a._renderer.matrix,this._renderer.matrix),this._renderer.scale instanceof t.Vector||(this._renderer.scale=new t.Vector),this._scale instanceof t.Vector?(this._renderer.scale.x=this._scale.x,this._renderer.scale.y=this._scale.y):(this._renderer.scale.x=this._scale,this._renderer.scale.y=this._scale),/renderer/i.test(a._renderer.type)||(this._renderer.scale.x*=a._renderer.scale.x,this._renderer.scale.y*=a._renderer.scale.y),o&&(this._flagMatrix=!0)),this._mask&&(e.clear(e.STENCIL_BUFFER_BIT),e.enable(e.STENCIL_TEST),e.stencilFunc(e.ALWAYS,1,0),e.stencilOp(e.KEEP,e.KEEP,e.REPLACE),h[this._mask._renderer.type].render.call(this._mask,e,r,this),e.stencilFunc(e.EQUAL,1,255),e.stencilOp(e.KEEP,e.KEEP,e.KEEP)),this._flagOpacity=a._flagOpacity||this._flagOpacity,this._renderer.opacity=this._opacity*(a&&a._renderer?a._renderer.opacity:1),this._flagSubtractions)for(s=0;s<this.subtractions.length;s++)h.group.removeChild(this.subtractions[s],e);for(s=0;s<this.children.length;s++){var c=this.children[s];h[c._renderer.type].render.call(c,e,r);}return this._mask&&e.disable(e.STENCIL_TEST),this.flagReset()}},path:{updateCanvas:function(e){var i,s,n,a,c,d,f,u,_,g,p,m,v,y,x,b=e._renderer.vertices,w=this.canvas,k=this.ctx,S=e._renderer.scale,A=e._stroke,E=e._linewidth,C=e._fill,R=e._renderer.opacity||e._opacity,F=e._cap,M=e._join,O=e._miter,P=e._closed,U=e.dashes,T=b.length,N=T-1;w.width=Math.max(Math.ceil(e._renderer.rect.width*S.x),1),w.height=Math.max(Math.ceil(e._renderer.rect.height*S.y),1);var L,j=e._renderer.rect.centroid,V=j.x,I=j.y;k.clearRect(0,0,w.width,w.height),C&&(l.isString(C)?k.fillStyle=C:(h[C._renderer.type].render.call(C,k,e),k.fillStyle=C._renderer.effect)),A&&(l.isString(A)?k.strokeStyle=A:(h[A._renderer.type].render.call(A,k,e),k.strokeStyle=A._renderer.effect),E&&(k.lineWidth=E),O&&(k.miterLimit=O),M&&(k.lineJoin=M),!P&&F&&(k.lineCap=F)),l.isNumber(R)&&(k.globalAlpha=R),U&&U.length>0&&(k.lineDashOffset=U.offset||0,k.setLineDash(U)),k.save(),k.scale(S.x,S.y),k.translate(V,I),k.beginPath();for(var G=0;G<b.length;G++){var B=b[G];switch(v=B.x,y=B.y,B.command){case t.Commands.close:k.closePath();break;case t.Commands.arc:var z=B.rx,D=B.ry,H=B.xAxisRotation,q=B.largeArcFlag,W=B.sweepFlag,X=(n=b[s=P?r(G-1,T):Math.max(G-1,0)]).x,Y=n.y;o.renderSvgArcCommand(k,X,Y,z,D,q,W,H,v,y);break;case t.Commands.curve:s=P?r(G-1,T):Math.max(G-1,0),i=P?r(G+1,T):Math.min(G+1,N),n=b[s],a=b[i],_=n.controls&&n.controls.right||t.Vector.zero,g=B.controls&&B.controls.left||t.Vector.zero,n._relative?(f=_.x+n.x,u=_.y+n.y):(f=_.x,u=_.y),B._relative?(c=g.x+B.x,d=g.y+B.y):(c=g.x,d=g.y),k.bezierCurveTo(f,u,c,d,v,y),G>=N&&P&&(a=L,p=B.controls&&B.controls.right||t.Vector.zero,m=a.controls&&a.controls.left||t.Vector.zero,B._relative?(f=p.x+B.x,u=p.y+B.y):(f=p.x,u=p.y),a._relative?(c=m.x+a.x,d=m.y+a.y):(c=m.x,d=m.y),v=a.x,y=a.y,k.bezierCurveTo(f,u,c,d,v,y));break;case t.Commands.line:k.lineTo(v,y);break;case t.Commands.move:L=B,k.moveTo(v,y);}}P&&k.closePath(),h.isHidden.test(C)||((x=C._renderer&&C._renderer.offset)&&(k.save(),k.translate(-C._renderer.offset.x,-C._renderer.offset.y),k.scale(C._renderer.scale.x,C._renderer.scale.y)),k.fill(),x&&k.restore()),h.isHidden.test(A)||((x=A._renderer&&A._renderer.offset)&&(k.save(),k.translate(-A._renderer.offset.x,-A._renderer.offset.y),k.scale(A._renderer.scale.x,A._renderer.scale.y),k.lineWidth=E/A._renderer.scale.x),k.stroke(),x&&k.restore()),k.restore();},getBoundingClientRect:function(t,e,i){var r,s,n=1/0,a=-1/0,o=1/0,h=-1/0;t.forEach((function(t){var e,i,r,s,l,c,d=t.x,f=t.y,u=t.controls;o=Math.min(f,o),n=Math.min(d,n),a=Math.max(d,a),h=Math.max(f,h),t.controls&&(l=u.left,c=u.right,l&&c&&(e=t._relative?l.x+d:l.x,i=t._relative?l.y+f:l.y,r=t._relative?c.x+d:c.x,s=t._relative?c.y+f:c.y,e&&i&&r&&s&&(o=Math.min(i,s,o),n=Math.min(e,r,n),a=Math.max(e,r,a),h=Math.max(i,s,h))));})),l.isNumber(e)&&(o-=e,n-=e,a+=e,h+=e),r=a-n,s=h-o,i.top=o,i.left=n,i.right=a,i.bottom=h,i.width=r,i.height=s,i.centroid||(i.centroid={}),i.centroid.x=-n,i.centroid.y=-o;},render:function(e,r,s){if(!this._visible||!this._opacity)return this;this._update();var a=this.parent,o=a._matrix.manual||a._flagMatrix,l=this._matrix.manual||this._flagMatrix,c=this._flagVertices||this._flagFill||this._fill instanceof t.LinearGradient&&(this._fill._flagSpread||this._fill._flagStops||this._fill._flagEndPoints)||this._fill instanceof t.RadialGradient&&(this._fill._flagSpread||this._fill._flagStops||this._fill._flagRadius||this._fill._flagCenter||this._fill._flagFocal)||this._fill instanceof t.Texture&&(this._fill._flagLoaded&&this._fill.loaded||this._fill._flagImage||this._fill._flagVideo||this._fill._flagRepeat||this._fill._flagOffset||this._fill._flagScale)||this._stroke instanceof t.LinearGradient&&(this._stroke._flagSpread||this._stroke._flagStops||this._stroke._flagEndPoints)||this._stroke instanceof t.RadialGradient&&(this._stroke._flagSpread||this._stroke._flagStops||this._stroke._flagRadius||this._stroke._flagCenter||this._stroke._flagFocal)||this._stroke instanceof t.Texture&&(this._stroke._flagLoaded&&this._stroke.loaded||this._stroke._flagImage||this._stroke._flagVideo||this._stroke._flagRepeat||this._stroke._flagOffset||this._fill._flagScale)||this._flagStroke||this._flagLinewidth||this._flagOpacity||a._flagOpacity||this._flagVisible||this._flagCap||this._flagJoin||this._flagMiter||this._flagScale||this.dashes&&this.dashes.length>0||!this._renderer.texture;if((o||l)&&(this._renderer.matrix||(this._renderer.matrix=new t.Array(9)),this._matrix.toTransformArray(!0,n),i(n,a._renderer.matrix,this._renderer.matrix),this._renderer.scale instanceof t.Vector||(this._renderer.scale=new t.Vector),this._scale instanceof t.Vector?(this._renderer.scale.x=this._scale.x*a._renderer.scale.x,this._renderer.scale.y=this._scale.y*a._renderer.scale.y):(this._renderer.scale.x=this._scale*a._renderer.scale.x,this._renderer.scale.y=this._scale*a._renderer.scale.y)),c?(this._renderer.rect||(this._renderer.rect={}),this._renderer.opacity=this._opacity*a._renderer.opacity,h.path.getBoundingClientRect(this._renderer.vertices,this._linewidth,this._renderer.rect),h.updateTexture.call(h,e,this)):(this._fill&&this._fill._update&&this._fill._update(),this._stroke&&this._stroke._update&&this._stroke._update()),!this._clip||s){e.bindTexture(e.TEXTURE_2D,this._renderer.texture);var d=this._renderer.rect;return e.uniformMatrix3fv(r.matrix,!1,this._renderer.matrix),e.uniform4f(r.rect,d.left,d.top,d.right,d.bottom),e.drawArrays(e.TRIANGLES,0,6),this.flagReset()}}},text:{updateCanvas:function(t){var e=this.canvas,i=this.ctx,r=t._renderer.scale,s=t._stroke,n=t._linewidth*r,a=t._fill,o=t._renderer.opacity||t._opacity,c=t.dashes;e.width=Math.max(Math.ceil(t._renderer.rect.width*r.x),1),e.height=Math.max(Math.ceil(t._renderer.rect.height*r.y),1);var d,f,u,_,g,p,m,v=t._renderer.rect.centroid,y=v.x,x=v.y,b=a._renderer&&a._renderer.offset&&s._renderer&&s._renderer.offset;i.clearRect(0,0,e.width,e.height),b||(i.font=[t._style,t._weight,t._size+"px/"+t._leading+"px",t._family].join(" ")),i.textAlign="center",i.textBaseline="middle",a&&(l.isString(a)?i.fillStyle=a:(h[a._renderer.type].render.call(a,i,t),i.fillStyle=a._renderer.effect)),s&&(l.isString(s)?i.strokeStyle=s:(h[s._renderer.type].render.call(s,i,t),i.strokeStyle=s._renderer.effect),n&&(i.lineWidth=n)),l.isNumber(o)&&(i.globalAlpha=o),c&&c.length>0&&(i.lineDashOffset=c.offset||0,i.setLineDash(c)),i.save(),i.scale(r.x,r.y),i.translate(y,x),h.isHidden.test(a)||(a._renderer&&a._renderer.offset?(p=a._renderer.scale.x,m=a._renderer.scale.y,i.save(),i.translate(-a._renderer.offset.x,-a._renderer.offset.y),i.scale(p,m),d=t._size/a._renderer.scale.y,f=t._leading/a._renderer.scale.y,i.font=[t._style,t._weight,d+"px/",f+"px",t._family].join(" "),u=a._renderer.offset.x/a._renderer.scale.x,_=a._renderer.offset.y/a._renderer.scale.y,i.fillText(t.value,u,_),i.restore()):i.fillText(t.value,0,0)),h.isHidden.test(s)||(s._renderer&&s._renderer.offset?(p=s._renderer.scale.x,m=s._renderer.scale.y,i.save(),i.translate(-s._renderer.offset.x,-s._renderer.offset.y),i.scale(p,m),d=t._size/s._renderer.scale.y,f=t._leading/s._renderer.scale.y,i.font=[t._style,t._weight,d+"px/",f+"px",t._family].join(" "),u=s._renderer.offset.x/s._renderer.scale.x,_=s._renderer.offset.y/s._renderer.scale.y,g=n/s._renderer.scale.x,i.lineWidth=g,i.strokeText(t.value,u,_),i.restore()):i.strokeText(t.value,0,0)),i.restore();},getBoundingClientRect:function(t,e){var i=h.ctx;i.font=[t._style,t._weight,t._size+"px/"+t._leading+"px",t._family].join(" "),i.textAlign="center",i.textBaseline=t._baseline;var r=1.25*i.measureText(t._value).width,s=1.25*Math.max(t._size,t._leading);this._linewidth&&!h.isHidden.test(this._stroke)&&(r+=2*this._linewidth,s+=2*this._linewidth);var n=r/2,a=s/2;switch(h.alignments[t._alignment]||t._alignment){case h.alignments.left:e.left=0,e.right=r;break;case h.alignments.right:e.left=-r,e.right=0;break;default:e.left=-n,e.right=n;}switch(t._baseline){case"bottom":e.top=-s,e.bottom=0;break;case"top":e.top=0,e.bottom=s;break;default:e.top=-a,e.bottom=a;}e.width=r,e.height=s,e.centroid||(e.centroid={}),e.centroid.x=n,e.centroid.y=a;},render:function(e,r,s){if(!this._visible||!this._opacity)return this;this._update();var a=this.parent,o=a._matrix.manual||a._flagMatrix,l=this._matrix.manual||this._flagMatrix,c=this._flagVertices||this._flagFill||this._fill instanceof t.LinearGradient&&(this._fill._flagSpread||this._fill._flagStops||this._fill._flagEndPoints)||this._fill instanceof t.RadialGradient&&(this._fill._flagSpread||this._fill._flagStops||this._fill._flagRadius||this._fill._flagCenter||this._fill._flagFocal)||this._fill instanceof t.Texture&&(this._fill._flagLoaded&&this._fill.loaded||this._fill._flagImage||this._fill._flagVideo||this._fill._flagRepeat||this._fill._flagOffset||this._fill._flagScale)||this._stroke instanceof t.LinearGradient&&(this._stroke._flagSpread||this._stroke._flagStops||this._stroke._flagEndPoints)||this._stroke instanceof t.RadialGradient&&(this._stroke._flagSpread||this._stroke._flagStops||this._stroke._flagRadius||this._stroke._flagCenter||this._stroke._flagFocal)||this._stroke instanceof t.Texture&&(this._stroke._flagLoaded&&this._stroke.loaded||this._stroke._flagImage||this._stroke._flagVideo||this._stroke._flagRepeat||this._stroke._flagOffset||this._fill._flagScale)||this._flagStroke||this._flagLinewidth||this._flagOpacity||a._flagOpacity||this._flagVisible||this._flagScale||this._flagValue||this._flagFamily||this._flagSize||this._flagLeading||this._flagAlignment||this._flagBaseline||this._flagStyle||this._flagWeight||this._flagDecoration||this.dashes&&this.dashes.length>0||!this._renderer.texture;if((o||l)&&(this._renderer.matrix||(this._renderer.matrix=new t.Array(9)),this._matrix.toTransformArray(!0,n),i(n,a._renderer.matrix,this._renderer.matrix),this._renderer.scale instanceof t.Vector||(this._renderer.scale=new t.Vector),this._scale instanceof t.Vector?(this._renderer.scale.x=this._scale.x*a._renderer.scale.x,this._renderer.scale.y=this._scale.y*a._renderer.scale.y):(this._renderer.scale.x=this._scale*a._renderer.scale.x,this._renderer.scale.y=this._scale*a._renderer.scale.y)),c?(this._renderer.rect||(this._renderer.rect={}),this._renderer.opacity=this._opacity*a._renderer.opacity,h.text.getBoundingClientRect(this,this._renderer.rect),h.updateTexture.call(h,e,this)):(this._fill&&this._fill._update&&this._fill._update(),this._stroke&&this._stroke._update&&this._stroke._update()),!this._clip||s){e.bindTexture(e.TEXTURE_2D,this._renderer.texture);var d=this._renderer.rect;return e.uniformMatrix3fv(r.matrix,!1,this._renderer.matrix),e.uniform4f(r.rect,d.left,d.top,d.right,d.bottom),e.drawArrays(e.TRIANGLES,0,6),this.flagReset()}}},"linear-gradient":{render:function(t,e){if(t.canvas.getContext("2d")){if(this._update(),!this._renderer.effect||this._flagEndPoints||this._flagStops){this._renderer.effect=t.createLinearGradient(this.left._x,this.left._y,this.right._x,this.right._y);for(var i=0;i<this.stops.length;i++){var r=this.stops[i];this._renderer.effect.addColorStop(r._offset,r._color);}}return this.flagReset()}}},"radial-gradient":{render:function(t,e){if(t.canvas.getContext("2d")){if(this._update(),!this._renderer.effect||this._flagCenter||this._flagFocal||this._flagRadius||this._flagStops){this._renderer.effect=t.createRadialGradient(this.center._x,this.center._y,0,this.focal._x,this.focal._y,this._radius);for(var i=0;i<this.stops.length;i++){var r=this.stops[i];this._renderer.effect.addColorStop(r._offset,r._color);}}return this.flagReset()}}},texture:{render:function(e,i){if(e.canvas.getContext("2d")){this._update();var r=this.image;if((this._flagLoaded||this._flagImage||this._flagVideo||this._flagRepeat)&&this.loaded)this._renderer.effect=e.createPattern(r,this._repeat);else if(!this._renderer.effect)return this.flagReset();return (this._flagOffset||this._flagLoaded||this._flagScale)&&(this._renderer.offset instanceof t.Vector||(this._renderer.offset=new t.Vector),this._renderer.offset.x=-this._offset.x,this._renderer.offset.y=-this._offset.y,r&&(this._renderer.offset.x+=r.width/2,this._renderer.offset.y+=r.height/2,this._scale instanceof t.Vector?(this._renderer.offset.x*=this._scale.x,this._renderer.offset.y*=this._scale.y):(this._renderer.offset.x*=this._scale,this._renderer.offset.y*=this._scale))),(this._flagScale||this._flagLoaded)&&(this._renderer.scale instanceof t.Vector||(this._renderer.scale=new t.Vector),this._scale instanceof t.Vector?this._renderer.scale.copy(this._scale):this._renderer.scale.set(this._scale,this._scale)),this.flagReset()}}},updateTexture:function(t,e){this[e._renderer.type].updateCanvas.call(h,e),e._renderer.texture||(e._renderer.texture=t.createTexture()),t.bindTexture(t.TEXTURE_2D,e._renderer.texture),t.texParameteri(t.TEXTURE_2D,t.TEXTURE_WRAP_S,t.CLAMP_TO_EDGE),t.texParameteri(t.TEXTURE_2D,t.TEXTURE_WRAP_T,t.CLAMP_TO_EDGE),t.texParameteri(t.TEXTURE_2D,t.TEXTURE_MIN_FILTER,t.LINEAR),this.canvas.width<=0||this.canvas.height<=0||t.texImage2D(t.TEXTURE_2D,0,t.RGBA,t.RGBA,t.UNSIGNED_BYTE,this.canvas);},program:{create:function(e,i){var r,s;if(r=e.createProgram(),l.each(i,(function(t){e.attachShader(r,t);})),e.linkProgram(r),!e.getProgramParameter(r,e.LINK_STATUS))throw s=e.getProgramInfoLog(r),e.deleteProgram(r),new t.Utils.Error("unable to link program: "+s);return r}},shaders:{create:function(e,i,r){var s,n;if(s=e.createShader(e[r]),e.shaderSource(s,i),e.compileShader(s),!e.getShaderParameter(s,e.COMPILE_STATUS))throw n=e.getShaderInfoLog(s),e.deleteShader(s),new t.Utils.Error("unable to compile shader "+s+": "+n);return s},types:{vertex:"VERTEX_SHADER",fragment:"FRAGMENT_SHADER"},vertex:["precision mediump float;","attribute vec2 a_position;","","uniform mat3 u_matrix;","uniform vec2 u_resolution;","uniform vec4 u_rect;","","varying vec2 v_textureCoords;","","void main() {","   vec2 rectCoords = (a_position * (u_rect.zw - u_rect.xy)) + u_rect.xy;","   vec2 projected = (u_matrix * vec3(rectCoords, 1.0)).xy;","   vec2 normal = projected / u_resolution;","   vec2 clipspace = (normal * 2.0) - 1.0;","","   gl_Position = vec4(clipspace * vec2(1.0, -1.0), 0.0, 1.0);","   v_textureCoords = a_position;","}"].join("\n"),fragment:["precision mediump float;","","uniform sampler2D u_image;","varying vec2 v_textureCoords;","","void main() {","  vec4 texel = texture2D(u_image, v_textureCoords);","  if (texel.a == 0.0) {","    discard;","  }","  gl_FragColor = texel;","}"].join("\n")},TextureRegistry:new t.Registry};h.ctx=h.canvas.getContext("2d");var c=t[t.Types.webgl]=function(e){var i,r,n;if(this.domElement=e.domElement||document.createElement("canvas"),l.isUndefined(e.offscreenElement)||(h.canvas=e.offscreenElement,h.ctx=h.canvas.getContext("2d")),this.scene=new t.Group,this.scene.parent=this,this._renderer={type:"renderer",matrix:new t.Array(s),scale:1,opacity:1},this._flagMatrix=!0,e=l.defaults(e||{},{antialias:!1,alpha:!0,premultipliedAlpha:!0,stencil:!0,preserveDrawingBuffer:!0,overdraw:!1}),this.overdraw=e.overdraw,i=this.ctx=this.domElement.getContext("webgl",e)||this.domElement.getContext("experimental-webgl",e),!this.ctx)throw new t.Utils.Error("unable to create a webgl context. Try using another renderer.");r=h.shaders.create(i,h.shaders.vertex,h.shaders.types.vertex),n=h.shaders.create(i,h.shaders.fragment,h.shaders.types.fragment),this.program=h.program.create(i,[r,n]),i.useProgram(this.program),this.program.position=i.getAttribLocation(this.program,"a_position"),this.program.matrix=i.getUniformLocation(this.program,"u_matrix"),this.program.rect=i.getUniformLocation(this.program,"u_rect");var a=i.createBuffer();i.bindBuffer(i.ARRAY_BUFFER,a),i.vertexAttribPointer(this.program.position,2,i.FLOAT,!1,0,0),i.enableVertexAttribArray(this.program.position),i.bufferData(i.ARRAY_BUFFER,new t.Array([0,0,1,0,0,1,0,1,1,0,1,1]),i.STATIC_DRAW),i.enable(i.BLEND),i.pixelStorei(i.UNPACK_PREMULTIPLY_ALPHA_WEBGL,!0),i.blendEquation(i.FUNC_ADD),i.blendFunc(i.ONE,i.ONE_MINUS_SRC_ALPHA);};l.extend(c,{Utils:h}),l.extend(c.prototype,t.Utils.Events,{constructor:c,setSize:function(e,i,r){this.width=e,this.height=i,this.ratio=l.isUndefined(r)?a(this.ctx):r,this.domElement.width=e*this.ratio,this.domElement.height=i*this.ratio,l.isObject(this.domElement.style)&&l.extend(this.domElement.style,{width:e+"px",height:i+"px"}),this._renderer.matrix[0]=this._renderer.matrix[4]=this._renderer.scale=this.ratio,this._flagMatrix=!0,this.ctx.viewport(0,0,e*this.ratio,i*this.ratio);var s=this.ctx.getUniformLocation(this.program,"u_resolution");return this.ctx.uniform2f(s,e*this.ratio,i*this.ratio),this.trigger(t.Events.resize,e,i,r)},render:function(){var t=this.ctx;return this.overdraw||t.clear(t.COLOR_BUFFER_BIT),h.group.render.call(this.scene,t,this.program),this._flagMatrix=!1,this}});}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Utils,i=t.Shape=function(){this._renderer={},this._renderer.flagMatrix=e.bind(i.FlagMatrix,this),this.isShape=!0,this.id=t.Identifier+t.uniqueId(),this.classList=[],this.matrix=new t.Matrix,this.translation=new t.Vector,this.rotation=0,this.scale=1;};e.extend(i,{FlagMatrix:function(){this._flagMatrix=!0;},MakeObservable:function(r){var s={enumerable:!1,get:function(){return this._translation},set:function(e){this._translation&&this._translation.unbind(t.Events.change,this._renderer.flagMatrix),this._translation=e,this._translation.bind(t.Events.change,this._renderer.flagMatrix),i.FlagMatrix.call(this);}};Object.defineProperty(r,"translation",s),Object.defineProperty(r,"position",s),Object.defineProperty(r,"rotation",{enumerable:!0,get:function(){return this._rotation},set:function(t){this._rotation=t,this._flagMatrix=!0;}}),Object.defineProperty(r,"scale",{enumerable:!0,get:function(){return this._scale},set:function(e){this._scale instanceof t.Vector&&this._scale.unbind(t.Events.change,this._renderer.flagMatrix),this._scale=e,this._scale instanceof t.Vector&&this._scale.bind(t.Events.change,this._renderer.flagMatrix),this._flagMatrix=!0,this._flagScale=!0;}}),Object.defineProperty(r,"matrix",{enumerable:!0,get:function(){return this._matrix},set:function(t){this._matrix=t,this._flagMatrix=!0;}}),Object.defineProperty(r,"className",{enumerable:!0,get:function(){return this._className},set:function(t){if(this._flagClassName=this._className!==t,this._flagClassName){for(var i=this._className.split(/\s+?/),r=t.split(/\s+?/),s=0;s<i.length;s++){var n=i[s],a=e.indexOf(this.classList,n);a>=0&&this.classList.splice(a,1);}this.classList=this.classList.concat(r);}this._className=t;}});}}),e.extend(i.prototype,t.Utils.Events,{_flagMatrix:!0,_flagScale:!1,_flagClassName:!1,_translation:null,_rotation:0,_scale:1,_className:"",constructor:i,addTo:function(t){return t.add(this),this},clone:function(t){var e=new i;return e.translation.copy(this.translation),e.rotation=this.rotation,e.scale=this.scale,this.matrix.manual&&e.matrix.copy(this.matrix),t&&t.add(e),e._update()},_update:function(e){return !this._matrix.manual&&this._flagMatrix&&(this._matrix.identity().translate(this.translation.x,this.translation.y),this._scale instanceof t.Vector?this._matrix.scale(this._scale.x,this._scale.y):this._matrix.scale(this._scale),this._matrix.rotate(this.rotation)),e&&this.parent&&this.parent._update&&this.parent._update(),this},flagReset:function(){return this._flagMatrix=this._flagScale=this._flagClassName=!1,this}}),i.MakeObservable(i.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=Math.min,i=Math.max,r=(Math.ceil),s=Math.floor,n=t.Utils.getComputedMatrix,o=t.Utils;o.each(t.Commands,(function(t,e){}));var l=t.Path=function(e,i,r,s){t.Shape.call(this),this._renderer.type="path",this._renderer.flagVertices=o.bind(l.FlagVertices,this),this._renderer.bindVertices=o.bind(l.BindVertices,this),this._renderer.unbindVertices=o.bind(l.UnbindVertices,this),this._renderer.flagFill=o.bind(l.FlagFill,this),this._renderer.flagStroke=o.bind(l.FlagStroke,this),this._renderer.vertices=[],this._renderer.collection=[],this._closed=!!i,this._curved=!!r,this.beginning=0,this.ending=1,this.fill="#fff",this.stroke="#000",this.linewidth=1,this.opacity=1,this.className="",this.visible=!0,this.cap="butt",this.join="miter",this.miter=4,this.vertices=e,this.automatic=!s,this.dashes=[],this.dashes.offset=0;};function h(t,e){if(0===e||1===e)return !0;for(var i=t._length*e,r=0,s=0;s<t._lengths.length;s++){var n=t._lengths[s];if(r>=i)return i-r>=0;r+=n;}return !1}function c(t,e){var i=t._length;if(e<=0)return 0;if(e>=i)return t._lengths.length-1;for(var r=0,s=0;r<t._lengths.length;r++){if(s+t._lengths[r]>=e)return e-=s,Math.max(r-1,0)+e/t._lengths[r];s+=t._lengths[r];}return -1}function d(e,i,r){var s,n,a,o,l,h,c,d,f=i.controls&&i.controls.right,u=e.controls&&e.controls.left;return s=i.x,l=i.y,n=(f||i).x,h=(f||i).y,a=(u||e).x,c=(u||e).y,o=e.x,d=e.y,f&&i._relative&&(n+=i.x,h+=i.y),u&&e._relative&&(a+=e.x,c+=e.y),t.Utils.getCurveLength(s,l,n,h,a,c,o,d,r)}function f(e,i,r){var s,n,a,o,l,h,c,d,f=i.controls&&i.controls.right,u=e.controls&&e.controls.left;return s=i.x,l=i.y,n=(f||i).x,h=(f||i).y,a=(u||e).x,c=(u||e).y,o=e.x,d=e.y,f&&i._relative&&(n+=i.x,h+=i.y),u&&e._relative&&(a+=e.x,c+=e.y),t.Utils.subdivide(s,l,n,h,a,c,o,d,r)}o.extend(l,{Properties:["fill","stroke","linewidth","opacity","visible","cap","join","miter","closed","curved","automatic","beginning","ending"],Utils:{getCurveLength:d},FlagVertices:function(){this._flagVertices=!0,this._flagLength=!0,this.parent&&(this.parent._flagLength=!0);},BindVertices:function(e){for(var i=e.length;i--;)e[i].bind(t.Events.change,this._renderer.flagVertices);this._renderer.flagVertices();},UnbindVertices:function(e){for(var i=e.length;i--;)e[i].unbind(t.Events.change,this._renderer.flagVertices);this._renderer.flagVertices();},FlagFill:function(){this._flagFill=!0;},FlagStroke:function(){this._flagStroke=!0;},MakeObservable:function(e){t.Shape.MakeObservable(e),o.each(l.Properties.slice(2,8),t.Utils.defineProperty,e),Object.defineProperty(e,"fill",{enumerable:!0,get:function(){return this._fill},set:function(e){(this._fill instanceof t.Gradient||this._fill instanceof t.LinearGradient||this._fill instanceof t.RadialGradient||this._fill instanceof t.Texture)&&this._fill.unbind(t.Events.change,this._renderer.flagFill),this._fill=e,this._flagFill=!0,(this._fill instanceof t.Gradient||this._fill instanceof t.LinearGradient||this._fill instanceof t.RadialGradient||this._fill instanceof t.Texture)&&this._fill.bind(t.Events.change,this._renderer.flagFill);}}),Object.defineProperty(e,"stroke",{enumerable:!0,get:function(){return this._stroke},set:function(e){(this._stroke instanceof t.Gradient||this._stroke instanceof t.LinearGradient||this._stroke instanceof t.RadialGradient||this._stroke instanceof t.Texture)&&this._stroke.unbind(t.Events.change,this._renderer.flagStroke),this._stroke=e,this._flagStroke=!0,(this._stroke instanceof t.Gradient||this._stroke instanceof t.LinearGradient||this._stroke instanceof t.RadialGradient||this._stroke instanceof t.Texture)&&this._stroke.bind(t.Events.change,this._renderer.flagStroke);}}),Object.defineProperty(e,"length",{get:function(){return this._flagLength&&this._updateLength(),this._length}}),Object.defineProperty(e,"closed",{enumerable:!0,get:function(){return this._closed},set:function(t){this._closed=!!t,this._flagVertices=!0;}}),Object.defineProperty(e,"curved",{enumerable:!0,get:function(){return this._curved},set:function(t){this._curved=!!t,this._flagVertices=!0;}}),Object.defineProperty(e,"automatic",{enumerable:!0,get:function(){return this._automatic},set:function(t){if(t!==this._automatic){this._automatic=!!t;var e=this._automatic?"ignore":"listen";o.each(this.vertices,(function(t){t[e]();}));}}}),Object.defineProperty(e,"beginning",{enumerable:!0,get:function(){return this._beginning},set:function(t){this._beginning=t,this._flagVertices=!0;}}),Object.defineProperty(e,"ending",{enumerable:!0,get:function(){return this._ending},set:function(t){this._ending=t,this._flagVertices=!0;}}),Object.defineProperty(e,"vertices",{enumerable:!0,get:function(){return this._collection},set:function(e){this._renderer.flagVertices;var i=this._renderer.bindVertices,r=this._renderer.unbindVertices;this._collection&&this._collection.unbind(t.Events.insert,i).unbind(t.Events.remove,r),e instanceof t.Utils.Collection?this._collection=e:this._collection=new t.Utils.Collection(e||[]),this._collection.bind(t.Events.insert,i).bind(t.Events.remove,r),i(this._collection);}}),Object.defineProperty(e,"clip",{enumerable:!0,get:function(){return this._clip},set:function(t){this._clip=t,this._flagClip=!0;}}),Object.defineProperty(e,"dashes",{enumerable:!0,get:function(){return this._dashes},set:function(t){o.isNumber(t.offset)||(t.offset=this._dashes.offset||0),this._dashes=t;}});}}),o.extend(l.prototype,t.Shape.prototype,{_flagVertices:!0,_flagLength:!0,_flagFill:!0,_flagStroke:!0,_flagLinewidth:!0,_flagOpacity:!0,_flagVisible:!0,_flagCap:!0,_flagJoin:!0,_flagMiter:!0,_flagClip:!1,_length:0,_fill:"#fff",_stroke:"#000",_linewidth:1,_opacity:1,_visible:!0,_cap:"round",_join:"round",_miter:4,_closed:!0,_curved:!1,_automatic:!0,_beginning:0,_ending:1,_clip:!1,_dashes:[],constructor:l,clone:function(t){for(var e=new l,i=0;i<this.vertices.length;i++)e.vertices.push(this.vertices[i].clone());for(var r=0;r<l.Properties.length;r++){var s=l.Properties[r];e[s]=this[s];}return e.className=this.className,e.translation.copy(this.translation),e.rotation=this.rotation,e.scale=this.scale,this.matrix.manual&&e.matrix.copy(this.matrix),t&&t.add(e),e._update()},toObject:function(){var e={vertices:o.map(this.vertices,(function(t){return t.toObject()}))};return o.each(t.Shape.Properties,(function(t){e[t]=this[t];}),this),e.className=this.className,e.translation=this.translation.toObject(),e.rotation=this.rotation,e.scale=this.scale instanceof t.Vector?this.scale.toObject():this.scale,this.matrix.manual&&(e.matrix=this.matrix.toObject()),e},noFill:function(){return this.fill="transparent",this},noStroke:function(){return this.stroke=void 0,this},corner:function(){var t=this.getBoundingClientRect(!0);return t.centroid={x:t.left+t.width/2,y:t.top+t.height/2},o.each(this.vertices,(function(e){e.subSelf(t.centroid),e.x+=t.width/2,e.y+=t.height/2;})),this},center:function(){var t=this.getBoundingClientRect(!0);return t.centroid={x:t.left+t.width/2-this.translation.x,y:t.top+t.height/2-this.translation.y},o.each(this.vertices,(function(e){e.subSelf(t.centroid);})),this},remove:function(){return this.parent?(this.parent.remove(this),this):this},getBoundingClientRect:function(r){var s,a,o,l,h,c,d,f,u=1/0,_=-1/0,g=1/0,p=-1/0;if(this._update(!0),s=r?this._matrix:n(this),a=this.linewidth/2,(o=this._renderer.vertices.length)<=0)return {width:0,height:0};for(l=0;l<o;l++)if(f=this._renderer.vertices[l],(h=this._renderer.vertices[(l+o-1)%o]).controls&&f.controls){c=h.relative?s.multiply(h.controls.right.x+h.x,h.controls.right.y+h.y,1):s.multiply(h.controls.right.x,h.controls.right.y,1),h=s.multiply(h.x,h.y,1),d=f.relative?s.multiply(f.controls.left.x+f.x,f.controls.left.y+f.y,1):s.multiply(f.controls.left.x,f.controls.left.y,1),f=s.multiply(f.x,f.y,1);var m=t.Utils.getCurveBoundingBox(h.x,h.y,c.x,c.y,d.x,d.y,f.x,f.y);g=e(m.min.y-a,g),u=e(m.min.x-a,u),_=i(m.max.x+a,_),p=i(m.max.y+a,p);}else l<=1&&(h=s.multiply(h.x,h.y,1),g=e(h.y-a,g),u=e(h.x-a,u),_=i(h.x+a,_),p=i(h.y+a,p)),f=s.multiply(f.x,f.y,1),g=e(f.y-a,g),u=e(f.x-a,u),_=i(f.x+a,_),p=i(f.y+a,p);return {top:g,left:u,right:_,bottom:p,width:_-u,height:p-g}},getPointAt:function(e,i){for(var r,s,n,a,l,h,c,d,f,u,_,g,p,m,v,y=this.length*Math.min(Math.max(e,0),1),x=this.vertices.length,b=x-1,w=null,k=null,S=0,A=this._lengths.length,E=0;S<A;S++){if(E+this._lengths[S]>=y){this._closed?(r=t.Utils.mod(S,x),s=t.Utils.mod(S-1,x),0===S&&(r=s,s=S)):(r=S,s=Math.min(Math.max(S-1,0),b)),w=this.vertices[r],k=this.vertices[s],y-=E,e=0!==this._lengths[S]?y/this._lengths[S]:0;break}E+=this._lengths[S];}if(o.isNull(w)||o.isNull(k))return null;if(!w)return k;if(!k)return w;v=k.controls&&k.controls.right,m=w.controls&&w.controls.left,l=k.x,u=k.y,h=(v||k).x,_=(v||k).y,c=(m||w).x,g=(m||w).y,d=w.x,p=w.y,v&&k.relative&&(h+=k.x,_+=k.y),m&&w.relative&&(c+=w.x,g+=w.y),a=t.Utils.getComponentOnCubicBezier(e,l,h,c,d),f=t.Utils.getComponentOnCubicBezier(e,u,_,g,p);var C=t.Utils.lerp(l,h,e),R=t.Utils.lerp(u,_,e),F=t.Utils.lerp(h,c,e),M=t.Utils.lerp(_,g,e),O=t.Utils.lerp(c,d,e),P=t.Utils.lerp(g,p,e),U=t.Utils.lerp(C,F,e),T=t.Utils.lerp(R,M,e),N=t.Utils.lerp(F,O,e),L=t.Utils.lerp(M,P,e);return o.isObject(i)?(i.x=a,i.y=f,o.isObject(i.controls)||t.Anchor.AppendCurveProperties(i),i.controls.left.x=U,i.controls.left.y=T,i.controls.right.x=N,i.controls.right.y=L,o.isBoolean(i.relative)&&!i.relative||(i.controls.left.x-=a,i.controls.left.y-=f,i.controls.right.x-=a,i.controls.right.y-=f),i.t=e,i):((n=new t.Anchor(a,f,U-a,T-f,N-a,L-f,this._curved?t.Commands.curve:t.Commands.line)).t=e,n)},plot:function(){if(this.curved)return t.Utils.getCurveFromPoints(this._collection,this.closed),this;for(var e=0;e<this._collection.length;e++)this._collection[e].command=0===e?t.Commands.move:t.Commands.line;return this},subdivide:function(e){this._update();var i=this.vertices.length-1,r=this.vertices[i],s=this._closed||this.vertices[i]._command===t.Commands.close,n=[];return o.each(this.vertices,(function(a,l){if(l<=0&&!s)r=a;else {if(a.command===t.Commands.move)return n.push(new t.Anchor(r.x,r.y)),l>0&&(n[n.length-1].command=t.Commands.line),void(r=a);var h=f(a,r,e);n=n.concat(h),o.each(h,(function(e,i){i<=0&&r.command===t.Commands.move?e.command=t.Commands.move:e.command=t.Commands.line;})),l>=i&&(this._closed&&this._automatic?(h=f(a,r=a,e),n=n.concat(h),o.each(h,(function(e,i){i<=0&&r.command===t.Commands.move?e.command=t.Commands.move:e.command=t.Commands.line;}))):s&&n.push(new t.Anchor(a.x,a.y)),n[n.length-1].command=s?t.Commands.close:t.Commands.line),r=a;}}),this),this._automatic=!1,this._curved=!1,this.vertices=n,this},_updateLength:function(e,i){i||this._update();var r=this.vertices.length,s=r-1,n=this.vertices[s],a=0;return o.isUndefined(this._lengths)&&(this._lengths=[]),o.each(this.vertices,(function(i,r){if(r<=0||i.command===t.Commands.move)return n=i,void(this._lengths[r]=0);this._lengths[r]=d(i,n,e),a+=this._lengths[r],n=i;}),this),this._length=a,this._flagLength=!1,this},_update:function(){if(this._flagVertices){this._automatic&&this.plot(),this._flagLength&&this._updateLength(void 0,!0);var e,i,n,a,o,l=this._collection.length,d=this._closed,f=Math.min(this._beginning,this._ending),u=Math.max(this._beginning,this._ending),_=c(this,f*this._length),g=c(this,u*this._length),p=r(_),m=s(g);this._renderer.vertices.length=0;for(var v=0;v<l;v++)this._renderer.collection.length<=v&&this._renderer.collection.push(new t.Anchor),v>m&&!i?((o=this._renderer.collection[v]).copy(this._collection[v]),this.getPointAt(u,o),o.command=this._renderer.collection[v].command,this._renderer.vertices.push(o),i=o,(n=this._collection[v-1])&&n.controls&&(o.controls.right.clear(),this._renderer.collection[v-1].controls.right.clear().lerp(n.controls.right,o.t))):v>=p&&v<=m&&(o=this._renderer.collection[v].copy(this._collection[v]),this._renderer.vertices.push(o),v===m&&h(this,u)?(i=o,!d&&i.controls&&i.controls.right.clear()):v===p&&h(this,f)&&((e=o).command=t.Commands.move,!d&&e.controls&&e.controls.left.clear()));p>0&&!e&&(v=p-1,(o=this._renderer.collection[v]).copy(this._collection[v]),this.getPointAt(f,o),o.command=t.Commands.move,this._renderer.vertices.unshift(o),e=o,(a=this._collection[v+1])&&a.controls&&(o.controls.left.clear(),this._renderer.collection[v+1].controls.left.copy(a.controls.left).lerp(t.Vector.zero,o.t)));}return t.Shape.prototype._update.apply(this,arguments),this},flagReset:function(){return this._flagVertices=this._flagFill=this._flagStroke=this._flagLinewidth=this._flagOpacity=this._flagVisible=this._flagCap=this._flagJoin=this._flagMiter=this._flagClip=!1,t.Shape.prototype.flagReset.call(this),this}}),l.MakeObservable(l.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Path,i=t.Utils,r=t.Line=function(i,r,s,n){e.call(this,[new t.Anchor(i,r),new t.Anchor(s,n)]),this.vertices[0].command=t.Commands.move,this.vertices[1].command=t.Commands.line,this.automatic=!1;};i.extend(r.prototype,e.prototype),r.prototype.constructor=r,e.MakeObservable(r.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Path,i=t.Utils,r=t.Rectangle=function(i,r,s,n){e.call(this,[new t.Anchor,new t.Anchor,new t.Anchor,new t.Anchor],!0,!1,!0),this.width=s,this.height=n,this.origin=new t.Vector,this.translation.set(i,r),this._update();};i.extend(r,{Properties:["width","height"],MakeObservable:function(s){e.MakeObservable(s),i.each(r.Properties,t.Utils.defineProperty,s),Object.defineProperty(s,"origin",{enumerable:!0,get:function(){return this._origin},set:function(e){this._origin&&this._origin.unbind(t.Events.change,this._renderer.flagVertices),this._origin=e,this._origin.bind(t.Events.change,this._renderer.flagVertices),this._renderer.flagVertices();}});}}),i.extend(r.prototype,e.prototype,{_flagWidth:0,_flagHeight:0,_width:0,_height:0,_origin:null,constructor:r,_update:function(){if(this._flagWidth||this._flagHeight){var i=this._width/2,r=this._height/2;this.vertices[0].set(-i,-r).add(this._origin).command=t.Commands.move,this.vertices[1].set(i,-r).add(this._origin).command=t.Commands.line,this.vertices[2].set(i,r).add(this._origin).command=t.Commands.line,this.vertices[3].set(-i,r).add(this._origin).command=t.Commands.line,this.vertices[4]&&(this.vertices[4].set(-i,-r).add(this._origin).command=t.Commands.line);}return e.prototype._update.call(this),this},flagReset:function(){return this._flagWidth=this._flagHeight=!1,e.prototype.flagReset.call(this),this},clone:function(e){var s=new r(0,0,this.width,this.height);return s.translation.copy(this.translation),s.rotation=this.rotation,s.scale=this.scale,this.matrix.manual&&s.matrix.copy(this.matrix),i.each(t.Path.Properties,(function(t){s[t]=this[t];}),this),e&&e.add(s),s},toObject:function(){var t=e.prototype.toObject.call(this);return t.width=this.width,t.height=this.height,t.origin=this.origin.toObject(),t}}),r.MakeObservable(r.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Path,i=2*Math.PI,r=Math.PI/2,s=Math.cos,n=Math.sin,a=t.Utils,o=t.Ellipse=function(i,r,s,n,o){a.isNumber(n)||(n=s);var l=o?Math.max(o,2):4,h=a.map(a.range(l),(function(e){return new t.Anchor}),this);e.call(this,h,!0,!0,!0),this.width=2*s,this.height=2*n,this._update(),this.translation.set(i,r);};a.extend(o,{Properties:["width","height"],MakeObservable:function(i){e.MakeObservable(i),a.each(o.Properties,t.Utils.defineProperty,i);}}),a.extend(o.prototype,e.prototype,{_flagWidth:!1,_flagHeight:!1,_width:0,_height:0,constructor:o,_update:function(){if(this._flagWidth||this._flagHeight)for(var a=4/3*Math.tan(Math.PI/(2*this.vertices.length)),o=this._width/2,l=this._height/2,h=0,c=this.vertices.length;h<c;h++){var d=h/c*i,f=o*s(d),u=l*n(d),_=o*a*s(d-r),g=l*a*n(d-r),p=o*a*s(d+r),m=l*a*n(d+r),v=this.vertices[h];v.command=t.Commands.curve,v.set(f,u),v.controls.left.set(_,g),v.controls.right.set(p,m);}return e.prototype._update.call(this),this},flagReset:function(){return this._flagWidth=this._flagHeight=!1,e.prototype.flagReset.call(this),this},clone:function(e){var i=this.width/2,r=this.height/2,s=this.vertices.length,n=new o(0,0,i,r,s);return n.translation.copy(this.translation),n.rotation=this.rotation,n.scale=this.scale,this.matrix.manual&&n.matrix.copy(this.matrix),a.each(t.Path.Properties,(function(t){n[t]=this[t];}),this),e&&e.add(n),n},toObject:function(){var t=e.prototype.toObject.call(this);return a.each(o.Properties,(function(e){t[e]=this[e];}),this),t}}),o.MakeObservable(o.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Path,i=2*Math.PI,r=Math.PI/2,s=Math.cos,n=Math.sin,a=t.Utils,o=t.Circle=function(i,r,s,n){var o=n?Math.max(n,2):4,l=a.map(a.range(o),(function(e){return new t.Anchor}),this);e.call(this,l,!0,!0,!0),this.radius=s,this._update(),a.isNumber(i)&&(this.translation.x=i),a.isNumber(r)&&(this.translation.y=r);};a.extend(o,{Properties:["radius"],MakeObservable:function(i){e.MakeObservable(i),a.each(o.Properties,t.Utils.defineProperty,i);}}),a.extend(o.prototype,e.prototype,{_flagRadius:!1,_radius:0,constructor:o,_update:function(){if(this._flagRadius)for(var a=4/3*Math.tan(Math.PI/(2*this.vertices.length)),o=this._radius,l=o*a,h=0,c=this.vertices.length;h<c;h++){var d=h/c*i,f=o*s(d),u=o*n(d),_=l*s(d-r),g=l*n(d-r),p=l*s(d+r),m=l*n(d+r),v=this.vertices[h];v.command=t.Commands.curve,v.set(f,u),v.controls.left.set(_,g),v.controls.right.set(p,m);}return e.prototype._update.call(this),this},flagReset:function(){return this._flagRadius=!1,e.prototype.flagReset.call(this),this},clone:function(e){var i=new o(0,0,this.radius,this.vertices.length);return i.translation.copy(this.translation),i.rotation=this.rotation,i.scale=this.scale,this.matrix.manual&&i.matrix.copy(this.matrix),a.each(t.Path.Properties,(function(t){i[t]=this[t];}),this),e&&e.add(i),i},toObject:function(){var t=e.prototype.toObject.call(this);return a.each(o.Properties,(function(e){t[e]=this[e];}),this),t}}),o.MakeObservable(o.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Path,i=2*Math.PI,r=Math.cos,s=Math.sin,n=t.Utils,a=t.Polygon=function(t,i,r,s){s=Math.max(s||0,3),e.call(this),this.closed=!0,this.automatic=!1,this.width=2*r,this.height=2*r,this.sides=s,this._update(),this.translation.set(t,i);};n.extend(a,{Properties:["width","height","sides"],MakeObservable:function(i){e.MakeObservable(i),n.each(a.Properties,t.Utils.defineProperty,i);}}),n.extend(a.prototype,e.prototype,{_flagWidth:!1,_flagHeight:!1,_flagSides:!1,_width:0,_height:0,_sides:0,constructor:a,_update:function(){if(this._flagWidth||this._flagHeight||this._flagSides){var n=this._sides,a=n+1,o=this.vertices.length;o>n&&(this.vertices.splice(n-1,o-n),o=n);for(var l=0;l<a;l++){var h=i*((l+.5)/n)+Math.PI/2,c=this._width*r(h)/2,d=this._height*s(h)/2;l>=o?this.vertices.push(new t.Anchor(c,d)):this.vertices[l].set(c,d),this.vertices[l].command=0===l?t.Commands.move:t.Commands.line;}}return e.prototype._update.call(this),this},flagReset:function(){return this._flagWidth=this._flagHeight=this._flagSides=!1,e.prototype.flagReset.call(this),this},clone:function(e){var i=new a(0,0,this.radius,this.sides);return i.translation.copy(this.translation),i.rotation=this.rotation,i.scale=this.scale,this.matrix.manual&&i.matrix.copy(this.matrix),n.each(t.Path.Properties,(function(t){i[t]=this[t];}),this),e&&e.add(i),i},toObject:function(){var t=e.prototype.toObject.call(this);return n.each(a.Properties,(function(e){t[e]=this[e];}),this),t}}),a.MakeObservable(a.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Path,i=(2*Math.PI),r=Math.PI/2,s=(t.Utils),n=t.Utils.mod,a=t.ArcSegment=function(i,r,n,a,o,l,h){var c=h||3*t.Resolution,d=s.map(s.range(c),(function(){return new t.Anchor}));e.call(this,d,!0,!1,!0),this.innerRadius=n,this.outerRadius=a,this.startAngle=o,this.endAngle=l,this._update(),s.isNumber(i)&&(this.translation.x=i),s.isNumber(r)&&(this.translation.y=r);};s.extend(a,{Properties:["startAngle","endAngle","innerRadius","outerRadius"],MakeObservable:function(i){e.MakeObservable(i),s.each(a.Properties,t.Utils.defineProperty,i);}}),s.extend(a.prototype,e.prototype,{_flagStartAngle:!1,_flagEndAngle:!1,_flagInnerRadius:!1,_flagOuterRadius:!1,_startAngle:0,_endAngle:i,_innerRadius:0,_outerRadius:0,constructor:a,_update:function(){if(this._flagStartAngle||this._flagEndAngle||this._flagInnerRadius||this._flagOuterRadius){var s,a=this._startAngle,o=this._endAngle,l=this._innerRadius,h=this._outerRadius,c=n(a,i)===n(o,i),d=l>0,f=this.vertices,u=d?f.length/2:f.length,_=0;c?u--:d||(u-=2);for(var g=0,p=u-1;g<u;g++){var m=g/p,v=f[_],y=m*(o-a)+a,x=(o-a)/u,b=h*Math.cos(y),w=h*Math.sin(y);switch(g){case 0:s=t.Commands.move;break;default:s=t.Commands.curve;}if(v.command=s,v.x=b,v.y=w,v.controls.left.clear(),v.controls.right.clear(),v.command===t.Commands.curve){var k=h*x/Math.PI;v.controls.left.x=k*Math.cos(y-r),v.controls.left.y=k*Math.sin(y-r),v.controls.right.x=k*Math.cos(y+r),v.controls.right.y=k*Math.sin(y+r),1===g&&v.controls.left.multiplyScalar(2),g===p&&v.controls.right.multiplyScalar(2);}_++;}if(d){for(c?(f[_].command=t.Commands.close,_++):p=--u-1,g=0;g<u;g++)m=g/p,v=f[_],y=(1-m)*(o-a)+a,x=(o-a)/u,b=l*Math.cos(y),w=l*Math.sin(y),s=t.Commands.curve,g<=0&&(s=c?t.Commands.move:t.Commands.line),v.command=s,v.x=b,v.y=w,v.controls.left.clear(),v.controls.right.clear(),v.command===t.Commands.curve&&(k=l*x/Math.PI,v.controls.left.x=k*Math.cos(y+r),v.controls.left.y=k*Math.sin(y+r),v.controls.right.x=k*Math.cos(y-r),v.controls.right.y=k*Math.sin(y-r),1===g&&v.controls.left.multiplyScalar(2),g===p&&v.controls.right.multiplyScalar(2)),_++;f[_].copy(f[0]),f[_].command=t.Commands.line;}else c||(f[_].command=t.Commands.line,f[_].x=0,f[_].y=0,f[++_].copy(f[0]),f[_].command=t.Commands.line);}return e.prototype._update.call(this),this},flagReset:function(){return e.prototype.flagReset.call(this),this._flagStartAngle=this._flagEndAngle=this._flagInnerRadius=this._flagOuterRadius=!1,this},clone:function(e){var i=this.innerRadius,r=this.outerradius,n=this.startAngle,o=this.endAngle,l=this.vertices.length,h=new a(0,0,i,r,n,o,l);return h.translation.copy(this.translation),h.rotation=this.rotation,h.scale=this.scale,this.matrix.manual&&h.matrix.copy(this.matrix),s.each(t.Path.Properties,(function(t){h[t]=this[t];}),this),e&&e.add(h),h},toObject:function(){var t=e.prototype.toObject.call(this);return s.each(a.Properties,(function(e){t[e]=this[e];}),this),t}}),a.MakeObservable(a.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Path,i=2*Math.PI,r=Math.cos,s=Math.sin,n=t.Utils,a=t.Star=function(t,i,r,s,a){arguments.length<=3&&(r=(s=r)/2),(!n.isNumber(a)||a<=0)&&(a=5);e.call(this),this.closed=!0,this.automatic=!1,this.innerRadius=r,this.outerRadius=s,this.sides=a,this._update(),this.translation.set(t,i);};n.extend(a,{Properties:["innerRadius","outerRadius","sides"],MakeObservable:function(i){e.MakeObservable(i),n.each(a.Properties,t.Utils.defineProperty,i);}}),n.extend(a.prototype,e.prototype,{_flagInnerRadius:!1,_flagOuterRadius:!1,_flagSides:!1,_innerRadius:0,_outerRadius:0,_sides:0,constructor:a,_update:function(){if(this._flagInnerRadius||this._flagOuterRadius||this._flagSides){var n=2*this._sides,a=n+1,o=this.vertices.length;o>n&&(this.vertices.splice(n-1,o-n),o=n);for(var l=0;l<a;l++){var h=i*((l+.5)/n),c=(l%2?this._outerRadius:this._innerRadius)/2,d=c*r(h),f=c*s(h);l>=o?this.vertices.push(new t.Anchor(d,f)):this.vertices[l].set(d,f),this.vertices[l].command=0===l?t.Commands.move:t.Commands.line;}}return e.prototype._update.call(this),this},flagReset:function(){return this._flagInnerRadius=this._flagOuterRadius=this._flagSides=!1,e.prototype.flagReset.call(this),this},clone:function(e){var i=this.innerRadius,r=this.outerRadius,s=this.sides,o=new a(0,0,i,r,s);return o.translation.copy(this.translation),o.rotation=this.rotation,o.scale=this.scale,this.matrix.manual&&o.matrix.copy(this.matrix),n.each(t.Path.Properties,(function(t){o[t]=this[t];}),this),e&&e.add(o),o},toObject:function(){var t=e.prototype.toObject.call(this);return n.each(a.Properties,(function(e){t[e]=this[e];}),this),t}}),a.MakeObservable(a.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Path,i=t.Utils,r=t.RoundedRectangle=function(s,n,a,o,l){i.isUndefined(l)&&(l=Math.floor(Math.min(a,o)/12));var h=i.map(i.range(10),(function(e){return new t.Anchor(0,0,0,0,0,0,0===e?t.Commands.move:t.Commands.curve)}));e.call(this,h),this.closed=!0,this.automatic=!1,this._renderer.flagRadius=i.bind(r.FlagRadius,this),this.width=a,this.height=o,this.radius=l,this._update(),this.translation.set(s,n);};i.extend(r,{Properties:["width","height"],FlagRadius:function(){this._flagRadius=!0;},MakeObservable:function(s){e.MakeObservable(s),i.each(r.Properties,t.Utils.defineProperty,s),Object.defineProperty(s,"radius",{enumerable:!0,get:function(){return this._radius},set:function(e){this._radius instanceof t.Vector&&this._radius.unbind(t.Events.change,this._renderer.flagRadius),this._radius=e,this._radius instanceof t.Vector&&this._radius.bind(t.Events.change,this._renderer.flagRadius),this._flagRadius=!0;}});}}),i.extend(r.prototype,e.prototype,{_flagWidth:!1,_flagHeight:!1,_flagRadius:!1,_width:0,_height:0,_radius:0,constructor:r,_update:function(){if(this._flagWidth||this._flagHeight||this._flagRadius){var i,r,s,n=this._width,a=this._height;this._radius instanceof t.Vector?(i=this._radius.x,r=this._radius.y):(i=this._radius,r=this._radius);var o=n/2,l=a/2;(s=this.vertices[0]).x=-(o-i),s.y=-l,(s=this.vertices[1]).x=o-i,s.y=-l,s.controls.left.clear(),s.controls.right.x=i,s.controls.right.y=0,(s=this.vertices[2]).x=o,s.y=-(l-r),s.controls.right.clear(),s.controls.left.clear(),(s=this.vertices[3]).x=o,s.y=l-r,s.controls.left.clear(),s.controls.right.x=0,s.controls.right.y=r,(s=this.vertices[4]).x=o-i,s.y=l,s.controls.right.clear(),s.controls.left.clear(),(s=this.vertices[5]).x=-(o-i),s.y=l,s.controls.left.clear(),s.controls.right.x=-i,s.controls.right.y=0,(s=this.vertices[6]).x=-o,s.y=l-r,s.controls.left.clear(),s.controls.right.clear(),(s=this.vertices[7]).x=-o,s.y=-(l-r),s.controls.left.clear(),s.controls.right.x=0,s.controls.right.y=-r,(s=this.vertices[8]).x=-(o-i),s.y=-l,s.controls.left.clear(),s.controls.right.clear(),(s=this.vertices[9]).copy(this.vertices[8]);}return e.prototype._update.call(this),this},flagReset:function(){return this._flagWidth=this._flagHeight=this._flagRadius=!1,e.prototype.flagReset.call(this),this},clone:function(e){var s=this.width,n=this.height,a=this.radius,o=new r(0,0,s,n,a);return o.translation.copy(this.translation),o.rotation=this.rotation,o.scale=this.scale,this.matrix.manual&&o.matrix.copy(this.matrix),i.each(t.Path.Properties,(function(t){o[t]=this[t];}),this),e&&e.add(o),o},toObject:function(){var t=e.prototype.toObject.call(this);return i.each(r.Properties,(function(e){t[e]=this[e];}),this),t.radius=i.isNumber(this.radius)?this.radius:this.radius.toObject(),t}}),r.MakeObservable(r.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){t.root;var e=t.Utils.getComputedMatrix,i=t.Utils,r=t.Text=function(e,r,s,n){if(t.Shape.call(this),this._renderer.type="text",this._renderer.flagFill=i.bind(t.Text.FlagFill,this),this._renderer.flagStroke=i.bind(t.Text.FlagStroke,this),this.value=e,i.isNumber(r)&&(this.translation.x=r),i.isNumber(s)&&(this.translation.y=s),this.dashes=[],this.dashes.offset=0,!i.isObject(n))return this;i.each(t.Text.Properties,(function(t){t in n&&(this[t]=n[t]);}),this);};i.extend(t.Text,{Ratio:.6,Properties:["value","family","size","leading","alignment","linewidth","style","className","weight","decoration","baseline","opacity","visible","fill","stroke"],FlagFill:function(){this._flagFill=!0;},FlagStroke:function(){this._flagStroke=!0;},MakeObservable:function(e){t.Shape.MakeObservable(e),i.each(t.Text.Properties.slice(0,13),t.Utils.defineProperty,e),Object.defineProperty(e,"fill",{enumerable:!0,get:function(){return this._fill},set:function(e){(this._fill instanceof t.Gradient||this._fill instanceof t.LinearGradient||this._fill instanceof t.RadialGradient||this._fill instanceof t.Texture)&&this._fill.unbind(t.Events.change,this._renderer.flagFill),this._fill=e,this._flagFill=!0,(this._fill instanceof t.Gradient||this._fill instanceof t.LinearGradient||this._fill instanceof t.RadialGradient||this._fill instanceof t.Texture)&&this._fill.bind(t.Events.change,this._renderer.flagFill);}}),Object.defineProperty(e,"stroke",{enumerable:!0,get:function(){return this._stroke},set:function(e){(this._stroke instanceof t.Gradient||this._stroke instanceof t.LinearGradient||this._stroke instanceof t.RadialGradient||this._stroke instanceof t.Texture)&&this._stroke.unbind(t.Events.change,this._renderer.flagStroke),this._stroke=e,this._flagStroke=!0,(this._stroke instanceof t.Gradient||this._stroke instanceof t.LinearGradient||this._stroke instanceof t.RadialGradient||this._stroke instanceof t.Texture)&&this._stroke.bind(t.Events.change,this._renderer.flagStroke);}}),Object.defineProperty(e,"clip",{enumerable:!0,get:function(){return this._clip},set:function(t){this._clip=t,this._flagClip=!0;}}),Object.defineProperty(e,"dashes",{enumerable:!0,get:function(){return this._dashes},set:function(t){i.isNumber(t.offset)||(t.offset=this._dashes.offset||0),this._dashes=t;}});}}),i.extend(t.Text.prototype,t.Shape.prototype,{_flagValue:!0,_flagFamily:!0,_flagSize:!0,_flagLeading:!0,_flagAlignment:!0,_flagBaseline:!0,_flagStyle:!0,_flagWeight:!0,_flagDecoration:!0,_flagFill:!0,_flagStroke:!0,_flagLinewidth:!0,_flagOpacity:!0,_flagClassName:!0,_flagVisible:!0,_flagClip:!1,_value:"",_family:"sans-serif",_size:13,_leading:17,_alignment:"center",_baseline:"middle",_style:"normal",_weight:500,_decoration:"none",_fill:"#000",_stroke:"transparent",_linewidth:1,_opacity:1,_className:"",_visible:!0,_clip:!1,_dashes:[],constructor:t.Text,remove:function(){return this.parent?(this.parent.remove(this),this):this},clone:function(e){var r=new t.Text(this.value);return r.translation.copy(this.translation),r.rotation=this.rotation,r.scale=this.scale,i.each(t.Text.Properties,(function(t){r[t]=this[t];}),this),this.matrix.manual&&r.matrix.copy(this.matrix),e&&e.add(r),r._update()},toObject:function(){var e={translation:this.translation.toObject(),rotation:this.rotation,scale:this.scale};return this.matrix.manual&&(e.matrix=this.matrix.toObject()),i.each(t.Text.Properties,(function(t){e[t]=this[t];}),this),e},noFill:function(){return this.fill="transparent",this},noStroke:function(){return this.stroke=void 0,this.linewidth=void 0,this},getBoundingClientRect:function(t){var i,s,n,a,o,l;this._update(!0),i=t?this._matrix:e(this);var h=this.leading,c=this.value.length*this.size*r.Ratio;switch(this.alignment){case"left":n=0,a=c;break;case"right":n=-c,a=0;break;default:n=-c/2,a=c/2;}switch(this.baseline){case"top":o=0,l=h;break;case"bottom":o=-h,l=0;break;default:o=-h/2,l=h/2;}return {top:o=(s=i.multiply(n,o,1)).y,left:n=s.x,right:a=(s=i.multiply(a,l,1)).x,bottom:l=s.y,width:a-n,height:l-o}},flagReset:function(){return this._flagValue=this._flagFamily=this._flagSize=this._flagLeading=this._flagAlignment=this._flagFill=this._flagStroke=this._flagLinewidth=this._flagOpacity=this._flagVisible=this._flagClip=this._flagDecoration=this._flagClassName=this._flagBaseline=!1,t.Shape.prototype.flagReset.call(this),this}}),t.Text.MakeObservable(t.Text.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Utils,i=t.Stop=function(t,r,s){this._renderer={},this._renderer.type="stop",this.offset=e.isNumber(t)?t:i.Index<=0?0:1,this.opacity=e.isNumber(s)?s:1,this.color=e.isString(r)?r:i.Index<=0?"#fff":"#000",i.Index=(i.Index+1)%2;};e.extend(i,{Index:0,Properties:["offset","opacity","color"],MakeObservable:function(t){e.each(i.Properties,(function(t){var e="_"+t,i="_flag"+t.charAt(0).toUpperCase()+t.slice(1);Object.defineProperty(this,t,{enumerable:!0,get:function(){return this[e]},set:function(t){this[e]=t,this[i]=!0,this.parent&&(this.parent._flagStops=!0);}});}),t);}}),e.extend(i.prototype,t.Utils.Events,{constructor:i,clone:function(){var t=new i;return e.each(i.Properties,(function(e){t[e]=this[e];}),this),t},toObject:function(){var t={};return e.each(i.Properties,(function(e){t[e]=this[e];}),this),t},flagReset:function(){return this._flagOffset=this._flagColor=this._flagOpacity=!1,this}}),i.MakeObservable(i.prototype),i.prototype.constructor=i;var r=t.Gradient=function(i){this._renderer={},this._renderer.type="gradient",this.id=t.Identifier+t.uniqueId(),this.classList=[],this._renderer.flagStops=e.bind(r.FlagStops,this),this._renderer.bindStops=e.bind(r.BindStops,this),this._renderer.unbindStops=e.bind(r.UnbindStops,this),this.spread="pad",this.stops=i;};e.extend(r,{Stop:i,Properties:["spread"],MakeObservable:function(i){e.each(r.Properties,t.Utils.defineProperty,i),Object.defineProperty(i,"stops",{enumerable:!0,get:function(){return this._stops},set:function(e){this._renderer.flagStops;var i=this._renderer.bindStops,r=this._renderer.unbindStops;this._stops&&this._stops.unbind(t.Events.insert,i).unbind(t.Events.remove,r),this._stops=new t.Utils.Collection((e||[]).slice(0)),this._stops.bind(t.Events.insert,i).bind(t.Events.remove,r),i(this._stops);}});},FlagStops:function(){this._flagStops=!0;},BindStops:function(e){for(var i=e.length;i--;)e[i].bind(t.Events.change,this._renderer.flagStops),e[i].parent=this;this._renderer.flagStops();},UnbindStops:function(e){for(var i=e.length;i--;)e[i].unbind(t.Events.change,this._renderer.flagStops),delete e[i].parent;this._renderer.flagStops();}}),e.extend(r.prototype,t.Utils.Events,{_flagStops:!1,_flagSpread:!1,clone:function(i){var s=e.map(this.stops,(function(t){return t.clone()})),n=new r(s);return e.each(t.Gradient.Properties,(function(t){n[t]=this[t];}),this),i&&i.add(n),n},toObject:function(){var t={stops:e.map(this.stops,(function(t){return t.toObject()}))};return e.each(r.Properties,(function(e){t[e]=this[e];}),this),t},_update:function(){return (this._flagSpread||this._flagStops)&&this.trigger(t.Events.change),this},flagReset:function(){return this._flagSpread=this._flagStops=!1,this}}),r.MakeObservable(r.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Utils,i=t.LinearGradient=function(r,s,n,a,o){t.Gradient.call(this,o),this._renderer.type="linear-gradient";var l=e.bind(i.FlagEndPoints,this);this.left=(new t.Vector).bind(t.Events.change,l),this.right=(new t.Vector).bind(t.Events.change,l),e.isNumber(r)&&(this.left.x=r),e.isNumber(s)&&(this.left.y=s),e.isNumber(n)&&(this.right.x=n),e.isNumber(a)&&(this.right.y=a);};e.extend(i,{Stop:t.Gradient.Stop,MakeObservable:function(e){t.Gradient.MakeObservable(e);},FlagEndPoints:function(){this._flagEndPoints=!0;}}),e.extend(i.prototype,t.Gradient.prototype,{_flagEndPoints:!1,constructor:i,clone:function(r){var s=e.map(this.stops,(function(t){return t.clone()})),n=new i(this.left._x,this.left._y,this.right._x,this.right._y,s);return e.each(t.Gradient.Properties,(function(t){n[t]=this[t];}),this),r&&r.add(n),n},toObject:function(){var e=t.Gradient.prototype.toObject.call(this);return e.left=this.left.toObject(),e.right=this.right.toObject(),e},_update:function(){return (this._flagEndPoints||this._flagSpread||this._flagStops)&&this.trigger(t.Events.change),this},flagReset:function(){return this._flagEndPoints=!1,t.Gradient.prototype.flagReset.call(this),this}}),i.MakeObservable(i.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Utils,i=t.RadialGradient=function(i,r,s,n,a,o){t.Gradient.call(this,n),this._renderer.type="radial-gradient",this.center=(new t.Vector).bind(t.Events.change,e.bind((function(){this._flagCenter=!0;}),this)),this.radius=e.isNumber(s)?s:20,this.focal=(new t.Vector).bind(t.Events.change,e.bind((function(){this._flagFocal=!0;}),this)),e.isNumber(i)&&(this.center.x=i),e.isNumber(r)&&(this.center.y=r),this.focal.copy(this.center),e.isNumber(a)&&(this.focal.x=a),e.isNumber(o)&&(this.focal.y=o);};e.extend(i,{Stop:t.Gradient.Stop,Properties:["radius"],MakeObservable:function(r){t.Gradient.MakeObservable(r),e.each(i.Properties,t.Utils.defineProperty,r);}}),e.extend(i.prototype,t.Gradient.prototype,{_flagRadius:!1,_flagCenter:!1,_flagFocal:!1,constructor:i,clone:function(r){var s=e.map(this.stops,(function(t){return t.clone()})),n=new i(this.center._x,this.center._y,this._radius,s,this.focal._x,this.focal._y);return e.each(t.Gradient.Properties.concat(i.Properties),(function(t){n[t]=this[t];}),this),r&&r.add(n),n},toObject:function(){var r=t.Gradient.prototype.toObject.call(this);return e.each(i.Properties,(function(t){r[t]=this[t];}),this),r.center=this.center.toObject(),r.focal=this.focal.toObject(),r},_update:function(){return (this._flagRadius||this._flatCenter||this._flagFocal||this._flagSpread||this._flagStops)&&this.trigger(t.Events.change),this},flagReset:function(){return this._flagRadius=this._flagCenter=this._flagFocal=!1,t.Gradient.prototype.flagReset.call(this),this}}),i.MakeObservable(i.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e,i=t.root,r=t.Utils,s={video:/\.(mp4|webm|ogg)$/i,image:/\.(jpe?g|png|gif|tiff|webp)$/i,effect:/texture|gradient/i};i.document&&(e=document.createElement("a"));var n=t.Texture=function(e,i){if(this._renderer={},this._renderer.type="texture",this._renderer.flagOffset=r.bind(n.FlagOffset,this),this._renderer.flagScale=r.bind(n.FlagScale,this),this.id=t.Identifier+t.uniqueId(),this.classList=[],this.loaded=!1,this.repeat="no-repeat",this.offset=new t.Vector,r.isFunction(i)){var s=r.bind((function(){this.unbind(t.Events.load,s),r.isFunction(i)&&i();}),this);this.bind(t.Events.load,s);}r.isString(e)?this.src=e:r.isElement(e)&&(this.image=e),this._update();};r.extend(n,{Properties:["src","loaded","repeat"],RegularExpressions:s,ImageRegistry:new t.Registry,getAbsoluteURL:function(t){return e?(e.href=t,e.href):t},loadHeadlessBuffer:new Function("texture","loaded",['var fs = require("fs");',"var buffer = fs.readFileSync(texture.src);","texture.image.src = buffer;","loaded();"].join("\n")),getImage:function(e){var r,a=n.getAbsoluteURL(e);return n.ImageRegistry.contains(a)?n.ImageRegistry.get(a):(t.Utils.Image?(r=new t.Utils.Image,t.CanvasRenderer.Utils.shim(r,"img")):i.document?r=s.video.test(a)?document.createElement("video"):document.createElement("img"):console.warn("Two.js: no prototypical image defined for Two.Texture"),r.crossOrigin="anonymous",r)},Register:{canvas:function(t,e){t._src="#"+t.id,n.ImageRegistry.add(t.src,t.image),r.isFunction(e)&&e();},img:function(e,i){var s=function(t){r.isFunction(e.image.removeEventListener)&&(e.image.removeEventListener("load",s,!1),e.image.removeEventListener("error",a,!1)),r.isFunction(i)&&i();},a=function(i){throw r.isFunction(e.image.removeEventListener)&&(e.image.removeEventListener("load",s,!1),e.image.removeEventListener("error",a,!1)),new t.Utils.Error("unable to load "+e.src)};r.isNumber(e.image.width)&&e.image.width>0&&r.isNumber(e.image.height)&&e.image.height>0?s():r.isFunction(e.image.addEventListener)&&(e.image.addEventListener("load",s,!1),e.image.addEventListener("error",a,!1)),e._src=n.getAbsoluteURL(e._src),e.image&&e.image.getAttribute("two-src")||(e.image.setAttribute("two-src",e.src),n.ImageRegistry.add(e.src,e.image),t.Utils.isHeadless?n.loadHeadlessBuffer(e,s):e.image.src=e.src);},video:function(e,i){var s=function(t){e.image.removeEventListener("canplaythrough",s,!1),e.image.removeEventListener("error",a,!1),e.image.width=e.image.videoWidth,e.image.height=e.image.videoHeight,e.image.play(),r.isFunction(i)&&i();},a=function(i){throw e.image.removeEventListener("canplaythrough",s,!1),e.image.removeEventListener("error",a,!1),new t.Utils.Error("unable to load "+e.src)};if(e._src=n.getAbsoluteURL(e._src),e.image.addEventListener("canplaythrough",s,!1),e.image.addEventListener("error",a,!1),!e.image||!e.image.getAttribute("two-src")){if(t.Utils.isHeadless)throw new t.Utils.Error("video textures are not implemented in headless environments.");e.image.setAttribute("two-src",e.src),n.ImageRegistry.add(e.src,e.image),e.image.src=e.src,e.image.loop=!0,e.image.load();}}},load:function(t,e){t.src;var i=t.image,r=i&&i.nodeName.toLowerCase();t._flagImage&&(/canvas/i.test(r)?n.Register.canvas(t,e):(t._src=i.getAttribute("two-src")||i.src,n.Register[r](t,e))),t._flagSrc&&(i||(t.image=n.getImage(t.src)),r=t.image.nodeName.toLowerCase(),n.Register[r](t,e));},FlagOffset:function(){this._flagOffset=!0;},FlagScale:function(){this._flagScale=!0;},MakeObservable:function(e){r.each(n.Properties,t.Utils.defineProperty,e),Object.defineProperty(e,"image",{enumerable:!0,get:function(){return this._image},set:function(t){var e;switch(t&&t.nodeName.toLowerCase()){case"canvas":e="#"+t.id;break;default:e=t.src;}n.ImageRegistry.contains(e)?this._image=n.ImageRegistry.get(t.src):this._image=t,this._flagImage=!0;}}),Object.defineProperty(e,"offset",{enumerable:!0,get:function(){return this._offset},set:function(e){this._offset&&this._offset.unbind(t.Events.change,this._renderer.flagOffset),this._offset=e,this._offset.bind(t.Events.change,this._renderer.flagOffset),this._flagOffset=!0;}}),Object.defineProperty(e,"scale",{enumerable:!0,get:function(){return this._scale},set:function(e){this._scale instanceof t.Vector&&this._scale.unbind(t.Events.change,this._renderer.flagScale),this._scale=e,this._scale instanceof t.Vector&&this._scale.bind(t.Events.change,this._renderer.flagScale),this._flagScale=!0;}});}}),r.extend(n.prototype,t.Utils.Events,t.Shape.prototype,{_flagSrc:!1,_flagImage:!1,_flagVideo:!1,_flagLoaded:!1,_flagRepeat:!1,_flagOffset:!1,_flagScale:!1,_src:"",_image:null,_loaded:!1,_repeat:"no-repeat",_scale:1,_offset:null,constructor:n,clone:function(){var t=new n(this.src);return t.repeat=this.repeat,t.offset.copy(this.origin),t.scale=this.scale,t},toObject:function(){return {src:this.src,repeat:this.repeat,origin:this.origin.toObject(),scale:r.isNumber(this.scale)?this.scale:this.scale.toObject()}},_update:function(){return (this._flagSrc||this._flagImage)&&(this.trigger(t.Events.change),(this._flagSrc||this._flagImage)&&(this.loaded=!1,n.load(this,r.bind((function(){this.loaded=!0,this.trigger(t.Events.change).trigger(t.Events.load);}),this)))),this._image&&this._image.readyState>=4&&(this._flagVideo=!0),this},flagReset:function(){return this._flagSrc=this._flagImage=this._flagLoaded=this._flagVideo=this._flagScale=this._flagOffset=!1,this}}),n.MakeObservable(n.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Utils,i=t.Path,r=t.Rectangle,s=t.Sprite=function(r,s,n,a,o,l){i.call(this,[new t.Anchor,new t.Anchor,new t.Anchor,new t.Anchor],!0),this.noStroke(),this.noFill(),r instanceof t.Texture?this.texture=r:e.isString(r)&&(this.texture=new t.Texture(r)),this.origin=new t.Vector,this._update(),this.translation.set(s||0,n||0),e.isNumber(a)&&(this.columns=a),e.isNumber(o)&&(this.rows=o),e.isNumber(l)&&(this.frameRate=l),this.index=0;};e.extend(s,{Properties:["texture","columns","rows","frameRate","index"],MakeObservable:function(i){r.MakeObservable(i),e.each(s.Properties,t.Utils.defineProperty,i);}}),e.extend(s.prototype,r.prototype,{_flagTexture:!1,_flagColumns:!1,_flagRows:!1,_flagFrameRate:!1,flagIndex:!1,_amount:1,_duration:0,_startTime:0,_playing:!1,_firstFrame:0,_lastFrame:0,_loop:!0,_texture:null,_columns:1,_rows:1,_frameRate:0,_index:0,_origin:null,constructor:s,play:function(t,i,r){return this._playing=!0,this._firstFrame=0,this._lastFrame=this.amount-1,this._startTime=e.performance.now(),e.isNumber(t)&&(this._firstFrame=t),e.isNumber(i)&&(this._lastFrame=i),e.isFunction(r)?this._onLastFrame=r:delete this._onLastFrame,this._index!==this._firstFrame&&(this._startTime-=1e3*Math.abs(this._index-this._firstFrame)/this._frameRate),this},pause:function(){return this._playing=!1,this},stop:function(){return this._playing=!1,this._index=0,this},clone:function(t){var e=new s(this.texture,this.translation.x,this.translation.y,this.columns,this.rows,this.frameRate);return this.playing&&(e.play(this._firstFrame,this._lastFrame),e._loop=this._loop),t&&t.add(e),e},toObject:function(){var t=r.prototype.toObject.call(this);return t.texture=this.texture.toObject(),t.columns=this.columns,t.rows=this.rows,t.frameRate=this.frameRate,t.index=this.index,t._firstFrame=this._firstFrame,t._lastFrame=this._lastFrame,t._loop=this._loop,t},_update:function(){var t,i,s,n,a,o,l,h,c,d=this._texture,f=this._columns,u=this._rows;if((this._flagColumns||this._flagRows)&&(this._amount=this._columns*this._rows),this._flagFrameRate&&(this._duration=1e3*this._amount/this._frameRate),this._flagTexture&&(this.fill=this._texture),this._texture.loaded){t=(l=d.image.width)/f,i=(h=d.image.height)/u,n=this._amount,this.width!==t&&(this.width=t),this.height!==i&&(this.height=i),this._playing&&this._frameRate>0&&(e.isNaN(this._lastFrame)&&(this._lastFrame=n-1),s=e.performance.now()-this._startTime,a=1e3*((c=this._lastFrame+1)-this._firstFrame)/this._frameRate,this._loop?s%=a:s=Math.min(s,a),o=e.lerp(this._firstFrame,c,s/a),(o=Math.floor(o))!==this._index&&(this._index=o,o>=this._lastFrame-1&&this._onLastFrame&&this._onLastFrame()));var _=-t*(this._index%f)+(l-t)/2,g=-i*Math.floor(this._index/f)+(h-i)/2;_!==d.offset.x&&(d.offset.x=_),g!==d.offset.y&&(d.offset.y=g);}return r.prototype._update.call(this),this},flagReset:function(){return this._flagTexture=this._flagColumns=this._flagRows=this._flagFrameRate=!1,r.prototype.flagReset.call(this),this}}),s.MakeObservable(s.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=t.Utils,i=t.Path,r=t.Rectangle,s=t.ImageSequence=function(r,n,a,o){i.call(this,[new t.Anchor,new t.Anchor,new t.Anchor,new t.Anchor],!0),this._renderer.flagTextures=e.bind(s.FlagTextures,this),this._renderer.bindTextures=e.bind(s.BindTextures,this),this._renderer.unbindTextures=e.bind(s.UnbindTextures,this),this.noStroke(),this.noFill(),e.isObject(r)?this.textures=e.map(r,s.GenerateTexture,this):this.textures=[s.GenerateTexture(r)],this.origin=new t.Vector,this._update(),this.translation.set(n||0,a||0),e.isNumber(o)?this.frameRate=o:this.frameRate=s.DefaultFrameRate,this.index=0;};e.extend(s,{Properties:["frameRate","index"],DefaultFrameRate:30,FlagTextures:function(){this._flagTextures=!0;},BindTextures:function(e){for(var i=e.length;i--;)e[i].bind(t.Events.change,this._renderer.flagTextures);this._renderer.flagTextures();},UnbindTextures:function(e){for(var i=e.length;i--;)e[i].unbind(t.Events.change,this._renderer.flagTextures);this._renderer.flagTextures();},MakeObservable:function(i){r.MakeObservable(i),e.each(s.Properties,t.Utils.defineProperty,i),Object.defineProperty(i,"textures",{enumerable:!0,get:function(){return this._textures},set:function(e){this._renderer.flagTextures;var i=this._renderer.bindTextures,r=this._renderer.unbindTextures;this._textures&&this._textures.unbind(t.Events.insert,i).unbind(t.Events.remove,r),this._textures=new t.Utils.Collection((e||[]).slice(0)),this._textures.bind(t.Events.insert,i).bind(t.Events.remove,r),i(this._textures);}});},GenerateTexture:function(i){return i instanceof t.Texture?i:e.isString(i)?new t.Texture(i):void 0}}),e.extend(s.prototype,r.prototype,{_flagTextures:!1,_flagFrameRate:!1,_flagIndex:!1,_amount:1,_duration:0,_index:0,_startTime:0,_playing:!1,_firstFrame:0,_lastFrame:0,_loop:!0,_textures:null,_frameRate:0,_origin:null,constructor:s,play:function(t,i,r){return this._playing=!0,this._firstFrame=0,this._lastFrame=this.amount-1,this._startTime=e.performance.now(),e.isNumber(t)&&(this._firstFrame=t),e.isNumber(i)&&(this._lastFrame=i),e.isFunction(r)?this._onLastFrame=r:delete this._onLastFrame,this._index!==this._firstFrame&&(this._startTime-=1e3*Math.abs(this._index-this._firstFrame)/this._frameRate),this},pause:function(){return this._playing=!1,this},stop:function(){return this._playing=!1,this._index=this._firstFrame,this},clone:function(t){var e=new s(this.textures,this.translation.x,this.translation.y,this.frameRate);return e._loop=this._loop,this._playing&&e.play(),t&&t.add(e),e},toObject:function(){var t=r.prototype.toObject.call(this);return t.textures=e.map(this.textures,(function(t){return t.toObject()})),t.frameRate=this.frameRate,t.index=this.index,t._firstFrame=this._firstFrame,t._lastFrame=this._lastFrame,t._loop=this._loop,t},_update:function(){var i,s,n,a,o,l,h,c,d=this._textures;return this._flagTextures&&(this._amount=d.length),this._flagFrameRate&&(this._duration=1e3*this._amount/this._frameRate),this._playing&&this._frameRate>0?(a=this._amount,e.isNaN(this._lastFrame)&&(this._lastFrame=a-1),n=e.performance.now()-this._startTime,o=1e3*((c=this._lastFrame+1)-this._firstFrame)/this._frameRate,this._loop?n%=o:n=Math.min(n,o),h=e.lerp(this._firstFrame,c,n/o),(h=Math.floor(h))!==this._index&&(this._index=h,(l=d[this._index]).loaded&&(i=l.image.width,s=l.image.height,this.width!==i&&(this.width=i),this.height!==s&&(this.height=s),this.fill=l,h>=this._lastFrame-1&&this._onLastFrame&&this._onLastFrame()))):!this._flagIndex&&this.fill instanceof t.Texture||((l=d[this._index]).loaded&&(i=l.image.width,s=l.image.height,this.width!==i&&(this.width=i),this.height!==s&&(this.height=s)),this.fill=l),r.prototype._update.call(this),this},flagReset:function(){return this._flagTextures=this._flagFrameRate=!1,r.prototype.flagReset.call(this),this}}),s.MakeObservable(s.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two),function(t){var e=Math.min,i=Math.max,r=t.Utils,s=function(){t.Utils.Collection.apply(this,arguments),Object.defineProperty(this,"_events",{value:{},enumerable:!1}),this.ids={},this.on(t.Events.insert,this.attach),this.on(t.Events.remove,this.detach),s.prototype.attach.apply(this,arguments);};s.prototype=new t.Utils.Collection,r.extend(s.prototype,{constructor:s,attach:function(t){for(var e=0;e<t.length;e++)this.ids[t[e].id]=t[e];return this},detach:function(t){for(var e=0;e<t.length;e++)delete this.ids[t[e].id];return this}});var n=t.Group=function(e){t.Shape.call(this,!0),this._renderer.type="group",this.additions=[],this.subtractions=[],this.children=r.isArray(e)?e:arguments;};function a(t,e){var i,s=t.parent;function n(){e.subtractions.length>0&&(i=r.indexOf(e.subtractions,t))>=0&&e.subtractions.splice(i,1),e.additions.length>0&&(i=r.indexOf(e.additions,t))>=0&&e.additions.splice(i,1),t.parent=e,e.additions.push(t),e._flagAdditions=!0;}function a(){(i=r.indexOf(s.additions,t))>=0&&s.additions.splice(i,1),(i=r.indexOf(s.subtractions,t))<0&&(s.subtractions.push(t),s._flagSubtractions=!0);}s!==e?(s&&s.children.ids[t.id]&&(i=r.indexOf(s.children,t),s.children.splice(i,1),a()),e?n():(a(),s._flagAdditions&&0===s.additions.length&&(s._flagAdditions=!1),s._flagSubtractions&&0===s.subtractions.length&&(s._flagSubtractions=!1),delete t.parent)):n();}r.extend(n,{Children:s,InsertChildren:function(t){for(var e=0;e<t.length;e++)a.call(this,t[e],this);},RemoveChildren:function(t){for(var e=0;e<t.length;e++)a.call(this,t[e]);},OrderChildren:function(t){this._flagOrder=!0;},Properties:["fill","stroke","linewidth","visible","cap","join","miter","closed","curved","automatic"],MakeObservable:function(e){var i=t.Group.Properties;Object.defineProperty(e,"opacity",{enumerable:!0,get:function(){return this._opacity},set:function(t){this._flagOpacity=this._opacity!==t,this._opacity=t;}}),Object.defineProperty(e,"beginning",{enumerable:!0,get:function(){return this._beginning},set:function(t){this._flagBeginning=this._beginning!==t,this._beginning=t;}}),Object.defineProperty(e,"ending",{enumerable:!0,get:function(){return this._ending},set:function(t){this._flagEnding=this._ending!==t,this._ending=t;}}),Object.defineProperty(e,"length",{enumerable:!0,get:function(){if(this._flagLength||this._length<=0){if(this._length=0,!this.children)return this._length;for(var t=0;t<this.children.length;t++){var e=this.children[t];this._length+=e.length;}}return this._length}}),t.Shape.MakeObservable(e),n.MakeGetterSetters(e,i),Object.defineProperty(e,"children",{enumerable:!0,get:function(){return this._children},set:function(e){var i=r.bind(n.InsertChildren,this),a=r.bind(n.RemoveChildren,this),o=r.bind(n.OrderChildren,this);this._children&&this._children.unbind(),this._children=new s(e),this._children.bind(t.Events.insert,i),this._children.bind(t.Events.remove,a),this._children.bind(t.Events.order,o);}}),Object.defineProperty(e,"mask",{enumerable:!0,get:function(){return this._mask},set:function(t){this._mask=t,this._flagMask=!0,t.clip||(t.clip=!0);}});},MakeGetterSetters:function(t,e){r.isArray(e)||(e=[e]),r.each(e,(function(e){n.MakeGetterSetter(t,e);}));},MakeGetterSetter:function(t,e){var i="_"+e;Object.defineProperty(t,e,{enumerable:!0,get:function(){return this[i]},set:function(t){this[i]=t;for(var r=0;r<this.children.length;r++){this.children[r][e]=t;}}});}}),r.extend(n.prototype,t.Shape.prototype,{_flagAdditions:!1,_flagSubtractions:!1,_flagOrder:!1,_flagOpacity:!0,_flagBeginning:!1,_flagEnding:!1,_flagLength:!1,_flagMask:!1,_fill:"#fff",_stroke:"#000",_linewidth:1,_opacity:1,_visible:!0,_cap:"round",_join:"round",_miter:4,_closed:!0,_curved:!1,_automatic:!0,_beginning:0,_ending:1,_length:0,_mask:null,constructor:n,clone:function(t){var e=new n,i=r.map(this.children,(function(t){return t.clone()}));return e.add(i),e.opacity=this.opacity,this.mask&&(e.mask=this.mask),e.translation.copy(this.translation),e.rotation=this.rotation,e.scale=this.scale,e.className=this.className,this.matrix.manual&&e.matrix.copy(this.matrix),t&&t.add(e),e._update()},toObject:function(){var e={children:[],translation:this.translation.toObject(),rotation:this.rotation,scale:this.scale instanceof t.Vector?this.scale.toObject():this.scale,opacity:this.opacity,className:this.className,mask:this.mask?this.mask.toObject():null};return this.matrix.manual&&(e.matrix=this.matrix.toObject()),r.each(this.children,(function(t,i){e.children[i]=t.toObject();}),this),e},corner:function(){var t=this.getBoundingClientRect(!0),e={x:t.left,y:t.top};return this.children.forEach((function(t){t.translation.sub(e);})),this},center:function(){var t=this.getBoundingClientRect(!0);return t.centroid={x:t.left+t.width/2-this.translation.x,y:t.top+t.height/2-this.translation.y},this.children.forEach((function(e){e.isShape&&e.translation.sub(t.centroid);})),this},getById:function(t){var e=null;return function i(r){if(r.id===t)return r;if(r.children)for(var s=0;s<r.children.length;s++)if(e=i(r.children[s]))return e;return null}(this)},getByClassName:function(t){var e=[];return function i(s){if(r.indexOf(s.classList,t)>=0&&e.push(s),s.children)for(var n=0;n<s.children.length;n++){i(s.children[n]);}return e}(this)},getByType:function(t){var e=[];return function i(r){if(r instanceof t&&e.push(r),r.children)for(var s=0;s<r.children.length;s++){i(r.children[s]);}return e}(this)},add:function(t){t=t instanceof Array?t.slice():r.toArray(arguments);for(var e=0;e<t.length;e++){var i=t[e];if(i&&i.id){var s=r.indexOf(this.children,i);s>=0&&this.children.splice(s,1),this.children.push(i);}}return this},remove:function(t){var e=arguments.length,i=this.parent;if(e<=0&&i)return i.remove(this),this;t=t instanceof Array?t.slice():r.toArray(arguments);for(var s=0;s<t.length;s++)t[s]&&this.children.ids[t[s].id]&&this.children.splice(r.indexOf(this.children,t[s]),1);return this},getBoundingClientRect:function(s){var n;this._update(!0);for(var a=1/0,o=-1/0,l=1/0,h=-1/0,c=t.Texture.RegularExpressions.effect,d=0;d<this.children.length;d++){var f=this.children[d];f.visible&&!c.test(f._renderer.type)&&(n=f.getBoundingClientRect(s),r.isNumber(n.top)&&r.isNumber(n.left)&&r.isNumber(n.right)&&r.isNumber(n.bottom)&&(l=e(n.top,l),a=e(n.left,a),o=i(n.right,o),h=i(n.bottom,h)));}return {top:l,left:a,right:o,bottom:h,width:o-a,height:h-l}},noFill:function(){return this.children.forEach((function(t){t.noFill();})),this},noStroke:function(){return this.children.forEach((function(t){t.noStroke();})),this},subdivide:function(){var t=arguments;return this.children.forEach((function(e){e.subdivide.apply(e,t);})),this},_update:function(){if(this._flagBeginning||this._flagEnding)for(var e=Math.min(this._beginning,this._ending),i=Math.max(this._beginning,this._ending),r=this.length,s=0,n=e*r,a=i*r,o=0;o<this.children.length;o++){var l=this.children[o],h=l.length;n>s+h?(l.beginning=1,l.ending=1):a<s?(l.beginning=0,l.ending=0):n>s&&n<s+h?(l.beginning=(n-s)/h,l.ending=1):a>s&&a<s+h?(l.beginning=0,l.ending=(a-s)/h):(l.beginning=0,l.ending=1),s+=h;}return t.Shape.prototype._update.apply(this,arguments)},flagReset:function(){return this._flagAdditions&&(this.additions.length=0,this._flagAdditions=!1),this._flagSubtractions&&(this.subtractions.length=0,this._flagSubtractions=!1),this._flagOrder=this._flagMask=this._flagOpacity=this._flagBeginning=this._flagEnding=!1,t.Shape.prototype.flagReset.call(this),this}}),n.MakeObservable(n.prototype);}(("undefined"!=typeof commonjsGlobal?commonjsGlobal:commonjsGlobal||self||window).Two);
    });

    /**
     * The $P Point-Cloud Recognizer (JavaScript version)
     *
     *  Radu-Daniel Vatavu, Ph.D.
     *  University Stefan cel Mare of Suceava
     *  Suceava 720229, Romania
     *  vatavu@eed.usv.ro
     *
     *  Lisa Anthony, Ph.D.
     *  UMBC
     *  Information Systems Department
     *  1000 Hilltop Circle
     *  Baltimore, MD 21250
     *  lanthony@umbc.edu
     *
     *  Jacob O. Wobbrock, Ph.D.
     *  The Information School
     *  University of Washington
     *  Seattle, WA 98195-2840
     *  wobbrock@uw.edu
     *
     * The academic publication for the $P recognizer, and what should be
     * used to cite it, is:
     *
     *     Vatavu, R.-D., Anthony, L. and Wobbrock, J.O. (2012).
     *     Gestures as point clouds: A $P recognizer for user interface
     *     prototypes. Proceedings of the ACM Int'l Conference on
     *     Multimodal Interfaces (ICMI '12). Santa Monica, California
     *     (October 22-26, 2012). New York: ACM Press, pp. 273-280.
     *     https://dl.acm.org/citation.cfm?id=2388732
     *
     * This software is distributed under the "New BSD License" agreement:
     *
     * Copyright (C) 2012, Radu-Daniel Vatavu, Lisa Anthony, and
     * Jacob O. Wobbrock. All rights reserved. Last updated July 14, 2018.
     *
     * Redistribution and use in source and binary forms, with or without
     * modification, are permitted provided that the following conditions are met:
     *    * Redistributions of source code must retain the above copyright
     *      notice, this list of conditions and the following disclaimer.
     *    * Redistributions in binary form must reproduce the above copyright
     *      notice, this list of conditions and the following disclaimer in the
     *      documentation and/or other materials provided with the distribution.
     *    * Neither the names of the University Stefan cel Mare of Suceava,
     *	University of Washington, nor UMBC, nor the names of its contributors
     *	may be used to endorse or promote products derived from this software
     *	without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
     * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
     * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
     * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Radu-Daniel Vatavu OR Lisa Anthony
     * OR Jacob O. Wobbrock BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
     * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
     * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
     * SUCH DAMAGE.
     **/
    //
    // Point class
    //
    function Point(x, y, id) {
      // constructor
      this.X = x;
      this.Y = y;
      this.ID = id; // stroke ID to which this point belongs (1,2,3,etc.)
    }
    //
    // PointCloud class: a point-cloud template
    //
    function PointCloud(name, points) {
      // constructor
      this.Name = name;
      this.Points = Resample(points, NumPoints);
      this.Points = Scale(this.Points);
      this.Points = TranslateTo(this.Points, Origin);
    }
    //
    // Result class
    //
    function Result(name, score, ms) {
      // constructor
      this.Name = name;
      this.Score = score;
      this.Time = ms;
    }
    //
    // PDollarRecognizer constants
    //
    const NumPointClouds = 0;
    const NumPoints = 32;
    const Origin = new Point(0, 0, 0);
    //
    // PDollarRecognizer class
    //
    function PDollarRecognizer() {
      // constructor
      //
      // one predefined point-cloud for each gesture
      //
      this.PointClouds = []; // new Array(NumPointClouds);
      // this.PointClouds[0] = new PointCloud("T", new Array(
      // 	new Point(30,7,1),new Point(103,7,1),
      // 	new Point(66,7,2),new Point(66,87,2)
      // ));

      //
      // The $P Point-Cloud Recognizer API begins here -- 3 methods: Recognize(), AddGesture(), DevareUserGestures()
      //
      this.Recognize = function (points, amount = 1) {
        var t0 = Date.now();
        var candidate = new PointCloud('', points);

        var u = -1;
        var b = +Infinity;
        let dist_index = [];
        let results;
        for (
          var i = 0;
          i < this.PointClouds.length;
          i++ // for each point-cloud template
        ) {
          var d = GreedyCloudMatch(candidate.Points, this.PointClouds[i]);
          dist_index.push([
            d,
            i,
            new Result(this.PointClouds[i].Name, d > 1.0 ? 1.0 / d : 1.0, 0),
          ]);
          if (d < b) {
            b = d; // best (least) distance
            u = i; // point-cloud index
          }
        }
        var t1 = Date.now();
        dist_index.sort((a, b) => a[0] - b[0]);
        if (u == -1) {
          results = {
            matched: false,
            values: [new Result('No match.', 0.0, t1 - t0)],
          };
        } else {
          results = {
            matched: true,
            values: dist_index.splice(0, amount).map((l) => {
              l[2].Time = t1 - t0;
              return l[2]
            }),
          };
        }
        // return (u == -1
        //   ? new Result('No match.', 0.0, t1 - t0)
        //   : new Result(this.PointClouds[u].Name, b > 1.0 ? 1.0 / b : 1.0, t1 - t0))
        return results
      };
      this.AddGesture = function (name, points) {
        this.PointClouds[this.PointClouds.length] = new PointCloud(name, points);
        var num = 0;
        for (var i = 0; i < this.PointClouds.length; i++) {
          if (this.PointClouds[i].Name == name) num++;
        }
        return num
      };
      this.DevareUserGestures = function () {
        this.PointClouds.length = NumPointClouds; // clears any beyond the original set
        return NumPointClouds
      };
    }
    //
    // Private helper functions from here on down
    //
    function GreedyCloudMatch(points, P) {
      var e = 0.5;
      var step = Math.floor(Math.pow(points.length, 1.0 - e));
      var min = +Infinity;
      for (var i = 0; i < points.length; i += step) {
        var d1 = CloudDistance(points, P.Points, i);
        var d2 = CloudDistance(P.Points, points, i);
        min = Math.min(min, Math.min(d1, d2)); // min3
      }
      return min
    }
    function CloudDistance(pts1, pts2, start) {
      var matched = new Array(pts1.length); // pts1.length == pts2.length
      for (var k = 0; k < pts1.length; k++) matched[k] = false;
      var sum = 0;
      var i = start;
      do {
        var index = -1;
        var min = +Infinity;
        for (var j = 0; j < matched.length; j++) {
          if (!matched[j]) {
            var d = Distance(pts1[i], pts2[j]);
            if (d < min) {
              min = d;
              index = j;
            }
          }
        }
        matched[index] = true;
        var weight = 1 - ((i - start + pts1.length) % pts1.length) / pts1.length;
        sum += weight * min;
        i = (i + 1) % pts1.length;
      } while (i != start)
      return sum
    }
    function Resample(points, n) {
      var I = PathLength(points) / (n - 1); // interval length
      var D = 0.0;
      var newpoints = new Array(points[0]);
      for (var i = 1; i < points.length; i++) {
        if (points[i].ID == points[i - 1].ID) {
          var d = Distance(points[i - 1], points[i]);
          if (D + d >= I) {
            var qx =
              points[i - 1].X + ((I - D) / d) * (points[i].X - points[i - 1].X);
            var qy =
              points[i - 1].Y + ((I - D) / d) * (points[i].Y - points[i - 1].Y);
            var q = new Point(qx, qy, points[i].ID);
            newpoints[newpoints.length] = q; // append new point 'q'
            points.splice(i, 0, q); // insert 'q' at position i in points s.t. 'q' will be the next i
            D = 0.0;
          } else D += d;
        }
      }
      if (newpoints.length == n - 1)
        // sometimes we fall a rounding-error short of adding the last point, so add it if so
        newpoints[newpoints.length] = new Point(
          points[points.length - 1].X,
          points[points.length - 1].Y,
          points[points.length - 1].ID,
        );
      return newpoints
    }
    function Scale(points) {
      var minX = +Infinity,
        maxX = -Infinity,
        minY = +Infinity,
        maxY = -Infinity;
      for (var i = 0; i < points.length; i++) {
        minX = Math.min(minX, points[i].X);
        minY = Math.min(minY, points[i].Y);
        maxX = Math.max(maxX, points[i].X);
        maxY = Math.max(maxY, points[i].Y);
      }
      var size = Math.max(maxX - minX, maxY - minY);
      var newpoints = new Array();
      for (var i = 0; i < points.length; i++) {
        var qx = (points[i].X - minX) / size;
        var qy = (points[i].Y - minY) / size;
        newpoints[newpoints.length] = new Point(qx, qy, points[i].ID);
      }
      return newpoints
    }
    function TranslateTo(points, pt) {
      // translates points' centroid to pt
      var c = Centroid(points);
      var newpoints = new Array();
      for (var i = 0; i < points.length; i++) {
        var qx = points[i].X + pt.X - c.X;
        var qy = points[i].Y + pt.Y - c.Y;
        newpoints[newpoints.length] = new Point(qx, qy, points[i].ID);
      }
      return newpoints
    }
    function Centroid(points) {
      var x = 0.0,
        y = 0.0;
      for (var i = 0; i < points.length; i++) {
        x += points[i].X;
        y += points[i].Y;
      }
      x /= points.length;
      y /= points.length;
      return new Point(x, y, 0)
    }
    function PathLength(points) {
      // length traversed by a point path
      var d = 0.0;
      for (var i = 1; i < points.length; i++) {
        if (points[i].ID == points[i - 1].ID)
          d += Distance(points[i - 1], points[i]);
      }
      return d
    }
    function Distance(p1, p2) {
      // Euclidean distance between two points
      var dx = p2.X - p1.X;
      var dy = p2.Y - p1.Y;
      return Math.sqrt(dx * dx + dy * dy)
    }

    const commandPath = {
      'group-left': [
        [
          {X: 0, Y: 0, ID: 1},
          {X: 12, Y: 40, ID: 1},
          {X: 0, Y: 80, ID: 1},
        ],
        [
          {X: 0, Y: 0, ID: 1},
          {X: 22, Y: 40, ID: 1},
          {X: 0, Y: 80, ID: 1},
        ],
        [
          {X: 0, Y: 0, ID: 1},
          {X: 12, Y: 20, ID: 1},
          {X: 0, Y: 80, ID: 1},
        ],
        [
          {X: 0, Y: 0, ID: 1},
          {X: 16, Y: 60, ID: 1},
          {X: 0, Y: 80, ID: 1},
        ],
      ],
      'group-right': [
        [
          {X: 12, Y: 0, ID: 1},
          {X: 0, Y: 40, ID: 1},
          {X: 12, Y: 80, ID: 1},
        ],
        [
          {X: 22, Y: 0, ID: 1},
          {X: 0, Y: 40, ID: 1},
          {X: 22, Y: 80, ID: 1},
        ],
        [
          {X: 12, Y: 0, ID: 1},
          {X: 0, Y: 20, ID: 1},
          {X: 12, Y: 80, ID: 1},
        ],
        [
          {X: 16, Y: 0, ID: 1},
          {X: 0, Y: 60, ID: 1},
          {X: 16, Y: 80, ID: 1},
        ],
      ],
      crossing: [
        // -
        [
          {X: 0, Y: 0, ID: 1},
          {X: 10, Y: 0, ID: 1},
        ],
        //    /
        [
          {X: 0, Y: 0, ID: 1},
          {X: -10, Y: -10, ID: 1},
        ],
        //  \
        [
          {X: 0, Y: 0, ID: 1},
          {X: 10, Y: 10, ID: 1},
        ],
      ],
    };
    const tagPath = {
      // A: [
      //   [
      //     {X: 0, Y: 0, ID: 1},
      //     {X: -5, Y: 10, ID: 1},
      //     {X: 0, Y: 0, ID: 2},
      //     {X: 5, Y: 10, ID: 2},
      //     {X: -3, Y: 5, ID: 3},
      //     {X: 3, Y: 5, ID: 3},
      //   ],
      // ],
      B: [
        [
          {X: 0, Y: 0, ID: 1},
          {X: 0, Y: 40, ID: 1},
          {X: 0, Y: 0, ID: 2},
          {X: 11, Y: 8, ID: 2},
          {X: 20, Y: 10, ID: 2},
          {X: 11, Y: 12, ID: 2},
          {X: 0, Y: 20, ID: 2},
          {X: 11, Y: 28, ID: 2},
          {X: 30, Y: 10, ID: 2},
          {X: 11, Y: 32, ID: 2},
          {X: 0, Y: 40, ID: 2},
        ],
      ],
      // 1: [
      //   [
      //     {X: 12, Y: 5, ID: 1},
      //     {X: 12, Y: 40, ID: 1},
      //     {X: 5, Y: 102, ID: 1},
      //   ],
      //   [
      //     {X: 17, Y: 5, ID: 1},
      //     {X: 15, Y: 47, ID: 1},
      //     {X: 5, Y: 85, ID: 1},
      //   ],
      //   [
      //     {X: 8, Y: 5, ID: 1},
      //     {X: 5, Y: 92, ID: 1},
      //   ],
      // ],
      // 2: [
      //   [
      //     {X: 16, Y: 21, ID: 1},
      //     {X: 30, Y: 5, ID: 1},
      //     {X: 87, Y: 7, ID: 1},
      //     {X: 86, Y: 29, ID: 1},
      //     {X: 62, Y: 59, ID: 1},
      //     {X: 52, Y: 62, ID: 1},
      //     {X: 48, Y: 67, ID: 1},
      //     {X: 16, Y: 76, ID: 1},
      //     {X: 5, Y: 83, ID: 1},
      //     {X: 5, Y: 86, ID: 1},
      //     {X: 28, Y: 81, ID: 1},
      //     {X: 89, Y: 79, ID: 1},
      //   ],
      //   [
      //     {X: 18, Y: 40, ID: 1},
      //     {X: 19, Y: 28, ID: 1},
      //     {X: 31, Y: 9, ID: 1},
      //     {X: 45, Y: 5, ID: 1},
      //     {X: 66, Y: 5, ID: 1},
      //     {X: 72, Y: 8, ID: 1},
      //     {X: 81, Y: 21, ID: 1},
      //     {X: 81, Y: 49, ID: 1},
      //     {X: 77, Y: 59, ID: 1},
      //     {X: 69, Y: 65, ID: 1},
      //     {X: 60, Y: 85, ID: 1},
      //     {X: 46, Y: 97, ID: 1},
      //     {X: 34, Y: 101, ID: 1},
      //     {X: 34, Y: 104, ID: 1},
      //     {X: 22, Y: 106, ID: 1},
      //     {X: 18, Y: 111, ID: 1},
      //     {X: 10, Y: 113, ID: 1},
      //     {X: 5, Y: 120, ID: 1},
      //     {X: 45, Y: 102, ID: 1},
      //     {X: 102, Y: 97, ID: 1},
      //   ],
      //   [
      //     {X: 5, Y: 37, ID: 1},
      //     {X: 8, Y: 18, ID: 1},
      //     {X: 18, Y: 5, ID: 1},
      //     {X: 58, Y: 5, ID: 1},
      //     {X: 72, Y: 15, ID: 1},
      //     {X: 79, Y: 32, ID: 1},
      //     {X: 82, Y: 42, ID: 1},
      //     {X: 82, Y: 70, ID: 1},
      //     {X: 68, Y: 95, ID: 1},
      //     {X: 44, Y: 106, ID: 1},
      //     {X: 27, Y: 106, ID: 1},
      //     {X: 27, Y: 100, ID: 1},
      //     {X: 37, Y: 92, ID: 1},
      //     {X: 75, Y: 88, ID: 1},
      //     {X: 114, Y: 89, ID: 1},
      //   ],
      // ],
      // 3: [
      //   [
      //     {X: 5, Y: 21, ID: 1},
      //     {X: 11, Y: 19, ID: 1},
      //     {X: 18, Y: 9, ID: 1},
      //     {X: 26, Y: 7, ID: 1},
      //     {X: 52, Y: 5, ID: 1},
      //     {X: 69, Y: 13, ID: 1},
      //     {X: 73, Y: 18, ID: 1},
      //     {X: 72, Y: 35, ID: 1},
      //     {X: 53, Y: 54, ID: 1},
      //     {X: 43, Y: 61, ID: 1},
      //     {X: 36, Y: 61, ID: 1},
      //     {X: 27, Y: 70, ID: 1},
      //     {X: 37, Y: 64, ID: 1},
      //     {X: 64, Y: 64, ID: 1},
      //     {X: 85, Y: 85, ID: 1},
      //     {X: 84, Y: 104, ID: 1},
      //     {X: 65, Y: 120, ID: 1},
      //     {X: 36, Y: 127, ID: 1},
      //     {X: 5, Y: 126, ID: 1},
      //   ],
      //   [
      //     {X: 24, Y: 43, ID: 1},
      //     {X: 43, Y: 18, ID: 1},
      //     {X: 59, Y: 5, ID: 1},
      //     {X: 82, Y: 5, ID: 1},
      //     {X: 101, Y: 29, ID: 1},
      //     {X: 101, Y: 46, ID: 1},
      //     {X: 96, Y: 55, ID: 1},
      //     {X: 32, Y: 84, ID: 1},
      //     {X: 34, Y: 77, ID: 1},
      //     {X: 46, Y: 72, ID: 1},
      //     {X: 67, Y: 72, ID: 1},
      //     {X: 84, Y: 79, ID: 1},
      //     {X: 101, Y: 93, ID: 1},
      //     {X: 107, Y: 105, ID: 1},
      //     {X: 110, Y: 124, ID: 1},
      //     {X: 94, Y: 138, ID: 1},
      //     {X: 82, Y: 143, ID: 1},
      //     {X: 20, Y: 143, ID: 1},
      //     {X: 5, Y: 134, ID: 1},
      //   ],
      // ],
      // 4: [
      //   [
      //     {X: 53, Y: 20, ID: 1},
      //     {X: 12, Y: 95, ID: 1},
      //     {X: 8, Y: 112, ID: 1},
      //     {X: 5, Y: 112, ID: 1},
      //     {X: 35, Y: 100, ID: 1},
      //     {X: 133, Y: 84, ID: 1},
      //     {X: 73, Y: 5, ID: 2},
      //     {X: 73, Y: 138, ID: 2},
      //   ],
      //   [
      //     {X: 60, Y: 23, ID: 1},
      //     {X: 52, Y: 30, ID: 1},
      //     {X: 33, Y: 61, ID: 1},
      //     {X: 10, Y: 116, ID: 1},
      //     {X: 5, Y: 138, ID: 1},
      //     {X: 26, Y: 138, ID: 1},
      //     {X: 88, Y: 126, ID: 1},
      //     {X: 140, Y: 122, ID: 1},
      //     {X: 139, Y: 119, ID: 1},
      //     {X: 103, Y: 5, ID: 2},
      //     {X: 100, Y: 5, ID: 2},
      //     {X: 96, Y: 64, ID: 2},
      //     {X: 88, Y: 106, ID: 2},
      //     {X: 88, Y: 168, ID: 2},
      //   ],
      //   [
      //     {X: 45, Y: 17, ID: 1},
      //     {X: 24, Y: 78, ID: 1},
      //     {X: 5, Y: 116, ID: 1},
      //     {X: 66, Y: 112, ID: 1},
      //     {X: 112, Y: 98, ID: 1},
      //     {X: 125, Y: 98, ID: 1},
      //     {X: 98, Y: 5, ID: 2},
      //     {X: 96, Y: 59, ID: 2},
      //     {X: 88, Y: 99, ID: 2},
      //     {X: 90, Y: 162, ID: 2},
      //   ],
      // ],
      circle: [
        [
          {X: 10, Y: 0, ID: 1},
          {X: 8.7, Y: 5, ID: 1},
          {X: 5, Y: 8.7, ID: 1},
          {X: 0, Y: 10, ID: 1},
          {X: -5, Y: 8.7, ID: 1},
          {X: -8.7, Y: 5, ID: 1},
          {X: -10, Y: 0, ID: 1},
          {X: -8.7, Y: -5, ID: 1},
          {X: -5, Y: -8.7, ID: 1},
          {X: 0, Y: -10, ID: 1},
          {X: 5, Y: -8.7, ID: 1},
          {X: 8.7, Y: -5, ID: 1},
          {X: 10, Y: 0, ID: 1},
        ],
        [
          {X: 46, Y: 14, ID: 1},
          {X: 30, Y: 18, ID: 1},
          {X: 11, Y: 37, ID: 1},
          {X: 5, Y: 59, ID: 1},
          {X: 6, Y: 76, ID: 1},
          {X: 27, Y: 95, ID: 1},
          {X: 44, Y: 99, ID: 1},
          {X: 66, Y: 98, ID: 1},
          {X: 106, Y: 79, ID: 1},
          {X: 125, Y: 52, ID: 1},
          {X: 130, Y: 38, ID: 1},
          {X: 126, Y: 24, ID: 1},
          {X: 100, Y: 9, ID: 1},
          {X: 85, Y: 5, ID: 1},
          {X: 52, Y: 5, ID: 1},
          {X: 45, Y: 9, ID: 1},
        ],
      ],
      tirangleUp: [
        [
          {X: 10, Y: 0, ID: 1},
          {X: 0, Y: -17.3, ID: 1},
          {X: -10, Y: 0, ID: 1},
          {X: 10, Y: 0, ID: 1},
        ],
      ],
      tirangleDown: [
        [
          {X: 10, Y: 0, ID: 1},
          {X: 0, Y: 17.3, ID: 1},
          {X: -10, Y: 0, ID: 1},
          {X: 10, Y: 0, ID: 1},
        ],
        [
          {X: 62, Y: 5, ID: 1},
          {X: 55, Y: 12, ID: 1},
          {X: 36, Y: 48, ID: 1},
          {X: 5, Y: 119, ID: 1},
          {X: 50, Y: 118, ID: 1},
          {X: 69, Y: 113, ID: 1},
          {X: 88, Y: 113, ID: 1},
          {X: 88, Y: 110, ID: 1},
          {X: 116, Y: 110, ID: 1},
          {X: 60, Y: 6, ID: 2},
          {X: 73, Y: 43, ID: 2},
          {X: 76, Y: 43, ID: 2},
          {X: 81, Y: 52, ID: 2},
          {X: 85, Y: 67, ID: 2},
          {X: 88, Y: 67, ID: 2},
          {X: 92, Y: 93, ID: 2},
          {X: 102, Y: 115, ID: 2},
        ],
      ],
      square: [
        [
          {X: 10, Y: 10, ID: 1},
          {X: -10, Y: 10, ID: 1},
          {X: -10, Y: -10, ID: 1},
          {X: 10, Y: -10, ID: 1},
          {X: 10, Y: 10, ID: 1},
        ],
        [
          {X: 9, Y: 15, ID: 1},
          {X: 11, Y: 102, ID: 1},
          {X: 9, Y: 9, ID: 2},
          {X: 59, Y: 8, ID: 2},
          {X: 64, Y: 5, ID: 2},
          {X: 76, Y: 7, ID: 2},
          {X: 78, Y: 98, ID: 2},
          {X: 5, Y: 88, ID: 3},
          {X: 81, Y: 90, ID: 3},
        ],
      ],
    };

    let tag = {
      uuid: v1(),
      pathList: [],
      targetIDs: new Set(),
      type: '', // command|tag
      info: {}, // for command only, info differs according to command type
      position: {x: 0, y: 0},
      valid: true,
    };
    function normalizedPathList(pathList) {
      let top = 10000,
        left = 10000,
        right = 0,
        bottom = 0;
      // get bounding box
      for (const path of pathList) {
        for (const pos of path) {
          top = top > pos.y ? pos.y : top;
          bottom = bottom < pos.y ? pos.y : bottom;
          left = left > pos.x ? pos.x : left;
          right = right < pos.x ? pos.x : right;
        }
      }
      // normalize coords
      for (const path of pathList) {
        for (const pos of path) {
          pos.x -= left - 5; // add some padding
          pos.y -= top - 5;
        }
      }
      tag.position.x = left;
      tag.position.y = top;
      return [{top: top - 10, bottom, left: left - 10, right}, pathList]
    }

    function buildTag(type, pathData) {
      Object.entries(pathData).forEach(([name, sampleList]) => {
        sampleList.forEach((l) => {
          let lastID = 1;
          let newL = [[]];
          for (let p of l) {
            if (p.ID > lastID) {
              newL[lastID] = [];
              lastID = p.ID;
            }
            newL[lastID - 1].push({x: p.X, y: p.Y});
          }
          const [dims, xxx] = normalizedPathList(newL);

          _tags.push({
            ...tag,
            uuid: 'PREDEFINED: ' + name,
            pathList: newL,
            dims: dims,
            type: type,
            name: name, // for command tag only
          });
        });
      });
    }
    console.log('+++++++', Object.entries(tagPath));
    let _tags = [];
    buildTag('tag', tagPath);
    buildTag('command', commandPath);
    console.log('_tags ', _tags);
    let TAGS = _tags;

    // Object.entries(tagPath).map(([name, l]) => {
    //   return {
    //     uuid: uuidv1(),
    //     pathList: l,
    //     targetIDs: new Set(),
    //     type: '', // command|tag
    //     info: {}, // for command only, info differs according to command type
    //     position: {x: 0, y: 0},
    //     valid: true,
    //   }
    // })
    console.log('GENERATED Predefined TGS:', TAGS);

    //TODO move all email items into a single list and use a logical folder tree to "Contain" them.
    let EMAILS = {
      inbox: [
        {
          title: 'Billing for March 2020',
          content: `Bonjour,
      
      Here is your billing info for the month of March:
      
      amount: 5000      1888
      date:2019-09-09   2020-04-05
      account: 1242424     1111

      Regards,
      XXXX
      `,
          time: new Date('2020-05-03'),
          from: 'sales@car.com',
          to: 'ming@me.com',
          transacs: [
            {account: '1242424', type: 'other', amount: 5000},
            {account: '1111', type: 'other', amount: 1888},
          ],
        },
        {
          title: 'New payment info',
          content: `amount: 5000      1888
      date:2019-09-09   2020-04-05
      account: 1242424     1111
      `,
          time: new Date('2020-05-02'),
          from: 'sales@car.com',
          to: 'ming@me.com',
          transacs: [
            {account: '1242424', type: 'other', amount: 5000},
            {account: '1111', type: 'other', amount: 1888},
          ],
        },
        {
          title: 'Job Application from Juliet',
          content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque nemo earum, accusamus cupiditate nisi eum quis similique reiciendis ipsa vitae autem quasi necessitatibus ratione mollitia dicta. Totam quam perspiciatis culpa.
      `,
          time: new Date('2020-06-22'),
          from: 'juliet@gmail.com',
          to: 'hr@company.com',
        },
        {
          title: 'Job Application from Brian',
          content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque nemo earum, accusamus cupiditate nisi eum quis similique reiciendis ipsa vitae autem quasi necessitatibus ratione mollitia dicta. Totam quam perspiciatis culpa.
      `,
          time: new Date('2020-06-07'),
          from: 'biran@yahoo.com',
          to: 'hr@company.com',
        },
        {
          title: 'Round 2: Job Application from Brian',
          content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque nemo earum, accusamus cupiditate nisi eum quis similique reiciendis ipsa vitae autem quasi necessitatibus ratione mollitia dicta. Totam quam perspiciatis culpa.
      `,
          time: new Date('2020-07-08'),
          from: 'hr2@company.com',
          to: 'hr@company.com',
        },
        {
          title: 'FW: Job Application from Juliet',
          content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque nemo earum, accusamus cupiditate nisi eum quis similique reiciendis ipsa vitae autem quasi necessitatibus ratione mollitia dicta. Totam quam perspiciatis culpa.
      `,
          time: new Date('2020-06-27'),
          from: 'brian@yahoo.com',
          to: 'hr@company.com',
        },
        {
          title: 'Follow Up: Job Application from Juliet',
          content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque nemo earum, accusamus cupiditate nisi eum quis similique reiciendis ipsa vitae autem quasi necessitatibus ratione mollitia dicta. Totam quam perspiciatis culpa.
      `,
          time: new Date('2020-07-07'),
          from: 'juliet@gmail.com',
          to: 'hr@company.com',
        },
        {
          title: 'Just a ramdom email',
          content: `Hello,

      blabalbla blabla bla bla

      bye
      `,
          time: new Date('2020-07-03'),
          from: 'sneakyguy@outlook.com',
          to: 'me@me.com',
        },
        {
          title: 'Invoice #June,2020',
          content: '',
          time: new Date('2020-05-01'),
          from: 'sales@car.com',
          to: 'ming@me.com',
          transacs: [
            {account: '2222', type: 'food', amount: 200},
            {account: '1111', type: 'transport', amount: 18},
          ],
        },
        {
          title: 'Welcome to the "All-exsitu" mailing list',
          content: `Welcome to the All-exsitu@lri.fr mailing list!

      To post to this list, send your message to:
      
        all-exsitu@lri.fr
      
      General information about the mailing list is at:
      
        http://serveur-listes.lri.fr/cgi-bin/mailman/listinfo/all-exsitu
      
      If you ever want to unsubscribe or change your options (eg, switch to
      or from digest mode, change your password, etc.), visit your
      subscription page at:
      
        http://serveur-listes.lri.fr/cgi-bin/mailman/options/all-exsitu/yyaomingm%40outlook.com
      
      
      You can also make such adjustments via email by sending a message to:
      
        All-exsitu-request@lri.fr
      
      with the word help
      
    `,
          time: new Date('2020-06-19'),
          from: 'exsitu@test.com',
          to: 'ming@me.com',
        },
        {
          title: 'To the Graduating Class of 2020',
          content: `Dear Ming,
 
      On this 10th anniversary of EIT Digital, we would like to congratulate you as you are about to step into the vibrant and international community of Alumni of the EIT Digital Master School! We are very proud of those of you who have already completed their studies as well as salute the students on the finishing line towards achieving their academic goals in these exceptional circumstances!
       
      Graduation is a moment to celebrate and honour the time, energy and effort you have committed to earn your degree. Unfortunately, in light of the significant uncertainty surrounding the current pandemic, EIT Digital Master School had to make a difficult decision to postpone the graduation ceremony which was scheduled to take place in Madrid, Spain, in November 2020.
       
      We understand this is disappointing news for you and we truly regret the inconvenience. We believe this is the best course of action to support the safety and wellbeing of you and your guests, as well as our staff and the wider community. As you know, our student community, along with friends and family, come from all over the world and this large event poses a potential negative health impact on those attending. Further, it is difficult to predict how the situation unfolds later this year and whether different governments will have travel restrictions, as well as restrictions on large gatherings.
       
      While we will not be able to welcome you in Madrid later this year, we have the intention to welcome you in the same location at the beginning of next year, 2021. Details of these arrangements will be communicated to you as soon as possible, depending on how the COVID-19 situation develops.
       
      Please contact your exit university for questions about the local graduation ceremony.
       
      We appreciate your patience, as we navigate all the changes together with you during these unprecedented times. If you have any questions, please reach out to us at masterschool@eitdigital.eu.
       
      Stay safe!
      
    `,
          time: new Date('2020-06-18'),
          from: 'eit@test.com',
          to: 'ming@me.com',
        },
        {
          title: '[All-exsitu] [Seminar] Kate and Zack',
          content: `Dear all,

    Tomorrow, Kate and Zack will both present their internship work:
    - Kate will present her research on creative writing and how technology can support the process. The presentation will summarize the literature and thematic analysis findings. She plans to show a video prototype and would be interested in hearing any feedback, that you can send her through this link. 
    - Zack will present his research on bimanual tools for creativity – from stories of the creative process to several technology probes he designed. 
    
    
    The seminar will be at 10h on this link: https://meet.jit.si/ExSitu
    
    
    
    Have a nice day,
    
    
    
    Téo
    
    `,
          time: new Date('2020-06-08'),
          from: 'user1@test.com',
          to: 'me@me.com',
        },
        {
          title: "Let's get you started with Listen Notes",
          content: `
      This is Wenbin, the founder of Listen Notes. I'm so excited to welcome you to the Listen Notes community (User #93645)! You may want to edit your profile page (please log in first) for a better presence in the podcast universe :) 
      You can search ALL podcast episodes by people or topics via listennotes.com or Chrome Extension, e.g., Coronavirus. 
      
      Tired of listening to a few top-chart podcasts from big publishers? Discover interesting niche podcasts in Listen Community! 
      

      We also provide some powerful tools to help you discover podcasts and explore podcast meta data, which can be found at the top right "..." menu: 
       

      If you have questions about using Listen Notes, you can visit listennotes.com/help or just reply this email - I reply every email myself. 
      
      PS: If you own a podcast, you can claim and manage it on Listen Notes or post classified ads to find guests, sponsors, co-hosts, cross-promotion... 
      
      PS: Check out our short-form audio project: inca.fm - it's like micro-podcasting :) 
      
      PS: You can embed a podcast player or a playlist to your own website, blog, or online courses. 
      
      - Wenbin 
      Founder and CEO of Listen Notes, Inc. 
      
        `,
          time: new Date('2020-04-08'),
          from: 'test@test.com',
          to: 'me@me.com',
        },
        {
          title: 'CodePen Verification',
          content: `Thanks for signing up for CodePen! 

      We're happy you're here. Let's get your email address verified: 
      Click to Verify Email 
      Verifying your email address enables these features:
      •	Full Page View
      •	Collections
      •	Commenting on Pens, Projects, and Posts
      •	Your Pens are searchable on CodePen
      Getting Started on CodePen 
      Ready to get coding? Here are a few links to help you! 
      •	Quick overview of what you can do with CodePen
      Take a guided tour through the Pen editor
      
        `,
          time: new Date('2019-05-05'),
          from: 'no-reply@codepen.com',
          to: 'me@me.com',
        },
        {
          title: 'UX in MR - UXinMR / #17',
          content: `Hi guys!
 
        In this issue, you won't read about Travis Scott event in Fortnite or Magic Leap, but you will find interaction demos, MR design guidelines, tutorials, projects and more.
        
        Vova Kurbatov
        `,
          time: new Date('2020-03-05'),
          from: 'test@test.com',
          to: 'me@me.com',
        },
        {
          title: 'Lime est de retour !',
          content: `Alors que les villes commencent à assouplir leurs restrictions de circulation, Lime est ravie de rétablir son service. Nous sommes de retour pour que vous puissiez utiliser Lime quand vous le souhaitez.
      Comme nos trottinettes sont prévues pour des balades en plein air, c'est l'un des moyens les plus sûrs de se déplacer. Pour des recommandations en matière de sécurité, consultez notre guide. 
      
        `,
          time: new Date('2020-05-15'),
          from: 'test@test.com',
          to: 'me@me.com',
        },
        {
          title: 'AD Microsoft 365 ',
          content: `
 
      2020年初的新冠病毒黑天鹅事件，让各大企业开启了远程办公模式，线上协同办公成为一段时间内很多人最主要的工作和协作方式。尽管随着新冠病毒疫情情况陆续好转，各地逐渐有序复产复工，线上协同办公依然以高效、便捷等优势成为很多办公室中职场人士的“利器”。然而如何在协同办公过程中充分提升企业的生产力和效率，通过更紧密的协作消除传统工作方式中存在的诸多局限，仍是很多企业面临的挑战！ 
      很多企业依然在使用老版本 Office 2010。Office 2010 在促进协作方面的功能在10年间虽有完善但滞后于新时代的企业需求，对于企业提供协作效率不利。再加上这个版本即将在2020年10月13日停止技术支持，微软建议客户尽快升级到 Microsoft 365（原名 Office 365）。 


      Microsoft 365（原名 Office 365）基于云的生产力平台，充分融合了云平台在集成方面的优势，提供了行业领先的生产力应用（Word、Excel、PowerPoint 等）、智能云服务以及一流的安全性。Microsoft 365 不仅提供了始终维持最新版本的软件，同时还具备丰富的联机协作办公功能，可以帮助企业用户用更简单、快捷的操作体验现代化协同办公的强大之处。 
      
        `,
          time: new Date('2020-04-28'),
          from: 'microsoft@test.com',
          to: 'me@me.com',
        },
        {
          title: "We're updating our Terms",
          content: `Hi, 

      We’re making some small changes to our Terms of Service, Payments Terms of Service, and Privacy Policy (collectively, “Terms”). You can review the new Terms by clicking here. We’ve also put up information to explain these changes in more detail on our Terms of Service Update Page. Both the old and new versions of the Terms can be found at the Terms of Service, Payments Terms of Service, and Privacy Policy pages through January 24, 2020. 
      Thank you for being a member of our global community. 
      Thanks, 
      The Airbnb Team 
      
        `,
          time: new Date('2020-03-05'),
          from: 'test@test.com',
          to: 'me@me.com',
        },
        {
          title: 'Welcome to Roam',
          content: `Welcome to Roam!

      Thanks for signing up for early access.
      
      If you haven't already, create your account here.
      
      Your account will start with just a blank page in your daily notes, if you want some inspiration for how to organize your Roam, check out the help database
      
      And if you need more help figuring out how to use Roam to organize your thoughts, you can book a call with our CEO and he can help you move notes over into Roam and figure out what workflows will be most helpful.  You can book a time on his calendar here
      
      Feel free to respond to this email with any questions
      
      Sincerely,
      
      The Roam Team
      
        `,
          time: new Date('2020-04-18'),
          from: 'roam@test.com',
          to: 'me@me.com',
        },
        {
          title: 'Welcome',
          content: `Thank you for creating an account with Bitwarden. You may now log in with your new account. 
      Did you know that Bitwarden is free to sync with all of your devices? Download Bitwarden today on: 
      Desktop 
      Access Bitwarden on Windows, macOS, and Linux desktops with our native desktop application. 
       
      Web Browser 
      Integrate Bitwarden directly into your favorite browser. Use our browser extensions for a seamless browsing experience. 
       
      Mobile 
      Take Bitwarden on the go with our mobile apps for your phone or tablet device. 
       
      Web 
      Stuck without any of your devices? Using a friend's computer? You can access your Bitwarden vault from any web enabled device by using the web vault. 
      vault.bitwarden.com 
      
      If you have any questions or problems you can email us from our website at https://bitwarden.com/contact. 
      
      Thank you!
      The Bitwarden Team 
       `,
          time: new Date('2020-04-08'),
          from: 'test@test.com',
          to: 'me@me.com',
        },
        {
          title: 'GraphQL live Docs | 50% OFF annual | Product Hunt',
          content: `Hello, 

      We have three major announcements for you.
      
      â€¢	50% OFF all annual plans coupon expires on 30.04. Use ILOV3GRAPHQL in checkout.
      â€¢	GraphQL Live Docs have been added to our app. Check below. It's only visible for paid users.
      We're live on Product Hunt with OS GraphQL Centaur CLI - This tool allows to bootstrap backend applications from GraphQL schema, you can auto generate basic resolvers like CRUD.
      
       `,
          time: new Date('2020-04-18'),
          from: 'test@test.com',
          to: 'me@me.com',
        },
        {
          title: 'New insurance factsheet',
          content: `Dear students,
 

      I hope all is well with you and your nearest and dearest!
      
      We have received an update from the insurance provider, Kammarkollegiet, that scanned insurance claims and receipts should be sent to the MSO by email even when we are back at the office (we are still working from home due to the pandemic). It means that you no longer need to send in claim forms and receipts in hard copy by regular mail to Stockholm.
      
      However, please save the original receipts and relevant documentation for at least 6 months after the date of the incident/visit to the doctor. Kammarkollegiet might ask you to send the originals by regular post as part of a random control check.
      
      We also created a file sharing space in the online portal (called Plaza) for you to be able to access commonly used EIT Digital documents at any time when needed. We have saved the updated factsheet here.
      
      Please let me know if you have any questions.
      
      
       
      
      Kind regards,
      
       `,
          time: new Date('2020-05-11'),
          from: 'test@test.com',
          to: 'me@me.com',
        },
      ].map((m, i) => {
        return {...m, id: i, read: false}
      }),
      flagged: new Array(7)
        .fill({
          title: 'mail title',
          content:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed temporDonec tincidunt diam felis, sed tempor est pellentesque vel. Mauris tempus convallis.',
          time: new Date(),
          from: 'test@test.com',
          to: 'me@me.com',
        })
        .map((m, i) => {
          return {...m, title: i + m.title, id: i, read: false}
        }),
      drafts: new Array(5)
        .fill({
          title: 'mail title',
          content:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus conv diam felis, sed tempor est pellentesque vel. Mauris tempus convallis.',
          time: new Date(),
          from: 'test@test.com',
          to: 'me@me.com',
        })
        .map((m, i) => {
          return {...m, title: i + m.title, id: i, read: true}
        }),
      sent: new Array(3)
        .fill({
          title: 'mail title',
          content:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felisipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felisipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felisipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felisipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felisipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felis, sed tempor est pellentesque vel. Mauris tempus convallis.',
          time: new Date(),
          from: 'test@test.com',
          to: 'me@me.com',
        })
        .map((m, i) => {
          return {...m, title: i + m.title, id: i, read: true}
        }),
      spam: new Array(2)
        .fill({
          title: 'mail title',
          content:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diasum dolor sit amet, consectetur adipiscing elit. Donec tincidunt diam felis, sed tempor est pellentesque vel. Mauris tempus convallis.',
          time: new Date(),
          from: 'test@test.com',
          to: 'me@me.com',
        })
        .map((m, i) => {
          return {...m, title: i + m.title, id: i, read: false}
        }),
      archive: [],
    };

    function map(v, inStart, inEnd, outStart, outEnd) {
      const inSpan = inEnd - inStart;
      let percent = (v - inStart) / inSpan;
      const outSpan = outEnd - outStart;
      if (v > inEnd) percent = 1;
      return percent * outSpan + outStart
    }

    console.log('TAGS', TAGS);
    let canvasTwo; // Two.js instance of CANVAS
    let twoInvokedMenu;

    let originEmails = {};
    Object.entries(EMAILS).map(([folder, items]) => {
      originEmails[folder] = items.map((item) => {
        item.wordCount = item.content.split(/\s/).length;
        item.id = v1();
        item.groupIDs = new Set(); // groups the email belongs to
        item.tagIDs = new Set(); // tags attached to the email
        return item
      });
    });
    let emails = {...originEmails};

    let recognizer = new PDollarRecognizer();
    function Point$1(x, y, id) {
      // constructor
      this.X = x;
      this.Y = y;
      this.ID = id; // stroke ID to which this point belongs (1,2,3,etc.)
    }

    let tags = {...TAGS}; // tag-uuid -> tag
    let tagsByInkID = {}; // inkID->tags
    let tagSearchRslt;
    let groups = {}; // id->group:{id:string,memberIDs:emailIDs,color:string,subGroups:[[a,b,c],[d,e]]}
    // subGroups are used to generate fragmented group lines, by default, it includes all members

    let isDrawing = false;
    let touchedElms = new Set(); // all UI elements that was underneath the ink while drawing
    let currentOnInkCmd;
    let currentGroupID;
    let currentMail = emails.inbox[0];
    let viewingSubElm = undefined;

    //// function ////////////
    //////////// function ////
    function setCurrentGroupID(id) {
      currentGroupID = id;
    }
    function setEmails(m) {
      emails = {...m};
      console.log('setEmails');
    }
    function getEmail(id) {
      for (const l of Object.values(emails)) {
        console.log(l);
        for (const v of l) {
          if (v.id === id) return v
        }
      }
      return null //not found
    }
    function getEmails(ids) {
      return Object.values(emails).reduce(
        (acc, cur) => acc.concat(cur.filter((email) => ids.includes(email.id))),

        [],
      )
    }
    function addGroup(group) {
      //TODO  check if a group with same members already exists
      const groupID = `group-${group.type}:` + v1();
      group.path._renderer.elem.setAttribute('data-id', groupID);
      Object.values(emails).forEach((list) => {
        list
          .filter((mail) => group.memberIDs.includes(mail.id))
          .forEach((mail) => mail.groupIDs.add(groupID));
      });
      groups[groupID] = {...group, id: groupID};
    }

    let trigger = false;
    function setTrigger(val) {
      trigger = val;
    }
    function bringTogether(groupID) {
      // console.log('bring together')
      let tmpEmails = {};
      Object.keys(emails).forEach((folder) => {
        console.log(
          'before bringTogether',
          emails[folder].map((e) => e.title),
        );
        let firstEmailID = false;
        let gotFirst = false;
        // console.log('folder', folder, 'emails', emails[folder])
        let emailsInGroup = emails[folder].filter((email) => {
          const found = groups[groupID].memberIDs.includes(email.id);
          if (found && !gotFirst) {
            console.log('GOT FIRST');
            gotFirst = true;
            firstEmailID = email.id;
          }
          return found
        });
        let emailsNotInGroup = emails[folder].filter((email) => {
          const found = !groups[groupID].memberIDs.includes(email.id);
          return found
        });

        const indexOfFirstEmailInGroup = emails[folder].indexOf(
          emails[folder].filter((e) => e.id == firstEmailID)[0],
        );
        console.log(
          'memberids',
          groups[groupID].memberIDs,
          'in',
          emailsInGroup.map((e) => e.id),
          'notin',
          emailsNotInGroup,
          'firstIndex',
          indexOfFirstEmailInGroup,
        );
        emailsNotInGroup.splice(indexOfFirstEmailInGroup, 0, ...emailsInGroup);
        console.log(
          'after bringTogether',
          emailsNotInGroup.map((e) => e.title),
        );
        tmpEmails[folder] = [...emailsNotInGroup];
      });
      setEmails({...tmpEmails});
      setTimeout(() => {
        trigger = true;
      }, 100);
    }
    function removeGroup(id) {
      //remove SVG
      groups[id].g.remove();
      // remove groupID in all member emails
      Object.values(emails).forEach((list) => {
        list
          .filter((mail) => groups[id].memberIDs.includes(mail.id))
          .forEach((mail) => mail.groupIDs.delete(id));
      });
      // delete group obj
      delete groups[id];
      twoInvokedMenu.update();
    }
    function redrawAllGroups() {
      Object.keys(groups).map((id) => redrawGroup(id));
    }
    function redrawGroup(id) {
      let isInSubGroup = false;
      let subMemberIDs = [];
      const group = groups[id];
      console.log('redraw group', group);
      group.allPath.forEach((p) => group.g.remove(p));
      group.allPath.length = 0;
      // traverse all emails
      const linewidth = 2;
      // const color =
      //   '#' + Math.round((0.2 + Math.random()) * 0x777777).toString(16) + '88'
      let path;
      let x,
        y,
        h = 0,
        w;
      group.subGroupCount = 0;
      Object.values(emails).forEach((folder) =>
        folder.forEach((mail) => {
          if (group.memberIDs.includes(mail.id)) {
            // 进入一个sub group
            if (!isInSubGroup) {
              group.subGroupCount++;
              path = createPath(group.color, linewidth, false);
              const firstElm = group.memberDoms[group.memberIDs.indexOf(mail.id)];
              x = firstElm.offsetLeft + linewidth;
              y = firstElm.offsetTop + linewidth;
              h = 0;
            }
            const elm = group.memberDoms[group.memberIDs.indexOf(mail.id)];
            console.log('redraw elm', elm);
            h += elm.clientHeight;
            w = elm.clientWidth;
            subMemberIDs.push(mail.id);
            isInSubGroup = true;
          } else {
            //刚结束一个 sub group
            if (isInSubGroup) {
              console.log('end subgroup', subMemberIDs);
              group.allPath.push(path);
              group.subGroupDims.push({x: x, y: y, h: h});
              const occupied = getEmails(subMemberIDs).reduce((acc, cur) => {
                const tmp = Array.from(cur.groupIDs.values())
                  .filter((id) => {
                    return groups[id].type === group.type
                  })
                  .map((id) => groups[id].lineLevel);
                acc = [...acc, ...tmp];
                return acc
              }, []);
              const lineLevel = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].filter(
                (l) => !occupied.includes(l),
              )[0];
              makeGroupLine(
                group.type === 'r' ? x : w,
                y,
                4,
                h,
                group.type,
                path,
                lineLevel,
              );
              group.g.add(path);
            }
            subMemberIDs.length = 0;
            isInSubGroup = false;
          }
        }),
      );

      group.two.update();
      // must call after update, because DOM with be re-generated by TWO.js
      const c = group.g._renderer.elem.children;
      for (let index = 0; index < c.length; index++) {
        const element = c[index];
        element.style.stroke = group.color;
      }
      group.g._renderer.elem.onpointerenter = (e) => {
        for (let index = 0; index < c.length; index++) {
          const element = c[index];
          element.style.stroke = '#333';
          element.style.strokeWidth = '3px';
        }
      };
      group.g._renderer.elem.onpointerout = (e) => {
        for (let index = 0; index < c.length; index++) {
          const element = c[index];
          element.style.stroke = group.color;
          element.style.zIndex = 10 - group.lineLevel;
          element.style.strokeWidth = '2px';
        }
      };
      group.g._renderer.elem.style.pointerEvents = 'visible';
      group.g._renderer.elem.style.zIndex = 10 - group.lineLevel;
      // leader-line related
      if (group.subGroupCount >= 2) {
        group.allPath.forEach((p) => {
          let leaderLines = [];
          const leaderLineG = new Two.Group();
          p._renderer.elem.onpointerenter = (e) => {
            viewingSubElm = p._renderer.elem;
            //iterate over all line-grouping widgets
            for (let i = 0; i < group.allPath.length; i++) {
              const pp = group.allPath[i]._renderer.elem;
              if (viewingSubElm != pp) {
                const selfBoundingRect = viewingSubElm.getBoundingClientRect();
                const otherBoundingRect = pp.getBoundingClientRect();

                /**
                 * LeaderLine V1
                 *
                 */
                // const line = new LeaderLine(viewingSubElm, pp, {
                //   path: 'arc',
                //   size: 3,
                //   // startSocket: 'right', //group.type === 'l' ? 'right' : 'left',
                //   // endSocket: 'right', //group.type === 'l' ? 'right' : 'left',
                //   startPlug: 'disc',
                //   endPlug: 'disc',
                // })
                // line.setOptions({
                //   startSocketGravity:
                //     selfBoundingRect.y < otherBoundingRect.y ? [30, 20] : [30, -20],
                //   endSocketGravity:
                //     selfBoundingRect.y < otherBoundingRect.y
                //       ? [-30, -20]
                //       : [30, 20],
                //   endSocket:
                //     selfBoundingRect.y < otherBoundingRect.y ? 'left' : 'right',
                // })
                // leaderLines.push(line)

                /**
                 * LeaderLine V2
                 *
                 */
                const startX = selfBoundingRect.right;
                const startY = selfBoundingRect.top + selfBoundingRect.height / 2;
                const endX = otherBoundingRect.right;
                const endY = otherBoundingRect.top + otherBoundingRect.height / 2;
                const lineLen = Math.abs(endY - startY);
                const midX = (startX + endX) / 2 + map(lineLen, 0, 3000, 0, 500);
                const midY = (startY + endY) / 2;
                console.log(
                  'start',
                  selfBoundingRect,
                  'e',
                  otherBoundingRect,
                  'mid',
                  midX,
                  midY,
                );
                let lLine = new Two.Path(
                  [
                    new Two.Vector(startX, startY),
                    // new Two.Vector((startX + midX) / 2, (startY + midY) / 2),
                    new Two.Vector(midX, midY),
                    // new Two.Vector((midX + endX) / 2, (midY + endY) / 2),
                    new Two.Vector(endX, endY),
                  ],
                  false,
                  true,
                );
                lLine.cap = lLine.join = 'round';
                lLine.curved = true;
                lLine.noFill().stroke = '#fa5';
                lLine.linewidth = 3;
                // group.g.add(lLine)
                canvasTwo.add(lLine);
                // }
                // else{

                // }
                leaderLines.push(lLine);
              }
              canvasTwo.update();
              // group.two.update()
            }
          };
          p._renderer.elem.onpointerout = (e) => {
            viewingSubElm = undefined;
            leaderLines.forEach((l) => l.remove());
            leaderLines.length = 0;
            // group.two.update()
            canvasTwo.update();
          };
        });
      }
    }

    function createPath(color, lineWidth, curved) {
      let path = new Two.Path([], false, true);
      path.curved = curved;
      path.cap = path.join = 'round';
      path.noFill().stroke = color;
      path.linewidth = lineWidth;
      return path
    }
    function setTwoInvokedMenu(two) {
      twoInvokedMenu = two;
    }
    function setCanvasTwo(two) {
      canvasTwo = two;
    }
    function setCurrentOnInkCmd(callback) {
      currentOnInkCmd = callback;
      console.log('set', currentOnInkCmd);
    }
    function setDrawing(flag) {
      isDrawing = flag;
    }
    function addFolder(name) {
      emails[name] = [];
    }
    function addMail(folder, mail) {
      const m = {...mail, id: emails[folder].length};
      emails[folder].push(m);
    }
    function setCurrentMail(mail) {
      currentMail = mail;
    }
    function getTag(uuid) {
      return tags[uuid]
    }
    function recognize(tag, amount = 1) {
      let points = [];
      for (const [i, path] of tag.pathList.entries()) {
        points = [...points, ...path.map((p) => new Point$1(p.x, p.y, i + 1))];
      }

      tagSearchRslt = recognizer.Recognize(points, amount);
      return tagSearchRslt
    }

    function addTag(tag) {
      let points = [];
      for (const [i, path] of tag.pathList.entries()) {
        points = [...points, ...path.map((p) => new Point$1(p.x, p.y, i + 1))];
        console.log(points, 'sdssdgsakldgaj;sdklj');
      }
      console.log('before adding', points);
      recognizer.AddGesture(tag.uuid, points);

      if (tags.hasOwnProperty(tag.uuid)) {
        for (let id of tag.targetIDs) {
          tags[tag.uuid].targetIDs.add(id);
        }
        tag = tags[tag.uuid];
      } else {
        tags[tag.uuid] = tag;
      }
      console.log('Recognizer: Added gesture', tag.uuid, points);
      tag.targetIDs.forEach((id) => {
        if (tagsByInkID[id] == undefined) {
          tagsByInkID[id] = {};
        }
        tagsByInkID[id][tag.uuid] = tag;
      });
      console.log('TAGS By ID:', tagsByInkID);
    }
    function findByTagUUID(uuid) {
      let tmp = {};
      console.log('search email with tag', uuid);
      for (const folder in emails) {
        if (emails.hasOwnProperty(folder)) {
          tmp[folder] = emails[folder].filter(
            (mail) =>
              tags[uuid] &&
              tags[uuid].targetIDs.has(folder + 'mail-item' + mail.id),
          );
          console.log('foler', folder, tags[uuid]);
        }
      }
      // emailResults = tmp
      console.log('tag search result emails:', tmp);
      return tmp
    }
    function clearSearch() {
      emailResults = emails;
    }
    function search(query, cap = false) {
      query = cap ? query : query.toLowerCase();
      let result = {};
      for (const key in emails) {
        if (!emails.hasOwnProperty(key)) continue
        const folder = [...emails[key]];
        result[key] = [];
        for (const mail of folder) {
          if (cap) {
            mail.title = mail.title.toLowerCase();
            mail.content = mail.content.toLowerCase();
          }
          if (mail.title.includes(query) || mail.content.includes(query)) {
            result[key].push(mail);
          }
        }
      }
      return result
    }

    TAGS.forEach((tag) => addTag(tag));
    console.log(TAGS, 'ADDED predefined tags', tags);

    //---------
    function makeGroupLine(x, y, w, h, type, path, level = 1) {
      const xOffset = type === 'r' ? 0 : 5;
      const levelOffset = 7 * level;
      // w = levelOffset;
      x += xOffset;
      path.vertices.push(
        new Two.Vector(type === 'r' ? x - levelOffset + w : x + levelOffset - w, y),
      );
      path.vertices.push(
        new Two.Vector(type === 'r' ? x - levelOffset : x + levelOffset, y),
      );
      path.vertices.push(
        new Two.Vector(type === 'r' ? x - levelOffset : x + levelOffset, y + h),
      );
      path.vertices.push(
        new Two.Vector(
          type === 'r' ? x - levelOffset + w : x + levelOffset - w,
          y + h,
        ),
      );
    }

    var model = /*#__PURE__*/Object.freeze({
        __proto__: null,
        originEmails: originEmails,
        get emails () { return emails; },
        tags: tags,
        tagsByInkID: tagsByInkID,
        get tagSearchRslt () { return tagSearchRslt; },
        groups: groups,
        get isDrawing () { return isDrawing; },
        touchedElms: touchedElms,
        get currentOnInkCmd () { return currentOnInkCmd; },
        get currentGroupID () { return currentGroupID; },
        setCurrentGroupID: setCurrentGroupID,
        setEmails: setEmails,
        getEmail: getEmail,
        getEmails: getEmails,
        addGroup: addGroup,
        get trigger () { return trigger; },
        setTrigger: setTrigger,
        bringTogether: bringTogether,
        removeGroup: removeGroup,
        redrawAllGroups: redrawAllGroups,
        redrawGroup: redrawGroup,
        setTwoInvokedMenu: setTwoInvokedMenu,
        setCanvasTwo: setCanvasTwo,
        setCurrentOnInkCmd: setCurrentOnInkCmd,
        setDrawing: setDrawing,
        addFolder: addFolder,
        addMail: addMail,
        setCurrentMail: setCurrentMail,
        getTag: getTag,
        recognize: recognize,
        addTag: addTag,
        findByTagUUID: findByTagUUID,
        clearSearch: clearSearch,
        search: search
    });

    // export const itemStates = writable({})
    // export const generalStates = writable({isShooting: true})
    // export const itemIds = writable([])
    // export const imgCount = writable(0)
    // export const sharedStates = writable({})
    const inkTarget = writable('inboxmail-item0');
    const currentMailID = writable(emails['inbox'][0].id);
    const pathTable = writable({}); //save stroke belonging to elements
    const allInks = writable([]); // {id: uuid, path:[....],targetID: targetid}
    const isPenActive = writable(false);
    const isInking = writable(false);
    const currentTool = writable('');
    const currentFolder = writable('inbox');

    /** for context menu */
    const menuPos = writable({x: 0, y: 0});
    const showMenu = writable(false);
    const menuTarget = writable(''); // could be emailID or groupID

    const DEV_SETTINGS = writable({
      postDrawDelay: 600, // ms
    });

    /* src/Inkable.svelte generated by Svelte v3.24.1 */

    const { console: console_1$1 } = globals;
    const file = "src/Inkable.svelte";

    // (426:2) {:else}
    function create_else_block$1(ctx) {
    	let current;
    	const default_slot_template = /*$$slots*/ ctx[17].default;
    	const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[16], null);
    	const default_slot_or_fallback = default_slot || fallback_block(ctx);

    	const block = {
    		c: function create() {
    			if (default_slot_or_fallback) default_slot_or_fallback.c();
    		},
    		m: function mount(target, anchor) {
    			if (default_slot_or_fallback) {
    				default_slot_or_fallback.m(target, anchor);
    			}

    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (default_slot) {
    				if (default_slot.p && dirty[0] & /*$$scope*/ 65536) {
    					update_slot(default_slot, default_slot_template, ctx, /*$$scope*/ ctx[16], dirty, null, null);
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(default_slot_or_fallback, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(default_slot_or_fallback, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (default_slot_or_fallback) default_slot_or_fallback.d(detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$1.name,
    		type: "else",
    		source: "(426:2) {:else}",
    		ctx
    	});

    	return block;
    }

    // (424:2) {#if tagData}
    function create_if_block$1(ctx) {
    	let div;

    	const block = {
    		c: function create() {
    			div = element("div");
    			attr_dev(div, "class", "view-frame svelte-10irk2o");
    			add_location(div, file, 424, 4, 11113);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$1.name,
    		type: "if",
    		source: "(424:2) {#if tagData}",
    		ctx
    	});

    	return block;
    }

    // (427:10) inkable element
    function fallback_block(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("inkable element");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: fallback_block.name,
    		type: "fallback",
    		source: "(427:10) inkable element",
    		ctx
    	});

    	return block;
    }

    function create_fragment$1(ctx) {
    	let div1;
    	let current_block_type_index;
    	let if_block;
    	let t;
    	let div0;
    	let div1_class_value;
    	let current;
    	let mounted;
    	let dispose;
    	const if_block_creators = [create_if_block$1, create_else_block$1];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*tagData*/ ctx[2]) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type(ctx);
    	if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			if_block.c();
    			t = space();
    			div0 = element("div");
    			attr_dev(div0, "class", "svg-root svelte-10irk2o");
    			toggle_class(div0, "view-only", /*tagData*/ ctx[2]);
    			toggle_class(div0, "show-ink", /*realOpts*/ ctx[8].alwaysInking || /*$inkTarget*/ ctx[7] === /*inkID*/ ctx[0]);
    			add_location(div0, file, 428, 2, 11197);
    			attr_dev(div1, "data-inkid", /*inkID*/ ctx[0]);
    			attr_dev(div1, "class", div1_class_value = "" + (null_to_empty("inkable " + /*className*/ ctx[1]) + " svelte-10irk2o"));
    			toggle_class(div1, "fill-parent", /*realOpts*/ ctx[8].fillParent);
    			toggle_class(div1, "target", /*$inkTarget*/ ctx[7] === /*inkID*/ ctx[0]);
    			toggle_class(div1, "ink-target", /*$inkTarget*/ ctx[7] === /*inkID*/ ctx[0] && /*$isInking*/ ctx[6]);
    			add_location(div1, file, 377, 0, 9820);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			if_blocks[current_block_type_index].m(div1, null);
    			append_dev(div1, t);
    			append_dev(div1, div0);
    			/*div0_binding*/ ctx[18](div0);
    			/*div1_binding*/ ctx[19](div1);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(div1, "contextmenu", contextmenu_handler, false, false, false),
    					listen_dev(div1, "mousedown", /*handleMouseDown*/ ctx[12], false, false, false),
    					listen_dev(div1, "mousemove", /*handleMouseMove*/ ctx[13], false, false, false),
    					listen_dev(div1, "mouseup", /*handleMouseUp*/ ctx[14], false, false, false),
    					listen_dev(div1, "touchstart", /*handleMouseDown*/ ctx[12], false, false, false),
    					listen_dev(div1, "touchmove", /*handleMouseMove*/ ctx[13], false, false, false),
    					listen_dev(div1, "touchend", /*handleMouseUp*/ ctx[14], false, false, false),
    					listen_dev(div1, "pointerdown", /*pointerdown_handler*/ ctx[20], false, false, false),
    					listen_dev(div1, "pointermove", /*pointermove_handler*/ ctx[21], false, false, false),
    					listen_dev(div1, "pointerup", /*pointerup_handler*/ ctx[22], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, dirty) {
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if_blocks[current_block_type_index].p(ctx, dirty);
    			} else {
    				group_outros();

    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});

    				check_outros();
    				if_block = if_blocks[current_block_type_index];

    				if (!if_block) {
    					if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block.c();
    				}

    				transition_in(if_block, 1);
    				if_block.m(div1, t);
    			}

    			if (dirty[0] & /*tagData*/ 4) {
    				toggle_class(div0, "view-only", /*tagData*/ ctx[2]);
    			}

    			if (dirty[0] & /*realOpts, $inkTarget, inkID*/ 385) {
    				toggle_class(div0, "show-ink", /*realOpts*/ ctx[8].alwaysInking || /*$inkTarget*/ ctx[7] === /*inkID*/ ctx[0]);
    			}

    			if (!current || dirty[0] & /*inkID*/ 1) {
    				attr_dev(div1, "data-inkid", /*inkID*/ ctx[0]);
    			}

    			if (!current || dirty[0] & /*className*/ 2 && div1_class_value !== (div1_class_value = "" + (null_to_empty("inkable " + /*className*/ ctx[1]) + " svelte-10irk2o"))) {
    				attr_dev(div1, "class", div1_class_value);
    			}

    			if (dirty[0] & /*className, realOpts*/ 258) {
    				toggle_class(div1, "fill-parent", /*realOpts*/ ctx[8].fillParent);
    			}

    			if (dirty[0] & /*className, $inkTarget, inkID*/ 131) {
    				toggle_class(div1, "target", /*$inkTarget*/ ctx[7] === /*inkID*/ ctx[0]);
    			}

    			if (dirty[0] & /*className, $inkTarget, inkID, $isInking*/ 195) {
    				toggle_class(div1, "ink-target", /*$inkTarget*/ ctx[7] === /*inkID*/ ctx[0] && /*$isInking*/ ctx[6]);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			if_blocks[current_block_type_index].d();
    			/*div0_binding*/ ctx[18](null);
    			/*div1_binding*/ ctx[19](null);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    const CLR_INKED = "#a8f";
    const CLR_INKING = "#7ff";

    const contextmenu_handler = evt => {
    	evt.preventDefault();
    };

    function instance$1($$self, $$props, $$invalidate) {
    	let $pathTable;
    	let $DEV_SETTINGS;
    	let $isInking;
    	let $isPenActive;
    	let $currentTool;
    	let $inkTarget;
    	validate_store(pathTable, "pathTable");
    	component_subscribe($$self, pathTable, $$value => $$invalidate(27, $pathTable = $$value));
    	validate_store(DEV_SETTINGS, "DEV_SETTINGS");
    	component_subscribe($$self, DEV_SETTINGS, $$value => $$invalidate(28, $DEV_SETTINGS = $$value));
    	validate_store(isInking, "isInking");
    	component_subscribe($$self, isInking, $$value => $$invalidate(6, $isInking = $$value));
    	validate_store(isPenActive, "isPenActive");
    	component_subscribe($$self, isPenActive, $$value => $$invalidate(29, $isPenActive = $$value));
    	validate_store(currentTool, "currentTool");
    	component_subscribe($$self, currentTool, $$value => $$invalidate(30, $currentTool = $$value));
    	validate_store(inkTarget, "inkTarget");
    	component_subscribe($$self, inkTarget, $$value => $$invalidate(7, $inkTarget = $$value));
    	let { inkID = 0 } = $$props;
    	let { className = "" } = $$props;
    	let { tagData } = $$props;
    	let { opts } = $$props;

    	const realOpts = {
    		alwaysInking: false,
    		autoClear: false,
    		onMouseUp: undefined,
    		onEndDraw: undefined,
    		onTagDrawn: undefined,
    		record: true,
    		acceptDrawing: true,
    		fillParent: false,
    		lineWidth: 4,
    		curved: false,
    		// above are default opts
    		...opts
    	};

    	console.log("REALOPTS:", realOpts);
    	let elInkable;
    	let svgRoot;
    	let two;
    	let isDrawing = false;
    	let isMouseDown = false;
    	let drawData = { newPath: [], drawingPath: undefined };
    	let postDrawTimer;
    	let tag;

    	// drawData.drawingPath.curved = true;
    	// drawData.drawingPath.noFill().stroke = "#aa44ff";
    	let clr = "white";

    	// TODO splite renderTag into seperate component
    	function renderTag(tagData) {
    		console.log("RENDER PATH:", tagData);

    		for (const pathData of tagData.pathList) {
    			let path = new two_min.Path([], false, true);
    			path.cap = path.join = "round";
    			path.curved = realOpts.curved;
    			path.noFill().stroke = CLR_INKED;
    			path.linewidth = realOpts.linewidth;

    			for (const point of pathData) {
    				path.vertices.push(new two_min.Vector(point.x, point.y));
    			}

    			two.add(path);
    		}

    		two.update();
    	}

    	function beginDraw(evt) {
    		if ($pathTable[inkID] == undefined) {
    			set_store_value(pathTable, $pathTable[inkID] = [], $pathTable);
    			console.log("create path list: ", inkID);
    		}

    		if (!postDrawTimer) {
    			// make tag into a class
    			tag = {
    				uuid: v1(),
    				pathList: [],
    				targetIDs: new Set([inkID]),
    				type: "", // command|tag
    				info: {}, // for command only, info differs according to command type
    				position: { x: 0, y: 0 },
    				valid: false
    			};
    		} else {
    			clearTimeout(postDrawTimer);
    		}

    		$$invalidate(5, isDrawing = true);
    		drawData.newPath.length = 0;

    		// drawData.drawingPath.vertices.length = 0;
    		drawData.drawingPath = new two_min.Path([], false, true);

    		drawData.drawingPath.cap = drawData.drawingPath.join = "round";
    		drawData.drawingPath.curved = realOpts.curved;
    		drawData.drawingPath.noFill().stroke = CLR_INKING;
    		drawData.drawingPath.linewidth = realOpts.linewidth;
    		two.add(drawData.drawingPath);
    		drawData.newPath.push({ x: evt.layerX, y: evt.layerY });
    		drawData.drawingPath.vertices.push(new two_min.Vector(evt.layerX, evt.layerY));
    	} // }

    	function drawing(evt) {
    		drawData.newPath.push({ x: evt.layerX, y: evt.layerY });
    		drawData.drawingPath.vertices.push(new two_min.Vector(evt.layerX, evt.layerY));

    		// }
    		two.update();

    		tag.valid = true;
    	}

    	function endDraw(evt) {
    		$$invalidate(5, isDrawing = false);

    		if (!tag.valid) {
    			return;
    		}

    		drawData.newPath.push({ x: evt.layerX, y: evt.layerY });

    		////  ////////////
    		console.log("before length", drawData.drawingPath.vertices.length);

    		let pathForDisplay = new two_min.Path([], false, true);
    		pathForDisplay.curved = realOpts.curved;
    		pathForDisplay.cap = pathForDisplay.join = "round";
    		pathForDisplay.noFill().stroke = CLR_INKING;
    		pathForDisplay.linewidth = realOpts.linewidth;
    		const simplifiedPath = simplify(drawData.newPath, 2.2, true);
    		drawData.drawingPath.vertices.length = 0;

    		// drawData.drawingPath.stroke = "#a4a";
    		for (const pos of simplifiedPath) {
    			drawData.drawingPath.vertices.push(new two_min.Vector(pos.x, pos.y));
    			pathForDisplay.vertices.push(new two_min.Vector(pos.x, pos.y));
    		}

    		two.add(pathForDisplay);
    		two.update();
    		tag.pathList.push(simplifiedPath);

    		// tag.targetIDs.push[inkID];
    		$pathTable[inkID].push(pathForDisplay);

    		postDrawTimer = setTimeout(
    			() => {
    				postDraw();
    				postDrawTimer = null;
    			},
    			$DEV_SETTINGS.postDrawDelay
    		);

    		// TODO support multi-strokes
    		// TODO group strokes by distance, now only according to time
    		// TODO show prediction while drawing
    		realOpts.onEndDraw && realOpts.onEndDraw(tag);
    	} // evt.preventDefault();

    	/**
     * Add tag to recognizer
     */
    	function postDraw() {
    		const [dims, l] = normalizedPathList(tag.pathList);
    		tag.pathList = l;
    		tag.dims = dims;

    		$pathTable[inkID].forEach(element => {
    			element.stroke = CLR_INKED;
    		});

    		two.update();

    		// if (realOpts.record) {
    		//   model.addTag(tag);
    		//   console.log("afte ta", model.tags);
    		//   $allInks = [...$allInks, tag];
    		//   $currentMailID = $currentMailID + ""; // to trigger re-render
    		//   console.log("pushed", inkID, tag, $pathTable[inkID]);
    		//   console.log("allinks", $allInks);
    		// }
    		realOpts.onTagDrawn && realOpts.onTagDrawn(tag);
    	}

    	////  ////////////
    	////////////  ////
    	function handleMouseDown(evt) {
    		console.log("/// in Inkable mousedown");

    		if ($pathTable[inkID] == undefined) {
    			set_store_value(pathTable, $pathTable[inkID] = [], $pathTable);
    			console.log("create path list: ", inkID);
    		}

    		console.log("mousedown", $isInking, inkID, evt);

    		if ($isPenActive) {
    			evt.preventDefault();
    			evt.stopPropagation();
    		}

    		// right button
    		if (realOpts.acceptDrawing && (realOpts.alwaysInking || evt.button === 2 || $isInking)) {
    			beginDraw(evt);
    			evt.preventDefault();
    		}

    		console.log(evt);
    	}

    	function handleMouseMove(evt) {

    		if ($isPenActive) {
    			evt.preventDefault();
    			evt.stopPropagation();
    		} // console.log("mousemove", evt);

    		if (isDrawing) {
    			drawing(evt);
    		}
    	}

    	function handleMouseUp(evt) {
    		//enable right click on what doesn't accept drawing
    		if (!realOpts.acceptDrawing) {
    			evt.target.dispatchEvent(new MouseEvent("contextmenu",
    			{
    					bubbles: true,
    					cancelable: true,
    					view: window,
    					button: 2,
    					buttons: 0,
    					clientX: evt.clientX,
    					clientY: evt.clientY
    				}));
    		}

    		if ($isPenActive) {
    			evt.preventDefault();
    			evt.stopPropagation();
    		}

    		if (isDrawing) {
    			endDraw(evt);
    		}
    	}

    	function loadPath() {
    		console.log("in loadPath", inkID, $pathTable[inkID]);

    		for (const index in $pathTable[inkID]) {
    			// const p = new Two.Path(path, false, true);
    			console.log("add path", inkID);

    			two.add($pathTable[inkID][index]);
    		}

    		two.update();
    	}

    	function normalizedPathList(pathList) {
    		let top = 10000, left = 10000, right = 0, bottom = 0;

    		// get bounding box
    		for (const path of pathList) {
    			for (const pos of path) {
    				top = top > pos.y ? pos.y : top;
    				bottom = bottom < pos.y ? pos.y : bottom;
    				left = left > pos.x ? pos.x : left;
    				right = right < pos.x ? pos.x : right;
    			}
    		}

    		// normalize coords
    		for (const path of pathList) {
    			for (const pos of path) {
    				pos.x -= left - 5; // add some padding
    				pos.y -= top - 5;
    			}
    		}

    		tag.position.x = left;
    		tag.position.y = top;

    		return [
    			{
    				top: top - 10,
    				bottom,
    				left: left - 10,
    				right
    			},
    			pathList
    		];
    	}

    	onMount(() => {
    		let params = { width: "100%", height: "100%" }; // class: "svg-root"
    		two = new two_min(params).appendTo(svgRoot);

    		// TODO refactor this into separate component
    		if (tagData) {
    			renderTag(tagData);
    			svgRoot.firstChild.setAttribute("viewBox", "0 0 " + (tagData.dims.right - tagData.dims.left) + " " + (tagData.dims.bottom - tagData.dims.top));
    		}

    		// two.add(drawData.drawingPath);
    		loadPath();

    		const observer = new MutationObserver(function (events) {
    				const dataid = events.filter(e => e.attributeName === "data-inkid");

    				if (dataid.length > 0) {
    					two.clear();
    					$$invalidate(0, inkID = dataid[0].target.attributes["data-inkid"].value);
    					console.log("data-inkid changed=====", inkID, events);

    					// loadPath();
    					two.update();
    				}

    				////  ////////////
    				const l = events.filter(e => e.attributeName === "class");

    				if (l.length > 0 && realOpts.autoClear) {
    					console.log("class changed", l);
    					two.clear();
    					two.update();
    				}
    			});

    		observer.observe(elInkable, {
    			attributes: true,
    			attributeFilter: ["data-inkid", "class"],
    			childList: false,
    			characterData: false
    		});
    	});

    	const writable_props = ["inkID", "className", "tagData", "opts"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1$1.warn(`<Inkable> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Inkable", $$slots, ['default']);

    	function div0_binding($$value) {
    		binding_callbacks[$$value ? "unshift" : "push"](() => {
    			svgRoot = $$value;
    			$$invalidate(4, svgRoot);
    		});
    	}

    	function div1_binding($$value) {
    		binding_callbacks[$$value ? "unshift" : "push"](() => {
    			elInkable = $$value;
    			$$invalidate(3, elInkable);
    		});
    	}

    	const pointerdown_handler = evt => {
    		const aa = [
    			"height",
    			"isPrimary",
    			"pointerId",
    			"pointerType",
    			"pressure",
    			"tangentialPressure",
    			"tiltX",
    			"tiltY",
    			"twist",
    			"width"
    		].reduce(
    			(acc, cur) => {
    				acc[cur] = evt[cur];
    				return acc;
    			},
    			{}
    		);

    		if (evt.pointerType === "pen") {
    			beginDraw(evt);
    			evt.stopPropagation();
    			evt.preventDefault();
    		}

    		console.info("pointer down", aa);
    	};

    	const pointermove_handler = evt => {
    		if (evt.pointerType === "pen" && isDrawing) {
    			drawing(evt);
    			evt.stopPropagation();
    			evt.preventDefault();
    		}
    	};

    	const pointerup_handler = evt => {
    		if (evt.pointerType === "pen" && isDrawing) {
    			endDraw(evt);
    			evt.preventDefault();
    			evt.stopPropagation();
    		}
    	};

    	$$self.$$set = $$props => {
    		if ("inkID" in $$props) $$invalidate(0, inkID = $$props.inkID);
    		if ("className" in $$props) $$invalidate(1, className = $$props.className);
    		if ("tagData" in $$props) $$invalidate(2, tagData = $$props.tagData);
    		if ("opts" in $$props) $$invalidate(15, opts = $$props.opts);
    		if ("$$scope" in $$props) $$invalidate(16, $$scope = $$props.$$scope);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		uuidv1: v1,
    		simplify,
    		Two: two_min,
    		allInks,
    		inkTarget,
    		isInking,
    		pathTable,
    		currentTool,
    		currentMailID,
    		DEV_SETTINGS,
    		isPenActive,
    		model,
    		inkID,
    		className,
    		tagData,
    		opts,
    		realOpts,
    		CLR_INKED,
    		CLR_INKING,
    		elInkable,
    		svgRoot,
    		two,
    		isDrawing,
    		isMouseDown,
    		drawData,
    		postDrawTimer,
    		tag,
    		clr,
    		renderTag,
    		beginDraw,
    		drawing,
    		endDraw,
    		postDraw,
    		handleMouseDown,
    		handleMouseMove,
    		handleMouseUp,
    		loadPath,
    		normalizedPathList,
    		$pathTable,
    		$DEV_SETTINGS,
    		$isInking,
    		$isPenActive,
    		$currentTool,
    		$inkTarget
    	});

    	$$self.$inject_state = $$props => {
    		if ("inkID" in $$props) $$invalidate(0, inkID = $$props.inkID);
    		if ("className" in $$props) $$invalidate(1, className = $$props.className);
    		if ("tagData" in $$props) $$invalidate(2, tagData = $$props.tagData);
    		if ("opts" in $$props) $$invalidate(15, opts = $$props.opts);
    		if ("elInkable" in $$props) $$invalidate(3, elInkable = $$props.elInkable);
    		if ("svgRoot" in $$props) $$invalidate(4, svgRoot = $$props.svgRoot);
    		if ("two" in $$props) two = $$props.two;
    		if ("isDrawing" in $$props) $$invalidate(5, isDrawing = $$props.isDrawing);
    		if ("isMouseDown" in $$props) isMouseDown = $$props.isMouseDown;
    		if ("drawData" in $$props) drawData = $$props.drawData;
    		if ("postDrawTimer" in $$props) postDrawTimer = $$props.postDrawTimer;
    		if ("tag" in $$props) tag = $$props.tag;
    		if ("clr" in $$props) clr = $$props.clr;
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		inkID,
    		className,
    		tagData,
    		elInkable,
    		svgRoot,
    		isDrawing,
    		$isInking,
    		$inkTarget,
    		realOpts,
    		beginDraw,
    		drawing,
    		endDraw,
    		handleMouseDown,
    		handleMouseMove,
    		handleMouseUp,
    		opts,
    		$$scope,
    		$$slots,
    		div0_binding,
    		div1_binding,
    		pointerdown_handler,
    		pointermove_handler,
    		pointerup_handler
    	];
    }

    class Inkable extends SvelteComponentDev {
    	constructor(options) {
    		super(options);

    		init(
    			this,
    			options,
    			instance$1,
    			create_fragment$1,
    			safe_not_equal,
    			{
    				inkID: 0,
    				className: 1,
    				tagData: 2,
    				opts: 15
    			},
    			[-1, -1]
    		);

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Inkable",
    			options,
    			id: create_fragment$1.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*tagData*/ ctx[2] === undefined && !("tagData" in props)) {
    			console_1$1.warn("<Inkable> was created without expected prop 'tagData'");
    		}

    		if (/*opts*/ ctx[15] === undefined && !("opts" in props)) {
    			console_1$1.warn("<Inkable> was created without expected prop 'opts'");
    		}
    	}

    	get inkID() {
    		throw new Error("<Inkable>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set inkID(value) {
    		throw new Error("<Inkable>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get className() {
    		throw new Error("<Inkable>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set className(value) {
    		throw new Error("<Inkable>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get tagData() {
    		throw new Error("<Inkable>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set tagData(value) {
    		throw new Error("<Inkable>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get opts() {
    		throw new Error("<Inkable>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set opts(value) {
    		throw new Error("<Inkable>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/ReplyBlock.svelte generated by Svelte v3.24.1 */
    const file$1 = "src/ReplyBlock.svelte";

    function create_fragment$2(ctx) {
    	let main;
    	let label0;
    	let div0;
    	let t1;
    	let input0;
    	let t2;
    	let label1;
    	let div1;
    	let t4;
    	let input1;
    	let t5;
    	let input2;
    	let t6;
    	let textarea;
    	let t7;
    	let div2;
    	let button0;
    	let t9;
    	let button1;
    	let t11;
    	let button2;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			main = element("main");
    			label0 = element("label");
    			div0 = element("div");
    			div0.textContent = "From";
    			t1 = space();
    			input0 = element("input");
    			t2 = space();
    			label1 = element("label");
    			div1 = element("div");
    			div1.textContent = "To";
    			t4 = space();
    			input1 = element("input");
    			t5 = space();
    			input2 = element("input");
    			t6 = space();
    			textarea = element("textarea");
    			t7 = space();
    			div2 = element("div");
    			button0 = element("button");
    			button0.textContent = "Cancel";
    			t9 = space();
    			button1 = element("button");
    			button1.textContent = "Save";
    			t11 = space();
    			button2 = element("button");
    			button2.textContent = "Send";
    			add_location(div0, file$1, 54, 4, 790);
    			attr_dev(input0, "type", "text");
    			attr_dev(input0, "class", "svelte-1rlf5x8");
    			add_location(input0, file$1, 55, 4, 811);
    			attr_dev(label0, "for", "sender");
    			add_location(label0, file$1, 53, 2, 764);
    			add_location(div1, file$1, 58, 4, 894);
    			attr_dev(input1, "type", "text");
    			attr_dev(input1, "class", "svelte-1rlf5x8");
    			add_location(input1, file$1, 59, 4, 913);
    			attr_dev(label1, "for", "receiver");
    			add_location(label1, file$1, 57, 2, 866);
    			attr_dev(input2, "class", "subject svelte-1rlf5x8");
    			attr_dev(input2, "type", "text");
    			attr_dev(input2, "placeholder", "subject");
    			add_location(input2, file$1, 61, 2, 966);
    			attr_dev(textarea, "name", "content");
    			attr_dev(textarea, "id", "");
    			attr_dev(textarea, "class", "svelte-1rlf5x8");
    			add_location(textarea, file$1, 62, 2, 1048);
    			attr_dev(button0, "class", "svelte-1rlf5x8");
    			add_location(button0, file$1, 64, 4, 1130);
    			attr_dev(button1, "class", "svelte-1rlf5x8");
    			add_location(button1, file$1, 70, 4, 1233);
    			attr_dev(button2, "class", "svelte-1rlf5x8");
    			add_location(button2, file$1, 84, 4, 1529);
    			attr_dev(div2, "class", "btns svelte-1rlf5x8");
    			add_location(div2, file$1, 63, 2, 1106);
    			attr_dev(main, "class", "svelte-1rlf5x8");
    			add_location(main, file$1, 51, 0, 752);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);
    			append_dev(main, label0);
    			append_dev(label0, div0);
    			append_dev(label0, t1);
    			append_dev(label0, input0);
    			set_input_value(input0, /*from*/ ctx[0]);
    			append_dev(main, t2);
    			append_dev(main, label1);
    			append_dev(label1, div1);
    			append_dev(label1, t4);
    			append_dev(label1, input1);
    			set_input_value(input1, /*to*/ ctx[1]);
    			append_dev(main, t5);
    			append_dev(main, input2);
    			set_input_value(input2, /*title*/ ctx[2]);
    			append_dev(main, t6);
    			append_dev(main, textarea);
    			set_input_value(textarea, /*content*/ ctx[3]);
    			append_dev(main, t7);
    			append_dev(main, div2);
    			append_dev(div2, button0);
    			append_dev(div2, t9);
    			append_dev(div2, button1);
    			append_dev(div2, t11);
    			append_dev(div2, button2);

    			if (!mounted) {
    				dispose = [
    					listen_dev(input0, "input", /*input0_input_handler*/ ctx[5]),
    					listen_dev(input1, "input", /*input1_input_handler*/ ctx[6]),
    					listen_dev(input2, "input", /*input2_input_handler*/ ctx[7]),
    					listen_dev(textarea, "input", /*textarea_input_handler*/ ctx[8]),
    					listen_dev(button0, "click", /*click_handler*/ ctx[9], false, false, false),
    					listen_dev(button1, "click", /*click_handler_1*/ ctx[10], false, false, false),
    					listen_dev(button2, "click", /*click_handler_2*/ ctx[11], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*from*/ 1 && input0.value !== /*from*/ ctx[0]) {
    				set_input_value(input0, /*from*/ ctx[0]);
    			}

    			if (dirty & /*to*/ 2 && input1.value !== /*to*/ ctx[1]) {
    				set_input_value(input1, /*to*/ ctx[1]);
    			}

    			if (dirty & /*title*/ 4 && input2.value !== /*title*/ ctx[2]) {
    				set_input_value(input2, /*title*/ ctx[2]);
    			}

    			if (dirty & /*content*/ 8) {
    				set_input_value(textarea, /*content*/ ctx[3]);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$2.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let { clickHandler } = $$props;
    	let { from = "" } = $$props;
    	let { to = "" } = $$props;
    	let { title = "" } = $$props;
    	let { content = "" } = $$props;
    	const writable_props = ["clickHandler", "from", "to", "title", "content"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<ReplyBlock> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("ReplyBlock", $$slots, []);

    	function input0_input_handler() {
    		from = this.value;
    		$$invalidate(0, from);
    	}

    	function input1_input_handler() {
    		to = this.value;
    		$$invalidate(1, to);
    	}

    	function input2_input_handler() {
    		title = this.value;
    		$$invalidate(2, title);
    	}

    	function textarea_input_handler() {
    		content = this.value;
    		$$invalidate(3, content);
    	}

    	const click_handler = () => {
    		clickHandler();
    	};

    	const click_handler_1 = () => {
    		clickHandler();

    		addMail("drafts", {
    			read: true,
    			title,
    			content,
    			from,
    			to,
    			time: new Date()
    		});
    	};

    	const click_handler_2 = () => {
    		clickHandler();

    		addMail("sent", {
    			read: true,
    			title,
    			content,
    			from,
    			to,
    			time: new Date()
    		});
    	};

    	$$self.$$set = $$props => {
    		if ("clickHandler" in $$props) $$invalidate(4, clickHandler = $$props.clickHandler);
    		if ("from" in $$props) $$invalidate(0, from = $$props.from);
    		if ("to" in $$props) $$invalidate(1, to = $$props.to);
    		if ("title" in $$props) $$invalidate(2, title = $$props.title);
    		if ("content" in $$props) $$invalidate(3, content = $$props.content);
    	};

    	$$self.$capture_state = () => ({
    		model,
    		clickHandler,
    		from,
    		to,
    		title,
    		content
    	});

    	$$self.$inject_state = $$props => {
    		if ("clickHandler" in $$props) $$invalidate(4, clickHandler = $$props.clickHandler);
    		if ("from" in $$props) $$invalidate(0, from = $$props.from);
    		if ("to" in $$props) $$invalidate(1, to = $$props.to);
    		if ("title" in $$props) $$invalidate(2, title = $$props.title);
    		if ("content" in $$props) $$invalidate(3, content = $$props.content);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		from,
    		to,
    		title,
    		content,
    		clickHandler,
    		input0_input_handler,
    		input1_input_handler,
    		input2_input_handler,
    		textarea_input_handler,
    		click_handler,
    		click_handler_1,
    		click_handler_2
    	];
    }

    class ReplyBlock extends SvelteComponentDev {
    	constructor(options) {
    		super(options);

    		init(this, options, instance$2, create_fragment$2, safe_not_equal, {
    			clickHandler: 4,
    			from: 0,
    			to: 1,
    			title: 2,
    			content: 3
    		});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "ReplyBlock",
    			options,
    			id: create_fragment$2.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*clickHandler*/ ctx[4] === undefined && !("clickHandler" in props)) {
    			console.warn("<ReplyBlock> was created without expected prop 'clickHandler'");
    		}
    	}

    	get clickHandler() {
    		throw new Error("<ReplyBlock>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set clickHandler(value) {
    		throw new Error("<ReplyBlock>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get from() {
    		throw new Error("<ReplyBlock>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set from(value) {
    		throw new Error("<ReplyBlock>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get to() {
    		throw new Error("<ReplyBlock>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set to(value) {
    		throw new Error("<ReplyBlock>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get title() {
    		throw new Error("<ReplyBlock>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set title(value) {
    		throw new Error("<ReplyBlock>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get content() {
    		throw new Error("<ReplyBlock>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set content(value) {
    		throw new Error("<ReplyBlock>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/ContextMenu.svelte generated by Svelte v3.24.1 */

    const { console: console_1$2 } = globals;
    const file$2 = "src/ContextMenu.svelte";

    function get_each_context_2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[11] = list[i];
    	return child_ctx;
    }

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[11] = list[i];
    	return child_ctx;
    }

    function get_each_context_1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[11] = list[i];
    	return child_ctx;
    }

    function get_each_context_3(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[18] = list[i];
    	return child_ctx;
    }

    // (125:2) {#each opts as opt}
    function create_each_block_3(ctx) {
    	let div;
    	let t_value = /*opt*/ ctx[18] + "";
    	let t;
    	let div_data_opt_value;

    	const block = {
    		c: function create() {
    			div = element("div");
    			t = text(t_value);
    			attr_dev(div, "class", "opt svelte-9tcyw1");
    			attr_dev(div, "data-opt", div_data_opt_value = /*opt*/ ctx[18]);
    			add_location(div, file$2, 125, 4, 2879);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, t);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*opts*/ 16 && t_value !== (t_value = /*opt*/ ctx[18] + "")) set_data_dev(t, t_value);

    			if (dirty & /*opts*/ 16 && div_data_opt_value !== (div_data_opt_value = /*opt*/ ctx[18])) {
    				attr_dev(div, "data-opt", div_data_opt_value);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_3.name,
    		type: "each",
    		source: "(125:2) {#each opts as opt}",
    		ctx
    	});

    	return block;
    }

    // (167:2) {:else}
    function create_else_block$2(ctx) {
    	const block = { c: noop, m: noop, p: noop, d: noop };

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$2.name,
    		type: "else",
    		source: "(167:2) {:else}",
    		ctx
    	});

    	return block;
    }

    // (152:32) 
    function create_if_block_1(ctx) {
    	let t0;
    	let div;
    	let t1;
    	let t2_value = /*allTransacs*/ ctx[0].reduce(/*func*/ ctx[9], 0) + "";
    	let t2;
    	let each_value_2 = /*allTransacs*/ ctx[0];
    	validate_each_argument(each_value_2);
    	let each_blocks = [];

    	for (let i = 0; i < each_value_2.length; i += 1) {
    		each_blocks[i] = create_each_block_2(get_each_context_2(ctx, each_value_2, i));
    	}

    	const block = {
    		c: function create() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t0 = space();
    			div = element("div");
    			t1 = text("Total Amount:");
    			t2 = text(t2_value);
    			add_location(div, file$2, 157, 4, 3704);
    		},
    		m: function mount(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert_dev(target, t0, anchor);
    			insert_dev(target, div, anchor);
    			append_dev(div, t1);
    			append_dev(div, t2);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*allTransacs*/ 1) {
    				each_value_2 = /*allTransacs*/ ctx[0];
    				validate_each_argument(each_value_2);
    				let i;

    				for (i = 0; i < each_value_2.length; i += 1) {
    					const child_ctx = get_each_context_2(ctx, each_value_2, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_2(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(t0.parentNode, t0);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_2.length;
    			}

    			if (dirty & /*allTransacs*/ 1 && t2_value !== (t2_value = /*allTransacs*/ ctx[0].reduce(/*func*/ ctx[9], 0) + "")) set_data_dev(t2, t2_value);
    		},
    		d: function destroy(detaching) {
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(152:32) ",
    		ctx
    	});

    	return block;
    }

    // (135:2) {#if infoType === 'meta'}
    function create_if_block$2(ctx) {
    	let div1;
    	let div0;
    	let t1;
    	let t2;
    	let div2;
    	let t3;
    	let div3;
    	let t5;
    	let t6;
    	let div4;
    	let t7;
    	let div5;
    	let t9;
    	let span;
    	let t10_value = /*meta*/ ctx[1].wordCount + "";
    	let t10;
    	let each_value_1 = Array.from(/*meta*/ ctx[1].senders);
    	validate_each_argument(each_value_1);
    	let each_blocks_1 = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks_1[i] = create_each_block_1(get_each_context_1(ctx, each_value_1, i));
    	}

    	let each_value = Array.from(/*meta*/ ctx[1].receivers);
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			div0 = element("div");
    			div0.textContent = "Senders:";
    			t1 = space();

    			for (let i = 0; i < each_blocks_1.length; i += 1) {
    				each_blocks_1[i].c();
    			}

    			t2 = space();
    			div2 = element("div");
    			t3 = space();
    			div3 = element("div");
    			div3.textContent = "Receivers:";
    			t5 = space();

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t6 = space();
    			div4 = element("div");
    			t7 = space();
    			div5 = element("div");
    			div5.textContent = "Word Count:";
    			t9 = space();
    			span = element("span");
    			t10 = text(t10_value);
    			add_location(div0, file$2, 136, 6, 3110);
    			attr_dev(div1, "class", "senders");
    			add_location(div1, file$2, 135, 4, 3082);
    			attr_dev(div2, "class", "receivers");
    			add_location(div2, file$2, 142, 4, 3249);
    			add_location(div3, file$2, 143, 4, 3279);
    			attr_dev(div4, "class", "word-count");
    			add_location(div4, file$2, 148, 4, 3403);
    			add_location(div5, file$2, 149, 4, 3434);
    			add_location(span, file$2, 150, 4, 3461);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, div0);
    			append_dev(div1, t1);

    			for (let i = 0; i < each_blocks_1.length; i += 1) {
    				each_blocks_1[i].m(div1, null);
    			}

    			insert_dev(target, t2, anchor);
    			insert_dev(target, div2, anchor);
    			insert_dev(target, t3, anchor);
    			insert_dev(target, div3, anchor);
    			insert_dev(target, t5, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert_dev(target, t6, anchor);
    			insert_dev(target, div4, anchor);
    			insert_dev(target, t7, anchor);
    			insert_dev(target, div5, anchor);
    			insert_dev(target, t9, anchor);
    			insert_dev(target, span, anchor);
    			append_dev(span, t10);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*Array, meta*/ 2) {
    				each_value_1 = Array.from(/*meta*/ ctx[1].senders);
    				validate_each_argument(each_value_1);
    				let i;

    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1(ctx, each_value_1, i);

    					if (each_blocks_1[i]) {
    						each_blocks_1[i].p(child_ctx, dirty);
    					} else {
    						each_blocks_1[i] = create_each_block_1(child_ctx);
    						each_blocks_1[i].c();
    						each_blocks_1[i].m(div1, null);
    					}
    				}

    				for (; i < each_blocks_1.length; i += 1) {
    					each_blocks_1[i].d(1);
    				}

    				each_blocks_1.length = each_value_1.length;
    			}

    			if (dirty & /*Array, meta*/ 2) {
    				each_value = Array.from(/*meta*/ ctx[1].receivers);
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(t6.parentNode, t6);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}

    			if (dirty & /*meta*/ 2 && t10_value !== (t10_value = /*meta*/ ctx[1].wordCount + "")) set_data_dev(t10, t10_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			destroy_each(each_blocks_1, detaching);
    			if (detaching) detach_dev(t2);
    			if (detaching) detach_dev(div2);
    			if (detaching) detach_dev(t3);
    			if (detaching) detach_dev(div3);
    			if (detaching) detach_dev(t5);
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(t6);
    			if (detaching) detach_dev(div4);
    			if (detaching) detach_dev(t7);
    			if (detaching) detach_dev(div5);
    			if (detaching) detach_dev(t9);
    			if (detaching) detach_dev(span);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$2.name,
    		type: "if",
    		source: "(135:2) {#if infoType === 'meta'}",
    		ctx
    	});

    	return block;
    }

    // (153:4) {#each allTransacs as item}
    function create_each_block_2(ctx) {
    	let div0;
    	let t0;
    	let t1_value = /*item*/ ctx[11].account + "";
    	let t1;
    	let t2;
    	let div1;
    	let t3;
    	let t4_value = /*item*/ ctx[11].type + "";
    	let t4;
    	let t5;
    	let div2;
    	let t6;
    	let t7_value = /*item*/ ctx[11].amount + "";
    	let t7;

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			t0 = text("Account:");
    			t1 = text(t1_value);
    			t2 = space();
    			div1 = element("div");
    			t3 = text("Type:");
    			t4 = text(t4_value);
    			t5 = space();
    			div2 = element("div");
    			t6 = text("Amount:");
    			t7 = text(t7_value);
    			add_location(div0, file$2, 153, 6, 3562);
    			add_location(div1, file$2, 154, 6, 3602);
    			attr_dev(div2, "class", "bill-amount svelte-9tcyw1");
    			add_location(div2, file$2, 155, 6, 3636);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			append_dev(div0, t0);
    			append_dev(div0, t1);
    			insert_dev(target, t2, anchor);
    			insert_dev(target, div1, anchor);
    			append_dev(div1, t3);
    			append_dev(div1, t4);
    			insert_dev(target, t5, anchor);
    			insert_dev(target, div2, anchor);
    			append_dev(div2, t6);
    			append_dev(div2, t7);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*allTransacs*/ 1 && t1_value !== (t1_value = /*item*/ ctx[11].account + "")) set_data_dev(t1, t1_value);
    			if (dirty & /*allTransacs*/ 1 && t4_value !== (t4_value = /*item*/ ctx[11].type + "")) set_data_dev(t4, t4_value);
    			if (dirty & /*allTransacs*/ 1 && t7_value !== (t7_value = /*item*/ ctx[11].amount + "")) set_data_dev(t7, t7_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			if (detaching) detach_dev(t2);
    			if (detaching) detach_dev(div1);
    			if (detaching) detach_dev(t5);
    			if (detaching) detach_dev(div2);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_2.name,
    		type: "each",
    		source: "(153:4) {#each allTransacs as item}",
    		ctx
    	});

    	return block;
    }

    // (138:6) {#each Array.from(meta.senders) as item}
    function create_each_block_1(ctx) {
    	let span;
    	let t0_value = /*item*/ ctx[11] + "";
    	let t0;
    	let t1;

    	const block = {
    		c: function create() {
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = text("\n         ");
    			add_location(span, file$2, 138, 8, 3185);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, span, anchor);
    			append_dev(span, t0);
    			insert_dev(target, t1, anchor);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*meta*/ 2 && t0_value !== (t0_value = /*item*/ ctx[11] + "")) set_data_dev(t0, t0_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(span);
    			if (detaching) detach_dev(t1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_1.name,
    		type: "each",
    		source: "(138:6) {#each Array.from(meta.senders) as item}",
    		ctx
    	});

    	return block;
    }

    // (145:4) {#each Array.from(meta.receivers) as item}
    function create_each_block(ctx) {
    	let span;
    	let t0_value = /*item*/ ctx[11] + "";
    	let t0;
    	let t1;

    	const block = {
    		c: function create() {
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = text("\n       ");
    			add_location(span, file$2, 145, 6, 3354);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, span, anchor);
    			append_dev(span, t0);
    			insert_dev(target, t1, anchor);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*meta*/ 2 && t0_value !== (t0_value = /*item*/ ctx[11] + "")) set_data_dev(t0, t0_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(span);
    			if (detaching) detach_dev(t1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(145:4) {#each Array.from(meta.receivers) as item}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$3(ctx) {
    	let div0;
    	let div0_style_value;
    	let t;
    	let div1;
    	let mounted;
    	let dispose;
    	let each_value_3 = /*opts*/ ctx[4];
    	validate_each_argument(each_value_3);
    	let each_blocks = [];

    	for (let i = 0; i < each_value_3.length; i += 1) {
    		each_blocks[i] = create_each_block_3(get_each_context_3(ctx, each_value_3, i));
    	}

    	function select_block_type(ctx, dirty) {
    		if (/*infoType*/ ctx[2] === "meta") return create_if_block$2;
    		if (/*infoType*/ ctx[2] === "bill") return create_if_block_1;
    		return create_else_block$2;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t = space();
    			div1 = element("div");
    			if_block.c();
    			attr_dev(div0, "class", "menu svelte-9tcyw1");
    			attr_dev(div0, "style", div0_style_value = `transform:translate(${/*$menuPos*/ ctx[6].x}px,${/*$menuPos*/ ctx[6].y}px);`);
    			toggle_class(div0, "hidden", !/*$showMenu*/ ctx[5]);
    			add_location(div0, file$2, 76, 0, 1330);
    			attr_dev(div1, "class", "info-board svelte-9tcyw1");
    			toggle_class(div1, "visible", /*showInfoBoard*/ ctx[3]);
    			add_location(div1, file$2, 128, 0, 2940);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div0, null);
    			}

    			insert_dev(target, t, anchor);
    			insert_dev(target, div1, anchor);
    			if_block.m(div1, null);

    			if (!mounted) {
    				dispose = [
    					listen_dev(div0, "click", /*click_handler*/ ctx[8], false, false, false),
    					listen_dev(div1, "click", /*click_handler_1*/ ctx[10], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*opts*/ 16) {
    				each_value_3 = /*opts*/ ctx[4];
    				validate_each_argument(each_value_3);
    				let i;

    				for (i = 0; i < each_value_3.length; i += 1) {
    					const child_ctx = get_each_context_3(ctx, each_value_3, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_3(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div0, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_3.length;
    			}

    			if (dirty & /*$menuPos*/ 64 && div0_style_value !== (div0_style_value = `transform:translate(${/*$menuPos*/ ctx[6].x}px,${/*$menuPos*/ ctx[6].y}px);`)) {
    				attr_dev(div0, "style", div0_style_value);
    			}

    			if (dirty & /*$showMenu*/ 32) {
    				toggle_class(div0, "hidden", !/*$showMenu*/ ctx[5]);
    			}

    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
    				if_block.p(ctx, dirty);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(div1, null);
    				}
    			}

    			if (dirty & /*showInfoBoard*/ 8) {
    				toggle_class(div1, "visible", /*showInfoBoard*/ ctx[3]);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(t);
    			if (detaching) detach_dev(div1);
    			if_block.d();
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$3.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let $showMenu;
    	let $menuPos;
    	validate_store(showMenu, "showMenu");
    	component_subscribe($$self, showMenu, $$value => $$invalidate(5, $showMenu = $$value));
    	validate_store(menuPos, "menuPos");
    	component_subscribe($$self, menuPos, $$value => $$invalidate(6, $menuPos = $$value));
    	let { options = [] } = $$props;
    	let allTransacs = [];

    	let meta = {
    			amount: 0,
    			senders: new Set(),
    			receivers: new Set(),
    			wordCount: 0
    		},
    		infoType = "bill";

    	let showInfoBoard = false;
    	const writable_props = ["options"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1$2.warn(`<ContextMenu> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("ContextMenu", $$slots, []);

    	const click_handler = evt => {
    		let ops = evt.target.getAttribute("data-opt").split("/");
    		const group = groups[currentGroupID];
    		console.log("ctx click", currentGroupID, group);
    		const emails = getEmails(group.memberIDs);

    		switch (ops[0]) {
    			case "bill":
    				$$invalidate(0, allTransacs.length = 0, allTransacs);
    				for (const mail of emails) {
    					if (mail.transacs) $$invalidate(0, allTransacs = allTransacs.concat(mail.transacs));
    				}
    				console.log("bill", allTransacs);
    				$$invalidate(3, showInfoBoard = true);
    				$$invalidate(2, infoType = "bill");
    				break;
    			case "meta":
    				$$invalidate(1, meta = {
    					amount: emails.length,
    					senders: new Set(),
    					receivers: new Set(),
    					wordCount: 0
    				});
    				console.log("meta", meta);
    				for (const mail of emails) {
    					meta.senders.add(mail.from);
    					meta.receivers.add(mail.to);
    					mail.wordCount = mail.content.split(/\s/).length;
    					console.log(mail.wordCount);
    					$$invalidate(1, meta.wordCount += mail.wordCount, meta);
    				}
    				console.log(meta.senders, meta.receivers, meta.wordCount);
    				$$invalidate(2, infoType = "meta");
    				$$invalidate(3, showInfoBoard = true);
    				break;
    			case "move to":
    				break;
    			case "delete":
    				break;
    			case "ungroup":
    				removeGroup(currentGroupID);
    				break;
    			case "bringtogether":
    				bringTogether(currentGroupID);
    				break;
    		}

    		set_store_value(showMenu, $showMenu = false);
    	};

    	const func = (acc, cur) => {
    		if (cur.amount) {
    			console.log("cur.amount", cur.amount);
    			acc += cur.amount;
    		}

    		return acc;
    	};

    	const click_handler_1 = () => {
    		$$invalidate(3, showInfoBoard = false);
    	};

    	$$self.$$set = $$props => {
    		if ("options" in $$props) $$invalidate(7, options = $$props.options);
    	};

    	$$self.$capture_state = () => ({
    		menuPos,
    		showMenu,
    		menuTarget,
    		model,
    		options,
    		allTransacs,
    		meta,
    		infoType,
    		showInfoBoard,
    		opts,
    		$showMenu,
    		$menuPos
    	});

    	$$self.$inject_state = $$props => {
    		if ("options" in $$props) $$invalidate(7, options = $$props.options);
    		if ("allTransacs" in $$props) $$invalidate(0, allTransacs = $$props.allTransacs);
    		if ("meta" in $$props) $$invalidate(1, meta = $$props.meta);
    		if ("infoType" in $$props) $$invalidate(2, infoType = $$props.infoType);
    		if ("showInfoBoard" in $$props) $$invalidate(3, showInfoBoard = $$props.showInfoBoard);
    		if ("opts" in $$props) $$invalidate(4, opts = $$props.opts);
    	};

    	let opts;

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*options*/ 128) {
    			// export let x = 0;
    			// export let y = 0;
    			// export let showMenu = false;
    			 $$invalidate(4, opts = [
    				...[
    					"bill",
    					"meta",
    					// "copy",
    					// "paste",
    					// "rename",
    					// 'move to',
    					// 'delete',
    					"bringtogether",
    					"ungroup"
    				],
    				...options
    			]);
    		}
    	};

    	return [
    		allTransacs,
    		meta,
    		infoType,
    		showInfoBoard,
    		opts,
    		$showMenu,
    		$menuPos,
    		options,
    		click_handler,
    		func,
    		click_handler_1
    	];
    }

    class ContextMenu extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, { options: 7 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "ContextMenu",
    			options,
    			id: create_fragment$3.name
    		});
    	}

    	get options() {
    		throw new Error("<ContextMenu>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set options(value) {
    		throw new Error("<ContextMenu>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/Canvas.svelte generated by Svelte v3.24.1 */

    const { console: console_1$3 } = globals;
    const file$3 = "src/Canvas.svelte";

    function create_fragment$4(ctx) {
    	let div;
    	let contextmenu;
    	let current;
    	contextmenu = new ContextMenu({ $$inline: true });

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(contextmenu.$$.fragment);
    			attr_dev(div, "class", "canvas svelte-80is7e");
    			add_location(div, file$3, 311, 0, 8373);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(contextmenu, div, null);
    			/*div_binding*/ ctx[2](div);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(contextmenu.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(contextmenu.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(contextmenu);
    			/*div_binding*/ ctx[2](null);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$4.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$4($$self, $$props, $$invalidate) {
    	let $menuPos;
    	let $DEV_SETTINGS;
    	let $isPenActive;
    	validate_store(menuPos, "menuPos");
    	component_subscribe($$self, menuPos, $$value => $$invalidate(11, $menuPos = $$value));
    	validate_store(DEV_SETTINGS, "DEV_SETTINGS");
    	component_subscribe($$self, DEV_SETTINGS, $$value => $$invalidate(12, $DEV_SETTINGS = $$value));
    	validate_store(isPenActive, "isPenActive");
    	component_subscribe($$self, isPenActive, $$value => $$invalidate(13, $isPenActive = $$value));
    	let { opts } = $$props;
    	let elCanvas;
    	let two;
    	let isMouseDown = false;
    	let isMouseMoved = false;
    	let isRightBtn = false;

    	// let showMenu = false,
    	//   menuX = 100,
    	//   menuY = 200;
    	let tag; // the tag user is drawing

    	let postDrawTimer; // timer for post-drawing processing
    	let isDrawing = false;
    	let drawingPath;
    	let allPaths = [];

    	// let drawingPathData = [];
    	const realOpts = {
    		lineWidth: 4,
    		curved: false,
    		color: "#7ef",
    		postColor: "#a8e",
    		// above are default opts
    		...opts
    	};

    	function handleMouseDown(evt) {
    		// $showMenu = false;
    		isMouseMoved = false;

    		isMouseDown = true;
    		evt.theX = evt.clientX || evt.pageX;
    		evt.theY = evt.clientY || evt.pageY;
    		isRightBtn = evt.button === 2;
    		console.log("////handle mouse down");
    	}

    	function handleMouseMove(evt) {
    		isMouseMoved = true;
    		evt.theX = evt.clientX || evt.pageX;
    		evt.theY = evt.clientY || evt.pageY; //  if ($currentTool === "eraser") {

    		//TODO
    		// }
    		if (isMouseDown && isRightBtn && !isDrawing) {
    			isDrawing = true;
    			beginDraw(evt);
    		}

    		if (isDrawing) {
    			drawing(evt);
    		} // evt.preventDefault();
    	}

    	function handleMouseUp(evt) {
    		// righ click
    		if (isMouseDown && !isMouseMoved && evt.button === 2) {
    			// $showMenu = true
    			isDrawing = false;

    			set_store_value(menuPos, $menuPos.x = evt.clientX, $menuPos);
    			set_store_value(menuPos, $menuPos.y = evt.clientY, $menuPos);
    			console.log("logging mouse pos", evt);
    		} else // menuY = evt.clientY;
    		//left click
    		if (isMouseDown && evt.button === 1) ; else {
    			evt.theX = evt.clientX || evt.pageX; // menuX = evt.clientX;
    			evt.theY = evt.clientY || evt.pageY;

    			if (isDrawing) {
    				console.log("drawing mouseup");
    				endDraw(evt);
    			} // evt.preventDefault();
    		}

    		// restore mouse state
    		isMouseDown = false;
    	}

    	function beginDraw(evt) {
    		console.log("////////////begin draw");
    		setDrawing(true);

    		if (!postDrawTimer) {
    			tag = {
    				uuid: v1(),
    				pathList: [],
    				targetIDs: new Set(), // TODO calc target IDs
    				type: "", // command|tag
    				info: {}, // for command only, info differs according to command type
    				position: { x: 0, y: 0 },
    				valid: false
    			};
    		} else {
    			clearTimeout(postDrawTimer);
    		}

    		// drawingPathData.length = 0;
    		drawingPath = createPath();

    		two.add(drawingPath);
    		drawingPath.vertices.push(new two_min.Vector(evt.theX, evt.theY));
    	} // drawingPathData.push()

    	function drawing(evt) {
    		drawingPath.vertices.push(new two_min.Vector(evt.theX, evt.theY));
    		two.update();
    		tag.valid = true;
    	}

    	function endDraw(evt) {
    		isDrawing = false;
    		setDrawing(false);
    		if (!tag.valid) return;
    		drawingPath.vertices.push(new two_min.Vector(evt.theX, evt.theY));
    		let pathForDisplay = createPath();
    		const simplifiedPath = simplify(drawingPath.vertices, 2.2, true);
    		drawingPath.vertices.length = 0;

    		for (const pos of simplifiedPath) {
    			drawingPath.vertices.push(new two_min.Vector(pos.x, pos.y));
    			pathForDisplay.vertices.push(new two_min.Vector(pos.x, pos.y));
    		}

    		two.add(pathForDisplay);
    		two.update();
    		allPaths.push(pathForDisplay);
    		tag.pathList.push(simplifiedPath);

    		postDrawTimer = setTimeout(
    			() => {
    				postDraw();
    				postDrawTimer = null;
    			},
    			$DEV_SETTINGS.postDrawDelay
    		);
    	}

    	function postDraw() {
    		const [dims, l] = normalizedPathList(tag.pathList);
    		tag.pathList = l;
    		tag.dims = dims;
    		allPaths.forEach(path => path.stroke = realOpts.postColor);
    		two.update();
    		const recRslt = recognize(tag);
    		console.log(touchedElms);
    		console.log("POST_DRAW Recognition:", recRslt);

    		if (recRslt.matched) {
    			if (recRslt.values[0].Name.includes("PREDEFINED")) {
    				const tagName = recRslt.values[0].Name.split(" ")[1];
    				const score = recRslt.values[0].Score;
    				console.log("matched tagName", tagName);

    				// Predefiend COMMANDS
    				if (tagName === "group-left") {
    					currentOnInkCmd("group-left");
    				} else if (tagName === "group-right") {
    					currentOnInkCmd("group-right");
    				} else if (tagName === "crossing") {
    					currentOnInkCmd("crossing");
    				} else // Predefined identifiers
    				{
    					const [dims, l] = normalizedPathList(tag.pathList, true);
    					tag.pathList = l;
    					console.log("drawn TAG", tagName, score);

    					currentOnInkCmd("tag", {
    						tag,
    						score,
    						recUUID: recRslt.values[0].Name
    					});
    				}
    			}
    		} else // a new tag created by user
    		{
    			console.log("NOT Matched");
    		}

    		// after all process was done
    		two.clear();

    		two.update();
    		touchedElms.clear();
    	}

    	onMount(() => {
    		let params = { width: "100%", height: "100%" };
    		two = new two_min(params).appendTo(elCanvas);
    		setCanvasTwo(two);

    		/// Reg mouse events on document
    		window.addEventListener("mousedown", handleMouseDown);

    		window.addEventListener("mousemove", handleMouseMove);
    		window.addEventListener("mouseup", handleMouseUp);
    		window.addEventListener("touchstart", handleMouseDown);
    		window.addEventListener("touchmove", handleMouseMove);
    		window.addEventListener("touchup", handleMouseUp);

    		window.oncontextmenu = evt => {
    			evt.preventDefault();
    		};

    		window.onpointerdown = evt => {
    			console.log("/// Canvas //onpointerdown", evt);

    			const aa = [
    				"height",
    				"isPrimary",
    				"pointerId",
    				"pointerType",
    				"pressure",
    				"tangentialPressure",
    				"tiltX",
    				"tiltY",
    				"twist",
    				"width"
    			].reduce(
    				(acc, cur) => {
    					acc[cur] = evt[cur];
    					return acc;
    				},
    				{}
    			);

    			if (evt.pointerType === "pen") {
    				evt.preventDefault();
    				evt.stopPropagation();
    				set_store_value(isPenActive, $isPenActive = true);
    				handleMouseDown(evt);
    			}

    			console.info(aa, evt);
    		};

    		window.onpointermove = evt => {
    			if (evt.pointerType === "pen") {
    				evt.preventDefault();
    				evt.stopPropagation();
    				handleMouseMove(evt);
    			}
    		};

    		window.onpointerup = evt => {
    			console.log("// Canvas ///onpointerup", evt);

    			if (evt.pointerType === "pen") {
    				evt.preventDefault();
    				evt.stopPropagation();
    				set_store_value(isPenActive, $isPenActive = false);
    				handleMouseUp(evt);
    			}
    		};
    	});

    	//// Helper functions ////////////
    	function createPath(
    		color = realOpts.color,
    	lineWidth = realOpts.lineWidth,
    	curved = realOpts.curved
    	) {
    		let path = new two_min.Path([], false, true);
    		path.curved = curved;
    		path.cap = path.join = "round";
    		path.noFill().stroke = color;
    		path.linewidth = lineWidth;
    		return path;
    	}

    	// function getPathList(twoPath) {}
    	function normalizedPathList(pathList, normalizeSize = false) {
    		//TODO normalize tag's size to 100*100
    		let top = 10000, left = 10000, right = 0, bottom = 0;

    		// get bounding box
    		for (const path of pathList) {
    			for (const pos of path) {
    				top = top > pos.y ? pos.y : top;
    				bottom = bottom < pos.y ? pos.y : bottom;
    				left = left > pos.x ? pos.x : left;
    				right = right < pos.x ? pos.x : right;
    			}
    		}

    		const w = right - left, h = bottom - top;

    		// normalize coords
    		for (const path of pathList) {
    			for (const pos of path) {
    				pos.x -= left;
    				pos.y -= top;

    				if (normalizeSize) {
    					pos.x = Math.floor(pos.x * 100 / w);
    					pos.y = Math.floor(pos.y * 100 / h);
    				}
    			}
    		}

    		tag.position.x = left;
    		tag.position.y = top;

    		return [
    			{
    				top: top - 10,
    				bottom,
    				left: left - 10,
    				right
    			},
    			pathList
    		];
    	}

    	const writable_props = ["opts"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1$3.warn(`<Canvas> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Canvas", $$slots, []);

    	function div_binding($$value) {
    		binding_callbacks[$$value ? "unshift" : "push"](() => {
    			elCanvas = $$value;
    			$$invalidate(0, elCanvas);
    		});
    	}

    	$$self.$$set = $$props => {
    		if ("opts" in $$props) $$invalidate(1, opts = $$props.opts);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		uuidv1: v1,
    		ContextMenu,
    		simplify,
    		Two: two_min,
    		model,
    		DEV_SETTINGS,
    		menuPos,
    		showMenu,
    		menuTarget,
    		isPenActive,
    		opts,
    		elCanvas,
    		two,
    		isMouseDown,
    		isMouseMoved,
    		isRightBtn,
    		tag,
    		postDrawTimer,
    		isDrawing,
    		drawingPath,
    		allPaths,
    		realOpts,
    		handleMouseDown,
    		handleMouseMove,
    		handleMouseUp,
    		beginDraw,
    		drawing,
    		endDraw,
    		postDraw,
    		createPath,
    		normalizedPathList,
    		$menuPos,
    		$DEV_SETTINGS,
    		$isPenActive
    	});

    	$$self.$inject_state = $$props => {
    		if ("opts" in $$props) $$invalidate(1, opts = $$props.opts);
    		if ("elCanvas" in $$props) $$invalidate(0, elCanvas = $$props.elCanvas);
    		if ("two" in $$props) two = $$props.two;
    		if ("isMouseDown" in $$props) isMouseDown = $$props.isMouseDown;
    		if ("isMouseMoved" in $$props) isMouseMoved = $$props.isMouseMoved;
    		if ("isRightBtn" in $$props) isRightBtn = $$props.isRightBtn;
    		if ("tag" in $$props) tag = $$props.tag;
    		if ("postDrawTimer" in $$props) postDrawTimer = $$props.postDrawTimer;
    		if ("isDrawing" in $$props) isDrawing = $$props.isDrawing;
    		if ("drawingPath" in $$props) drawingPath = $$props.drawingPath;
    		if ("allPaths" in $$props) allPaths = $$props.allPaths;
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [elCanvas, opts, div_binding];
    }

    class Canvas extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, { opts: 1 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Canvas",
    			options,
    			id: create_fragment$4.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*opts*/ ctx[1] === undefined && !("opts" in props)) {
    			console_1$3.warn("<Canvas> was created without expected prop 'opts'");
    		}
    	}

    	get opts() {
    		throw new Error("<Canvas>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set opts(value) {
    		throw new Error("<Canvas>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/MarkerWrapper.svelte generated by Svelte v3.24.1 */

    const { console: console_1$4 } = globals;
    const file$4 = "src/MarkerWrapper.svelte";

    // (328:8) marker
    function fallback_block$1(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("marker");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: fallback_block$1.name,
    		type: "fallback",
    		source: "(328:8) marker",
    		ctx
    	});

    	return block;
    }

    function create_fragment$5(ctx) {
    	let div1;
    	let t;
    	let div0;
    	let current;
    	let mounted;
    	let dispose;
    	const default_slot_template = /*$$slots*/ ctx[6].default;
    	const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[5], null);
    	const default_slot_or_fallback = default_slot || fallback_block$1(ctx);

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			if (default_slot_or_fallback) default_slot_or_fallback.c();
    			t = space();
    			div0 = element("div");
    			attr_dev(div0, "class", "svg-root svelte-1nayfv3");
    			add_location(div0, file$4, 328, 2, 10166);
    			attr_dev(div1, "class", "marker-wrapper svelte-1nayfv3");
    			add_location(div1, file$4, 300, 0, 9512);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);

    			if (default_slot_or_fallback) {
    				default_slot_or_fallback.m(div1, null);
    			}

    			append_dev(div1, t);
    			append_dev(div1, div0);
    			/*div0_binding*/ ctx[7](div0);
    			/*div1_binding*/ ctx[8](div1);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(div1, "mousedown", /*handleMouseDown*/ ctx[2], false, false, false),
    					listen_dev(div1, "mousemove", /*handleMouseMove*/ ctx[3], false, false, false),
    					listen_dev(div1, "mouseup", /*handleMouseUp*/ ctx[4], false, false, false),
    					listen_dev(div1, "touchstart", /*handleMouseDown*/ ctx[2], false, false, false),
    					listen_dev(div1, "touchmove", /*handleMouseMove*/ ctx[3], false, false, false),
    					listen_dev(div1, "touchend", /*handleMouseUp*/ ctx[4], false, false, false),
    					listen_dev(div1, "pointerdown", /*pointerdown_handler*/ ctx[9], false, false, false),
    					listen_dev(div1, "pointermove", /*pointermove_handler*/ ctx[10], false, false, false),
    					listen_dev(div1, "pointerup", /*pointerup_handler*/ ctx[11], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (default_slot) {
    				if (default_slot.p && dirty & /*$$scope*/ 32) {
    					update_slot(default_slot, default_slot_template, ctx, /*$$scope*/ ctx[5], dirty, null, null);
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(default_slot_or_fallback, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(default_slot_or_fallback, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			if (default_slot_or_fallback) default_slot_or_fallback.d(detaching);
    			/*div0_binding*/ ctx[7](null);
    			/*div1_binding*/ ctx[8](null);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$5.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function createPath$1(color, lineWidth, curved) {
    	let path = new Two.Path([], false, true);
    	path.curved = curved;
    	path.cap = path.join = "round";
    	path.noFill().stroke = color;
    	path.linewidth = lineWidth;
    	return path;
    }

    // TODO move into utils
    function makeGroupLine$1(x, y, w, h, type, path, level = 1) {
    	const xOffset = type === "group-right" ? 0 : 5;
    	const levelOffset = 7 * level;

    	// w = levelOffset;
    	x += xOffset;

    	path.vertices.push(new Two.Vector(type === "group-right"
    		? x - levelOffset + w
    		: x + levelOffset - w,
    	y));

    	path.vertices.push(new Two.Vector(type === "group-right"
    		? x - levelOffset
    		: x + levelOffset,
    	y));

    	path.vertices.push(new Two.Vector(type === "group-right"
    		? x - levelOffset
    		: x + levelOffset,
    	y + h));

    	path.vertices.push(new Two.Vector(type === "group-right"
    		? x - levelOffset + w
    		: x + levelOffset - w,
    	y + h));
    }

    function instance$5($$self, $$props, $$invalidate) {
    	let $showMenu;
    	let $menuPos;
    	validate_store(showMenu, "showMenu");
    	component_subscribe($$self, showMenu, $$value => $$invalidate(13, $showMenu = $$value));
    	validate_store(menuPos, "menuPos");
    	component_subscribe($$self, menuPos, $$value => $$invalidate(14, $menuPos = $$value));
    	let elWrapper;
    	let svgRoot;
    	let two;
    	let markers = []; // [{id:string,color:string,path:Two.Path,targets:dom-objs}]

    	function handleMouseDown(evt) {
    		setCurrentOnInkCmd((type, obj = undefined) => {
    			console.log("CurrentOnInkCmd", "type", type);
    			const elms = Array.from(touchedElms);
    			console.log("TOUCHED ELMS", elms);

    			// making line-group
    			if (type === "group-left" || type === "group-right") {
    				const linewidth = 2;

    				// const color =
    				//   "#" +
    				//   Math.round((0.2 + Math.random()) * 0xaaaaaa).toString(16) +
    				//   "88";
    				const g = new Two.Group();

    				const p = createPath$1("#33a", linewidth, false);
    				console.log("onInkCmd", elms);
    				const x = elms[0].offsetLeft + linewidth;
    				const y = elms[0].offsetTop + linewidth;
    				const endY = elms[elms.length - 1].offsetTop + linewidth;
    				const endH = elms[elms.length - 1].clientHeight;
    				const endW = elms[elms.length - 1].clientWidth;
    				console.log(x, y, endY, endH);
    				const memberIDs = elms.map(elm => elm.getAttribute("data-id"));
    				const touchedEmails = getEmails(memberIDs);
    				console.log("LINEpre", touchedEmails);

    				const occupied = touchedEmails.reduce(
    					(acc, cur) => {
    						const tmp = Array.from(cur.groupIDs.values()).// filter out groups at the same side , left or right
    						filter(id => {
    							const t = type === "group-right" ? "r" : "l"; // const a = acc > cur.groupIDs.size ? acc : cur.groupIDs.size;
    							// console.log(cur.groupIDs.size);

    							return groups[id].type === t;
    						}).// get all linelevels being occupied
    						map(id => groups[id].lineLevel);

    						acc = [...acc, ...tmp];
    						return acc;
    					},
    					// return a;
    					// 0
    					[]
    				); // const a = acc > cur.groupIDs.size ? acc : cur.groupIDs.size;
    				// console.log(cur.groupIDs.size);

    				console.log("occ", occupied);
    				let groupLineLevel = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].filter(l => !occupied.includes(l))[0];

    				const lineColor = [
    					"#33a",
    					"#a33",
    					"#3a3",
    					"#333",
    					"#333",
    					"#333",
    					"#333",
    					"#333",
    					"#333",
    					"#333"
    				][groupLineLevel - 1];

    				p.stroke = lineColor;
    				console.log("LINELEVEL", groupLineLevel);
    				makeGroupLine$1(type === "group-right" ? x : endW, y, 4, endY - y + endH, type, p, groupLineLevel);
    				g.add(p);
    				two.add(g);
    				two.update();
    				console.log("...", p._renderer.elem.parentNode);

    				// must call after update
    				g._renderer.elem.onpointerenter = e => {
    					const c = g._renderer.elem.children;

    					for (let index = 0; index < c.length; index++) {
    						const element = c[index];
    						element.style.stroke = "#333";
    						element.style.strokeWidth = "3px";
    					}
    				};

    				g._renderer.elem.onpointerout = e => {
    					const c = g._renderer.elem.children;

    					for (let index = 0; index < c.length; index++) {
    						const element = c[index];
    						element.style.stroke = lineColor;
    						element.style.strokeWidth = "2px";
    					}
    				};

    				g._renderer.elem.parentNode.style.pointerEvents = "visible";
    				g._renderer.elem.style.zIndex = 10 - groupLineLevel;

    				addGroup({
    					x: type === "group-right" ? x : endW,
    					y,
    					tagIDs: new Set(), //tag ids attached to this group
    					memberIDs,
    					memberDoms: elms,
    					subGroups: elms,
    					subGroupCount: 1,
    					subGroupDims: [],
    					color: lineColor,
    					type: type === "group-right" ? "r" : "l",
    					path: g,
    					g,
    					allPath: [p], // when drawing a group ink, there is only one path generated
    					two,
    					lineLevel: groupLineLevel
    				});

    				console.log("AADDEEDD", groups);
    			} else // cross selecting a line-group
    			if (type === "crossing") {
    				console.log(elms, "X");

    				const targetGroup = elms.filter(elm => {
    					const dataID = elm.parentNode.getAttribute("data-id");
    					return dataID && dataID.includes("group-");
    				});

    				if (targetGroup.length > 0) {
    					console.log("---crossing----", targetGroup[0]);
    					set_store_value(showMenu, $showMenu = true);
    					setTwoInvokedMenu(two);
    					setCurrentGroupID(targetGroup[0].parentNode.getAttribute("data-id"));
    				}
    			} else // drawing a tag
    			if (type === "tag") {
    				//TODO attach tag to a single item
    				// TODO use separate <g> for each line-group
    				// TODO add delete func to line-group
    				// attach tag to a line-group
    				const targetGroup = elms.filter(elm => {
    					const dataID = elm.parentNode.getAttribute("data-id");
    					return dataID && dataID.includes("group-");
    				});

    				if (targetGroup.length > 0) {
    					const groupID = targetGroup[0].parentNode.getAttribute("data-id");
    					const group = groups[groupID];

    					// display tag on line-group
    					const tagH = 30;

    					const offsetY = group.tagIDs.size * (tagH + 4);
    					console.log("OBJ", obj);
    					let pList = obj.tag.pathList;

    					if (obj.score > 0.7) {
    						obj.tag.uuid = obj.recUUID;
    						console.log(pList);
    						obj.tag.pathList = [...getTag(obj.recUUID).pathList].map(l => l.map(p => new Two.Vector(p.x, p.y)));
    						console.log(">.7", pList);
    					}

    					if (!group.tagIDs.has(obj.tag.uuid)) {
    						obj.tag.pathList.forEach(l => {
    							let p = new Two.Path(l, false, true);
    							p.cap = p.join = "round";
    							p.noFill().stroke = group.color;

    							// p.fill = group.color;
    							p.linewidth = 10;

    							p.translation = new Two.Vector(group.x - tagH / 2, group.y + offsetY);
    							console.log("OFFSET", p.translation);
    							p.scale = 0.3;
    							group.g.add(p);
    						});

    						two.update();

    						// connect tag with group
    						group.tagIDs.add(obj.tag.uuid);

    						obj.tag.targetIDs.add(groupID);
    						addTag(obj.tag);
    					}
    				}
    			}
    		});

    		set_store_value(menuPos, $menuPos = { x: evt.clientX, y: evt.clientY });
    		let dataID = evt.target.parentNode.getAttribute("data-id");
    		const isGroupLine = dataID && dataID.includes("group-");

    		if (// model.isDrawing && // mousedown always add to touchedElms
    		evt.target.getAttribute("data-name") === "marker" || isGroupLine) {
    			touchedElms.add(evt.target);
    		}
    	} // evt.preventDefault();

    	function handleMouseMove(evt) {
    		let dataID = evt.target.parentNode.getAttribute("data-id");
    		const isGroupLine = dataID && dataID.includes("group-");

    		if (isDrawing && (evt.target.getAttribute("data-name") === "marker" || isGroupLine)) {
    			touchedElms.add(evt.target);
    		}
    	} // evt.preventDefault();

    	function handleMouseUp(evt) {
    		let dataID = evt.target.parentNode.getAttribute("data-id");
    		const isGroupLine = dataID && dataID.includes("group-");

    		if (isDrawing && (evt.target.getAttribute("data-name") === "marker" || isGroupLine)) {
    			console.log("up in");
    			touchedElms.add(evt.target);
    		}
    	} // evt.preventDefault();

    	onMount(() => {
    		let params = { width: "100%", height: "100%" }; // class: "svg-root"
    		two = new Two(params).appendTo(svgRoot);
    		two.renderer.domElement.style.overflow = "visible";
    	});

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1$4.warn(`<MarkerWrapper> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("MarkerWrapper", $$slots, ['default']);

    	function div0_binding($$value) {
    		binding_callbacks[$$value ? "unshift" : "push"](() => {
    			svgRoot = $$value;
    			$$invalidate(1, svgRoot);
    		});
    	}

    	function div1_binding($$value) {
    		binding_callbacks[$$value ? "unshift" : "push"](() => {
    			elWrapper = $$value;
    			$$invalidate(0, elWrapper);
    		});
    	}

    	const pointerdown_handler = evt => {
    		if (evt.pointerType === "pen") {
    			evt.preventDefault();
    		}

    		handleMouseDown(evt);
    	};

    	const pointermove_handler = evt => {
    		if (evt.pointerType === "pen") {
    			evt.preventDefault();
    		}

    		handleMouseMove(evt);
    	};

    	const pointerup_handler = evt => {
    		if (evt.pointerType === "pen") {
    			evt.preventDefault();
    		}

    		handleMouseUp(evt);
    	};

    	$$self.$$set = $$props => {
    		if ("$$scope" in $$props) $$invalidate(5, $$scope = $$props.$$scope);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		model,
    		uuidv1: v1,
    		menuPos,
    		showMenu,
    		menuTarget,
    		elWrapper,
    		svgRoot,
    		two,
    		markers,
    		handleMouseDown,
    		handleMouseMove,
    		handleMouseUp,
    		createPath: createPath$1,
    		makeGroupLine: makeGroupLine$1,
    		$showMenu,
    		$menuPos
    	});

    	$$self.$inject_state = $$props => {
    		if ("elWrapper" in $$props) $$invalidate(0, elWrapper = $$props.elWrapper);
    		if ("svgRoot" in $$props) $$invalidate(1, svgRoot = $$props.svgRoot);
    		if ("two" in $$props) two = $$props.two;
    		if ("markers" in $$props) markers = $$props.markers;
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		elWrapper,
    		svgRoot,
    		handleMouseDown,
    		handleMouseMove,
    		handleMouseUp,
    		$$scope,
    		$$slots,
    		div0_binding,
    		div1_binding,
    		pointerdown_handler,
    		pointermove_handler,
    		pointerup_handler
    	];
    }

    class MarkerWrapper extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$5, create_fragment$5, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "MarkerWrapper",
    			options,
    			id: create_fragment$5.name
    		});
    	}
    }

    /* src/MailDemo.svelte generated by Svelte v3.24.1 */

    const { Object: Object_1$1, console: console_1$5 } = globals;
    const file$5 = "src/MailDemo.svelte";

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[33] = list[i];
    	return child_ctx;
    }

    function get_each_context_1$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[36] = list[i];
    	child_ctx[38] = i;
    	return child_ctx;
    }

    function get_each_context_2$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[39] = list[i];
    	child_ctx[40] = list;
    	child_ctx[41] = i;
    	return child_ctx;
    }

    function get_each_context_3$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[42] = list[i];
    	return child_ctx;
    }

    // (381:6) <Inkable         opts={{alwaysInking: true, autoClear: true, record: false, onTagDrawn: (tag) => {             let points = []             for (const [i, path] of tag.pathList.entries()) {               points = [...points, ...path.map((p) => {                   return {X: p.x, Y: p.y, ID: i + 1}                 })]             }             console.log('Gen PRE Defined TAG:', JSON.stringify(points))             const results = model.recognize(tag)             console.log('search tag result:', results)             emails = model.findByTagUUID(results.values[0].Name)           }}}         className={$currentTool === 'search'}>
    function create_default_slot_3(ctx) {
    	let div;

    	const block = {
    		c: function create() {
    			div = element("div");
    			attr_dev(div, "class", "search-placeholder svelte-13ox3uy");
    			add_location(div, file$5, 394, 8, 8247);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot_3.name,
    		type: "slot",
    		source: "(381:6) <Inkable         opts={{alwaysInking: true, autoClear: true, record: false, onTagDrawn: (tag) => {             let points = []             for (const [i, path] of tag.pathList.entries()) {               points = [...points, ...path.map((p) => {                   return {X: p.x, Y: p.y, ID: i + 1}                 })]             }             console.log('Gen PRE Defined TAG:', JSON.stringify(points))             const results = model.recognize(tag)             console.log('search tag result:', results)             emails = model.findByTagUUID(results.values[0].Name)           }}}         className={$currentTool === 'search'}>",
    		ctx
    	});

    	return block;
    }

    // (408:4) {#each Object.keys(allEmails) as folder}
    function create_each_block_3$1(ctx) {
    	let div;
    	let span0;
    	let t0_value = /*folder*/ ctx[42] + "";
    	let t0;
    	let t1;
    	let span1;
    	let t2;
    	let t3_value = /*allEmails*/ ctx[9][/*folder*/ ctx[42]].length + "";
    	let t3;
    	let t4;
    	let mounted;
    	let dispose;

    	function click_handler_2(...args) {
    		return /*click_handler_2*/ ctx[20](/*folder*/ ctx[42], ...args);
    	}

    	const block = {
    		c: function create() {
    			div = element("div");
    			span0 = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			span1 = element("span");
    			t2 = text("(");
    			t3 = text(t3_value);
    			t4 = text(")");
    			add_location(span0, file$5, 415, 8, 8802);
    			add_location(span1, file$5, 416, 8, 8832);
    			attr_dev(div, "class", "folder svelte-13ox3uy");
    			toggle_class(div, "selected", /*$currentFolder*/ ctx[12] === /*folder*/ ctx[42]);
    			add_location(div, file$5, 409, 6, 8643);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, span0);
    			append_dev(span0, t0);
    			append_dev(div, t1);
    			append_dev(div, span1);
    			append_dev(span1, t2);
    			append_dev(span1, t3);
    			append_dev(span1, t4);

    			if (!mounted) {
    				dispose = listen_dev(div, "click", click_handler_2, false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty[0] & /*allEmails*/ 512 && t0_value !== (t0_value = /*folder*/ ctx[42] + "")) set_data_dev(t0, t0_value);
    			if (dirty[0] & /*allEmails*/ 512 && t3_value !== (t3_value = /*allEmails*/ ctx[9][/*folder*/ ctx[42]].length + "")) set_data_dev(t3, t3_value);

    			if (dirty[0] & /*$currentFolder, allEmails*/ 4608) {
    				toggle_class(div, "selected", /*$currentFolder*/ ctx[12] === /*folder*/ ctx[42]);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_3$1.name,
    		type: "each",
    		source: "(408:4) {#each Object.keys(allEmails) as folder}",
    		ctx
    	});

    	return block;
    }

    // (452:6) {#each emails[$currentFolder] as mail, index (mail.id)}
    function create_each_block_2$1(key_1, ctx) {
    	let div2;
    	let h3;
    	let t0_value = /*mail*/ ctx[39].title + "";
    	let t0;
    	let t1;
    	let div0;
    	let t2_value = /*mail*/ ctx[39].time.toDateString() + "";
    	let t2;
    	let t3;
    	let div1;
    	let t4_value = /*mail*/ ctx[39].content.substring(0, 40) + "";
    	let t4;
    	let div2_data_id_value;
    	let t5;
    	let mounted;
    	let dispose;

    	function click_handler_5(...args) {
    		return /*click_handler_5*/ ctx[24](/*mail*/ ctx[39], /*each_value_2*/ ctx[40], /*index*/ ctx[41], ...args);
    	}

    	const block = {
    		key: key_1,
    		first: null,
    		c: function create() {
    			div2 = element("div");
    			h3 = element("h3");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");
    			t2 = text(t2_value);
    			t3 = space();
    			div1 = element("div");
    			t4 = text(t4_value);
    			t5 = space();
    			attr_dev(h3, "class", "title svelte-13ox3uy");
    			add_location(h3, file$5, 468, 10, 10357);
    			attr_dev(div0, "class", "time svelte-13ox3uy");
    			add_location(div0, file$5, 469, 10, 10403);
    			attr_dev(div1, "class", "brief");
    			add_location(div1, file$5, 470, 10, 10464);
    			attr_dev(div2, "data-name", "marker");
    			attr_dev(div2, "data-id", div2_data_id_value = /*mail*/ ctx[39].id);
    			attr_dev(div2, "class", "mail-item svelte-13ox3uy");
    			toggle_class(div2, "unread", !/*mail*/ ctx[39].read);
    			toggle_class(div2, "target", /*$currentMailID*/ ctx[13] === /*mail*/ ctx[39].id);
    			add_location(div2, file$5, 452, 8, 9776);
    			this.first = div2;
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, h3);
    			append_dev(h3, t0);
    			append_dev(div2, t1);
    			append_dev(div2, div0);
    			append_dev(div0, t2);
    			append_dev(div2, t3);
    			append_dev(div2, div1);
    			append_dev(div1, t4);
    			insert_dev(target, t5, anchor);

    			if (!mounted) {
    				dispose = listen_dev(div2, "click", click_handler_5, false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty[0] & /*emails, $currentFolder*/ 5120 && t0_value !== (t0_value = /*mail*/ ctx[39].title + "")) set_data_dev(t0, t0_value);
    			if (dirty[0] & /*emails, $currentFolder*/ 5120 && t2_value !== (t2_value = /*mail*/ ctx[39].time.toDateString() + "")) set_data_dev(t2, t2_value);
    			if (dirty[0] & /*emails, $currentFolder*/ 5120 && t4_value !== (t4_value = /*mail*/ ctx[39].content.substring(0, 40) + "")) set_data_dev(t4, t4_value);

    			if (dirty[0] & /*emails, $currentFolder*/ 5120 && div2_data_id_value !== (div2_data_id_value = /*mail*/ ctx[39].id)) {
    				attr_dev(div2, "data-id", div2_data_id_value);
    			}

    			if (dirty[0] & /*emails, $currentFolder*/ 5120) {
    				toggle_class(div2, "unread", !/*mail*/ ctx[39].read);
    			}

    			if (dirty[0] & /*$currentMailID, emails, $currentFolder*/ 13312) {
    				toggle_class(div2, "target", /*$currentMailID*/ ctx[13] === /*mail*/ ctx[39].id);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			if (detaching) detach_dev(t5);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_2$1.name,
    		type: "each",
    		source: "(452:6) {#each emails[$currentFolder] as mail, index (mail.id)}",
    		ctx
    	});

    	return block;
    }

    // (451:4) <MarkerWrapper>
    function create_default_slot_2(ctx) {
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_1_anchor;
    	let each_value_2 = /*emails*/ ctx[10][/*$currentFolder*/ ctx[12]];
    	validate_each_argument(each_value_2);
    	const get_key = ctx => /*mail*/ ctx[39].id;
    	validate_each_keys(ctx, each_value_2, get_each_context_2$1, get_key);

    	for (let i = 0; i < each_value_2.length; i += 1) {
    		let child_ctx = get_each_context_2$1(ctx, each_value_2, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_2$1(key, child_ctx));
    	}

    	const block = {
    		c: function create() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert_dev(target, each_1_anchor, anchor);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty[0] & /*emails, $currentFolder, $currentMailID, $isInking, allEmails, selectedMailIndex, $inkTarget*/ 62984) {
    				const each_value_2 = /*emails*/ ctx[10][/*$currentFolder*/ ctx[12]];
    				validate_each_argument(each_value_2);
    				validate_each_keys(ctx, each_value_2, get_each_context_2$1, get_key);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_2, each_1_lookup, each_1_anchor.parentNode, destroy_block, create_each_block_2$1, each_1_anchor, get_each_context_2$1);
    			}
    		},
    		d: function destroy(detaching) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d(detaching);
    			}

    			if (detaching) detach_dev(each_1_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot_2.name,
    		type: "slot",
    		source: "(451:4) <MarkerWrapper>",
    		ctx
    	});

    	return block;
    }

    // (488:51) 
    function create_if_block_1$1(ctx) {
    	let div0;
    	let button0;
    	let t1;
    	let button1;
    	let t3;
    	let button2;
    	let t5;
    	let div1;
    	let t6_value = getEmail(/*$currentMailID*/ ctx[13]).title + "";
    	let t6;
    	let t7;
    	let t8;
    	let inkable;
    	let t9;
    	let if_block1_anchor;
    	let current;
    	let mounted;
    	let dispose;
    	let if_block0 = tagsByInkID[/*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13]] && create_if_block_3(ctx);

    	inkable = new Inkable({
    			props: {
    				inkID: /*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13],
    				opts: { onTagDrawn: /*func_2*/ ctx[29] },
    				$$slots: { default: [create_default_slot] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	let if_block1 = /*predictResults*/ ctx[0].length > 0 && /*showPredict*/ ctx[1] && create_if_block_2(ctx);

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			button0 = element("button");
    			button0.textContent = "Reply";
    			t1 = space();
    			button1 = element("button");
    			button1.textContent = "Archive";
    			t3 = space();
    			button2 = element("button");
    			button2.textContent = "Delete";
    			t5 = space();
    			div1 = element("div");
    			t6 = text(t6_value);
    			t7 = space();
    			if (if_block0) if_block0.c();
    			t8 = space();
    			create_component(inkable.$$.fragment);
    			t9 = space();
    			if (if_block1) if_block1.c();
    			if_block1_anchor = empty();
    			attr_dev(button0, "class", "btn-reply svelte-13ox3uy");
    			add_location(button0, file$5, 489, 8, 11033);
    			attr_dev(button1, "class", "btn-archive svelte-13ox3uy");
    			add_location(button1, file$5, 497, 8, 11233);
    			attr_dev(button2, "class", "btn-delete svelte-13ox3uy");
    			add_location(button2, file$5, 511, 8, 11725);
    			attr_dev(div0, "class", "mail-tools svelte-13ox3uy");
    			add_location(div0, file$5, 488, 6, 11000);
    			attr_dev(div1, "class", "mail-title svelte-13ox3uy");
    			add_location(div1, file$5, 526, 6, 12220);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			append_dev(div0, button0);
    			append_dev(div0, t1);
    			append_dev(div0, button1);
    			append_dev(div0, t3);
    			append_dev(div0, button2);
    			insert_dev(target, t5, anchor);
    			insert_dev(target, div1, anchor);
    			append_dev(div1, t6);
    			insert_dev(target, t7, anchor);
    			if (if_block0) if_block0.m(target, anchor);
    			insert_dev(target, t8, anchor);
    			mount_component(inkable, target, anchor);
    			insert_dev(target, t9, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert_dev(target, if_block1_anchor, anchor);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(button0, "click", /*click_handler_6*/ ctx[26], false, false, false),
    					listen_dev(button1, "click", /*click_handler_7*/ ctx[27], false, false, false),
    					listen_dev(button2, "click", /*click_handler_8*/ ctx[28], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, dirty) {
    			if ((!current || dirty[0] & /*$currentMailID*/ 8192) && t6_value !== (t6_value = getEmail(/*$currentMailID*/ ctx[13]).title + "")) set_data_dev(t6, t6_value);

    			if (tagsByInkID[/*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13]]) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);

    					if (dirty[0] & /*$currentFolder, $currentMailID*/ 12288) {
    						transition_in(if_block0, 1);
    					}
    				} else {
    					if_block0 = create_if_block_3(ctx);
    					if_block0.c();
    					transition_in(if_block0, 1);
    					if_block0.m(t8.parentNode, t8);
    				}
    			} else if (if_block0) {
    				group_outros();

    				transition_out(if_block0, 1, 1, () => {
    					if_block0 = null;
    				});

    				check_outros();
    			}

    			const inkable_changes = {};
    			if (dirty[0] & /*$currentFolder, $currentMailID*/ 12288) inkable_changes.inkID = /*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13];
    			if (dirty[0] & /*predictResults, $allInks, $currentMailID, $currentFolder*/ 77825) inkable_changes.opts = { onTagDrawn: /*func_2*/ ctx[29] };

    			if (dirty[0] & /*$currentMailID, $currentFolder*/ 12288 | dirty[1] & /*$$scope*/ 16384) {
    				inkable_changes.$$scope = { dirty, ctx };
    			}

    			inkable.$set(inkable_changes);

    			if (/*predictResults*/ ctx[0].length > 0 && /*showPredict*/ ctx[1]) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);

    					if (dirty[0] & /*predictResults, showPredict*/ 3) {
    						transition_in(if_block1, 1);
    					}
    				} else {
    					if_block1 = create_if_block_2(ctx);
    					if_block1.c();
    					transition_in(if_block1, 1);
    					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
    				}
    			} else if (if_block1) {
    				group_outros();

    				transition_out(if_block1, 1, 1, () => {
    					if_block1 = null;
    				});

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block0);
    			transition_in(inkable.$$.fragment, local);
    			transition_in(if_block1);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block0);
    			transition_out(inkable.$$.fragment, local);
    			transition_out(if_block1);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			if (detaching) detach_dev(t5);
    			if (detaching) detach_dev(div1);
    			if (detaching) detach_dev(t7);
    			if (if_block0) if_block0.d(detaching);
    			if (detaching) detach_dev(t8);
    			destroy_component(inkable, detaching);
    			if (detaching) detach_dev(t9);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach_dev(if_block1_anchor);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1$1.name,
    		type: "if",
    		source: "(488:51) ",
    		ctx
    	});

    	return block;
    }

    // (480:4) {#if writingMail}
    function create_if_block$3(ctx) {
    	let replyblock;
    	let current;

    	replyblock = new ReplyBlock({
    			props: {
    				from: getEmail(/*$currentMailID*/ ctx[13]).to,
    				to: getEmail(/*$currentMailID*/ ctx[13]).from,
    				title: "RE: " + getEmail(/*$currentMailID*/ ctx[13]).title,
    				clickHandler: /*func_1*/ ctx[25]
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(replyblock.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(replyblock, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const replyblock_changes = {};
    			if (dirty[0] & /*$currentMailID*/ 8192) replyblock_changes.from = getEmail(/*$currentMailID*/ ctx[13]).to;
    			if (dirty[0] & /*$currentMailID*/ 8192) replyblock_changes.to = getEmail(/*$currentMailID*/ ctx[13]).from;
    			if (dirty[0] & /*$currentMailID*/ 8192) replyblock_changes.title = "RE: " + getEmail(/*$currentMailID*/ ctx[13]).title;
    			if (dirty[0] & /*writingMail*/ 128) replyblock_changes.clickHandler = /*func_1*/ ctx[25];
    			replyblock.$set(replyblock_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(replyblock.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(replyblock.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(replyblock, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$3.name,
    		type: "if",
    		source: "(480:4) {#if writingMail}",
    		ctx
    	});

    	return block;
    }

    // (528:6) {#if model.tagsByInkID[$currentFolder + 'mail-item' + $currentMailID]}
    function create_if_block_3(ctx) {
    	let div;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let current;
    	let each_value_1 = Object.values(tagsByInkID[/*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13]]);
    	validate_each_argument(each_value_1);
    	const get_key = ctx => /*tag*/ ctx[36].uuid;
    	validate_each_keys(ctx, each_value_1, get_each_context_1$1, get_key);

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		let child_ctx = get_each_context_1$1(ctx, each_value_1, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_1$1(key, child_ctx));
    	}

    	const block = {
    		c: function create() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(div, "class", "own-tags svelte-13ox3uy");
    			add_location(div, file$5, 528, 8, 12374);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (dirty[0] & /*$currentFolder, $currentMailID*/ 12288) {
    				const each_value_1 = Object.values(tagsByInkID[/*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13]]);
    				validate_each_argument(each_value_1);
    				group_outros();
    				validate_each_keys(ctx, each_value_1, get_each_context_1$1, get_key);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_1, each_1_lookup, div, outro_and_destroy_block, create_each_block_1$1, null, get_each_context_1$1);
    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value_1.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_3.name,
    		type: "if",
    		source: "(528:6) {#if model.tagsByInkID[$currentFolder + 'mail-item' + $currentMailID]}",
    		ctx
    	});

    	return block;
    }

    // (539:16) <Inkable                   opts={{acceptDrawing: false, fillParent: true}}                   tagData={tag}>
    function create_default_slot_1(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("inks");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot_1.name,
    		type: "slot",
    		source: "(539:16) <Inkable                   opts={{acceptDrawing: false, fillParent: true}}                   tagData={tag}>",
    		ctx
    	});

    	return block;
    }

    // (530:10) {#each Object.values(model.tagsByInkID[$currentFolder + 'mail-item' + $currentMailID]) as tag, i (tag.uuid)}
    function create_each_block_1$1(key_1, ctx) {
    	let div;
    	let label;
    	let label_for_value;
    	let t0;
    	let button;
    	let inkable;
    	let button_id_value;
    	let t1;
    	let current;

    	inkable = new Inkable({
    			props: {
    				opts: { acceptDrawing: false, fillParent: true },
    				tagData: /*tag*/ ctx[36],
    				$$slots: { default: [create_default_slot_1] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	const block = {
    		key: key_1,
    		first: null,
    		c: function create() {
    			div = element("div");
    			label = element("label");
    			t0 = space();
    			button = element("button");
    			create_component(inkable.$$.fragment);
    			t1 = space();
    			attr_dev(label, "for", label_for_value = "btn-" + /*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13]);
    			add_location(label, file$5, 531, 14, 12564);
    			attr_dev(button, "id", button_id_value = "btn-" + /*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13]);
    			attr_dev(button, "class", "svelte-13ox3uy");
    			add_location(button, file$5, 536, 14, 12776);
    			attr_dev(div, "class", "own-tag svelte-13ox3uy");
    			add_location(div, file$5, 530, 12, 12528);
    			this.first = div;
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, label);
    			append_dev(div, t0);
    			append_dev(div, button);
    			mount_component(inkable, button, null);
    			append_dev(div, t1);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (!current || dirty[0] & /*$currentFolder, $currentMailID*/ 12288 && label_for_value !== (label_for_value = "btn-" + /*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13])) {
    				attr_dev(label, "for", label_for_value);
    			}

    			const inkable_changes = {};
    			if (dirty[0] & /*$currentFolder, $currentMailID*/ 12288) inkable_changes.tagData = /*tag*/ ctx[36];

    			if (dirty[1] & /*$$scope*/ 16384) {
    				inkable_changes.$$scope = { dirty, ctx };
    			}

    			inkable.$set(inkable_changes);

    			if (!current || dirty[0] & /*$currentFolder, $currentMailID*/ 12288 && button_id_value !== (button_id_value = "btn-" + /*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13])) {
    				attr_dev(button, "id", button_id_value);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(inkable.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(inkable.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(inkable);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_1$1.name,
    		type: "each",
    		source: "(530:10) {#each Object.values(model.tagsByInkID[$currentFolder + 'mail-item' + $currentMailID]) as tag, i (tag.uuid)}",
    		ctx
    	});

    	return block;
    }

    // (551:6) <Inkable         inkID={$currentFolder + 'mail-item' + $currentMailID}         opts={{onTagDrawn: (tag) => {             predictResults = model.recognize(tag, 3)             console.table(predictResults.values)             predictResults = predictResults.matched ? predictResults.values : []             if (predictResults.length > 0 && predictResults[0].Score > 0.7) {               tag.uuid = predictResults[0].Name               console.log('APPEND existing tag:', model.getTag(tag.uuid))             }             model.addTag(tag)             $allInks = [...$allInks, tag]             $currentMailID = $currentMailID + ''             const tgs = model.tagsByInkID[$currentFolder + 'mail-item' + $currentMailID]           }}}>
    function create_default_slot(ctx) {
    	let div1;
    	let div0;
    	let t0_value = /*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13] + "";
    	let t0;
    	let t1;
    	let p;
    	let t2_value = getEmail(/*$currentMailID*/ ctx[13]).content + "";
    	let t2;

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			div0 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			p = element("p");
    			t2 = text(t2_value);
    			attr_dev(div0, "class", "debugger svelte-13ox3uy");
    			add_location(div0, file$5, 566, 10, 13907);
    			attr_dev(p, "class", "svelte-13ox3uy");
    			add_location(p, file$5, 569, 10, 14017);
    			attr_dev(div1, "class", "mail-content svelte-13ox3uy");
    			add_location(div1, file$5, 565, 8, 13870);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, div0);
    			append_dev(div0, t0);
    			append_dev(div1, t1);
    			append_dev(div1, p);
    			append_dev(p, t2);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty[0] & /*$currentFolder, $currentMailID*/ 12288 && t0_value !== (t0_value = /*$currentFolder*/ ctx[12] + "mail-item" + /*$currentMailID*/ ctx[13] + "")) set_data_dev(t0, t0_value);
    			if (dirty[0] & /*$currentMailID*/ 8192 && t2_value !== (t2_value = getEmail(/*$currentMailID*/ ctx[13]).content + "")) set_data_dev(t2, t2_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot.name,
    		type: "slot",
    		source: "(551:6) <Inkable         inkID={$currentFolder + 'mail-item' + $currentMailID}         opts={{onTagDrawn: (tag) => {             predictResults = model.recognize(tag, 3)             console.table(predictResults.values)             predictResults = predictResults.matched ? predictResults.values : []             if (predictResults.length > 0 && predictResults[0].Score > 0.7) {               tag.uuid = predictResults[0].Name               console.log('APPEND existing tag:', model.getTag(tag.uuid))             }             model.addTag(tag)             $allInks = [...$allInks, tag]             $currentMailID = $currentMailID + ''             const tgs = model.tagsByInkID[$currentFolder + 'mail-item' + $currentMailID]           }}}>",
    		ctx
    	});

    	return block;
    }

    // (573:6) {#if predictResults.length > 0 && showPredict}
    function create_if_block_2(ctx) {
    	let div;
    	let current;
    	let each_value = /*predictResults*/ ctx[0];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	const block = {
    		c: function create() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(div, "class", "predict-tags svelte-13ox3uy");
    			add_location(div, file$5, 573, 8, 14158);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (dirty[0] & /*predictResults*/ 1) {
    				each_value = /*predictResults*/ ctx[0];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2.name,
    		type: "if",
    		source: "(573:6) {#if predictResults.length > 0 && showPredict}",
    		ctx
    	});

    	return block;
    }

    // (575:10) {#each predictResults as tg}
    function create_each_block$1(ctx) {
    	let div;
    	let inkable;
    	let t0;
    	let span;
    	let t1;
    	let t2_value = /*tg*/ ctx[33].Score + "";
    	let t2;
    	let t3;
    	let current;

    	inkable = new Inkable({
    			props: {
    				opts: { acceptDrawing: false, fillParent: true },
    				tagData: getTag(/*tg*/ ctx[33].Name)
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(inkable.$$.fragment);
    			t0 = space();
    			span = element("span");
    			t1 = text("Score: ");
    			t2 = text(t2_value);
    			t3 = space();
    			attr_dev(span, "class", "svelte-13ox3uy");
    			add_location(span, file$5, 579, 14, 14414);
    			attr_dev(div, "class", "predict-tag svelte-13ox3uy");
    			add_location(div, file$5, 575, 12, 14236);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(inkable, div, null);
    			append_dev(div, t0);
    			append_dev(div, span);
    			append_dev(span, t1);
    			append_dev(span, t2);
    			append_dev(div, t3);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const inkable_changes = {};
    			if (dirty[0] & /*predictResults*/ 1) inkable_changes.tagData = getTag(/*tg*/ ctx[33].Name);
    			inkable.$set(inkable_changes);
    			if ((!current || dirty[0] & /*predictResults*/ 1) && t2_value !== (t2_value = /*tg*/ ctx[33].Score + "")) set_data_dev(t2, t2_value);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(inkable.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(inkable.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(inkable);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$1.name,
    		type: "each",
    		source: "(575:10) {#each predictResults as tg}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$6(ctx) {
    	let script;
    	let script_src_value;
    	let t0;
    	let canvas;
    	let t1;
    	let div7;
    	let nav;
    	let div0;
    	let button0;
    	let t3;
    	let button1;
    	let t5;
    	let button2;
    	let t7;
    	let div1;
    	let inkable;
    	let t8;
    	let div3;
    	let t9;
    	let div2;
    	let input;
    	let t10;
    	let button3;
    	let t12;
    	let div5;
    	let div4;
    	let button4;
    	let t13;
    	let t14;
    	let t15;
    	let markerwrapper;
    	let t16;
    	let div6;
    	let current_block_type_index;
    	let if_block;
    	let current;
    	let mounted;
    	let dispose;
    	canvas = new Canvas({ $$inline: true });

    	inkable = new Inkable({
    			props: {
    				opts: {
    					alwaysInking: true,
    					autoClear: true,
    					record: false,
    					onTagDrawn: /*func*/ ctx[19]
    				},
    				className: /*$currentTool*/ ctx[11] === "search",
    				$$slots: { default: [create_default_slot_3] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	let each_value_3 = Object.keys(/*allEmails*/ ctx[9]);
    	validate_each_argument(each_value_3);
    	let each_blocks = [];

    	for (let i = 0; i < each_value_3.length; i += 1) {
    		each_blocks[i] = create_each_block_3$1(get_each_context_3$1(ctx, each_value_3, i));
    	}

    	markerwrapper = new MarkerWrapper({
    			props: {
    				$$slots: { default: [create_default_slot_2] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	const if_block_creators = [create_if_block$3, create_if_block_1$1];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*writingMail*/ ctx[7]) return 0;
    		if (/*allEmails*/ ctx[9][/*$currentFolder*/ ctx[12]].length > 0) return 1;
    		return -1;
    	}

    	if (~(current_block_type_index = select_block_type(ctx))) {
    		if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    	}

    	const block = {
    		c: function create() {
    			script = element("script");
    			t0 = space();
    			create_component(canvas.$$.fragment);
    			t1 = space();
    			div7 = element("div");
    			nav = element("nav");
    			div0 = element("div");
    			button0 = element("button");
    			button0.textContent = "New Mail";
    			t3 = space();
    			button1 = element("button");
    			button1.textContent = "Predict";
    			t5 = space();
    			button2 = element("button");
    			button2.textContent = "Search";
    			t7 = space();
    			div1 = element("div");
    			create_component(inkable.$$.fragment);
    			t8 = space();
    			div3 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t9 = space();
    			div2 = element("div");
    			input = element("input");
    			t10 = space();
    			button3 = element("button");
    			button3.textContent = "add";
    			t12 = space();
    			div5 = element("div");
    			div4 = element("div");
    			button4 = element("button");
    			t13 = text("order by:");
    			t14 = text(/*sortAlgorithm*/ ctx[8]);
    			t15 = space();
    			create_component(markerwrapper.$$.fragment);
    			t16 = space();
    			div6 = element("div");
    			if (if_block) if_block.c();
    			document.title = "MailDemo";
    			if (script.src !== (script_src_value = "https://cdn.jsdelivr.net/npm/leader-line@1.0.5/leader-line.min.js")) attr_dev(script, "src", script_src_value);
    			attr_dev(script, "class", "svelte-13ox3uy");
    			add_location(script, file$5, 310, 2, 5793);
    			attr_dev(button0, "class", "svelte-13ox3uy");
    			add_location(button0, file$5, 343, 6, 6601);
    			attr_dev(button1, "class", "svelte-13ox3uy");
    			toggle_class(button1, "btn-on", /*$currentTool*/ ctx[11] === "eraser");
    			add_location(button1, file$5, 344, 6, 6633);
    			attr_dev(button2, "class", "btn-search svelte-13ox3uy");
    			toggle_class(button2, "btn-on", /*$currentTool*/ ctx[11] === "search");
    			add_location(button2, file$5, 351, 6, 6803);
    			add_location(div0, file$5, 319, 4, 6016);
    			attr_dev(div1, "class", "search-panel svelte-13ox3uy");
    			toggle_class(div1, "hidden", /*$currentTool*/ ctx[11] !== "search");
    			add_location(div1, file$5, 375, 4, 7413);
    			attr_dev(nav, "class", "svelte-13ox3uy");
    			add_location(nav, file$5, 318, 2, 6006);
    			attr_dev(input, "type", "text");
    			attr_dev(input, "class", "svelte-13ox3uy");
    			add_location(input, file$5, 421, 6, 8962);
    			add_location(button3, file$5, 422, 6, 9013);
    			attr_dev(div2, "class", "folder-adder svelte-13ox3uy");
    			add_location(div2, file$5, 420, 4, 8929);
    			attr_dev(div3, "class", "sidebar svelte-13ox3uy");
    			add_location(div3, file$5, 406, 2, 8534);
    			add_location(button4, file$5, 436, 6, 9324);
    			attr_dev(div4, "class", "list-tools");
    			add_location(div4, file$5, 435, 4, 9293);
    			attr_dev(div5, "class", "mail-list svelte-13ox3uy");
    			add_location(div5, file$5, 433, 2, 9264);
    			attr_dev(div6, "class", "detail-container svelte-13ox3uy");
    			add_location(div6, file$5, 478, 2, 10637);
    			attr_dev(div7, "class", "root svelte-13ox3uy");
    			add_location(div7, file$5, 317, 0, 5985);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			append_dev(document.head, script);
    			insert_dev(target, t0, anchor);
    			mount_component(canvas, target, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, div7, anchor);
    			append_dev(div7, nav);
    			append_dev(nav, div0);
    			append_dev(div0, button0);
    			append_dev(div0, t3);
    			append_dev(div0, button1);
    			append_dev(div0, t5);
    			append_dev(div0, button2);
    			append_dev(nav, t7);
    			append_dev(nav, div1);
    			mount_component(inkable, div1, null);
    			append_dev(div7, t8);
    			append_dev(div7, div3);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div3, null);
    			}

    			append_dev(div3, t9);
    			append_dev(div3, div2);
    			append_dev(div2, input);
    			/*input_binding*/ ctx[21](input);
    			append_dev(div2, t10);
    			append_dev(div2, button3);
    			append_dev(div7, t12);
    			append_dev(div7, div5);
    			append_dev(div5, div4);
    			append_dev(div4, button4);
    			append_dev(button4, t13);
    			append_dev(button4, t14);
    			append_dev(div5, t15);
    			mount_component(markerwrapper, div5, null);
    			append_dev(div7, t16);
    			append_dev(div7, div6);

    			if (~current_block_type_index) {
    				if_blocks[current_block_type_index].m(div6, null);
    			}

    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(button1, "click", /*click_handler*/ ctx[17], false, false, false),
    					listen_dev(button2, "click", /*click_handler_1*/ ctx[18], false, false, false),
    					listen_dev(button3, "click", /*click_handler_3*/ ctx[22], false, false, false),
    					listen_dev(button4, "click", /*click_handler_4*/ ctx[23], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, dirty) {
    			if (dirty[0] & /*$currentTool*/ 2048) {
    				toggle_class(button1, "btn-on", /*$currentTool*/ ctx[11] === "eraser");
    			}

    			if (dirty[0] & /*$currentTool*/ 2048) {
    				toggle_class(button2, "btn-on", /*$currentTool*/ ctx[11] === "search");
    			}

    			const inkable_changes = {};

    			if (dirty[0] & /*emails*/ 1024) inkable_changes.opts = {
    				alwaysInking: true,
    				autoClear: true,
    				record: false,
    				onTagDrawn: /*func*/ ctx[19]
    			};

    			if (dirty[0] & /*$currentTool*/ 2048) inkable_changes.className = /*$currentTool*/ ctx[11] === "search";

    			if (dirty[1] & /*$$scope*/ 16384) {
    				inkable_changes.$$scope = { dirty, ctx };
    			}

    			inkable.$set(inkable_changes);

    			if (dirty[0] & /*$currentTool*/ 2048) {
    				toggle_class(div1, "hidden", /*$currentTool*/ ctx[11] !== "search");
    			}

    			if (dirty[0] & /*$currentFolder, allEmails*/ 4608) {
    				each_value_3 = Object.keys(/*allEmails*/ ctx[9]);
    				validate_each_argument(each_value_3);
    				let i;

    				for (i = 0; i < each_value_3.length; i += 1) {
    					const child_ctx = get_each_context_3$1(ctx, each_value_3, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_3$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div3, t9);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_3.length;
    			}

    			if (!current || dirty[0] & /*sortAlgorithm*/ 256) set_data_dev(t14, /*sortAlgorithm*/ ctx[8]);
    			const markerwrapper_changes = {};

    			if (dirty[0] & /*emails, $currentFolder, $currentMailID, $isInking, allEmails, selectedMailIndex, $inkTarget*/ 62984 | dirty[1] & /*$$scope*/ 16384) {
    				markerwrapper_changes.$$scope = { dirty, ctx };
    			}

    			markerwrapper.$set(markerwrapper_changes);
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if (~current_block_type_index) {
    					if_blocks[current_block_type_index].p(ctx, dirty);
    				}
    			} else {
    				if (if_block) {
    					group_outros();

    					transition_out(if_blocks[previous_block_index], 1, 1, () => {
    						if_blocks[previous_block_index] = null;
    					});

    					check_outros();
    				}

    				if (~current_block_type_index) {
    					if_block = if_blocks[current_block_type_index];

    					if (!if_block) {
    						if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    						if_block.c();
    					}

    					transition_in(if_block, 1);
    					if_block.m(div6, null);
    				} else {
    					if_block = null;
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(canvas.$$.fragment, local);
    			transition_in(inkable.$$.fragment, local);
    			transition_in(markerwrapper.$$.fragment, local);
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(canvas.$$.fragment, local);
    			transition_out(inkable.$$.fragment, local);
    			transition_out(markerwrapper.$$.fragment, local);
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			detach_dev(script);
    			if (detaching) detach_dev(t0);
    			destroy_component(canvas, detaching);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(div7);
    			destroy_component(inkable);
    			destroy_each(each_blocks, detaching);
    			/*input_binding*/ ctx[21](null);
    			destroy_component(markerwrapper);

    			if (~current_block_type_index) {
    				if_blocks[current_block_type_index].d();
    			}

    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$6.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$6($$self, $$props, $$invalidate) {
    	let $currentTool;
    	let $currentFolder;
    	let $currentMailID;
    	let $isInking;
    	let $inkTarget;
    	let $allInks;
    	validate_store(currentTool, "currentTool");
    	component_subscribe($$self, currentTool, $$value => $$invalidate(11, $currentTool = $$value));
    	validate_store(currentFolder, "currentFolder");
    	component_subscribe($$self, currentFolder, $$value => $$invalidate(12, $currentFolder = $$value));
    	validate_store(currentMailID, "currentMailID");
    	component_subscribe($$self, currentMailID, $$value => $$invalidate(13, $currentMailID = $$value));
    	validate_store(isInking, "isInking");
    	component_subscribe($$self, isInking, $$value => $$invalidate(14, $isInking = $$value));
    	validate_store(inkTarget, "inkTarget");
    	component_subscribe($$self, inkTarget, $$value => $$invalidate(15, $inkTarget = $$value));
    	validate_store(allInks, "allInks");
    	component_subscribe($$self, allInks, $$value => $$invalidate(16, $allInks = $$value));
    	let tmpTagProbs = {};
    	let predictResults = [];
    	let showPredict = false;

    	// let isInking = false;
    	let fnameInput;

    	let selectedMailIndex = 0;

    	const listLen = {
    		inbox: 10,
    		flagged: 7,
    		drafts: 5,
    		sent: 3,
    		spam: 2
    	};

    	let folderList = ["inbox", "flagged", "drafts", "sent", "archive", "spam"];
    	let searching = false;
    	let writingMail = false;

    	////  ////////////
    	let tmpEmails = {};

    	let sortAlgorithm = "time";
    	let allEmails = {};
    	let timer;

    	onMount(() => {
    		timer = setInterval(
    			() => {
    				if (trigger) {
    					setTrigger(false);
    					$$invalidate(10, emails$1 = emails);
    					console.log("updated emails by trigger");

    					setTimeout(
    						() => {
    							redrawAllGroups();
    						},
    						200
    					);
    				}
    			},
    			100
    		);

    		return () => {
    			clearInterval(timer);
    		};
    	});

    	const writable_props = [];

    	Object_1$1.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1$5.warn(`<MailDemo> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("MailDemo", $$slots, []);

    	const click_handler = () => {
    		$$invalidate(1, showPredict = !showPredict);
    	};

    	const click_handler_1 = () => {
    		if ($currentTool === "search") {
    			set_store_value(currentTool, $currentTool = "");
    			$$invalidate(6, searching = false);
    			$$invalidate(10, emails$1 = emails);

    			setTimeout(
    				() => {
    					redrawAllGroups();
    				},
    				0
    			);
    		} else {
    			$$invalidate(6, searching = true);
    			set_store_value(currentTool, $currentTool = "search");
    		}
    	};

    	const func = tag => {
    		let points = [];

    		for (const [i, path] of tag.pathList.entries()) {
    			points = [
    				...points,
    				...path.map(p => {
    					return { X: p.x, Y: p.y, ID: i + 1 };
    				})
    			];
    		}

    		console.log("Gen PRE Defined TAG:", JSON.stringify(points));
    		const results = recognize(tag);
    		console.log("search tag result:", results);
    		$$invalidate(10, emails$1 = findByTagUUID(results.values[0].Name));
    	};

    	const click_handler_2 = folder => {
    		set_store_value(currentFolder, $currentFolder = folder);
    	};

    	function input_binding($$value) {
    		binding_callbacks[$$value ? "unshift" : "push"](() => {
    			fnameInput = $$value;
    			$$invalidate(2, fnameInput);
    		});
    	}

    	const click_handler_3 = () => {
    		const fname = fnameInput.value.trim();
    		$$invalidate(5, folderList = [...folderList, fname]);
    		$$invalidate(9, allEmails[fname] = [], allEmails);
    		$$invalidate(4, listLen[fname] = 0, listLen);
    	};

    	const click_handler_4 = () => {
    		if (sortAlgorithm === "time") {
    			$$invalidate(8, sortAlgorithm = "title");
    		} else if (sortAlgorithm === "title") {
    			$$invalidate(8, sortAlgorithm = "time");
    		}

    		setTimeout(
    			() => {
    				redrawAllGroups();
    			},
    			0
    		);
    	};

    	const click_handler_5 = (mail, each_value_2, index, evt) => {
    		$$invalidate(10, each_value_2[index].read = true, emails$1, currentFolder.set($currentFolder));

    		if (!$isInking) {
    			setCurrentMail(allEmails[$currentFolder][$currentMailID]);
    			set_store_value(currentMailID, $currentMailID = mail.id);
    			$$invalidate(3, selectedMailIndex = index - 1);
    			set_store_value(inkTarget, $inkTarget = $currentFolder + "mail-item" + mail.id);
    			console.log("Mail-item", mail.id);
    		}
    	};

    	const func_1 = () => {
    		$$invalidate(7, writingMail = false);
    	};

    	const click_handler_6 = () => {
    		$$invalidate(7, writingMail = true);
    		console.log("writing?", writingMail);
    	};

    	const click_handler_7 = () => {
    		let index = 0;

    		allEmails[$currentFolder].forEach((email, i) => {
    			if (email.id == $currentMailID) index = i;
    		});

    		console.log("index to delete is", index);
    		const moveMail = allEmails[$currentFolder].splice(index, 1);
    		allEmails.archive.push(moveMail);
    		$$invalidate(9, allEmails = { ...allEmails });
    	};

    	const click_handler_8 = () => {
    		console.log($currentFolder, $currentMailID);
    		let index = 0;

    		allEmails[$currentFolder].forEach((email, i) => {
    			if (email.id == $currentMailID) index = i;
    		});

    		console.log("index to delete is", index);
    		allEmails[$currentFolder].splice(index, 1);
    		$$invalidate(9, allEmails = { ...allEmails });
    	};

    	const func_2 = tag => {
    		$$invalidate(0, predictResults = recognize(tag, 3));
    		console.table(predictResults.values);
    		$$invalidate(0, predictResults = predictResults.matched ? predictResults.values : []);

    		if (predictResults.length > 0 && predictResults[0].Score > 0.7) {
    			tag.uuid = predictResults[0].Name;
    			console.log("APPEND existing tag:", getTag(tag.uuid));
    		}

    		addTag(tag);
    		set_store_value(allInks, $allInks = [...$allInks, tag]);
    		set_store_value(currentMailID, $currentMailID = $currentMailID + "");
    	};

    	$$self.$capture_state = () => ({
    		Inkable,
    		ReplyBlock,
    		Canvas,
    		MarkerWrapper,
    		onMount,
    		currentFolder,
    		currentTool,
    		inkTarget,
    		isInking,
    		allInks,
    		currentMailID,
    		model,
    		tmpTagProbs,
    		predictResults,
    		showPredict,
    		fnameInput,
    		selectedMailIndex,
    		listLen,
    		folderList,
    		searching,
    		writingMail,
    		tmpEmails,
    		sortAlgorithm,
    		allEmails,
    		timer,
    		emails: emails$1,
    		$currentTool,
    		$currentFolder,
    		$currentMailID,
    		$isInking,
    		$inkTarget,
    		$allInks
    	});

    	$$self.$inject_state = $$props => {
    		if ("tmpTagProbs" in $$props) tmpTagProbs = $$props.tmpTagProbs;
    		if ("predictResults" in $$props) $$invalidate(0, predictResults = $$props.predictResults);
    		if ("showPredict" in $$props) $$invalidate(1, showPredict = $$props.showPredict);
    		if ("fnameInput" in $$props) $$invalidate(2, fnameInput = $$props.fnameInput);
    		if ("selectedMailIndex" in $$props) $$invalidate(3, selectedMailIndex = $$props.selectedMailIndex);
    		if ("folderList" in $$props) $$invalidate(5, folderList = $$props.folderList);
    		if ("searching" in $$props) $$invalidate(6, searching = $$props.searching);
    		if ("writingMail" in $$props) $$invalidate(7, writingMail = $$props.writingMail);
    		if ("tmpEmails" in $$props) $$invalidate(30, tmpEmails = $$props.tmpEmails);
    		if ("sortAlgorithm" in $$props) $$invalidate(8, sortAlgorithm = $$props.sortAlgorithm);
    		if ("allEmails" in $$props) $$invalidate(9, allEmails = $$props.allEmails);
    		if ("timer" in $$props) timer = $$props.timer;
    		if ("emails" in $$props) $$invalidate(10, emails$1 = $$props.emails);
    	};

    	let emails$1;

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty[0] & /*sortAlgorithm, tmpEmails*/ 1073742080) {
    			 {
    				if (sortAlgorithm === "time") {
    					Object.keys(originEmails).forEach(folder => $$invalidate(30, tmpEmails[folder] = [...originEmails[folder]].sort((a, b) => b.time - a.time), tmpEmails));
    				} else if (sortAlgorithm === "title") {
    					Object.keys(originEmails).forEach(folder => $$invalidate(30, tmpEmails[folder] = [...originEmails[folder]].sort((a, b) => a.title.localeCompare(b.title)), tmpEmails));
    				}

    				setEmails({ ...tmpEmails });
    				console.log("REACTIVE");
    			}
    		}

    		if ($$self.$$.dirty[0] & /*tmpEmails*/ 1073741824) {
    			 $$invalidate(10, emails$1 = tmpEmails);
    		}
    	};

    	 {
    		console.log("allEmails updated");
    		$$invalidate(9, allEmails = { ...emails });
    	}

    	return [
    		predictResults,
    		showPredict,
    		fnameInput,
    		selectedMailIndex,
    		listLen,
    		folderList,
    		searching,
    		writingMail,
    		sortAlgorithm,
    		allEmails,
    		emails$1,
    		$currentTool,
    		$currentFolder,
    		$currentMailID,
    		$isInking,
    		$inkTarget,
    		$allInks,
    		click_handler,
    		click_handler_1,
    		func,
    		click_handler_2,
    		input_binding,
    		click_handler_3,
    		click_handler_4,
    		click_handler_5,
    		func_1,
    		click_handler_6,
    		click_handler_7,
    		click_handler_8,
    		func_2
    	];
    }

    class MailDemo extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$6, create_fragment$6, safe_not_equal, {}, [-1, -1]);

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "MailDemo",
    			options,
    			id: create_fragment$6.name
    		});
    	}
    }

    /* src/App.svelte generated by Svelte v3.24.1 */

    function create_fragment$7(ctx) {
    	let router;
    	let current;

    	router = new Router({
    			props: { routes: /*routes*/ ctx[0] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(router.$$.fragment);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			mount_component(router, target, anchor);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(router.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(router.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(router, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$7.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$7($$self, $$props, $$invalidate) {
    	const routes = { "/mail": MailDemo, "*": MailDemo };
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("App", $$slots, []);
    	$$self.$capture_state = () => ({ Router, MailDemo, routes });
    	return [routes];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$7, create_fragment$7, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment$7.name
    		});
    	}
    }

    const app = new App({
    	target: document.body,
    	props: {
    		name: 'world'
    	}
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
